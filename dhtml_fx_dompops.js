df.dompops = {

	poparray: [],

	popalert:	function(args){
		var self = df.dompops;

		if (self.alertbox){
			self.poparray.push(args)
	console.log(self.poparray.length)
		return
		}

		var newpop = {
			tagname	: "div",
			target	: args.target,
			attributes	: {
				id		: "dhtmlfx_dompops_div",
				style	: "position:absolute;top:50%;left:50%;padding:20;border:1px solid black;background-color:"+((args.bgcolor)?args.bgcolor:"#ffffff")+";color:"+((args.fontcolor)?args.fontcolor:"#000000")+";border-radius:12px;text-align:center;z-index:"+df.topzindex()
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						style	: "font-family:arial;font-size:34px;text-align:left;padding:30px;",
						html	: args.message.replace(/\n/g,"<br>")
					}
				}
			]
		}

		//TEXT INPUT
		if (args.type == "prompt") {

			newpop.children.push({
				tagname		: "input",
				attributes	: {
					type	: (args.ispassword==true)?"password":"text",
					style	: "position:relative;margin:10px;padding:5px;font-size:30px;",
					value	: args.defaulttext
				},
				events		: [
					{
						name	: "click",
						handler	: function(){

						}
					}
				]
			})
			newpop.children.push({tagname:"br"})
		}

		//OKAY
		if (args.type == "prompt" || args.type == "alert" || args.type == "confirm") {
			newpop.children.push({
				tagname		: "input",
				attributes	: {
					type	: "button",
					style	: "position:relative;margin:10px;padding:5px;font-size:30px;",
					value	: "Okay"
				},
				events		: [
					{
						name	: "click",
						handler	: function(){

							var returntext
							if (args.type == "prompt") {
								returntext = self.alertbox.getElementsByTagName("input")[0].value
							}

							self.closepop()
							if (args.callback) {args.callback(returntext || true)}
						}
					}
				]
			})
		}

		//CANCEL
		if ( (args.type == "prompt" || args.type == "confirm") && args.callback ) {
			newpop.children.push({
				tagname		: "input",
				attributes	: {
					type	: "button",
					style	: "position:relative;margin:10px;padding:5px;font-size:30px;margin-left:30px;",
					value	: "Cancel"
				},
				events		: [
					{
						name	: "click",
						handler	: function(){
							self.closepop()
						}
					}
				]
			})
		}


		self.alertbox = df.newdom(newpop)

		self.alertbox.style.marginLeft = -Math.round(self.alertbox.clientWidth/2)+"px"
		self.alertbox.style.marginTop  = -Math.round(self.alertbox.clientHeight/2)+"px"


	},

	closepop: function(){
		var self = df.dompops;
		if (self.alertbox){
			df.removedom(self.alertbox)
			self.alertbox = undefined;
		}

		if (self.poparray.length>1){
			var nextpop = self.poparray.shift()
			self.popalert(nextpop)
		}
	},

	getargs:function(args){
		var thisalert = {}
		if (args[0] && args[0].message!=undefined) {
			thisalert.message		= args[0].message
			thisalert.target  		= args[0].target || document.getElementsByTagName("body")[0]
			thisalert.fontcolor		= args[0].fontcolor
			thisalert.fontsize 		= args[0].fontsize
			thisalert.bgcolor		= args[0].bgcolor
			thisalert.callback		= args[0].callback
			thisalert.defaulttext 	= args[0].defaulttext //used for prompt feature
			thisalert.ispassword	= args[0].ispassword
		} else {
			thisalert.message		= args[0]
			thisalert.callback		= args[1]
			thisalert.bgcolor		= args[2]
			thisalert.fontcolor		= args[3]
			thisalert.fontsize 		= args[4]
			thisalert.target  		= args[5] || document.getElementsByTagName("body")[0]
			thisalert.defaulttext	= args[6] //used for prompt feature
			thisalert.ispassword	= args[7]
		}
		return thisalert
	},

	alert:	function(){
		var thisalert = df.dompops.getargs(arguments)
		thisalert.type = "alert"
		df.dompops.popalert(thisalert)
	},

	prompt:	function(message,value){
		var thisalert = df.dompops.getargs(arguments)
		thisalert.type = "prompt"
		df.dompops.popalert(thisalert)
	},

	confirm:	function(message){
		var thisalert = df.dompops.getargs(arguments)
		thisalert.type = "confirm"
		df.dompops.popalert(thisalert)
	}
};

(function(){
	var save_alert = window.alert
	var save_prompt = window.prompt
	var save_confirm = window.confirm

	window.df.restorepromts = function(){
		window.alert = save_alert
		window.prompt = save_prompt
		window.confirm = save_confirm
	}

	window.alert = df.dompops.alert
	window.prompt = df.dompops.prompt
	window.confirm = df.dompops.confirm

})()
