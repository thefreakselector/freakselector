/*
Code by Damien Otis 2004-2009
License found at http://www.dhtmldf.net/license.html
API Docs at http://www.dhtmldf.net/
*/
/*
To Do:
1. does not handle escaped characters. needs to handle &#x0040;  &#32;  \x43  ,etc.. (maybe integrate this with the dhtml_fx_html.js library?)
2. 2D truncate does not handle breaking-on-space or other non-alphanumeric (punctuation, etc)
3. 2D truncate does not add dash-on-continuation of words.

*/
dhtmlfx.fontsize={

	fontsizes:	{},
	
	testwin:"",

	periods:	3,

	// fontobject = {"family":"arial","size":10,"weight":700,"maxwidth":150}
	//fontobject  = {"css":"font-family:arial;font-size:12px;font-weight:700","maxwidth":150}
	//fontobject  = {"classname":"fontclass","maxwidth":150}
	//fontobject  = {"style":"font-family:arial;","classname":"fontclass","maxwidth":150}

	//df.fontsizes{"font-family:arial;fontclass":[{w:x,h:y},{w:x,h:y},{w:x,h:y}]
	//selector is made up of css+class (either can be null, but not both)
	
	fonttest:	function(fontobj) {
		var df = dhtmlfx
		var thisstyle = fontobj.style||"";
		var thisclass = fontobj.classname||"";
		var fontselector = thisstyle+thisclass;
		var fonthash = (fontselector.replace(/[^a-zA-Z0-9]/g,""))
		
		if (thisstyle=="") {return undefined}

		if (df.fontsize.fontsizes[fontselector]!=undefined) {return df.fontsize.fontsizes[fontselector]}

		df.fontsize.maketestwin();

		var fonthtml=[];
		var setstart = fontobj.setstart||32;
		var setend = fontobj.setend||127;
		if (thisclass!="") {thisclass='class="'+thisclass+'"'}
		for (var ix=32;ix<127;ix++) {
			var thischar = String.fromCharCode(ix);
			if (ix==32) {thischar = "&nbsp;"}
			fonthtml.push('<div id="'+fonthash+'_'+ix+'" style="float:left;'+thisstyle+'" '+thisclass+'>'+thischar+'</div>');
		}

		df.dg("dhtmlfx_fontsizes").innerHTML = fonthtml.join("");

		var thisfonttable = df.fontsize.fontsizes[fontselector]=[];
		for (var ix=32;ix<127;ix++) {
			var thischar = df.dg(fonthash+"_"+ix);
			thisfonttable[ix]={w:thischar.offsetWidth,h:thischar.offsetHeight}//df.fontsize.fontsizes[fontselector]
		}
		
		df.dg("dhtmlfx_fontsizes").innerHTML="";
		
		return thisfonttable //df.fontsize.fontsizes[fontselector]
	},

	maketestwin:	function() {
		var df = dhtmlfx
		if (df.dg('dhtmlfx_fontsizes')==null) {
			var newdiv = df.newdom({
				tagname:"div",
				attributes:{
					id:"dhtmlfx_fontsizes",
					style:"width:100px;height:100px;overflow:hidden;position:absolute;top:-200px;left:-200px;"
				}
			})
		}
	},
	
	chartest:	function(fontobj,charnum) {
		var df = dhtmlfx
		var thisstyle = fontobj.style||"";
		var thisclass = fontobj.classname||"";
		var fontselector = thisstyle+thisclass;

		if (df.fontsize.fontsizes[fontselector]!=undefined) {
			if (df.fontsize.fontsizes[fontselector][charnum]!=undefined) {
				return df.fontsize.fontsizes[fontselector][charnum]
			}
		}

		var thischar = String.fromCharCode(charnum);
		if (charnum==32) {thischar = "&nbsp;"}
		df.dg("dhtmlfx_fontsizes").innerHTML = '<div id="'+(fontselector.replace(/[^a-zA-Z0-9]/g,""))+'_'+charnum+'" style="float:left;'+thisstyle+'" '+thisclass+'>'+thischar+'</div>';
		
		var testchar = df.dg(fontselector.replace(/[^a-zA-Z0-9]/g,"")+"_"+charnum);
		df.fontsize.fontsizes[fontselector][charnum]={w:testchar.offsetWidth,h:testchar.offsetHeight};
		df.dg("dhtmlfx_fontsizes").innerHTML="";

		return df.fontsize.fontsizes[fontselector][charnum]

	},

/*	
//had some problems with optimizations

	fonttest:	function(fontobj) {
		var df = dhtmlfx
		var thisstyle = fontobj.style||"";
		var thisclass = fontobj.classname||"";
		var fontselector = thisstyle+thisclass;
		var fonthash = (fontselector.replace(/[^a-zA-Z0-9]/g,""))
		
		if (thisstyle=="") {return undefined}

		if (df.fontsize.fontsizes[fontselector]!=undefined) {return df.fontsize.fontsizes[fontselector]}

		if (df.fontsize.testwin=="") {
			df.fontsize.testwin = df.newdom({tagname:"div",attributes:{id:"dhtmlfx_fontsizes",style:"width:100px;height:100px;overflow:hidden;position:absolute;top:-200px;left:-200px;"}})
		}

		var setstart = fontobj.setstart||33
		var setend = fontobj.setend||127
		var fonthtml=new Array(127)//[]
		if (thisclass!="") {thisclass='class="'+thisclass+'"'}
		for (var ix=setstart;ix<setend;ix++) {
			fonthtml[ix]='<div id="'+fonthash+'_'+ix+'" style="float:left;'+thisstyle+'" '+thisclass+'>'+String.fromCharCode(ix)+'</div>'
			//fonthtml.push('<div id="'+fonthash+'_'+ix+'" style="float:left;'+thisstyle+'" '+thisclass+'>'+String.fromCharCode(ix)+'</div>');
		}
		
		fonthtml[32]='<div id="'+fonthash+'_32" style="float:left;'+thisstyle+'" '+thisclass+'>'+String.fromCharCode(ix)+'</div>'
		//fonthtml.push('<div id="'+fonthash+'_32" style="float:left;'+thisstyle+'" '+thisclass+'>&nbsp;</div>');

		df.fontsize.testwin.innerHTML = fonthtml.join("");

//df.debug.debugwin({title:"",message:fonthtml.join("")+"<br><br>",color:""})

		var thisfonttable = df.fontsize.fontsizes[fontselector]=[];
		for (var ix=32;ix<127;ix++) {
			var thischar = df.dg(fonthash+"_"+ix,true);
			thisfonttable[ix]={w:thischar.offsetWidth,h:thischar.offsetHeight}//df.fontsize.fontsizes[fontselector]
		}
		thischar = undefined
		
		df.fontsize.testwin.innerHTML="";
		
		return thisfonttable //df.fontsize.fontsizes[fontselector]
	},

	chartest:	function(fontobj,charnum) {
		var df = dhtmlfx
		var thisstyle = fontobj.style||"";
		var thisclass = fontobj.classname||"";
		var fontselector = thisstyle+thisclass;
		if (df.fontsize.fontsizes[fontselector]!=undefined) {
			if (df.fontsize.fontsizes[fontselector][charnum]!=undefined) {
				return df.fontsize.fontsizes[fontselector][charnum]
			}
		}

		var thischar = String.fromCharCode(charnum);
		if (charnum==32) {thischar = "&nbsp;"}
		df.fontsize.testwin.innerHTML = '<div id="'+(fontselector.replace(/[^a-zA-Z0-9]/g,""))+'_'+charnum+'" style="float:left;'+thisstyle+'" '+thisclass+'>'+thischar+'</div>';
		
		var testchar = df.dg(fontselector.replace(/[^a-zA-Z0-9]/g,"")+"_"+charnum);
		df.fontsize.fontsizes[fontselector][charnum]={w:testchar.offsetWidth,h:testchar.offsetHeight};
		df.fontsize.testwin.innerHTML="";

		return df.fontsize.fontsizes[fontselector][charnum]

	},
*/
	calcsize:	function (fontobj) {
		var df = dhtmlfx
	// fontobject = {"text":"hello world!! this is a test","style":"font-family:arial;font-size:12px;"}
		var thisfont = df.fontsize.fonttest(fontobj);
		fontobj.text = String(fontobj.text)

		//fontobj.maxwidth=fontobj.maxwidth||Number.POSITIVE_INFINITY
		var textwidth=0;
		for (var i=0;i<fontobj.text.length;i++) {
			try{textwidth+=thisfont[String(fontobj.text).charCodeAt(i)].w}catch(e){}
			//if (textwidth>fontobj.maxwidth) {break}
		}
		
		return textwidth	
	},
	
	calcsize2d:	function (fontobj) {
		var df = dhtmlfx
	// fontobject = {"text":"hello world!! this is a test","style":"font-family:arial;font-size:12px;"}
		var thisfont = df.fontsize.fonttest(fontobj);
		fontobj.text = String(fontobj.text)

		//fontobj.maxwidth=fontobj.maxwidth||Number.POSITIVE_INFINITY
		var textwidth=0;
		var textheight=0;
		for (var i=0;i<fontobj.text.length;i++) {
			try{textwidth+=thisfont[String(fontobj.text).charCodeAt(i)].w}catch(e){}
			//if (textwidth>fontobj.maxwidth) {break}
			if (textwidth>fontobj.width) {
				try{textheight+=thisfont[String(fontobj.text).charCodeAt(i)].h}catch(e){}
				try{textwidth=thisfont[String(fontobj.text).charCodeAt(i)].w}catch(e){textwidth=0}
			}
		}
		
		return textheight
	},
/*	
	truncate_working:	function(fontobj) {
		var df = dhtmlfx
	// fontobject = {"text":"hello world!! this is a test","style":"font-family:arial;font-size:12px;","maxwidth":150,"periods":3}
		if (fontobj.text==""||fontobj.text==undefined) {return ""}
		fontobj.text = String(fontobj.text)
	
		var thisfont = df.fontsize.fonttest(fontobj);

		fontobj.maxwidth=fontobj.maxwidth||Number.POSITIVE_INFINITY;
		fontobj.periods=fontobj.periods||df.fontsize.periods;

		fontobj.maxwidth-=(thisfont[46].w*fontobj.periods);
		var textwidth=0;
		var returntext = fontobj.text;
		var ishtml=false
		for (var i=0;i<fontobj.text.length;i++) {
			if (fontobj.text[i]=="<"){ishtml=true}
			if (fontobj.text[i]==">"){ishtml=false}
			if (ishtml==true) {continue}
			try{
				textwidth+=thisfont[String(fontobj.text).charCodeAt(i)].w;
			}catch(e){
				textwidth+=df.fontsize.chartest(fontobj,String(fontobj.text).charCodeAt(i)).w;
			}
			if (textwidth>fontobj.maxwidth) {returntext=fontobj.text.substr(0,i)+String("..........").slice(-fontobj.periods);break}
		}
		return returntext	
	},
*/
	stripxml:	function(thistext) {
		var splittext = String(thistext).replace(/\&nbsp\;/g," ").replace(/\,\s{0,}/g,", ")
		var matchtext = thistext.match(/\<[\/a-zA-Z].*?\>/g)
		if (matchtext!=null) {
			for (var i=0;i<matchtext.length;i++) {
				splittext = splittext.replace(matchtext[i],"")
			}
		}
		return splittext
	},
	
	truncate:	function(fontobj) {
		var df = dhtmlfx
	// fontobject = {"text":"hello world!! this is a test","style":"font-family:arial;font-size:12px;","maxwidth":150,"periods":3}
		if (fontobj.text==""||fontobj.text==undefined) {return ""}
		fontobj.text = String(fontobj.text)
	
		var thisfont = df.fontsize.fonttest(fontobj);


		fontobj.maxheight = df.fontsize.fontheight(fontobj)


		return df.fontsize.truncate2d(fontobj)	
	},

	truncate2d:	function(fontobj) {
		var df = dhtmlfx
	try{
		if (fontobj.text==""||fontobj.text==undefined) {return ""}
		fontobj.text = String(fontobj.text)
		fontobj.maxwidth=Math.floor(fontobj.maxwidth)
		fontobj.maxheight=Math.floor(fontobj.maxheight)

		var thisfont = df.fontsize.fonttest(fontobj);

	//	if (fontobj.maxwidth<20) {fontobj.maxwidth=20}


		fontobj.periods=fontobj.periods||df.fontsize.periods;

//the width of the last periods ...
		var periodwidth = (thisfont[46].w*fontobj.periods)

//the maximum length the last line of text can be not including the width of the final periods
		var lastmaxwidth = fontobj.maxwidth - periodwidth;

//fonts characters should all be the same height (?)
		var textheight=thisfont[65].h;

		var textwidth=0;

		var returntext = [];
//%3C = <
//%3E = >
		//allow HTML, skipping it when calculating elipses
		var splittext = String(fontobj.text).replace(/\&nbsp\;/g," ").replace(/\,\s{0,}/g,", ")
		if (fontobj.striphtml==true) {
			var splittext = splittext.replace(/\<[\/a-zA-Z].*?\>/g,"")
		} else {
			var matchtext = splittext.match(/\<[\/a-zA-Z].*?\>/g)
			if (matchtext!=null) {
				for (var i=0;i<matchtext.length;i++) {
					splittext = splittext.replace(matchtext[i]," "+encodeURIComponent(matchtext[i])+" ")
				}
			}
		}


		//splittext = splittext.replace(/\<[a-zA-Z]{1,}\s.*?\>/g,"XXX")
		//splittext = splittext.replace(/\<\/[a-zA-Z]{1,}\s.*?\>/g,"XXX")
		
		splittext = splittext.split(" ")
//		var splittext = String(fontobj.text).replace(/\&nbsp\;/g," ").split(",").join(", ").split(" ")


/*		
		if (splittext.length==1) {
			var thistext = String(fontobj.text)
			var twidth = 0
			for (var i=0;i<thistext.length;i++) {
				try{
					twidth+=thisfont[String(thistext).charCodeAt(i)].w;
				}catch(e){
					twidth+=df.fontsize.chartest(fontobj,String(thistext).charCodeAt(i)).w;
				}
				if (twidth>fontobj.maxlength) {
					return df.fontsize.truncate(fontobj)
				}
			}
			return df.fontsize.truncate2dnowrap(fontobj)
		}
*/

		var linewidth = 0
		var thisline=[]
		var wrapheight=0
		blockloop:for (var ix=0;ix<splittext.length;ix++) {
		
			var wordwidth = 0
			var thistext = splittext[ix]+" "
			
			if (thistext.substr(0,3)=="%3C"){//if this is HTML, then put it in and continue
				thisline.push(decodeURIComponent(thistext))
				continue
			}
			
			wordloop:for (var iw=0;iw<thistext.length;iw++) {
				try{
					wordwidth+=thisfont[thistext.charCodeAt(iw)].w;
				}catch(e){
					wordwidth+=df.fontsize.chartest(fontobj,thistext.charCodeAt(iw)).w;
				}
				if (wordwidth>=fontobj.maxwidth) {
					if ( (wrapheight+thisfont[65].h)>=fontobj.maxheight) { 
						lastline = true
					}
					wrapheight+=thisfont[65].h
					if ( (wrapheight+thisfont[65].h)>=fontobj.maxheight) { 
						if (thisline.length>0) {
							var endline = String(thisline[thisline.length-1]+" "+thistext.substr(0,iw))
							thisline[thisline.length-1] = endline.substr(0,endline.length-fontobj.periods) + String("...............").substr(0,fontobj.periods)
						} else {
							thisline.push(thistext.substr(0,iw-2)+String("...............").substr(0,fontobj.periods))
						}
						break blockloop
					}
					if (fontobj.nodash==true) {
						thisline.push(thistext.substr(0,iw-1)+"<br/>")
					} else {
						thisline.push(thistext.substr(0,iw-1)+"-<br/>")
					}
					splittext[ix] = splittext[ix].substr(iw-1)
					linewidth=0
					ix--
					continue blockloop
				}
				if ( (linewidth+wordwidth)>fontobj.maxwidth) {
					if ( (wrapheight+thisfont[65].h)>=fontobj.maxheight) { 
						lastline = true
					}
					if (wrapheight+thisfont[65].h>=fontobj.maxheight) {
						if (thisline.length>0) {
							var endline = String(thisline[thisline.length-1]+" "+thistext.substr(0,iw))
							thisline[thisline.length-1] = endline.substr(0,endline.length-fontobj.periods) + String("...............").substr(0,fontobj.periods)
						} else {
							thisline.push(thistext.substr(0,iw-2)+String("...............").substr(0,fontobj.periods))
						}
						break blockloop
					}
					
					wrapheight+=thisfont[65].h
					thisline.push('<br/>')
					linewidth=0
					ix--
					continue blockloop
				}
			}
			thisline.push(thistext)
			linewidth+=wordwidth
		}
		lastloop:for (var ixz=ix;ixz<splittext.length;ixz++) {
			if (splittext[ixz].substr(0,6)=="%3C%2F"){//if this is HTML closing tag, then put it in and end
				thisline.push(decodeURIComponent(splittext[ixz]))
				break lastloop
			}
		}
		
		return thisline.join("")

	}catch(e){
		//df.debug.debugwin({title:"truncate2d",color:"red",message:e.message+"<br>"+e.description})
		return fontobj.text
	}
		
	},

	truncate2dnowrap:	function(fontobj) {
		var df = dhtmlfx

		if (fontobj.text==""||fontobj.text==undefined) {return ""}
		fontobj.text = String(fontobj.text)
		fontobj.maxwidth=Math.floor(fontobj.maxwidth)
		fontobj.maxheight=Math.floor(fontobj.maxheight)
	
		var thisfont = df.fontsize.fonttest(fontobj);

		fontobj.periods=fontobj.periods||2;
		
		fontobj.lastmaxwidth = Math.round(fontobj.maxwidth - (thisfont[46].w*fontobj.periods));
		
		var textwidth=0;
		var textheight=0

		var returntext = [];

		for (var i=0;i<fontobj.text.length;i++) {
			var thischar = fontobj.text.charCodeAt(i)
			var fontchar = thisfont[thischar]

			try{
				textwidth+=fontchar.w;
			}catch(e){
				textwidth+=df.fontsize.chartest(fontobj,thischar).w;
			}

			if (textwidth>=fontobj.maxwidth) {
				textheight += thisfont[65].h;
				if (textheight+thisfont[65].h>=fontobj.maxheight) {
					returntext.splice(returntext.length-fontobj.periods,fontobj.periods,String("...........").substr(0,fontobj.periods));
					break
				} else {
					returntext.push("<br/>");
				}				
				textwidth=0
				i--
			} else {
				returntext.push(fontobj.text.substr(i,1));
			}
		}
		return returntext.join("")
	},

	fontheight:	function(fontobj){
		var df = dhtmlfx
		var thisstyle = fontobj.style||"";
		var thisclass = fontobj.classname||"";
		var fontselector = thisstyle+thisclass;

		var thisfont = df.fontsize.fonttest(fontobj);
		
		for (var selector in thisfont) {
			if (thisfont[selector].h==undefined) {continue}
			var thish = thisfont[selector].h
			break
		}
		
		return thish
	},
	
	getrows:	function(fonttext) {
	try{
		return fonttext.split("<br/>").length
	}catch(e){return 0}
	}
	
};

