var grid = {
	SantaHat: []
};

for (var y=6;y>-1;y--){
	for (var x=0;x<40;x++){
    var xx = (y & 1) ? 39-x : x
    var yy =  y//41 - x
    grid.SantaHat.push({x:xx,y:yy})
  }

}

//====================================================================================================

console.log("LED Count: ",grid.SantaHat.length)

//----------------------------------------------

var fs = require('fs');
var path = require('path');

var outfile = path.resolve(process.mainModule.filename.replace(/\.js$/,'.pmap.json'));

fs.writeFile(outfile, JSON.stringify(grid), function (err,data) {
  if (err) {
    console.log("Could not write "+outfile)
    return
  }
  console.log("Pixel Map File: "+outfile)
})

