var grid = {
  "front":[],
  "left":[],
  "back":[],
  "right":[]
};

var x = 65;
var y = 0;
for (var i=0;i<20;i++){
  for (var iy=0;iy<13;iy++){
    var xy = (x & 1) ? 12-iy : iy
    var xx = 100 - x
    grid['right'].push({x:xx,y:xy})
  }
  x++
  if (x > 99){x=0}
}
for (var i=0;i<30;i++){
  for (var iy=0;iy<13;iy++){
    var xy = (x & 1) ? 12-iy : iy
    var xx = 100 - x
    grid['front'].push({x:xx,y:xy})
  }
  x++
  if (x > 99){x=0}
}
for (var i=0;i<20;i++){
  for (var iy=0;iy<13;iy++){
    var xy = (x & 1) ? 12-iy : iy
    var xx = 100 - x
    grid['left'].push({x:xx,y:xy})
  }
  x++
  if (x > 99){x=0}
}
for (var i=0;i<30;i++){
  for (var iy=0;iy<13;iy++){
    var xy = (x & 1) ? 12-iy : iy
    var xx = 100 - x
    grid['back'].push({x:xx,y:xy})
  }
  x++
  if (x > 99){x=0}
}

//====================================================================================================

var pixels = 0;
for (var panel in grid){
  pixels += grid[panel].length
}

console.log("LED Count: ",pixels)

//----------------------------------------------

var fs = require('fs');
var path = require('path');

var outfile = path.resolve(process.mainModule.filename.replace(/\.js$/,'.pmap.json'));

fs.writeFile(outfile, JSON.stringify(grid), function (err,data) {
  if (err) {
    console.log("Could not write "+outfile)
    return
  }
  console.log("Pixel Map File: "+outfile)
})

