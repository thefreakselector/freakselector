/*
node pixelmap_arbitrary.js [width] [height]

*/

var yargs = require('yargs');
var argv = yargs.argv;

var grid = {
  "screen1":[]
};
var width = process.argv[2] ? parseInt(process.argv[2],10) || 200 : 200;
var height = process.argv[3] ? parseInt(process.argv[3],10) || 100 : 100;
var limit = process.argv[4] ? parseInt(process.argv[4],10) :  undefined;

outerloop: for (var w=0;w<width;w++){
  for (var h=0;h<height;h++){
    if (limit && grid['screen1'].length >= limit) {
      break outerloop;
    }
    grid['screen1'].push({x:w,y:h})
  }
}

//====================================================================================================

var pixels = 0;
for (var panel in grid){
  pixels += grid[panel].length
}

console.log("LED Count: ",pixels)

//----------------------------------------------

var fs = require('fs');
var path = require('path');

var outfile = path.resolve(argv.FILENAME || process.mainModule.filename.replace(/\.js$/,'-'+width+'x'+height+(limit ? '-'+limit : '')+'.pmap.json'));

fs.writeFile(outfile, JSON.stringify(grid), function (err,data) {
  if (err) {
    console.log("Could not write "+outfile)
    return
  }
  console.log("Pixel Map File: "+outfile)
})

