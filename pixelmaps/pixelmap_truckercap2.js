/*
node pixelmap_arbitrary.js [width] [height]

*/

var yargs = require('yargs');
var argv = yargs.argv;

var grid = {
  "screen1":[]
};

var lines = [
	{leds: 10, offset: .18},
	{leds: 11, offset: .15},
	{leds: 12, offset: .1},
	{leds: 12, offset: .12},
	{leds: 13, offset: .06},
	{leds: 14, offset: 0},
	{leds: 14, offset: 0},
	{leds: 14, offset: 0},
	{leds: 13, offset: .06},
	{leds: 12, offset: .12},
	{leds: 12, offset: .1},
	{leds: 11, offset: .15},
	{leds: 10, offset: .18}
];

var cnt = 0;
for (var i=0;i<lines.length;i++){
	cnt += lines[i].leds
	for (var iy=0;iy<lines[i].leds;iy++) {
		var x = i / (lines.length-1);
		var y = (iy / 13)
		if (i&1) {
//			y -=  (lines[i].offset / 13)
		} else {
			y = (lines[i].leds / 14) - y
	//		y +=  (lines[i].offset / 13)
		}
		y = y + lines[i].offset
		grid.screen1.push({x:Math.round(x*40),y:Math.round(y*40)})

	}
}

console.log(cnt)


//----------------------------------------------

var fs = require('fs');
var path = require('path');

var outfile = path.resolve(argv.FILENAME || process.mainModule.filename.replace(/\.js$/,'.pmap.json'));

fs.writeFile(outfile, JSON.stringify(grid), function (err,data) {
  if (err) {
    console.log("Could not write "+outfile)
    return
  }
  console.log("Pixel Map File: "+outfile)
})

