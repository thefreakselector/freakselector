/*
node pixelmap_arbitrary.js [width] [height]

*/

var yargs = require('yargs');
var argv = yargs.argv;

var grid = {
  "leftear":[],
  "middleband":[],
  "rightear":[]
};

var cnt = 0;

var angle = 360/24;
//var radii = [16,12,8,4];
var radii = [4,8,12,16];


//-------------------------------------------------------------------------------------------
/*
	LEFT SIDE EAR
*/
var rot = 0;

var leftear_rightside = 0;
var leftear_bottomside = 0;

var x_offset = 0;
var y_offset = 0;
var rot_offset = 90;

for (var deg=0;deg<360;deg+=angle){

	var rdeg = 360 - deg;

	var seg;
	if (rot & 1){
		seg = 4
	} else {
		seg = 3
	}

	for (var ring = 0;ring <= seg-1; ring++){

		var radius = radii[rot & 1 ? -ring + 3 : ring + 1];
		var rads = (rdeg + rot_offset) * (Math.PI / 180);

		var x = (Math.sin(rads) * radius) + radii[3];
		var y = (Math.cos(rads) * radius) + radii[3];

		x += x_offset;
		y += y_offset;

		if (x > leftear_rightside) {
			leftear_rightside = x;
		}
		if (y > leftear_bottomside) {
			leftear_bottomside = y;
		}

		grid["leftear"].push({
			x: Math.round(x),
			y: Math.round(y)
		});

	}

	cnt += seg;

	rot++;
}

grid["leftear"].push({
	x:Math.round(leftear_rightside/2),
	y:Math.round(leftear_bottomside/2)
});

//----------------------------------------------
console.log("LED Count leftear:",grid["leftear"].length)
console.log("leftear_rightside",leftear_rightside)
console.log("leftear_bottomside",leftear_bottomside)

//-------------------------------------------------------------------------------------------
var band_width = 18;
var band_height = 5;

var xw = 3;
var yw = 3;

var band_offset_x = leftear_rightside;
var band_offset_y = (leftear_bottomside / 2) - (( band_height * yw)/2);

var xx, yy;


var band_max_x = 0;
var band_max_y = 0;

for (var y = 0; y < band_height; y++) {
	for (var x = 0;x < band_width; x++ ) {

		xx = x * xw;
		yy = y * yw;

		if (y & 1) {
			xx = -xx + ((band_width-1)*xw)
		}

		xx += band_offset_x;
		yy += band_offset_y;

		if (xx > band_max_x) { band_max_x = xx}
		if (yy > band_max_y) { band_max_y = yy}

		grid["middleband"].push({
			x:Math.round(xx),
			y:Math.round(yy)
		});
	}
}

//-------------------------------------------------------------------------------------------
/*
	RIGHT SIDE EAR
*/
var rot = 0;

var rightear_rightside = 0;
var rightear_bottomside = 0;

var x_offset = band_max_x;
var y_offset = 0;
var rot_offset = -90;

for (var deg=0;deg<360;deg+=angle){
	var seg;
	if (rot & 1){
		seg = 4
	} else {
		seg = 3
	}

	for (var ring = 0;ring <= seg-1; ring++){

		var radius = radii[rot & 1 ? -ring + 3 : ring + 1];
		var rads = (deg + rot_offset) * (Math.PI / 180);

		var x = (Math.sin(rads) * radius) + radii[3];
		var y = (Math.cos(rads) * radius) + radii[3];

		x += x_offset;
		y += y_offset;

		if (x > rightear_rightside) {
			rightear_rightside = x;
		}
		if (y > rightear_bottomside) {
			rightear_bottomside = y;
		}

		grid["rightear"].push({
			x: Math.round(x),
			y: Math.round(y)
		});

	}
	cnt += seg;

	rot++
//	console.log(deg, rot, seg)
}

grid["rightear"].push({
	x:Math.round(rightear_rightside + ((band_max_x - rightear_rightside)/2)),
	y:Math.round(rightear_bottomside/2)
});


//----------------------------------------------
console.log("LED Count rightear:", grid["rightear"].length);

console.log("rightear_rightside",rightear_rightside);
console.log("rightear_bottomside",rightear_bottomside);

console.log("Pixel Count:", grid.leftear.length + grid.rightear.length + grid.middleband.length)
//----------------------------------------------

var fs = require('fs');
var path = require('path');

var grid_concat = {muf:[].concat(grid.leftear,grid.middleband, grid.rightear)}

var outfile = path.resolve(argv.FILENAME || process.mainModule.filename.replace(/\.js$/,'.pmap.json'));

fs.writeFile(outfile, JSON.stringify(grid_concat), function (err,data) {
  if (err) {
    console.log("Could not write "+outfile)
    return
  }
  console.log("Pixel Map File: "+outfile)
})

