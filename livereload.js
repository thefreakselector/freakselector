/*
Front-end livereload, pairs with livereload.js in Gulp folder.
*/
//-----------------------------------------------------------------------------
(function(){

  var socket = null

  function socketConnect(){
      socket = new WebSocket( ((location.protocol==="https:")?'wss:':'ws:')+'//localhost:'+(window.live_reload_port || 3100))
      window.LIVERELOAD_SOCKET = socket;
      socket.onopen = function(){
        console.log('livereload connected')
        if (tab_needs_reload == true){
          doReload()
        }
      }
      socket.onclose = function(){
        socket = null;
        setTimeout(socketReconnect,500)
        console.log('livereload closed')
      }
      socket.onmessage = function(message){
        if (message.data == "reload"){
          tab_needs_reload = true;
          doReload();
        }
      }
  }

  function socketReconnect(){
    console.log("socket reconnect")
    tab_needs_reload = true;
    socketConnect()
  }

  function socketDisconnect(){
      socket.close()
  }

  document.addEventListener(tab_visibilityChange, function(evt) {
      doReload()
  }, false);
  socketConnect()

  var tab_needs_reload = false;
  var tab_hidden, tab_state, tab_visibilityChange;

  if (typeof document.hidden !== "undefined") {
    tab_hidden = "hidden";
    tab_visibilityChange = "visibilitychange";
    tab_state = "visibilityState";
  } else if (typeof document.mozHidden !== "undefined") {
    tab_hidden = "mozHidden";
    tab_visibilityChange = "mozvisibilitychange";
    tab_state = "mozVisibilityState";
  } else if (typeof document.msHidden !== "undefined") {
    tab_hidden = "msHidden";
    tab_visibilityChange = "msvisibilitychange";
    tab_state = "msVisibilityState";
  } else if (typeof document.webkitHidden !== "undefined") {
    tab_hidden = "webkitHidden";
    tab_visibilityChange = "webkitvisibilitychange";
    tab_state = "webkitVisibilityState";
  }

  var pageload = false;

  function doReload() {
    if ( !document[tab_hidden] ) {
      if (tab_needs_reload) {
        //if (pageload == true){return}
        location.reload()
      } else {
        setTimeout(doReload,500)
      }
    } else {
      setTimeout(doReload,500)
    }
  }


})();
