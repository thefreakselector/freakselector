
df.debug.debugmode = true

//cross browser bullshit
window.requestAnimationFrame =
    window.requestAnimationFrame       ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame;

//==================================================================================

(function(){

var LED = window.LED = {

	render_fast : true,

	frame:	[],

	grid_width:0,
	grid_height:0,

  display_width : 350,
  display_height: 178,

  LCD_brightness: 0,

//==================================================================================
  onLoad: function(){

//    var stored_pixelmap = localStorage.getItem('pixelmap');
	df.ajax.send({
		url: '/getSetting/pixelmap',
		callback:function(ajaxobj) {
	  		var stored_pixelmap = JSON.parse(ajaxobj.text).pixelmap;
console.log('>>>>>',stored_pixelmap)
		    if (stored_pixelmap){
		      df.ajax.send({
		         url: '/getJSONFile/pixelmaps/'+stored_pixelmap,
		         callback:function(ajaxobj2) {
		          var pixelmap = JSON.parse(ajaxobj2.text);
		          LED.initPixelMap(pixelmap);
		          LED.start(pixelmap);
		        }
		      })
		    } else {
	          df.ajax.send({
	          	url: '/saveSetting/pixelmap',
	          	verb: 'post',
	          	formdata: JSON.stringify({"pixelmap":"pixelmap_arbitrary-30x13.pmap.json"}),
	          	callback: function(ajaxobj4){
	          	//	LED.onLoad()
	          	}
	          })
		    }
		}
	})


  },

  loadPixelMap: function(stored_pixelmap){
    df.ajax.send({
       url: '/getJSONFile/pixelmaps/'+stored_pixelmap,
       callback:function(ajaxobj) {
        var pixelmap = JSON.parse(ajaxobj.text);
        LED.initPixelMap(pixelmap);
      }
    })

  },

  initPixelMap: function(map){

      LED.pixelmap = map;

      var width = 0;
      var height= 0;

      var pixelcount = 0;

      for (var selector in map) {
        for (var i=0;i<map[selector].length;i++) {
          if (map[selector][i].x>width) {width = map[selector][i].x}
          if (map[selector][i].y>height){height= map[selector][i].y}
          pixelcount++
        }
      }

      width++
      height++

console.log("width=",width,"\nheight=",height)

      LED.grid_width = width;
      LED.grid_height= height;

      if (LED.composite){
        LED.composite.setAttribute('width',width);
        LED.composite.setAttribute('height',height);
        LED.composite.style.width = (LED.display_width)+"px";
        LED.composite.style.height = (LED.display_height)+"px";
      }

  },

	start:	function(map){
			LED.location = String(location)
			LED.urlParameters={}
			var qs = LED.location.split("?")[1]||""
			var ht = qs.split("#")[1]||""
			var qs_split = qs.split("&")
			var ht_split = ht.split("&")
			var params = qs_split.concat(ht_split)
			for (var i=0;i<params.length;i++) {
				var pname = params[i].split("=")[0]
				var pval  = params[i].split("=")[1]
				if (pname==undefined||pval==undefined) {continue}
				LED.urlParameters[pname] = parseFloat(pval)||((pval==="true"||pval==="false")?eval(pval):String(pval).replace(/eval/g,"").replace(/function/g,""))||undefined
			}
//========================================================

			soundManager.onload = function() {
				soundManager.url = './'; // path to directory containing SoundManager2 .SWF file
				soundManager.flashVersion = 9;


				soundManager.flashLoadTimeout = 6000;
				soundManager.flash9Options.usePeakData = true;
				soundManager.flash9Options.useWaveformData = true;
				soundManager.flash9Options.useEQData = true;
				soundManager.flashPollingInterval = 22

				soundManager.waitForWindowLoad = true;
			//	soundManager.debugMode = true;
				soundManager.allowPolling = true;
				soundManager.useHighPerformance = true;
				soundManager.useFastPolling = true;
				soundManager.features.eqData = true
				soundManager.features.peakData = true
				soundManager.features.waveformData = true
				soundManager.preferFlash = true
				soundManager.html5PollingInterval  = 33

			}

//========================================================
			df.attachevent({target:window,name:"resize",handler:LED.windowResize});
			LED.windowResize()
//========================================================

			LED.windowtarget = document.getElementsByTagName("body")[0]

			//-------------------------------------------
      if (window.AudioContext){
        LED.audioContext = new AudioContext();
      }

			LED.LEDwindow = df.newdom({
				tagname		: "div",
				target		: LED.windowtarget,
				attributes	: {
					id		: "LEDwindow",
					style	: "position:absolute;top:50%;left:50%;margin-top:-384px;margin-left:-640px;width:1280px;height:768px;overflow:hidden;background-color:#4a4f6a;z-index:"+(df.zindexorder()+100)
				}
			})

			//-------------------------------------------

      LED.LEDholder = df.newdom({
        tagname   : "div",
        target    : LED.LEDwindow,
        attributes  : {
          id    : "LEDholder",
          style : "position:absolute;top:10px;left:907px;background-color:#000000;border-radius:5px;border:1px solid black;padding:5px;overflow:hidden;"
        },
        children  : [
        ]
      })

      LED.LEDholder.style.width = LED.display_width+"px";
      LED.LEDholder.style.height = LED.display_height+"px";

      LED.LEDbackground = df.newdom({
        tagname   : "div",
        target    : LED.LEDholder,
        attributes  : {
          id    : "LEDbackground",
          style : "width:"+LED.display_width+"px;height:"+LED.display_height+"px;position:relative;overflow:hidden;"
        }
      })

			LED.composite = df.newdom({
				tagname		: "canvas",
				target		: LED.LEDholder,
				attributes:{
					id		: "compositecanvas",
					width	: LED.grid_width,
					height	: LED.grid_height,
//          style : "position:fixed;top:-"+LED.grid_width+"px;left:-"+LED.grid_width+"px;display:none;" //"position:fixed;top:20px;left:20px;border:1px solid white;" //
//          style : "position:absolute;top:15px;left:914px;display:block;z-index:999999;user-select:none;cursor:default;"
          style : "position:absolute;top:5px;left:5px;display:block;z-index:999999;user-select:none;cursor:default;"
				}
			})

      var disp_width = LED.display_width;
      var disp_height = (LED.display_width * (LED.grid_height / LED.grid_width) );
      if (disp_height > LED.display_height){
        disp_height = LED.display_height;
        disp_width = (LED.grid_width / LED.grid_height) * disp_height
      }
      LED.composite.style.width = disp_width+"px";
      LED.composite.style.height= disp_height+"px";

      //LED.LEDwindow.style.width = (disp_width+20)+"px";

      LED.LEDholder.style.width = (disp_width)+"px";
      LED.LEDholder.style.height = (disp_height)+"px";

      //LED.LEDholder.style.left = "calc(100% - "+disp_width+"px - 20px)";
      //LED.composite.style.left = "calc(100% - "+disp_width+"px - 15px)";


			LED.composite_ctx = LED.composite.getContext("2d")

			//-------------------------------------------

			LED.pluginwindow = df.newdom({
				tagname		: "div",
				target		: LED.LEDwindow,
				attributes	: {
					id		: "pluginwindow",
					style	: "position:absolute;top:205px;left:380px;width:890px;height:550px;green;z-index:10;background-color:#6a7aaa;"
				}
			})

			//-------------------------------------------

			LED.actionwindow = df.newdom({
				tagname		: "div",
				target		: LED.LEDwindow,
				attributes	: {
					id		: "actionwindow",
					style	: "position:absolute;top:120px;left:380px;width:520px;height:82px;green;z-index:999999;"
				}
			})

			//-------------------------------------------
			//not shown on screen
			LED.contextHolder = df.newdom({
				tagname		: "div",
				target		: LED.LEDwindow,
				attributes	: {
					style	: "position:absolute;top:-250px;left:380px;width:520px;height:87px;overflow:hidden;overflow-y:auto;background-color:#000000;z-index:999999;"

				}
			})

			//-------------------------------------------

			LED.buttonList = df.newdom({
				tagname	: "div",
				target	: LED.LEDwindow,
				attributes	: {
					id		: "buttonList",
					style	: "position:absolute;top:10px;left:10px;width:350px;height:740px;",
					className: "buttonlist"
				},
				children	: [
					{
						tagname	: "ul",
						attributes	: {
							id	: "actionlist"
						}
					}
				]

			})

			//-------------------------------------------

			df.attachevent({name:"beforeunload",target:window,handler:function(){
				for (var i=0;i<LED.queue.length;i++) {
					LED.stopPlugin(LED.plugins[i])
				}
			}})


			//-------------------------------------------

			if (LED.plugins.length==0) {

				df.ajax.send({
					url : "/getFreakselectorPlugins",
					callback:function(data) {

						data = df.json.deserialize(data.text)


					var pluginlist = data.files
					var pluginlen = pluginlist.length

					df.waitfor(function(){
						return (pluginlen == LED.plugins.length)
					},function(){
						LED.pluginsLoaded()
						LED.drawPluginList()
						df.noselectDOM(LED.LEDwindow,true)
					})

					var loadPlugin = function() {
						if (pluginlist.length==0){
							setTimeout(function autoStartPlugins(){
								for (var ii=0;ii<LED.plugins.length;ii++){
									if (LED.plugins[ii].autostart == true) {
										LED.startPlugin(ii)//LED.plugins[ii].name
									}
								}
							},400)
							return
						}
						var thisplugin = pluginlist.shift()

						df.ajax.xssloader({url:unescape(thisplugin).split("\\").pop()})

						//var new_script = document.createElement("script");
						//new_script.src = unescape(thisplugin).split("\\").pop();
						//document.getElementsByTagName("head")[0].appendChild(new_script)

						//setTimeout(loadPlugin,0)
						loadPlugin()
					}

					if (pluginlist.length>0) {
						loadPlugin()
					}

				}

			})
		}

	},

  //--------------------------------------------------------------

	window_zoom: 1,

	allow_scaling: true,

  windowResize_tmr: undefined,
	windowResize:	function(evtobj){
    if (LED.windowResize_tmr){clearTimeout(LED.windowResize_tmr)}
    LED.windowResize_tmr = setTimeout(function(){
  		var ui_aspect = 768 / 1280;

  		var aspect = window.innerHeight / window.innerWidth;

  		var newzoom

  		if (aspect >= ui_aspect ){
  			newzoom = (window.innerWidth / 1280 )
  		} else {
  			newzoom = (window.innerHeight / 768 )
  			console.log(newzoom * window.innerWidth)
  		}

  		LED.window_zoom = newzoom;
  		window.window_zoom = newzoom;

  		if (LED.allow_scaling === true){
  			df.dg("body").style.zoom = newzoom
  		} else {
  			df.dg("body").style.zoom = ""
  		}
    },200)
	},

	pluginsLoaded:	function(){
		//load stored settings
		//start plugins marked as auto-start
		//etc..

		//set LCD brightness to 100
		df.ajax.send({
			url	: "nircmd/"+LED.base64encode("setbrightness 0 3")
		})

		//--------------------------------------------------------------
		//sort plugin list based on plugin type (group plugins)
		//this may also be over-ridden by a saved plugin layering preference

		LED.plugins.sort(function(a,b){

			var a_del = 0
			var b_del = 0

			if (a.sort_top && !b.sort_top) {return -1}
			if (b.sort_top && !a.sort_top) {return 1}

			if (a.fullscreen == true)	{a_del += 1}
			if (b.fullscreen == true)	{b_del += 1}

			if (a.render != undefined)	{a_del += 2}
			if (b.render != undefined)	{b_del += 2}

			if (a.has_audio == true || a.has_eqData == true)	{a_del += 4}
			if (b.has_audio == true || b.has_eqData == true)	{b_del += 4}

			if (a.render != undefined && a.fullscreen == true)	{a_del -= 2}
			if (b.render != undefined && b.fullscreen == true)	{b_del -= 2}

			if (a_del>b_del)		{return -1}
			if (a_del<b_del)		{return 1}

			if (a.name>b.name)		{return -1}
			if (a.name<b.name)		{return 1}

			return 0
		})

    for (var i=0;i<LED.plugins.length;i++){
      if (LED.plugins[i].sendFrame !== undefined){
        LED.output_devices.push(LED.plugins[i]);
      }
      if (LED.plugins[i].auto_start === true) {
        setTimeout(function(xx){LED.startPlugin(xx.i)},100,{i:i})
      }
    }
		//--------------------------------------------------------------
	},

//==================================================================================

  openOnScreenKeyboard: function(){
    df.ajax.send({url:'/openOnScreenKeyboard', calback:function(ajaxobj){
      console.log("start OSK",ajaxobj.text)
    }})
  },

	setSystemVolume:	function(newval){

		if (newval>1||newval<0) {
			return
		}

		try{clearTimeout(LED.sysvol_tmr)}catch(e){}
		LED.sysvol_tmr = setTimeout(function(){
			LED.system_volume = newval
			df.ajax.send({
				url	: "nircmd/"+LED.base64encode('setsysvolume '+(newval*65535))
			})
		},20)
	},
//==================================================================================

	master_volume	: 1,
	frame_time		: 34,

//==================================================================================

	normalizeInput				: true,
	autoFit						: true,
	showInputImage				: false,
	setBilinearInterpolator		: true,
	ledRenderSize				: 3.08,

	setPixel:	function (x,y,r,g,b,a,context) {
		var frameidx = LED.getCanvasIndex(x,y)
		var frame = context || LED.frame
		frame[frameidx]   = String.fromCharCode(Math.floor(b))
		frame[frameidx+1] = String.fromCharCode(Math.floor(g))
		frame[frameidx+2] = String.fromCharCode(Math.floor(r))
		if (a) {
			frame[frameidx+3] = String.fromCharCode(Math.floor(a))
		}
	},

	setPixelHex:	function (x,y,hex,context) {
		var frameidx = LED.getCanvasIndex(x,y) //((y*grid_width)+x)*4
		var r = df.getdec(df.hexcolor(hex).substr(0,2))
		var g = df.getdec(df.hexcolor(hex).substr(2,2))
		var b = df.getdec(df.hexcolor(hex).substr(4,2))
		var frame = context || LED.frame
		frame[frameidx]   = String.fromCharCode(Math.floor(b))
		frame[frameidx+1] = String.fromCharCode(Math.floor(g))
		frame[frameidx+2] = String.fromCharCode(Math.floor(r))
	},

	getCanvasIndex:	function(x,y){
		return ((Math.floor(y)*LED.grid_width)+Math.floor(x))*4
	},

//=======================================================================================================================
//=======================================================================================================================
//=======================================================================================================================
//PLUGINS

	currentplugin		: 0,

	queue:[],	//The queue holds the plugins that are currently active. The order indicates which plugins get rendered on the context first, allowing compositing in layer order

//-----------------------------------------------------------------------

	drawPluginList:	function() {

		var list = df.dg("actionlist")
		list.innerHTML = ""

		var thiswidth = list.offsetWidth-6-20-22

		for (var i=0;i<LED.plugins.length;i++) {

			var thisplugin = LED.plugins[i]

			var html = []
			html.push('<span style="position:relative;float:left;width:100%;">')
			html.push(	((thisplugin.icon)?'<image src="freakselector_plugins/icons/'+thisplugin.icon+'" style="float:left;top:-5px;left:5px;position:absolute;z-index:0;"/>':'') )
			html.push(	'<span style="position:absolute;font-size:30px;margin-top:-4px;top:0pxp;left:0px;z-index:100;'+((thisplugin.icon)?'margin-left:55px;':'margin-left:10px;;font-size:30px;margin-top:-4px')+'">')
			html.push(		thisplugin.name )
			html.push(	'</span>')
			html.push(	'<span style="position:absolute;top:0pxp;left:0px;z-index:100;width:100%;">')
			html.push(	((thisplugin.fullscreen)?'<image title="Fullscreen" src="images/icon_small_fullscreen.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	((thisplugin.has_audio)?'<image title="Has Audio Data" src="images/icon_small_hasaudiodata.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	((thisplugin.has_eqData)?'<image title="Has EQ Data" src="images/icon_small_haseqdata.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	((thisplugin.render)?'<image title="Has Video" src="images/icon_small_hasvideo.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	((thisplugin.uses_camera)?'<image title="Uses Camera" src="images/icon_camera.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	'</span>')
			html.push('</span>')

			var thisbutton = df.newdom({
				tagname		: "div",
				target		: list,
				attributes	: {
					style	: "width:"+(thiswidth)+"px;",
					html	: html.join(""),
					className: (LED.current_interface && LED.plugins[i].name == LED.current_interface.name)?"hasinterface":((LED.plugins[i].started==true)?"playing":"")
				},
				events:[
					{
						name	: "click",
						handler	: function(evtobj){
							LED.startPlugin(evtobj.srcdata.index)
							//evtobj.srcdata.plugin.interface()
						},
						index	: i,
						plugin	: thisplugin
					}
				]
			})

			thisplugin.actionbutton = thisbutton

			LED.registry[thisplugin.name] = thisplugin

		}

	},

//-----------------------------------------------------------------------

	startPlugin:	function(index){

		var thisplugin = LED.plugins[index] || LED.registry[index]

//		if (thisplugin.render==undefined) {
//			thisplugin.start()
//			LED.takeInterface(thisplugin)
//			LED.actionButtonState()
//			return
//		}

		var queued = false
		var queued_idx
		for (var i=0;i<LED.queue.length;i++){
			if (thisplugin.name == LED.queue[i].name) {
				queued = true
				queued_idx = i
				break
			}
		}

		if (LED.current_interface && LED.current_interface.render == undefined && LED.current_interface.has_eqData != true && LED.current_interface.has_audio != true ) {
			var removefromqueue
			for (var i=0;i<LED.queue.length;i++){
				if (LED.current_interface.name == LED.queue[i].name) {
					removefromqueue = i
					break
				}
			}
			if (removefromqueue!=undefined) {
				LED.queue.splice(removefromqueue,1)
			}
		}

		if (queued != true) {
			if (thisplugin.fullscreen == true && LED.queue.length>0) {
				if (LED.queue[LED.queue.length-1].fullscreen==true) {
					if (LED.queue[LED.queue.length-1].pause) {
						LED.queue[LED.queue.length-1].pause();
						LED.queue[LED.queue.length-1].paused = true
					}
					LED.queue[LED.queue.length-1].started = false
					LED.queue.splice(LED.queue.length-1,1)
				}
				LED.queue.push(thisplugin)
				if (thisplugin.play && thisplugin.paused==true) {thisplugin.play();thisplugin.paused=false}

			} else {
				LED.queue.unshift(thisplugin)
			}
		}

		if (LED.queue.length == 1 && LED.queue[0].render && LED.bright_dimmer <1) {
			LED.fadeIn()
		}


		var startinterface = false
		if (thisplugin.started != true) {


			startinterface = ( thisplugin.start() != false )

			if (thisplugin.render || thisplugin.has_eqData || thisplugin.has_audio) {
				thisplugin.started = startinterface
			}

			if (LED.queue.length >= 1){
				LED.toggleRender(true)
			}

		} else if (thisplugin.has_interface!==true) {
			startinterface = true
		}

		if (thisplugin.has_eqData==true) {
			if (LED.eqData_source != undefined) {
					if (LED.eqData_source.stopEQData) {LED.eqData_source.stopEQData()}
					var eqdata_queued = false
					var eqdata_queued_idx
					for (var i=0;i<LED.queue.length;i++){
						if (LED.eqData_source.name == LED.queue[i].name) {
							eqdata_queued = true
							eqdata_queued_idx = i
							break
						}
					}
			//		if (eqdata_queued == true) {
			//			LED.queue[eqdata_queued_idx].started = false
			//			LED.queue.splice(eqdata_queued_idx,1)
			//		}

					//TODO: turn on/off eq data icon highlighting

			}

			LED.eqData_source = thisplugin
		}

		if (startinterface!=false) {
			LED.takeInterface(thisplugin)
		}

		LED.actionButtonState()

	},


	stopAll:	function() {

		LED.fadeOut(function(){
			for (var i=LED.queue.length-1;i>-1;i--) {
				if (LED.queue[i].render && LED.queue[i].stop){
					LED.queue[i].stop()
					LED.queue.splice(i,1)
				}
			}
			LED.pluginwindow.innerHTML = ""

		})

	},

	takeInterface:	function(thisplugin) {

		if (thisplugin.interface && thisplugin.has_interface!==true ) {
			if (LED.current_interface){
				LED.current_interface.has_interface = false
				if (LED.current_interface.removeInterface) {LED.current_interface.removeInterface()}
			}

			LED.pluginwindow.innerHTML = ""
//			try{
				thisplugin.interface()
//			}catch(e){
//				console.error(e)
//			}
			thisplugin.has_interface = true
			LED.current_interface = thisplugin
		}

		var firstlevel_left = 383
		if (thisplugin.fullscreen != true && thisplugin.render!=undefined && thisplugin.has_EQdata != true) {

			var pluginup = df.newdom({
				tagname	: "img",
				target	: LED.LEDwindow,
				attributes:{
					id	: "plugin_move_up",
					src:"images/icon_up.png",
					style:"cursor:pointer;position:absolute;top:10px;left:"+firstlevel_left+"px;width:55px;"
				},
				events:[
					{
						name:"click",
						handler:function(evt){

							var plugin_idx = -1
							for (var i=0;i<LED.plugins.length;i++){
                if (LED.current_interface.name == LED.plugins[i].name) {
                  plugin_idx=i;
                  break
                }
              }
							if (plugin_idx>0){
								var this_plugin = LED.plugins[plugin_idx]
								var next_plugin = LED.plugins[plugin_idx-1]
								LED.plugins[plugin_idx]   = next_plugin
								LED.plugins[plugin_idx-1] = this_plugin
							}

							var new_queue = []
							for (var i=0;i<LED.plugins.length;i++){
								if (LED.plugins[i].started == true) {
									new_queue.push(LED.plugins[i])
								}
							}
							LED.queue = new_queue

							var top = LED.buttonList.scrollTop
							LED.drawPluginList()
							LED.buttonList.scrollTop = top
						}
					}
				]
			})
			var plugindown = df.newdom({
				tagname	: "img",
				target	: LED.LEDwindow,
				attributes:{
					id	: "plugin_move_down",
					src:"images/icon_down.png",
					style:"cursor:pointer;position:absolute;top:60px;left:"+firstlevel_left+"px;width:55px;"
				},
				events:[
					{
						name:"click",
						handler:function(evt){

							var plugin_idx = -1
							for (var i=0;i<LED.plugins.length;i++){if (LED.current_interface.name == LED.plugins[i].name) {plugin_idx=i;break}}
							if (plugin_idx<LED.plugins.length-1){
								var this_plugin = LED.plugins[plugin_idx]
								var next_plugin = LED.plugins[plugin_idx+1]
								LED.plugins[plugin_idx]   = next_plugin
								LED.plugins[plugin_idx+1] = this_plugin
							}

							var new_queue = []
							for (var i=0;i<LED.plugins.length;i++){
								if (LED.plugins[i].started == true) {
									new_queue.push(LED.plugins[i])
								}
							}
							LED.queue = new_queue

							var top = LED.buttonList.scrollTop
							LED.drawPluginList()
							LED.buttonList.scrollTop = top
						}
					}
				]
			})
			firstlevel_left += 70
		} else {
			try{
				df.removedom(df.dg("plugin_move_up"))
				df.removedom(df.dg("plugin_move_down"))
			}catch(e){}
		}


		if (thisplugin.settings_to_save!=undefined) {

			var pluginup = df.newdom({
				tagname	: "img",
				target	: LED.LEDwindow,
				attributes:{
					id	: "plugin_settings",
					src:"images/settings.png",
					style:"cursor:pointer;position:absolute;top:10px;left:"+firstlevel_left+"px;width:95px;"
				},
				events:[
					{
						name:"click",
						handler:function(){
							LED.saveSettings()
						}
					}
				]
			})

			firstlevel_left += 100

		} else {
			if (df.dg("plugin_settings")){
				df.removedom(df.dg("plugin_settings"))
			}
		}

		if (thisplugin.play || thisplugin.stop || thisplugin.pause || thisplugin.next || thisplugin.last || thisplugin.mute  || thisplugin.end) {
			//df.setstyle("","","")
			//LED.pluginwindow.style.top = "100px"
			//df.setstyle(".buttonlist","height","628px")
			var actionwindow = df.dg("actionwindow")
			actionwindow.style.display="block"
			actionwindow.innerHTML = ""

			try{clearInterval(LED.monitor_playstate)}catch(e){}

			if (thisplugin.play_state == undefined) {thisplugin.play_state = "playing"}

			var last,play,pause,stop,next,pluginup,plugindown


			if (thisplugin.end) {
				eject = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_eject.png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(){
								LED.stopPlugin(thisplugin)
								if (LED.queue.length==0){
									LED.toggleRender()
								}

							}
						}
					]
				})
			}

			if (thisplugin.last) {
				last = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_back.png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(){
								thisplugin.last()
							}
						}
					]
				})
			}

			if (thisplugin.play) {
				play = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_play"+((thisplugin.play_state=="playing")?"_active":"")+".png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(evtobj){
								thisplugin.play()
								thisplugin.play_state = "playing"
								evtobj.target.src = "images/button_play_active.png"
								if (stop){stop.src="images/button_stop.png"}
								if (pause){pause.src="images/button_pause.png"}
							}
						}
					]
				})
			}

			if (thisplugin.pause) {
				pause = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_pause"+((thisplugin.play_state=="paused")?"_active":"")+".png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(evtobj){
								thisplugin.pause()
								thisplugin.play_state = "paused"
								evtobj.target.src = "images/button_pause_active.png"
								if(stop){stop.src="images/button_stop.png"}
								if(play){play.src="images/button_play.png"}
							}
						}
					]
				})
			}

			if (thisplugin.stop) {
				stop = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_stop"+((thisplugin.play_state=="stopped")?"_active":"")+".png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(evtobj){
								thisplugin.stop()
								thisplugin.play_state = "stopped"
								evtobj.target.src = "images/button_stop_active.png"
								if(play){play.src="images/button_play.png"}
								if(pause){pause.src="images/button_pause.png"}
							}
						}
					]
				})
			}

			if (thisplugin.next) {
				next = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_ffwd.png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(){
								thisplugin.next()
							}
						}
					]
				})
			}



			LED.monitor_playstate = setInterval(function(){

				if (play && thisplugin.play_state != "playing" && play.src.indexOf("images/button_play.png")==-1){play.src = "images/button_play.png"}
				if (stop && thisplugin.play_state == "stopped" && stop.src.indexOf("images/button_stop.png")==-1){stop.src="images/button_stop.png"}
				if (pause&& thisplugin.play_state == "paused"  && pause.src.indexOf("images/button_pause.png")==-1){pause.src="images/button_pause.png"}

				if (play && thisplugin.play_state == "playing" && play.src.indexOf("images/button_play_active.png")==-1){play.src = "images/button_play_active.png"}
				if (stop && thisplugin.play_state == "stopped" && stop.src.indexOf("images/button_stop_active.png")==-1){stop.src = "images/button_stop_active.png"}
				if (pause&& thisplugin.play_state == "paused" && pause.src.indexOf("images/button_pause_active.png")==-1){pause.src = "images/button_pause_active.png"}

			},100)


		} else {
		//	LED.pluginwindow.style.top = "0px"
			//df.setstyle(".buttonlist","height","738px")
			df.dg("actionwindow").style.display="none"
		}

		if (df.dg("saveSettingsDiv")!=null) {
			LED.saveSettings()
		}
	},

	actionButtonState:	function() {
		for (var i=0;i<LED.plugins.length;i++) {
			LED.plugins[i].actionbutton.removeAttribute("class")
		}
		for (var i=0;i<LED.queue.length;i++) {
			if (LED.queue[i].started == true) {
				LED.queue[i].actionbutton.setAttribute("class","playing")
			}
			if (LED.queue[i].has_interface == true) {
				LED.queue[i].actionbutton.setAttribute("class","hasinterface")
			}
		}
	},

//-----------------------------------------------------------------------

	toggleRender:	function(force){

		if (force==true) {try{clearTimeout(LED.render_tmr)}catch(e){};LED.render_tmr = undefined}

		if (LED.render_tmr == undefined || (force != undefined && force == true) ) {

			LED.render_tmr = setTimeout(function(){LED.render()} , LED.frame_time)
		} else if (LED.render_tmr != undefined || (force != undefined && force == false)) {
			try{clearTimeout(LED.render_tmr)}catch(e){}
			LED.render_tmr = undefined
			LED.render()
		}

	},

	forceRender:	function() {
		try{clearTimeout(LED.render_tmr)}catch(e){}
		LED.render()
	},

//-----------------------------------------------------------------------
	render_tmr:undefined,

	testx:false,

	car_volume:1,

	render:	function(){
		try{clearTimeout(LED.render_tmr)}catch(e){}

		LED.composite_ctx.clearRect(0, 0, LED.grid_width,LED.grid_height);


/*
		var plugins_rendered = 0
		for (var i=LED.queue.length-1;i>-1;i--) {
			try{
				if (LED.queue[i].render != undefined){
					if (LED.queue[i].output_plugin == true) {continue}
					LED.queue[i].render()
					plugins_rendered++
				}
			}catch(e){
				console.warn("************************* Error rendering "+LED.queue[i].name)
				console.error(e)
			}
		}
*/
    var render_queue = [].concat(LED.queue);
    render_queue.sort(function(a,b){
      if ((a.render_last || a.output_plugin) && (!b.render_last || !b.output_plugin)){return 1}
      if ((b.render_last || b.output_plugin) && (!a.render_last || !a.output_plugin)){return -1}
      return 0
    })


    var plugins_rendered = 0
		for (var i=0;i<render_queue.length;i++) {
      if (LED.queue[i].output_plugin === true && render_queue[i].render === undefined) {continue}
			if (render_queue[i].render && render_queue[i].sendFrame === undefined) {
        try{
	  			render_queue[i].render()
          plugins_rendered++
        }catch(e){
          console.warn("************************* Error rendering "+LED.queue[i].name)
          console.error(e)
        }
			}
		}

//console.log("plugins_rendered",plugins_rendered)

		if (LED.queue.length === 0 && plugins_rendered === 0) {
			LED.composite_ctx.clearRect(0, 0, LED.grid_width,LED.grid_height);
			LED.sendFrame()
		} else if (LED.queue.length>0 && plugins_rendered>0) {
			LED.sendFrame()
		}

		if (LED.render_fast) {
			LED.render_tmr = requestAnimationFrame(function(){LED.render()} ,LED.frame_time_override || LED.frame_time)
		} else {
			LED.render_tmr = setTimeout(function(){LED.render()} , LED.frame_time_override || LED.frame_time)
		}

	},

//-----------------------------------------------------------------------
  output_devices:[],
  sendFrame: function(){
    var frame = Array.apply(null,LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height).data);
    for (var i=0;i<LED.output_devices.length;i++){
      if(LED.output_devices[i].render){
        LED.output_devices[i].render();
      }
      LED.output_devices[i].sendFrame(frame)
    }
  },

//-----------------------------------------------------------------------

	analog_inputs:	[],

	lastframe:"",


//-----------------------------------------------------------------------
	fade_tmr	: undefined,
	fade_speed	: .04,
	fade_timeout	: 40,
	fade_inprogress	: false,
	fadeOut:	function(callback) {
		try{clearInterval(LED.fade_tmr)}catch(e){}
		LED.fade_inprogress = true
		LED.fade_tmr = setInterval(function() {
			if (LED.bright_dimmer > 0) {
				LED.bright_dimmer -= LED.fade_speed
				if (LED.bright_dimmer<0){LED.bright_dimmer=0}
				LED.ledAttenuation = LED.bright_dimmer
				if (LED.bright_dimmer==0) {
					try{clearInterval(LED.fade_tmr)}catch(e){}
					LED.fade_inprogress = false
					if (callback) {callback()}
				}
			} else {
				try{clearInterval(LED.fade_tmr)}catch(e){}
				if (callback) {callback()}
				LED.fade_inprogress = false
			}
		},LED.fade_timeout)
	},

	fadeIn:		function(callback) {
		try{clearInterval(LED.fade_tmr)}catch(e){}
		LED.fade_inprogress = true
		LED.fade_tmr = setInterval(function() {
			if (LED.bright_dimmer < 1) {
				LED.bright_dimmer += LED.fade_speed
				if (LED.bright_dimmer>1){
					LED.bright_dimmer=1
				}
				LED.ledAttenuation = LED.bright_dimmer
				if (LED.bright_dimmer==1) {
					try{clearInterval(LED.fade_tmr)}catch(e){}
					LED.fade_inprogress = false
					if (callback) {callback()}
				}
			} else {
				try{clearInterval(LED.fade_tmr)}catch(e){}
				if (callback) {callback()}
				LED.fade_inprogress = false
			}
		},LED.fade_timeout)
	},

//-----------------------------------------------------------------------

	stopPlugin:	function(index,faded) {
		var thisplugin = LED.registry[index.name] || LED.plugins[index] || LED.registry[index]

		if (faded!=true && LED.queue.length==1 && LED.queue[0].render != undefined) {
			LED.fadeOut(function() {
				LED.stopPlugin(index,true)
			})
			return
		}

		thisplugin.started = false

		if (LED.current_interface == thisplugin) {
			LED.pluginwindow.innerHTML = ""
			thisplugin.has_interface = false
			LED.current_interface = undefined
		}

		if (LED.eqData_source == thisplugin) {
			LED.eqData_source = undefined
		}


		thisplugin.actionbutton.className=""

		LED.actionwindow.innerHTML = ""

		if (thisplugin.end){thisplugin.end()}

		for (var i=LED.queue.length-1;i>-1;i--) {
			if (LED.queue[i].name === thisplugin.name) {
				LED.queue.splice(i,1)
				break
			}
		}


	},

//=======================================================================================================================

	getTree:	function(thispath,goback,target,thiscallback) {

		if (thiscallback==undefined) {thiscallback = LED.renderTree}

		var thistree = []

		if (goback!=undefined && goback.push) {
			goback.push(thispath)
		}

		df.ajax.send({
			url : "gettree.aspx?thispath="+thispath,
			callback:function(data) {

				var tree = df.json.deserialize(data.text)

				if (thiscallback!=undefined) {thiscallback(tree,target,goback)}

			}
		})
	},

	renderTree:	function(tree,target,goback,select_callback,current) {

		var playlist = target

		var folders = tree.folders

		target.innerHTML=""

		//if (LED.goback.length>1) {
		if (goback[goback.length-1]!=undefined){
			df.newdom({
				tagname	: "li",
				target	: target,
				attributes:{
					className: "folderlist",
					html	: '[Back] <span style="color:#ffffff;">'+unescape(goback[goback.length-1])+'</span>'
				},
				events:	[
					{
						name	: "mouseup",
						handler	: function(evtobj) {
							if (goback.length>0) {
								goback.pop()
								var treeback = goback.pop()
console.log("treeback",treeback)
								LED.getTree(treeback,goback,target,LED.renderTree)
							}

/*
							if (goback.length==0) {
								tree = {
									folders:{
										"D:%5Cmusic":[],
										"E:%5Cmusic2":[]
									},
									files:[]
								}
								LED.renderTree()
								return
							}
*/

						}
					}
				]
			})
		}
		//}

		for (var selector in folders) {
			df.newdom({
				tagname	: "li",
				target	: target,
				attributes:{
					className: "folderlist"+((current && current.indexOf(selector)!=-1)?" playingfolder":""),
					html	: unescape(selector.replace(/_/g," "))
				},
				events:	[
					{
						name	: "mouseup",
						handler	: function(evtobj) {
							var folder = evtobj.srcdata.selector
							LED.getTree(folder,goback,target)

						},
						selector		: selector
					}
				]
			})
		}

		var current_file
		for (var i=0;i<tree.files.length;i++) {
			var thisfilebutton = df.newdom({
				tagname	: "li",
				target	: target,
				attributes:{
					className: "folderbutton"+((LED.currently_playing == tree.files[i])?" playing":""),
					html	: unescape(tree.files[i].replace(/_/g," ")).substr(unescape(tree.files[i].replace(/_/g," ")).lastIndexOf("\\")+1)
				},
				events:	[
					{
						name	: "mouseup",
						handler	: function(evtobj) {
							var thisfile = tree.files[evtobj.srcdata.idx]
							if (select_callback) {select_callback( unescape(music).substr(3), thisfile )}
							LED.renderTree(tree,target,goback,select_callback,current)
						},
						idx		: i
					}
				]
			})
			if (current==tree.files[i]) {
				current_file = thisfilebutton
			}
		}

		if (current_file!=undefined){
			current_file.scrollIntoView()
		}

	},


//=======================================================================================================================
	sliderList:	function(slidertypes) {

		var sliders = []

		var thisvalue;

		for (var i=0;i<slidertypes.length;i++) {

			sliders.push(
				{
					tagname	: "div",
					attributes:{
						style	: "clear:both;position:relative;padding:3px;margin:4px;border:1px solid black;border-radius:5px;background-color:#2a2a4a;font-family:arial;font-size:12px;"
					},
					children:[
						{
							tagname	: "div",
							attributes	: {
								html	: slidertypes[i].name,
								style	: "font-size:20px;margin-bottom:10px;position:relative;top:5px;left:5px;"
							}
						},
						thisvalue = {
							tagname	: "div",
							attributes	: {
								id	: "slidervalue_"+slidertypes[i].id,
								html	: parseFloat(slidertypes[i].startvaluetext||slidertypes[i].startvalue||0).toFixed(2),
								style	: "font-size:20px;position:absolute;top:5px;left:-5px;text-align:right;width:100%;"
							}
						},
						{
							tagname	: "df-slider",
							attributes	: {
								id			: "slider_"+slidertypes[i].id,
								name		: slidertypes[i].name,
								className	: slidertypes[i].classname||"slider",
								min			: (slidertypes[i].min!=undefined)?slidertypes[i].min:0,
								max			: (slidertypes[i].max!=undefined)?slidertypes[i].max:1,
								startvalue	: slidertypes[i].startvalue||0,
								height		: (slidertypes[i].height!=undefined)?slidertypes[i].height:44,
								width		: (slidertypes[i].width!=undefined)?slidertypes[i].width:285,
								onchange	: slidertypes[i].handler,
								onmousedown	: slidertypes[i].mousedown,
								onmouseup	: slidertypes[i].mouseup,
								style		: slidertypes[i].style||"",
								showvalue	: thisvalue.attributes.id
							}
						}
					]
				}

			)
		}

		return sliders
	},
//=======================================================================================================================
	settings_to_save:[
		"capture_analog",
		"rdimmer",
		"gdimmer",
		"bdimmer",
		"bright_dimmer",
		"ledGamma"

	],

	capture_analog: false,

	rdimmer		: 1,
	gdimmer		: 1,
	bdimmer		: 1,
	bright_dimmer	: 1,
	ledGamma	: 2,

	settings_global_save:false,

	saveSettings:	function(){

		var savedom = df.newdom({
			tagname	: "div",
			target	: LED.LEDwindow ,//LED.pluginwindow,
			attributes:{
				id	: "saveSettingsDiv",
				style:	"position:absolute;z-index:999999;top:44px;left:377px;width:520px;height:100px;background-color:#eaeaea;border-radius:20px;border:5px solid black;",
				html	: ""
			}
		})

		df.newdom({
			tagname	: "div",
			target	: savedom,
			attributes	: {
				html	:"Saved Settings",
				style	: "position:absolute;top:9px;left:7px;font-size:24px;color:black;font-family:arial;"
			}
		})

		var plugin_settings

		var thisstorage = localStorage.getItem("savedSettings")
		if (thisstorage == null) {
			localStorage.setItem("savedSettings","{}")
			plugin_settings = {}
		} else {
			plugin_settings = df.json.deserialize(thisstorage)
		}

		var has_settings = false
		for (var selector in plugin_settings) {
			has_settings = true
		}

		if (has_settings) {

			var optionslist = []
			for (var selector in plugin_settings) {
				if (LED.settings_global_save != true && plugin_settings[selector][LED.current_interface.name]==undefined) {
					continue
				}

				optionslist.push(df.newdom({
					tagname	: "option",
					attributes:{
						value	: selector,
						html	: selector
					}
				}))
			}

			var saveselect = df.newdom({
				tagname	: "select",
				target	: savedom,
				attributes	: {
					style	: "position:absolute;top:43px;width:368px;left:11px;font-size:30px;color:black;font-family:arial;"
				},
				children:optionslist,
				events:[
					{
						name:["change","click"],
						handler:function(){
							savename.value = saveselect.getElementsByTagName("option")[saveselect.selectedIndex].value
						}
					}
				]
			})

		}

		var savename = df.newdom({
			tagname	: "input",
			target	: savedom,
			attributes	: {
				value	: "",
				type	: "text",
				style	: "position:absolute;top:43px;width:350px;left:11px;font-size:30px;color:black;font-family:arial;"
			}
		})


		df.newdom({
			tagname	: "input",
			target	: savedom,
			attributes	: {
				value	: "SAVE",
				type	: "button",
				style	: "position:absolute;top:4px;width:84px;left:179px;font-size:18px;color:black;font-family:arial;"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt) {

						if (savename.value=="") {
							alert("Please enter a name for these settings")
							return
						}

						var save_global = global_setting.getState()

						var settingsobj = {}

						if (save_global == true) {
							settingsobj["LED"] = {
								settings:{}
							}
							for (var ix=0;ix<LED.settings_to_save.length;ix++) {
								settingsobj["LED"].settings[ LED.settings_to_save[ix] ] = LED[LED.settings_to_save[ix]]
							}
						}

						for (var regname in LED.registry) {

							var thisplugin = LED.registry[regname]

							if (save_global != true && thisplugin.has_interface != true) {continue}

							if (thisplugin.settings_to_save) {

								settingsobj[regname] = {
									settings:{}
								}
								for (var ix=0;ix<thisplugin.settings_to_save.length;ix++) {
									settingsobj[thisplugin.name].settings[ thisplugin.settings_to_save[ix] ] = thisplugin[thisplugin.settings_to_save[ix]]
								}
							}
						}

						var savedSettings = df.json.deserialize(localStorage.getItem("savedSettings"))
						savedSettings[savename.value] = settingsobj
						localStorage.setItem("savedSettings",df.json.serialize(savedSettings))

						LED.saveSettings()
					}
				}
			]
		})

		if (has_settings) {
			df.newdom({
				tagname	: "input",
				target	: savedom,
				attributes	: {
					value	: "LOAD",
					type	: "button",
					style	: "position:absolute;top:4px;width:84px;left:274px;font-size:18px;color:black;font-family:arial;"
				},
				events	: [
					{
						name	: "click",
						handler	: function(evt) {

							if (savename.value=="") {
								alert("Please select a setting to load")
								return
							}

							var load_global = global_setting.getState()

							var savedSettings = df.json.deserialize(localStorage.getItem("savedSettings"))

							var loaded_settings = savedSettings[savename.value]
console.log(setting)
							for (var plugin in loaded_settings) {
								if (plugin == "LED") {
									var thisplugin = window.LED
								} else {
									var thisplugin = LED.registry[plugin]
								}

								if (load_global != true && thisplugin.has_interface != true) {continue}

								for (var setting in loaded_settings[plugin].settings) {
									thisplugin[setting] = loaded_settings[plugin].settings[setting]
								}

								if (thisplugin.settingsUpdate != undefined) {
									thisplugin.settingsUpdate()
								}

							}

							var settings_open = (df.dg("saveSettingsDiv")!=null)

							LED.pluginwindow.innerHTML = ""
							LED.current_interface.interface()

							if (settings_open==true) {
								LED.saveSettings()
							}
						}
					}
				]
			})


			df.newdom({
				tagname	: "input",
				target	: savedom,
				attributes	: {
					value	: "DELETE",
					type	: "button",
					style	: "position:absolute;top:4px;width:84px;left:372px;font-size:18px;color:black;font-family:arial;"
				},
				events	: [
					{
						name	: "click",
						handler	: function(evt) {

							if (savename.value=="") {
								alert("Please select a setting to delete")
								return
							}
							var savedSettings = df.json.deserialize(localStorage.getItem("savedSettings"))

							delete savedSettings[savename.value]

							localStorage.setItem("savedSettings",df.json.serialize(savedSettings))

							LED.saveSettings()



						}
					}
				]
			})
		}

		df.newdom({
			tagname	: "div",
			target	: savedom,
			attributes	: {
				style	: "position:absolute;border:2px solid black;top:4px;border-radius:20px;left:100%;margin-left:-40px;background-color:red;font-size:24px;width:25px;height:27px;padding-top:3px;padding-left:3px;padding-right:2px;cursor:pointer;",
				html	: '\u2716'
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt) {
						df.removedom(savedom)
					}
				}
			]

		})


		var global_setting = df.newdom({
			tagname		:"df-onoff",
			target		: savedom,
			attributes:{
				id		: "globalsettings",
				name		: '<span style="float:right;margin-top:1px;">Global</span>',
				namestyle	: "width:100px;font-size:20px;",
				style		: "position:absolute;top:46px;left:396px;",
				className	: "onoff",
				width		: 25,
				value		: LED.settings_global_save,
				onchange	: function(val){
					LED.settings_global_save = val
					LED.saveSettings()
				}
			}
		})


	},

	loadSettings:	function(plugin){


	},
//=======================================================================================================================

	reorderQueue:	function(){
	},

	soundlocked:undefined,	//need a property to track which plugin has ownership of the sound

	registry:	{}	//makes it possible to access plugisn by LED.registry["plugin name"]
	,
	plugins:[],

	music:{
		eqData:[],
		waveformData:{left:[],right:[]},
		peakData:{left:0.0,right:0.0},
		juice:{
			frameCount : 0,
			volume : 0,
			freqBands : [],
			freqBands16 : [],
			avgFreqBands : [],
			relFreqBands : [],
			longAvgFreqBands : [],
			avgRelFreqBands : [],
			waveData : [],
			eqData : [],
			freqData : []

		}

	},	//soundManager2 music Object, or Audio Input emulated soundmanager2 Object, or other input with eq and waveform data

//=======================================================================================================================

	fontlist: [
		'Arial',
		'Arial Bold',
		'Courier',
		'Calibri',
		'Cambria',
		'Courier New',
		'Terminal',
		'Tahoma',
		'Times New Roman',
		'Trebuchet MS',
		'Verdana',
		'Impact Regular',
		'Franklin Gothic',
		'Georgia',
		'Fixedsys Regular',
		'BanksiaBold',
		'BanksiaBlack',
		'BanksiaRegular',
		'BurnstownDamRegular',
		'ChunkFiveRoman',
		'DaysRegular',
		'FirecatMedium',
		'GongNormal',
		'GreenFuzRegular',
		'JungleFeverRegular',
		'PincoyablackBlack',
		'RadiolandRegular',
		'RadiolandSlimRegular',
		'LCDBold',
		'TinetRegular',
		'WhitehallRegular'
	],

  radio_off_image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREVDM0YyQzhEMjExRTE4NzRFRkI0QUYyQjJEQUJBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREVDM0YzQzhEMjExRTE4NzRFRkI0QUYyQjJEQUJBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBERUMzRjBDOEQyMTFFMTg3NEVGQjRBRjJCMkRBQkEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBERUMzRjFDOEQyMTFFMTg3NEVGQjRBRjJCMkRBQkEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5VRjJsAAAPFUlEQVR42tRba4xd11X+9j6P+563PTN+Tx3XTtomTZsUxwaq0hY1kdug/igIED9AiH+AkGiQqARSQZXoD2hBldqqPxAS/ACUJqGCug2pUoxLjUMedeO8HNcZP+J6nvd1nnuz1t7n3rn33DszSXFnhmNt33POPfec9e211rceZ48oju6D2bSGcH1UZt8N6RYQ1W/SqRRSekiTNtxCFa5fvVc43u9Lr3wSUDPSLRXoWArasDUbiZSoNG7FtLukVXoujYMvJeHq6bixQHL7dIGCkBJ+eRIkPOpXn0MSrEIIaW7gbnJ7A5rA/rZfm3nELY7MpUrIOEnpPE1QChrJ8J/Sv56DzZG8NcCCHiuVLro0wyXPcx4ulMYe9qKRW0KLryXB8h8ZwJDr3mB9wGam3OOliQN/5xZH74hihTBKsW92EsfmZnH44DR2T46gXCzQRAqwkknV5lOYY5hZleYz973gyZfZNYPfZdiGTkMQxri5sIKLl67hfy5cxg9fvYpUFabK08ceUWH9d1oLr31axcFX3yZgDbc0+rnS+P4/TJTjhFGCd92xFw996F6ceN87MT5SgetKI5IRVgoDQPbu06eTO8+mZs4J2XcdDwvw7W0s19PnXsTfP34Gp7/7PEIUx2p77vlKsDz/sIrbp/h5AyYy4MMzd8GvTH5W+tXPBHTDPbvH8CunTuDn7juGUsFFFCcQjgvX882QjrQAjNbE2gQIa1hC9hx3tZsdy34r4GM7GehqvPs7ufGUfO/ZV/DnX/o6vnvuJVRKBeho5YmoufCJxo0f9PlwH2A4Dkb23/dZrzb7GaUU7r/7Hfj1h38W02S6YZzCK5ZRGplEoVyB43qZoFYQmQkks2MDLPteZse9+32/HbK/2W86n70bc8vnv/oEvvwPTyJJNdL2j7+zfOk/PpQEdZowxwIujOzp0B8qu4/+WXX27j9O0hSfeug4PnryPUiSGA4x9MjsQfglAiqta/UB6xUuL/gQUJ3zQ4FuMGEbTmb2ydu/n72AX/uDvyG389C+9crZldfPnDBX0AWOPzJLJurAK4/fP7L//X9L5CTe965D+MRH7kOz2YJbnSStH4X0CkQOFIw6Q/cPnfsc9p3ZR+6cGvytXue+Zh926M5vs/uZ89lFhw9MG3f49pkXUJvYsz+NmyoJG087hMFxCyNEyAlG9t17Rni18YnRMn751AMQikx4dJrAHqObSaRkIgMAVA6gGgSqVU74/DFy9+tOiBj+jN57on8SOSCxKyqy0JPvP4rnL17Bi5euozK2+2Tr1mtfUUnUdJh0/MrEr1Zn3v1bKV344Afvwb7pMSivgvG5e8xNOmB1DpjeSEN5YAxg2LV0Lu2dIGxsNcOfJdaOSTlxkoCCCI5RZHns9PeRioJDIetQsPyjf3SKk4dRnb7zUe1VJ/YTUGbjKFEYP3QPZKFCWZYaaqZrgET3XEr/JQwghTF/M+j7tf2ez559lR133KXvXPYMnddur5nnJ4GUFLTaJme4dnMJ33/uVVRqE0fi9tIXncqud763NDlHGYoWP/PeI2CTlsVRVPceBZPXMAFStXac0lOihIFqOOQ3Hs2sS4TI5OYIO5jBO8c25NA+bOhxOgSWZ+KMiPg5PIlJzrQ1adWYtBpUhKYQlFD4VHGE3VOj+MZTz0BJ39FxK3Qppj4C4YmRqotp+rLebGNq716EqTDIOHz1CyO6QrEAbIN7xgSOTEmMlwV8QvCWUwix4aExb9ZomyZ0oamxGtrMy+U4zs7WI0sva5t8Q/hYbixhbv8ufODuw3jqv14mYp74TZeSjOPs6BOjFYMgUg4EJd6cxfDxwA2FPcczXisIfOCgi7lJ+dMrF8gUajSJ/KwGAZ5fVmhGGp7DE6T7479RDk24YhkdUOpAYTXF3ccO4smzL8Ivjky5Qnq7ONGvlosEMqQwNIXUKSGgvJnBWZBiLZ5KC3ZXReDBOz2MFLeqUAKqBPrILgev3Uqx2LKgpeqJ6ylnZNrI6ZCgsZZotUMcIF82qXDqeq5TqBUYsEOOFwQRSjUqDRV9qWJrMsacddecuUqqEciP3VnYUrBdhZNAdxDoH1xPsNom0G6PMgSZubIypw7zikAShqiUCyj6LuLIc1zpepLZVdPgSsSli8JYG7MwQGHzWHtTbZj4wbuKGC1tPdjOxrIcnnJw/kpsZJWZKVulWKt0SNaEK1fCxLPh0kzFwhFupjdi5ITMOIJHhh/GPFOqz5z5psyA/KBjM44ptLXeHsA81RVfYKYm8fpCav3ZWKHomrfDIZKwqChGzM7cXx4KROSzbNJuyMAtYJmFEqb7gGZysaXw0F1lzmcouGuI7VMyXHr47prAyzcVWd2adjuu55BJx4RJEaaQtKz7AAtbaYQE1qf4FSecnikDshEo1IkdW8SMRQ/YOypM6tYJ+tu1peRnnsPZGwFTa2Zty0/iJJoExpFGbPaJiXGiA5h3OMmg79Am0DdXE6w0IwOYQQkbklGtSFR98mM6YPPezs2YLYHmgiGILZlJsRZGHfZZ0nBqTDrpdk/c3tZJu9mEdFcxPkNZSpJmRCXMtQxQcqdKr+XV27mpjFQVpXtMXG6W8HQyOUkCx8RFKSkwyvsw2/xKvUVOHmB0Mu0mqUqvtde6ZSH9Z9K6bdewMI1CNlsm4lTpnho782ECGpOGuUujM1N28zfh21hQKt/Ts4k7l187ALDK+mBhwiat4KneNFNYHybACQNmXx3exLNVCfuoTvsRmQoo3XrAeh3fkVn8CQhUQISaOqKvk+LQcRSShomlgyhBR8VuPr4ZH00HNdxr0jsBMOcN5MImPLKGWT/9cVgZsjIajuP127TsF3oDk9Za37aQpP8PN+HQGFO+UG9zGM1IdQAwm7QlLZ1n6d63DSpLNQc1rG+7hjcDve73hIoCCc5faqJa8rB33KNiQa+ZNQmYRJkPx8k6GhZrxLRVJv2TAjb0qgVa5KcXr0WID5Swh0CrLG9wXGVSy40BZyZtMqkcaam000bRtzXL+kkBd2SwdYDCC2+0CHwB+yY927Klio/NOQljkzb3ZVr5B6yrYcPUlL2ktk+1rSbNvTKWgwlWcyos8NK1FlZbHt4xXYR0yMcp6UhDyqUjnculc32VjUx6p2nYuhjJy4RFx1cXIvPFgd0Vo1lLWmoDk2ZAqR1959kqUr2jfNi0ZzNZdU9ovbYYolT0USYfjtikU2zkw5mGdU7D3a7g1gLe4NW1jRpszmaIPlkvv9nGPieBQ4QVpXqweOjOj+KuRtZIzpNWunPicNekU2vSqrfnSbtc07/RaGKafDjV0sqbz6VFl6Uzh93BmZY16UyeNAfYNL+olm8GwMoKRmq1DeJwxtL/7wAPtF9SE7JWVltkrBKdZSjDw9IwDac7rFoyWV9P9peKgSUbneZFsx1usORBr712HCAttXMaALoTUUwIJZnkOk6+2RoPQ/BDNKxNFx+G0HZCA4Bbx1wp8UIbBpwPo91qZ1PA5rp0ADDPA3MAs1/F2wEdDxqtUFO1ZIENyqNsb30zwICtlpCrlviHi40Er9yIMX3EM28Mt1XDVOAvNVI0grTbc8vjkFw95SCvQ1pZ0M0h5mbZY+dbeOBwzZrRNgLmvvSZlwPUWwnKBTFovVoapclNTTqrhwccng4LDvD0iwHOXfJx/5yPZrj1zXgWy/eA+cUU/3yuBVfq4e7F2h1CWlIrfhdopTaxSqXd4iE/eBq58virf21gqcmvOGxWprZw8Aszbhf/9ekm5hcodRR6qKwml+A2UG+WTWYpk7ARr61Y4457mxgvMf1eE9Bzw3eo9rwS4U/+aQVtIrCS10k3f/qD3yHx+MK/NfAvz7RQdIfLaIZZe9HqvmVnZaokUq5Oo2U6nLElpoBM6tAxgXYKQ2mdNwb51IUAv9tS+PSpGu7a55p2i10Kcft8u7O0kd8q+K6gKijBF7/ZwDeeDUgGsQGP8C9jyGgp4/OsI6uSxPEruz5cHJ09YqsjAqwiJN44Eme0m60MG7yO48qtBN98ro2FhkKtKA3gzjKF9DYMqt+poFe4Sv76KPnr5x6rE39EKPmiL7cYHMTNSQPF1ktriajjIlq9sSBG9t370dFDJ053SgehqZzyZ1CvHaf9dPOXWjTL3Awv+RIVYktmzKJ3e5iMW1HNUBly5Jd6rGV+gbYpsQkXldYLKLZfNfs2jHlYuXz2yy45+Lfi1tKCX52a5IRDCwdevAgRr0I5lXXNutd4iq4t01aawHJDm9U1t8OuO28RhLBu1M0RNknBZNqAH84bLPY+ZH1hQ8Wt5c87iv1VxbXS5NzPIzNrocn+VYjAnbXVylsZZnWUzt7q3Z7RWXVgmPYtyqHgoBxeQiF+k9VqQ5HroXH9wn/Xrz37F8Kr7DJvIKaO/uKiV5mq8TJEMytk2vXCUTT8I2bfqkxgZ25WNjbfYnIDY8Gz3XOsXZXG+ubzj96fBCvnHa86xWagSNO3SmP7P97FRHZUSG8ZAgjExBobab3jhl2XyWCvYyx8HnbJmmVnSb5Lmn08XLn6l9Ir8jtlxywuTZoLzwi38EChtvuONf8jAlK34Og2KIGj4WfWq7Ys9m407LJHflMYoJpexmh80SxY64BlZg4Wf3Rt+fX/PKHTWDFHCY//+qOTihH40bkTT5QmDp7SPbm0AL+bcdGSu1CX+ynCVWGzVHF72OltR2dbDXlooarmUVY3yXPDHpnYaF2EqzfmF1759tE0bLY6C8TdPMMl7eWPx63RJ73y2C/Y3rRZxWUeUFHXzc0jAhwLHkUzEVsLV8HVATzdJHurk2SRkc/K2PFGB3F78UqwOv8eUmSr96+M3EFWp4tbCx9OguWzpfFDxw3gjL3tTTUKWEVRL3ffuW49QXVjAsPvUxiTVLBy9Y1o9foRwhLlqxvH8cp9YP3aNNk+/Whp/mtxe0m4xZHj5Oyu6GHDbJXjtg70mK8BSv6qk1A1blz4erB0+QThShh81LhJtUHc/fsAuZ7hWE0v/mn96nOHGm/+8DEqMkIIe97+iYnE9izUEl1NikwOlQSq9eOXzy9f/t7JYPmNT9p2x3DZ3I2TFvYN9WawdOWX0qA+RtnY70m/+huOX56mr30pKaO2ZLB1yKliMIxLJY5OoqUkWH08ai58Iapfvygdn2T2NhTnfwUYAD6rBaOq1Xm+AAAAAElFTkSuQmCC',
  radio_on_image : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkU1RUZENjk2QzhEMjExRTE4Njg1Q0RGNkI0RUQ0ODQ0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkU1RUZENjk3QzhEMjExRTE4Njg1Q0RGNkI0RUQ0ODQ0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RTVFRkQ2OTRDOEQyMTFFMTg2ODVDREY2QjRFRDQ4NDQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RTVFRkQ2OTVDOEQyMTFFMTg2ODVDREY2QjRFRDQ4NDQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6O65tlAAAT70lEQVR42sxba4wd5Xl+vrme+9nds+v1ZX3DNiwEAtghBUdWKYIUUxJFVRpSVSVV/1SRGv60qmgqpeo9Ulu1kJZK9EejVGrVKimhCuKSJgRiagdCuBhiwMYxxl476931Xs5lrt/0fb+ZOWfOnLNrm7puzurznJkzl+95L897mc+iUJ+C+kQRhGGhvOF6aIYNb2WWDoXQNBNh0IFhV2BYlY1CN+/RzNI+QE5rRnGC9ktCCBNX5iOjMGiHfnuBJnw8kuGB0HeeCNzld/zmPM3bojlLCE2DVWqAJo+V068hcJYhhKZuYKx5exICgyawt1nV9b9lFGq/FEox7gchHScBhaARDL+U/jI7F/xEF4mYHkuoC1sEcJNp6r9sF0f+3PRq3xOReDRwFh9TgKGtev3qgJWkjC3FsS1/ZBTq93u+1FwvxNSGBqa3b8COrZNY16ihVLBJkIIkSI8R8VaofSipamqb+12w8LXknMHf4o8YKgbH9TE7v4S3js/glTdP4MfHThdCad9dmpy+W7or32vPv/tF6TsHLxFwBKNY/1RxdPPfB1Lf6HoBPrRzE+75hZuxd/fVGK2VYRiampKarCYUAC37nbZ67jibmjomtL7zeMQAL+3D83r+pSP4l/98Ac98/3W4KNxe3Xjjc87iqT+Qfucv+Xn5jxjw4fXXwSo3PqFZlccdLxAb143gs/fuxb6PTKNoG/D8AEI3YJiWGpquxQCU1kRPACI2LKFl9rvaTfa1fivg/VgY6Gq8e522tkgOvXoUf/bIN/H9l95GuWgj8pZ+12vN/3Xz7Bt9PqyTX2bgA4XRLZ/QS41/lTKybr1pJ377138R1+7YCAKPgLjJqk2gUJ+AWRmFUahAt8vQrBL0Qom2Reg0NLMAQUOzeGsrEmRhEuuRsGirm7Q11ZZ+TIaOSOgsIdrSAA8B9sjYb2MDjzJzzQpgan0Dv7L/ViXQV358ApFW+LhACOf8e8/JwO0B1ol9FcUQOZXGd36SxjdDKe3P3HMrPsM3oN98sOa3wx6ZJIAFmoSuJhBKGjwZmfmeHFfb3H7fNVH/sey+HHJO9jcFnr/n2E4na9t3yzRunN6Kbzz1A5iF2u2abtru4vvfSc/TrdoGkrYOszQ6Vdu851tETpXdH9qGT975EbRabRiVBmqbryGt2fHEZO+h2RHltsN+S7XUd0wOXhutct8UpEzZWvbup44nJ+3YMqnc4b9eOIzq2MZ9od96MXCbxwg7mbRdI0IOUJu6+RFhVm8bq5dw3723QcgQZn2SwE7TzTSEJPIBADIHUA4CjWRu8vl95O7XFYgY/ozsPdEvRDZ/SRfIMMTH9lyD1986iSPHz6A8su6W9ty7/yQDz9eZdKzy2O7K+usfCsNQ2//zN2JqcgTSLGN0+43qJinYaJhZraahPDAGMOzcjPlG2clfhMZ7z8r6uQY/CEBBBNMUWR5/5kWEwm5QyDrjLL73ol5o7EBl8tovR2bl5s0ElNnYCyRGt90IjQgppO/RGr4kU00oX4wQMIAw8VUFRmS+Z7Y5f065QOaPJc+I8trNmnleCKQkp91ROcPM7Hm8+NoxlKtjO/3O+X/UyxNXbyg2tj9EmVPx527aBTZprVBHZdM1CGjmwyaQJaWQnuIFDDSCTn5jkmQNIlsyHNqPB4eZdD8OOfQdcehRx3iomI4kRCUj0TYLMciZdiRFbNJDCI9ZPqDwKX0P68breOLZH0FqViPy2wcNiqmfotgxWqsYmKQfV1odjG/aBDcUChmzef9kRHdSURInNo4I7BrXMFoSsAjBRacQYs3dOAwRgA4JdL4VYdmNqdbgOM7OlplLd4tYqBAWFpvnsX3zBD764R149gfvEDGP3W9QkvExdvSxelkh8KQOQYk3ZzG8P3BDER9jiVdtgY9uNbC9of3flQtkClUSIj+rSYBPLUq0vAgmh2z22CQhUfNTyiGBS56jDp9cK6C8/8MUpr5z8AisQm2XITTzGk70K6UCgXQpDI0j1IuUaIQKXAwykWQiPQY7URbYf62JWkHgSn0qBHrXhI5350IstGPQmuwpRAs5I4vUPHWaqB9paHdcbCFfVqlwaNQN3a5OMGCdHM9xPBSrVBpK+lH6sckoc4665sxVUpVA3n2tfVFgo2jtOkiISxMYc8FOAv3GTIBlh0AbGWUIMnMZzznUmVcEAtdFuWSjYBnwPbNkaIZZZnaNaHAlYtBJrh8ps1BAEeex8U0jxcT7ryugXhRrAsxuh4FOj2UBi27FJNYUBs9lB4F++aSv5qolphwrJbZKSlTJnLnC8JU0DJKUT3mtEaf39CPVtY7nwSTDd32WlOwzZ74pM+COcR3T63VVaOdxqKBPg+I5PSxQWx55jcZVkjYAiPd1yvrS39NzhgEvU0o+WdVwYj6M/VlZoeiat84hkrBIz4fvd+cgjJQfPfJZNmnDZeAxYC0JJUz3DklyoS1xz3UlldL7QdStXVlbDMz3fbqHg06nQ77TpljowG276rgkxldNgSgGZpgGbMtGsVhEoVCAbdvKrYQR/2aaJvmdoUYKvl84DFjg6CwJWPa0m7qeTibtEyZJmFzSctRXD9NJ3MVwCaxF8csPpJogg2w6EivEjm1ixgIVN5vqQqVuadDnD4P1yDra7TaWV5axvLiM9lwb55rnMOPOoON3YsAZ8+6aNM3QojKzUq1gcmwSu8YoFyiPwSt5xKoWLCseeRPncGUSKKrq4MueWcflJ3ESCYFxhB6bfRALOgXMXzjJoN/QIdCzywGWWp4CHEWxNDnZqJQ1VCzWplTmnU6cNchgFxcXsTC7gLNnz+Ip/Sl8d+S7OGGeIHuQq5IQ6Q5FragmxN+3Lm7FvYv34tPFT2NychKyEl/LoNncszE6tj7iHj8mM030wihXTqzhUJl00C2rjGzrpNNqUWm6jNH1lKUEYUJUQp3LADXEGpJDtNtsNrEwt4CTMyfxcP1hHGocQkmjQmTkPlT0Sn+PKwP2nH8OT55/kjK2UBX/b9LfG/INPLfwHB6cfZDy4Wk01jUU2Kw/p6WhpHSPictIEp40k9Nowj5xUUgK9Ho+nGiYbrK00iYnd1BvhN0kVUa9grNbFtI/Kq2TsXaZnFyi/uXlZSz9dAlfK30Nh8YPwQgMfGnTl/D5DZ/vgtH5T2QGYo09evZRfOH4F5RwC/THceVg4yAeOf0IHnjzAXihh6mpqS6pZRt6bLZMxKGMtdvnwwTUJw1zlyZKTNnop3uhQk8MSuZ7enHirpi4HzATVXuxjcOdw3h+4nloroZt9jZcZV+Fby98OyYp4scUZBa0LWzcULoBm/RNeN97H2bS8eXjB8YO4I6Td6A6W0W5TNXb6Kgit5TAGLAbsElLmDKbZorYhwlwwIDZV4c38eKqhH00CvsBqwoo7AcsldnEzNxebuMV8QraYRumZ1KW4+PA/AEFlLWbsrNFOa6lWeq4EgT9sbmHTgjh9fo2bO4traXuedPyTYr5mc1ZyMzgJjE6z8EhUA4Raqj3QpIKS7TvuaRhYmnHC5Cq2Mgn78pHw0ENZ006BZzGWQW646DpNyFckm6ooxW28PLcy+iEHSwFS0oQbNoMtGbUMGlPYtQcRckoqWey0DS/PzYzg6/4K3CdOLRl47qU3BTXVXhkDbN++uOwVGSlNOz7q7dpWdrRGiYdh5bUzaNuouEHFGs5fhNgjVJTDkWHncNUdbnqniLpuvE15+jvXfqzNRt1s46yUVag+Lo+BdDMQ2Jaz/C6YNPQFpIr+bS/0uEwmpDqAGA26Zi0ojxLZ5lAJqnmoIajAZNOJ6Imw8mIJ1Q+yw8g+SqTXfW9Cf3NO/NYoD/VnsUgYLoFaS++f5rJpWVhQGp9+XgLlaKJTaPsRlHPrOm8wEt82A9W0bDoEdPFmnQ3nZQxYLhIgry4iHJYdJl6tXpZ+rL/OfRdiNRagDb56VszHvwtRWwk0DLJG3RDqtRybcCJSatMKkda7DIy6jdpJZjExFTmRPdlkxbR5SkZFbAQfc/oPjPhm7gOkDj8fpvA25hqmPEbEXIPNufA9VXa3Jdp5auYVTUcpkQV96nSHLrrWz4n3ZfwZuxiOiL+4HPUVqSRQ6pCRtKc3p6h1LZt4qrJAjSdeIWSjtClXNqLcrl0rq+ylknnSWtAw46AwGXSMAY13B0Z5aj5MmHRwdPznprclnVlpdmYtOQaJs03C+PRd5wfHA6SVmpiKl/mSOFdvg6IAhwkkSMDLvVhpYBkrlHGKGYWXBSp8CiRD3ts0iHW8uHUN3Ma7nYF+wGnEk+1sUad8ME+EfoaCXnAks1ZDdE31xM/7WBKD6ATYXlhNFg89GiRuxpJIzlPWmG/Sfe1cHgO3FMKxSW3bVbVMIEQetKKzHdRMkmSUkLWjegr1/TvN1uYJB8OIy2ebz6XFl2WThz2AmFJJueoZJ1SPY1v7ETAZWpici6glbRupZR2Vbq6SfOCMAdYNb+olm8Rgy4toVatrhGHE7O5FMCczFu2hYbegOZoccJwGT6ceTVqDXVvfkZ3bsMAD1hVqELWEuX4oeylrMPD0jANh8Orpfg1pQ6zYOLq2tUozxA70p/2v1QzBSFUREXdk++darkLWuSyv1AMLNlImxetjpupwYeFJVULDxkybQD0woOqeAwDdtHGtsY27B3dq+pjFaJSn77Ewdd65Ht7R/Zi29g21erhZ2TJKx4yyfuHz3dYt3RopoUhGo5UFx+K0NIGQO9FtK5Kt3K9jP3b96Plt3Bo4RDFxkhVRxd8n5L5BFGgyGrf+D7cvf1ulOol1ejLalgJWoOqlHihDQPOh9FutXNBwOq8cAAwT4w5gNmvbIq+nlYKmBtxnAXdp92HnTM78cO5H2LWnVVhY9W1SZn1LNwQWGevw57xPdizcQ9GG6OoEuGkRX/Wh9k0225E1VIMTMrB0kT11i+8iic2XeSqJb5woRng6Fkfk7tM9cYwm/Nyk427Eor6yfxur9yO3Rt243zzPFzfHeha5pvv/J7aNm2MVcdQqVRQLBeVAEulkrp3VrvqOgpX55shmk7Y7bnlcWhpfrCmhqO4eFAslUPMzbLHX27jth3V2IyyjEoaUL1l1rZdgFt1URujQt+bpNo1VFpeEzD3vCi0mZapAPK9eJv2pfPXGnTNC+84WGkHKNli0HojTSlNG6LhaLB4yGQWGZO2qZJ7/oiDl45buGW7hZYbIRsNeGLpBLkNk7596Mu3V6mK0jcSLLB0ZBvwvaQDIJng1EKIb7zUhqFFQ7SbLMMaRlqRDByhmQqRilUyrj1FJFdZDAb87ZNN/N3n6iiTZLmJJnKazr42GfauadjLtHSbvT4LthsCk4b7V55p4dR8oOaQb1Ykjgk9DCC6vQ5V7Ada4DYXeivWOB/uEOMFqt+rAnpuWDrVnic9/OHXl9AhAiuag9VMVlus8XTLQzXgMiM9np6TfaeUvy+/Q+Lx0FNNfOtHbRSM4XNUQ629aHcJke8nA69tRKH3E9q9QUmFJxqsUF1LoHV7KK3zh0E++6aDB9oSv3dvFddNGVSEp0shhrXcxcVGpUECZ0shfViGoCoowMNPN/HEqw7NQQzwSL6Q1rzzmVSDzw9WdKs8sbVQ33BnXB1xA85DYI4i0OvdbGXY4HUcJ+cCPP1aB/NNiWpBi5sCSWgLVxlBboRrDKrfqaCXOE3++hj56188vkL84aFoia4/Dx/ECUEThfbbvUJTN+Atn31J1KZu3lXftvcwHbSVH1Pg96z1WKneSt/DC6eAJGX246KlKX9ixiyYlyeX5lZUy5WKHPmlHmvZ1C/iJTwlO+X2YRQ6x9T3OIyZWDpx8H4iLXnUb59/3qqM38UJB693NP0FCH8ZUi+vatZZ4ykYUH6z1AIWm9HAksAP3tPqtV6LZiZHWPMi4oCwCcs9Fa/dRLyANXSbc3578Qmjs8AujK9Y1dvv6qo/8lDuvIWl0u6Y3i9y9loiAR0faDXw6h2AIVFy1dYvgSy6J8k1nYx2DbRnjn61PX9sQdftGgJ35VihtuEu3a5sVholiRjhkqoxXX08edsfreEz/98jSpYxUhHjn0HVPRKbh3pfrBE7u4uLxw/cH4X+im5WxhlgJP3OkeLI5t/orp+nC+xwThGAI8Z6bPQziDhel2mgEJzBiPs64iVrMQyNfHdl5tUvukunn+FlzbrgdcoyQNCaPyUM27Or6+7MLkouyDnoUYcqthINK1mMJod3Eq/wiJc98ptCB5XwBOr+W2rBWgqWTdlZeO8/Fn/y379D2o2Yo3TdLEEkf35z9oBeqLXM0ujHu00gutiKllAOz1Ba1iTQNsLIogdxfqt1w8CVG1qypdIzaqEWHsdI8A6K8lx3vjF3GXCXz3594d1nf5VMOmTFQoh8i0dD0Fn8K79dL5qlkT+Oe9NqFZcqt8ryDEpyFh4q8AWPAv/vCFzJD5urETkwCayFFZqZp+YXZV7ZCH6r2Fn4N2f51OdIMn62qWgMsjqd3J7/k8BZLBVHtz0Yr5aPk5L4phHpeBmFaLH7zvXKfpKcPF54qIgqqzAmKWfp9L97y2d+TfDrByEuVA+rNazoLJz6fYrPB8oTV3+Z2Pv62G9T4PHDfmY+DJQURcQ715x9+0+95uzDZnEsGvYGRPlwVrtWdZ1y9tBrc7A+6rfm/znwmnOaUdiiGdaE8gVk1vdeeXSJJpP+tyoKnLPOwolHm2eP/KbfmnuG/Ve3Sup3Ak/FkA+s1rXMmzdxYdM5f/JvQmflHygbu0OzKp+lm03Tz6OaZpQV7+OKqZtfXAcEoE1flqLAOx44y495rfmnvZUz5zTdQlzqrq6J/xFgABYmFSieOlzIAAAAAElFTkSuQmCC',

};

//=======================================================================================================================
//=======================================================================================================================
//=======================================================================================================================

df.attachevent({
	name:"load",
	target:window,
	handler:LED.onLoad
});

//=======================================================================================================================

(function(){

//auto-scroller - allows dragging of DIVs with an overflow-auto-y or overflow-scroll-y for touch devices

	var touchsrcoll
	var	touchsrcoll_click
	var	touchsrcoll_mouseup
	var	touchsrcoll_mouseout

	var touchsrcoll_startx
	var touchsrcoll_starty

	var lastscrollTop
	var lastscrollLeft

	var hasmoved = 0

	df.attachevent({
		target	: window,
		name	: "mousedown",
		handler	: function(evt) {
			if (evt.srcElement.tagName.toLowerCase() == "textarea"){
				return
			}

			if (df.mouse.xdrag!=undefined&&df.mouse.xdrag!="") {return}

			var thisobj = evt.target

			while (thisobj!=document && thisobj.tagName!=undefined && thisobj.tagName.toLowerCase()!="html" && thisobj.tagName.toLowerCase()!="body") {
				var computed
				try{computed = df.getComputedStyle(thisobj,"overflow-y")||df.getComputedStyle(thisobj,"overflowY")}catch(e){computed=""}
				if (computed=="auto"||computed=="scroll") {
					break
				}
				thisobj = thisobj.parentNode
			}

			if (thisobj==document || thisobj.tagName==undefined || thisobj.tagName.toLowerCase()=="html" || thisobj.tagName.toLowerCase()=="body") {return}

			touchsrcoll = thisobj
			touchsrcoll_click = thisobj
			touchsrcoll_mouseup = thisobj
			touchsrcoll_mouseout = thisobj


			touchsrcoll_startx	= evt.clientX
			touchsrcoll_starty	= evt.clientY

			touchsrcoll_scrollTop = touchsrcoll.scrollTop
			touchsrcoll_scrollLeft= touchsrcoll.scrollLeft

			lastscrollTop = touchsrcoll.scrollTop
			lastscrollLeft= touchsrcoll.scrollLeft

			hasmoved = 0

			df.killevent(evt)

		}
	})

	df.attachevent({
		target	: window,
		name	: "mousemove",
		bubble	: true,
		handler	: function(evt) {

			if (evt.which!=1) {
				touchsrcoll_click = undefined
				touchsrcoll = undefined
				touchsrcoll_mouseup=undefined
				return
			}

			if (touchsrcoll==undefined||touchsrcoll_click==undefined||touchsrcoll_mouseup==undefined) {
				touchsrcoll = undefined
				return
			}

			if (lastscrollTop!=touchsrcoll.scrollTop || lastscrollLeft!=touchsrcoll.scrollLeft) {touchsrcoll=undefined;return}
				hasmoved++

			setTimeout(function(){

				if (touchsrcoll==undefined) {return}

				var offset_x = evt.clientX - touchsrcoll_startx
				var offset_y = evt.clientY - touchsrcoll_starty

				touchsrcoll.scrollTop = touchsrcoll_scrollTop - offset_y
				touchsrcoll.scrollLeft= touchsrcoll_scrollLeft- offset_x

				lastscrollTop = touchsrcoll.scrollTop
				lastscrollLeft= touchsrcoll.scrollLeft


			},6)

		}
	})


	df.attachevent({
		target	: window,
		name	: "click",
		bubble	: true,
		handler	: function(evt) {
			if (touchsrcoll_click!=undefined && hasmoved>5) {
				evt.cancelBubble = true
				df.killevent(evt)
				touchsrcoll_click = undefined
				return false
			}

			touchsrcoll_click = undefined

		}
	})

	df.attachevent({
		target	: window,
		name	: "mouseup",
		bubble	: true,
		handler	: function(evt) {
			if (touchsrcoll_mouseup!=undefined && hasmoved>5) {
				evt.cancelBubble = true
				df.killevent(evt)
				touchsrcoll_mouseup = undefined
				return false
			}

			touchsrcoll_mouseup=undefined


		}
	})




})()

//=======================================================================================================================
//=======================================================================================================================
/*! Copyright (C) 1999 Masanao Izumo <iz@onicos.co.jp>
 * Version: 1.0
 * LastModified: Dec 25 1999
 * This library is free.  You can redistribute it and/or modify it.
 */
LED.base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
LED.base64DecodeChars = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,-1,-1,-1,63,52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,-1,-1,-1,-1,-1]
LED.base64encode = function(str) {var out, i, len; var c1, c2, c3; len = str.length; i = 0; out = []; while(i < len) { c1 = str.charCodeAt(i++) & 0xff; if(i == len) { out.push(LED.base64EncodeChars.charAt(c1 >> 2)); out.push(LED.base64EncodeChars.charAt((c1 & 0x3) << 4)); out.push("=="); break; }; c2 = str.charCodeAt(i++); if(i == len){ out.push(LED.base64EncodeChars.charAt(c1 >> 2)); out.push(LED.base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4))); out.push(LED.base64EncodeChars.charAt((c2 & 0xF) << 2)); out.push("="); break; }; c3 = str.charCodeAt(i++); out.push(LED.base64EncodeChars.charAt(c1 >> 2)); out.push(LED.base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4))); out.push(LED.base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >>6))); out.push(LED.base64EncodeChars.charAt(c3 & 0x3F)); }; return out.join(""); }
LED.base64decode = function(str) {var c1, c2, c3, c4; var i, len, out; len = str.length; i = 0; out = []; while(i < len) { do { c1 = LED.base64DecodeChars[str.charCodeAt(i++) & 0xff]; } while(i < len && c1 == -1); if(c1 == -1) break; do { c2 = LED.base64DecodeChars[str.charCodeAt(i++) & 0xff]; } while(i < len && c2 == -1); if(c2 == -1) break; out.push( String.fromCharCode((c1 << 2) | ((c2 & 0x30) >> 4)) ); do { c3 = str.charCodeAt(i++) & 0xff; if(c3 == 61) return out.join(""); c3 = LED.base64DecodeChars[c3]; } while(i < len && c3 == -1); if(c3 == -1) break; out.push( String.fromCharCode(((c2 & 0XF) << 4) | ((c3 & 0x3C) >> 2)) ); do { c4 = str.charCodeAt(i++) & 0xff; if(c4 == 61) return out.join(""); c4 = LED.base64DecodeChars[c4]; } while(i < len && c4 == -1); if(c4 == -1) break; out.push( String.fromCharCode(((c3 & 0x03) << 6) | c4) ); } return out.join("")}


//=======================================================================================================================
//=======================================================================================================================

function xhr(url, callback, error, method) {
	var http = null;
	if (window.XMLHttpRequest) {
		http = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		http = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (http) {
		if (callback) {
			if (typeof(http.onload) != "undefined")
				http.onload = function() {
					callback(http);
					http = null;
				};
			else {
				http.onreadystatechange = function() {
					if (http.readyState == 4) {
						callback(http);
						http = null;
					}
				};
			}
		}
		http.open(method || "GET", url + "?time=" + new Date().getTime(), true);
		http.send(null);
	} else {
		if (error) error();
	}
}

//=======================================================================================================================




//=============================================================================

function autoMap(){
	var size = 173


	var pixels=[]
	for (var i=0;i<size;i++){
		pixels.push({idx:i,on:false})
	}

	var position = 0

	function nextPixel(){

		for (var i=0;i<size;i++){
			pixels[i].on = false
		}

		if (position>=173){
			test.newdata_callback = undefined
			sendAutoFrame(pixels)
			return
		}

		pixels[position].on = true

		sendAutoFrame(pixels)

		position++

	}
	test.newdata_callback = nextPixel

	nextPixel()
}

function sendAutoFrame(pixels){
	var pixeldata = []
	for (var i=0;i<pixels.length;i++){
		var thiscolor = (pixels[i].on)?"ffffff":"000000"

		test.pixel_test[i].style.backgroundColor = "#"+thiscolor
		var this_r = df.getdec(thiscolor.substr(0,2))
		var this_g = df.getdec(thiscolor.substr(2,2))
		var this_b = df.getdec(thiscolor.substr(4,2))

		pixeldata.push(String.fromCharCode(this_b))
		pixeldata.push(String.fromCharCode(this_r))
		pixeldata.push(String.fromCharCode(this_g))
	}

	var dataframe = window.btoa("MCX"+pixeldata.join(""))

	test.opencom.writeAsync(dataframe,true)
}


})();
