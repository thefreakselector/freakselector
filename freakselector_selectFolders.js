(function(){

	LED.selectFolders = selectFolders

	//-------------------------------------------------------------------------------------------------------------
	var drives = [];
	var this_callback;
	var this_plugin;
  var this_with_files;
	var current_path;
  var start_folder;

	var mapped_folders = {};

	function selectFolders(plugin_name, callback, config){
		this_plugin = plugin_name;
		this_callback = callback;
console.log("selectFolders",config)
    config = config || {};
    this_with_files = config.fileselector;
    this_filetypes = config.filetypes || [];
    start_folder = config.startfolder || undefined;

    if (start_folder){
      setFolder(start_folder);
    }

    if (this_with_files){
      mapped_folders = {};
      interface(plugin_name)
    } else {
  		getMappedFolders(plugin_name, function(folders){
  			mapped_folders = folders || {};
  			interface(plugin_name)
  		})
    }

		getDrives(function(drives_arr){

			drives_arr.sort(function(a,b){
				if (a.mountpoint > b.mountpoint){return 1}
				if (a.mountpoint < b.mountpoint){return -1}
				return 0
			})

			drives = drives_arr;

			for (var i=0;i<drives.length;i++){
				df.newdom({
					tagname:"div",
					target: this_drivelist,
					attributes:{
						style: "text-align:left;padding:10px;word-break:break-word;",
						html: drives[i].mountpoint + " " + drives[i].description
					},
					events:[
						{
							name: "click",
							handler:function(evt){
								getPath(encodeURIComponent(evt.srcdata.data.drive.mountpoint/*+"\\"*/), function(path_data){
									edit_map_name = undefined;
									current_path = evt.srcdata.data.drive.mountpoint /*+ "\\"*/
									this_selected_input.value = current_path
									this_add_to_list ? this_add_to_list.innerHTML = "Add Folder \u25bc" : '';
									drawPath(path_data, []);
								})
							},
							data:{
								drive: drives[i]
							}
						}
					]
				})
			}
			console.log("DRIVES",drives)

		})

	}

	//-------------------------------------------------------------------------------------------------------------

	function getMappedFolders(plugin_name, callback){
		df.ajax.send({
			url : "folder_paths.json" ,
			callback:function(data){
				callback(df.json.deserialize(data.text)[plugin_name])
			}
		})
	}

	//-------------------------------------------------------------------------------------------------------------

	function closeInterface(){
		df.removedom(this_close_button);
		df.removedom(this_maplist);
		df.removedom(this_drivelist);
		df.removedom(this_window);
		df.removedom(this_window_blocker);
	}

  function setFolder(this_path){
    var segments = this_path.split('/')

    parent_paths = [];

    for (var i=0;i<segments.length-2;i++){
      parent_paths.push( segments.slice(0,i+2).join("/") )
    }

    current_path = this_path;

    getPath(encodeURIComponent(start_folder), function(new_path){
      drawPath(new_path, parent_paths)
    })

  }
	//-------------------------------------------------------------------------------------------------------------

	function drawPath(path_data, parent_paths){

			this_current_path.innerHTML = current_path;

			this_folderlist.innerHTML = "";


			if (parent_paths.length > 0){
				df.newdom({
					tagname: "div",
					target: this_folderlist,
					attributes:{
						html: "&lt; BACK",
						style: "text-align:left;padding-left:20px;"
					},
					events:[
						{
							name: "click",
							handler: function(evt){

								if (parent_paths.length === 0){
									//back to nothing
									return
								}

								var new_parent = parent_paths.pop();

								current_path = new_parent;

								this_selected_input.value = (new_parent.split("\\").pop()) || new_parent
								this_current_path.innerHTML = new_parent;

								getPath(encodeURIComponent(new_parent), function(new_path){
									drawPath(new_path, parent_paths)
								})

							},
							data: {path: ""}
						}
					]
				})
			}


      for (var i=0;i<path_data.folders.length;i++){
        df.newdom({
          tagname: "li",
          target: this_folderlist,
          attributes:{
            html: path_data.folders[i],
            className: "folderbutton",
            style: "text-align:left;padding-left:20px;width:calc(100% - 55px);padding-right:20px;float:left;overflow-y:hidden;text-overflow:ellipsis;"
          },
          events:[
            {
              name: "click",
              handler: function(evt){

                parent_paths.push(current_path);

                var new_path = parent_paths.slice(-1) + "/" + evt.srcdata.data.path;

                current_path = new_path;

                this_selected_input.value = evt.srcdata.data.path
                this_current_path.innerHTML = new_path;

                getPath(encodeURIComponent(new_path), function(new_path){
                  drawPath(new_path, parent_paths)
                })

                this_folderlist.scrollTop = 0;

              },
              data: {path: path_data.folders[i]}
            }
          ]
        })
      }

    if (this_with_files === true){

      for (var i=0;i<path_data.files.length;i++){
        df.newdom({
          tagname: "li",
          target: this_folderlist,
          attributes:{
            html: path_data.files[i],
            className: "folderbutton",
            style: "background:orange;border:2px solid white;text-align:left;padding-left:20px;width:calc(100% - 55px);padding-right:20px;float:left;overflow-y:hidden;text-overflow:ellipsis;"
          },
          events:[
            {
              name: "click",
              handler: function(evt){

                this_callback({path:current_path,file:evt.srcdata.data.filename});
                closeInterface()

              },
              data: {filename: path_data.files[i]}
            }
          ]
        })
      }

    }

	}
	//-------------------------------------------------------------------------------------------------------------

	function getDrives(callback){
		df.ajax.send({
			url : "getDrives" ,
			callback:function(data) {
				var drives = df.json.deserialize(data.text);
				if (callback){callback(drives)};
			}
		})

	}
	//-------------------------------------------------------------------------------------------------------------
	function getPath(path, callback){
		df.ajax.send({
			url : "getPath/"+path ,
			callback:function(data) {
				var folders_and_files = df.json.deserialize(data.text);

        if (this_filetypes.length>0){
          var files = folders_and_files.files;
          var filtered_files = [];
          for (var i=0;i<files.length;i++){
            for (var ix=0;ix<this_filetypes.length;ix++){
              if (files[i].substr(-this_filetypes[ix].length)===this_filetypes[ix]){
                filtered_files.push(files[i]);
              }
            }
          }
          folders_and_files.files = filtered_files;
        }

				if (callback){callback(folders_and_files)};
			}
		})
	}
	//-------------------------------------------------------------------------------------------------------------
	var this_window;
	var this_drivelist;
	var this_maplist;
	var this_close_button;
	var this_current_path;
	var this_window_blocker;
  var this_add_to_list;

	function interface(plugin_name){

		this_window = df.newdom({
			tagname:"div",
			target: LED.LEDwindow,
			attributes:{
				id: "mapFolders",
				style:"position:absolute;top:50px;left:50px;width:calc(100% - 100px);height:calc(100% - 100px);background-color:white;border-radius:5px;border:1px solid black;z-index:99999999;"
			}
		});

		this_window_blocker = df.newdom({
			tagname: "div",
			target: LED.LEDwindow,
			attributes:{
				style: "position:fixed;top:0px;left:0px;width:100%;height:100%;background-color:rgba(20,20,20,.4);z-index:99999998;"
			}
		})

		this_drivelist = df.newdom({
			tagname: "div",
			target: this_window,
			attributes:{
				className: "actionList buttonlist",
				style: "position: absolute;overflow-y: auto;top: 8px;left: 8px;width: calc(23% - 20px);background-color: rgb(202, 202, 202);height: calc(100% - 95px);border: 1px solid rgb(122, 122, 122);border-radius: 5px;"
			}
		});

		this_folderlist = df.newdom({
			tagname: "div",
			target: this_window,
			attributes:{
				className: "actionList buttonlist",
				style: "position:absolute;overflow-y:auto;top:80px;left:calc(22% + 20px);width:calc(55% - 60px);background-color:#cacaca;height:calc(100% - 167px);border:1px solid #7a7a7a;border-radius:5px;"
			}
		})

    if (this_with_files){
      this_folderlist.style.width = 'calc(80% - 60px)';
      this_folderlist.style.top = '33px';
      this_folderlist.style.height = 'calc(100% - 120px)';
    }


    if (this_with_files !== true){

	   	this_maplist = df.newdom({
   			tagname: "div",
  			target: this_window,
  			attributes:{
  				className: "actionList buttonlist",
  				style: "position:absolute;overflow-y:auto;top:80px;left:calc(81% - 68px);width:calc(25% - 20px);height:calc(100% - 167px);background-color:#73b1e7;border:1px solid #7a7a7a;border-radius:5px;"
  			}
  		});


  		drawMapped()
    }

    this_selected_input = df.newdom({
      tagname: "input",
      target: this_window,
      type: "text",
      attributes:{
        style: "position: absolute;top: 31px;left: calc(22% + 20px);width: calc(55% - 47px);height: 44px;border: 1px solid black;border-radius: 5px;font-size: 37px;padding-left: 9px;padding-right: 9px;"
      }
    });

    if (this_with_files === true){
      this_selected_input.style.display="none";
    };

		this_current_path = df.newdom({
			tagname: "span",
			target: this_window,
			attributes:{
				className: "nooverflow",
				style: "font-style: normal;font-variant: normal;font-weight: normal;font-stretch: normal;font-size: 17px;line-height: normal;font-family: Arial;position: absolute;top: 7px;left: calc(22% + 20px);width: calc(55% - 66px);height: 19px;border: 1px solid black;border-radius: 5px;padding-left: 9px;padding-right: 9px;background-color: rgb(202, 202, 202);text-overflow: ellipsis;overflow-x: hidden;word-break: break-all;"
			}
		})

    if (this_with_files){
      this_current_path.style.width = 'calc(80% - 66px)';
    }

    if (this_with_files !== true){

  		this_add_to_list = df.newdom({
  			tagname: "div",
  			target:this_window,
  			attributes:{
  				html:"Add Folder \u25bc",
  				className: "bigbutton",
  				style: "position: absolute;top: 2px;left: calc(77% - 26px);font-family: arial;width: calc(22.1%);height: 38px;border: 1px solid black;border-radius: 5px;font-size: 28px;text-align: center;padding-top: 11px;cursor: pointer;padding-top: 17px;"
  			},
  			events:[
  				{
  					name: "click",
  					handler: addMapToList
  				}
  			]
  		})

    }

    if (this_with_files !== true){

  		this_save_button = df.newdom({
  			tagname: "div",
  			target: this_window,
  			attributes:{
  				html:"SAVE",
  				className: "bigbutton2",
  				style:"text-align: center;position: absolute;top:calc(100% - 71px);left: calc(56% - 38px);width: 44%;height: 30px;background-color: rgb(8, 202, 58);border-radius: 5px;border: 2px solid black;cursor: pointer;"
  			},
  			events:[
  				{
  					name: "click",
  					handler:function(evt){
  						saveMappings();

              var folders = []
              for (var folder in mapped_folders){
                folders.push(folder)
              }
              if (this_callback) {this_callback(folders)}

              closeInterface();

  					}
  				}
  			]
  		})
    }

		this_close_button = df.newdom({
			tagname: "div",
			target: this_window,
			attributes:{
				html:"CANCEL",
				className: "bigbutton2",
				style:"text-align: center;position: absolute;top:calc(100% - 71px);left: 3px;width: 44%;height: 30px;background-color: rgb(202, 58, 8);border-radius: 5px;border: 2px solid black;cursor: pointer;"
			},
			events:[
				{
					name: "click",
					handler:function(evt){
						closeInterface()
					}
				}
			]
		});

    if (this_with_files){
      this_close_button.style.left = 'calc(50% - 22%)';
    }

	}

	//-------------------------------------------------------------------------------------------------------------

	function saveMappings(){
		df.ajax.send({
			url : "setFolders/" + this_plugin,
			verb:"POST",
			formdata: "mapped_folders="+df.json.serialize(mapped_folders),
			callback:function(data) {
				console.log("save DATA",data)
			}
		})

	}

	//-------------------------------------------------------------------------------------------------------------

	function addMapToList(){
		if (this_selected_input.value === ""){
			alert("Please select a drive, or subfolder.")
			return
		}

		if (edit_map_name && current_path){
			if (this_selected_input.value !== edit_map_name.map){
				delete mapped_folders[edit_map_name.map]
				mapped_folders[this_selected_input.value] = edit_map_name.path
				edit_map_name = {map: this_selected_input.value, path: edit_map_name.path}
				drawMapped()
			}
			return
		}

		mapped_folders[this_selected_input.value] = current_path
		drawMapped()
	}

	//-------------------------------------------------------------------------------------------------------------

	function removeMapping(map){
		confirm("Remove '"+map.map+"'?", function(sure){
			if (sure){
				delete mapped_folders[map.map]
				drawMapped()

				if (edit_map_name && map.map === edit_map_name.map){
					edit_map_name = undefined;
					current_path = undefined;
					this_selected_input.value = "";
					this_current_path.innerHTML = "";
					this_add_to_list ? this_add_to_list.innerHTML = "Add Folder \u25bc" : '';
				}
			}
		})
	}

	//-------------------------------------------------------------------------------------------------------------

	function getMap(callback){
		df.ajax.send({
			url : "getFolders/" + this_plugin,
			callback:function(data) {
				var drives = df.json.deserialize(data.text)||[];
				if (callback){callback(drives)};
			}
		})
	}

	//-------------------------------------------------------------------------------------------------------------

	var edit_map_name;

	function drawMapped(){

		this_maplist.innerHTML = "";


		for (var folder in mapped_folders){

			df.newdom({
				tagname:"div",
				target: this_maplist,
				attributes:{
					html: folder,
					style:"width: 71%;float: left;text-align: left;padding: 10px;word-wrap:break-word;"
				},
				events:[
					{
						name: "click",
						data: {map: folder, path: mapped_folders[folder]},
						handler: function(evt){

							this_add_to_list ? this_add_to_list.innerHTML = "Edit Folder \u25bc" : '';

							edit_map_name = evt.srcdata.data

							this_selected_input.value = evt.srcdata.data.map
							this_current_path.innerHTML = evt.srcdata.data.path

							this_folderlist.innerHTML = "";

						}
					}
				]
			})


			df.newdom({
				tagname: "span",
				target: this_maplist,
				attributes:{
					html:"X",
					className: "bigbutton2",
					style:"position:relative;top:-3px;float:right;text-align: center;width: 16px;height: 33px;background-color: rgb(202, 58, 8);border-radius: 5px;border: 2px solid black;cursor: pointer;"
				},
				events:[
					{
						name: "click",
						handler:function(evt){
							removeMapping(evt.srcdata.data)
						},
						data: {map: folder, path: mapped_folders[folder]},

					}
				]
			})



		}


	}

})();
