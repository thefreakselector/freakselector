
(function(){


dhtmlfx.ColorPicker = function(config){

	var self = {

		init: function(config){

			if (config.beforeInit){
				config.beforeInit(self)
			};

			config.startvalue = config.startvalue!=undefined ? tinycolor(config.startvalue).toHex() : "ff0000";

			config.width = config.width || 256
			config.height= config.height|| 50

			self.config = config;

			self.containerDiv = df.newdom({
				tagname: "div",
				target: config.target,
				attributes:{
					style:"width:"+config.width+"px;height:"+(config.height * 3)+"px;position:relative;background-color:#cecece;border-radius:10px;overflow:hidden;border:2px solid #2a2a2a;"
				}
			})

			var start_hsl = tinycolor(config.startvalue).toHsl()
			self.containerDiv.style.backgroundColor = "#"+config.startvalue

			//--------------------------------------------------------------------------------------------

			self.hueSlider = df.newdom({
				tagname:"input",
				target : self.containerDiv,
				attributes:{
					type:"range",
					step:".01",
					style:"width:"+(config.width||256)+"px;height:"+(config.height)+"px;position:absolute;top:0px;left:0px;",
					className:"df_slider_race hueslider slider-horizontal"
				},
				events:[
					{
						name:"input",
						handler:function(evt){
							self.updateHue(self.hueSlider.value)
							evt.cancelBubble=true;
							evt.stopPropagation();
						}
					},
					{
						name:"change",
						handler:function(evt){
							self.updateHue(self.hueSlider.value)
							evt.cancelBubble=true;
							evt.stopPropagation();
						}
					},
					{
						name:"click",
						handler:function(evt){
							evt.cancelBubble=true;
							evt.stopPropagation();
							//evt.preventDefault();
						}
					},
					{
						name:"mousedown",
						handler:function(evt){
							evt.cancelBubble=true;
							evt.stopPropagation();
							if(config.mousedown){config.mousedown(self.hueSlider)}
							//evt.preventDefault();
						}
					},
					{
						name:"mouseup",
						handler:function(evt){
							evt.cancelBubble=true;
							evt.stopPropagation();
							if(config.mouseup){config.mouseup(self.hueSlider)}
							//evt.preventDefault();
						}
					}
				]
			})


			self.hueSlider.value = (start_hsl.h/360) * 100;

			//--------------------------------------------------------------------------------------------


			self.brightSlider = df.newdom({
				tagname:"input",
				target : self.containerDiv,
				attributes:{
					type:"range",
					step:".01",
					style:"width:"+(config.width||256)+"px;height:"+(config.height)+"px;position:absolute;top:"+(config.height)+"px;left:0px;",
					className:"df_slider_race brightslider slider-horizontal"
				},
				events:[
					{
						name:"input",
						handler:function(evt){
							self.updateBrightness(self.brightSlider.value)
							evt.cancelBubble=true;
							evt.stopPropagation();
						}
					},
					{
						name:"change",
						handler:function(evt){
							self.updateBrightness(self.brightSlider.value)
							evt.cancelBubble=true;
							evt.stopPropagation();
						}
					},
					{
						name:"click",
						handler:function(evt){
							evt.cancelBubble=true;
							evt.stopPropagation();
							//evt.preventDefault();
						}
					},
					{
						name:"mousedown",
						handler:function(evt){
							evt.cancelBubble=true;
							evt.stopPropagation();
							if(config.mousedown){self.mousedown(self.brightSlider)}
							//evt.preventDefault();
						}
					},
					{
						name:"mouseup",
						handler:function(evt){
							evt.cancelBubble=true;
							evt.stopPropagation();
							if(config.mouseup){self.mouseup(self.brightSlider)}
							//evt.preventDefault();
						}
					}
				]
			})

			self.brightSlider.value = (start_hsl.l) * 100;

			self.hue = start_hsl.h
			self.lum = start_hsl.l


			//--------------------------------------------------------------------------------------------

			self.resultHEX = df.newdom({
				tagname: "div",
				target: self.containerDiv,
				attributes:{
					style:"position:absolute;top:"+(config.height * 2)+"px;height:"+(config.height)+"px;left:0px;font-size:30px;font-family:arial;text-align:center;width:100%;padding-top:10px;"
				}
			})

			self.resultHEX.innerHTML = config.startvalue
			self.resultHEX.style.color = tinycolor(config.startvalue).isDark() ? "#ffffff" : "#000000"

			if (config.onStart){
				config.onStart(self)
			}

		},
  //--------------------------------------------------------------------------------------------

		updateHue: function(val){

			self.hue = (val/100)*360;

			var new_color = tinycolor({
				h:self.hue,
				s:self.sat,
				l:self.lum
			}).toHex();

			self.containerDiv.style.backgroundColor = "#"+new_color
			self.resultHEX.innerHTML = new_color

			var new_lum_color = tinycolor({
				h:self.hue,
				s:100,
				l:50
			}).toHex();

			var style_gradient = "-webkit-linear-gradient(left,  #000000 0%,#"+new_lum_color+" 50%,#ffffff 100%)"

			self.brightSlider.style.background = style_gradient

			if (config.callback){
				config.callback(new_color, config)
			}


		},

		//--------------------------------------------------------------------------------------------
		sat: 100,

		updateBrightness: function(val){

			if (val <2){val=0}

			self.lum =  val

		//	self.sat = self.lum < 50 ? 100 : ((Math.max(val,50) -50 )/50) * 100

			var new_color = tinycolor({
				h:self.hue,
				s:self.sat,
				l:self.lum
			});

			self.containerDiv.style.backgroundColor = "#"+new_color.toHex()
			self.resultHEX.innerHTML = new_color.toHex()

			self.resultHEX.style.color = new_color.isDark() ? "#ffffff" : "#000000"

			if (config.callback){
				config.callback(new_color.toHex(), config)
			}


		}

		//--------------------------------------------------------------------------------------------



	}

	self.init(config)

	return self;
}


})();
