/*
custom slider

two ways to instantiate:

inline:
	<df-slider id="slider_green" name="Green" style="float:left;" class="slider" min="0" max="1" startvalue="1" height="35" width="285" onchange="slide_green"></df-slider>



javascript:

	df.newdom({
		tagname		:"df-slider",
		target		: scrollwin,
		attributes:{
			id			: "slider_scrollspeed",
			name		: "Scroll Speed",
			style		: "float:left;",
			class		: "slider",
			min			: 0,
			max			: 1,
			startvalue	: .5,
			height		: 35,
			width		: 285,
			onchange	: function(val){
				self.scroll_speed = (val * 10)-5
			}
		}

	})


*/
df.gadgets=df.gadgets||{}
df.gadgets.slider = {

	init:	function() {

		var sliders = document.getElementsByTagName("df-slider")
		for (var i=0;i<sliders.length;i++) {
			df.gadgets.slider.initSlider(sliders[i])
		}
	},
	
	initSlider:	function(thisslider) {

		var thisw = parseInt(thisslider.getAttribute("width"),10)||200
		var thish = parseInt(thisslider.getAttribute("height"),10)||20
		
		var onchange
		var inline = false
		if (df.objtype(thisslider.getAttribute("onchange")) == "string"){
			onchange = thisslider.getAttribute("onchange")
			inline = true
		} else {
			onchange = thisslider.onchange
		}

		var mousedown
		var mdinline = false
		if (df.objtype(thisslider.getAttribute("onmousedown")) == "string"){
			mousedown = thisslider.getAttribute("onmousedown")
			mdinline = true
		} else {
			mousedown = thisslider.onmousedown
		}

		var mouseup
		var muinline = false
		if (df.objtype(thisslider.getAttribute("onmouseup")) == "string"){
			mouseup = thisslider.getAttribute("onmouseup")
			muinline = true
		} else {
			mouseup = thisslider.onmouseup
		}
		

		df.gadgets.slider.draw({
			id		: thisslider.id,
			width		: thisw,
			height		: thish,
			gadget_size	: Math.min(thisw,thish),
			target		: thisslider,
			onchange	: onchange,
			mousedown	: mousedown,
			mdinline	: mdinline,
			mouseup		: mouseup,
			muinline	: muinline,
			inline		: inline,
			min		: (parseFloat(thisslider.getAttribute("min")))?parseFloat(thisslider.getAttribute("min")):0,
			max		: (parseFloat(thisslider.getAttribute("max")))?parseFloat(thisslider.getAttribute("max")):1,
			startval	: (parseFloat(thisslider.getAttribute("startvalue")))?parseFloat(thisslider.getAttribute("startvalue")):0
		})

	},

	draw:	function(slider) {
			
			var self = this
			
			var xmax = 0
			var ymax = 0
			var slider_width = slider.width
			var slider_height = slider.height
			var gadget_top = 0
			var gadget_left = 0

			if (slider.width>slider.height) {
				xmax = slider.width-slider.gadget_size
				gadget_left = parseInt((xmax * (slider.startval/slider.max)),10)
			} else{
				ymax = slider.height-slider.gadget_size
				gadget_top = parseInt((ymax * (slider.startval/slider.max)),10)
			}
			
			var onchange = ""
			if (slider.inline) {				
				onchange = ",onchange:"+slider.onchange				
			}
			var onmousedown = ""
			if (slider.mdinline){
				onmousedown = ",onmousedown:"+slider.mousedown
			}
			var onmouseup = ""
			if (slider.muinline){
				onmouseup = ",onmouseup:"+slider.mouseup
			}
			

			try{
//				slider.target.innerHTML =     '<div class="df_slider_race" style="width:'+slider_width+'px;height:'+slider_height+'px;position:relative;top:0px;left:0px;"><div mousefx="{floating:true,xmin:0,xmax:'+xmax+',ymin:0,ymax:'+ymax+',min:'+slider.min+',max:'+slider.max+',report:df.gadgets.slider.report,mousedown:df.gadgets.slider.mousedown,mouseup:df.gadgets.slider.mouseup'+onchange+onmousedown+onmouseup+'}" class="df_slider_gadget" style="width:'+slider.gadget_size+'px;height:'+slider.gadget_size+'px;position:absolute;top:'+gadget_top+'px;left:'+gadget_left+'px;"></div></div>'
								
				var newslider = df.newdom({
					tagname	: "div",
					target	: slider.target,
					attributes	: {
						style		: 'width:'+slider_width+'px;height:'+slider_height+'px;position:relative;top:0px;left:0px;',
						className	: "df_slider_race"
					},
					events	: [
						{
							name	: "click",
							handler	: function(evtobj) {
							
//console.log("slider race",evtobj)
							
							}
						},
						
						{
							name	: "mousedown",
							handler	: function(evtobj) {
								if (evtobj.target === gadget) {return}
								//
//console.log(evtobj,gadget.style.left , evtobj.layerX,gadget.offsetWidth)

								if (slider.mousedown) {slider.mousedown()}

								var gadget_left = parseInt(gadget.style.left,10)
								var gadget_top = parseInt(gadget.style.top,10)
								var gadget_width= gadget.offsetWidth
								var gadget_height= gadget.offsetHeight
								var mouse_x = evtobj.layerX
								var mouse_y = evtobj.layerY
								var slider_w = parseInt(newslider.style.width,10)
								var slider_h = parseInt(newslider.style.height,10)

								if (slider_w > slider_h) {
									if (mouse_x < gadget_left) {
										df.gadgets.slider.report(mouse_x,mouse_y,df.json.deserialize(gadget.getAttribute("mousefx")),gadget)
										gadget.style.left = mouse_x+"px"
									}
									if (mouse_x > gadget_left + gadget_width){
										df.gadgets.slider.report(mouse_x-gadget_width,mouse_y-gadget_height,df.json.deserialize(gadget.getAttribute("mousefx")),gadget)								
										gadget.style.left = (mouse_x-gadget_width)+"px"
									}
								} else {
									if (mouse_y < gadget_top) {
										df.gadgets.slider.report(mouse_x,mouse_y,df.json.deserialize(gadget.getAttribute("mousefx")),gadget)
										gadget.style.top = mouse_y+"px"
									}
									if (mouse_y > gadget_top + gadget_height){
										df.gadgets.slider.report(mouse_x-gadget_width,mouse_y-gadget_height,df.json.deserialize(gadget.getAttribute("mousefx")),gadget)								
										gadget.style.top = (mouse_y-gadget_height)+"px"
									}
								}
								
								if (slider.mouseup) {slider.mouseup()}
							}
						}
					]
				})

				var mousefx = '{floating:true,xmin:0,xmax:'+xmax+',ymin:0,ymax:'+ymax+',min:'+slider.min+',max:'+slider.max+',report:df.gadgets.slider.report}'
				
				var gadget = df.newdom({
					tagname	: "div",
					target	: newslider,
					attributes	: {
						mousefx		: mousefx,
						className	: "df_slider_gadget",
						style		: 'width:'+slider.gadget_size+'px;height:'+slider.gadget_size+'px;position:absolute;top:'+gadget_top+'px;left:'+gadget_left+'px;'
					},
					events	: [
						{
							name	: "mousedown",
							handler	: function(evt){
								if (slider.mousedown) {slider.mousedown()}
								df.gadgets.slider.gadget_slider = slider
							}
						},
						{
							name	: "mouseup",
							handler	: function(){
								if (slider.mouseup) {slider.mouseup()}
								df.gadgets.slider.gadget_slider = undefined
							}
						}
					]
				})	
				
				
			} catch(e){

console.error(e)

				try{
					slider.target.outerHTML = '<div class="df_slider_race" style="width:'+slider_width+'px;height:'+slider_height+'px;position:relative;top:0px;left:0px;"><div mousefx="{floating:true,xmin:0,xmax:'+xmax+',ymin:0,ymax:'+ymax+',min:'+slider.min+',max:'+slider.max+',report:df.gadgets.slider.report'+onchange+onmousedown+onmouseup+'}" class="df_slider_gadget" style="width:'+slider.gadget_size+'px;height:'+slider.gadget_size+'px;position:absolute;top:'+gadget_top+'px;left:'+gadget_left+'px;"></div></div>'
				}catch(e){
				
				}
			} 

			//initialize slider
			setTimeout(function(){
				df.gadgets.slider.report(gadget_left,gadget_top, df.json.deserialize(mousefx) , gadget)
			},1)



/*				var sels=[]
				for (var selector in slider.target) {
					sels.push(selector+" = "+slider.target[selector])
				}
				console.log("-------------------")
				console.log(sels.join("\n"))
*/			
//console.log("slider",slider)
//console.log("slider_target",slider.target)
				

	},

	report:	function(x,y,xdrag,dragobj) {
//console.log(x,y,xdrag,dragobj)
		var xdrag 	= xdrag || df.mouse.xdrag
		var dragobj	= dragobj || df.mouse.dragobj

		if (xdrag.xmax > xdrag.ymax) {
			var val = ((x /xdrag.xmax) * xdrag.max) - xdrag.min
		} else {
			var val = ((y / xdrag.ymax) * xdrag.max) - xdrag.min
		}
		
		if (isNaN(val)) {val=0}

		var scrollparent = dragobj
		
		var slider = dragobj.parentNode.parentNode

		var displayval = val.toFixed(2)
		
		do  {
			scrollparent = scrollparent.parentNode
		} while (scrollparent.tagName.toLowerCase()!="df-slider"&&scrollparent.tagName!="body")

		if (scrollparent.onchange!=null) {
			try{var displayval_override = scrollparent.onchange(val,slider)}catch(e){}
		}

		var changeval
		if (xdrag.onchange!=undefined) {
			changeval = xdrag.onchange(val,slider)
		}

		if (slider.getAttribute("showvalue")!=null || changeval!=undefined) {
			var thisshowval = slider.getAttribute("showvalue")
			df.dg(thisshowval).innerHTML = changeval || (displayval_override!=undefined)?displayval_override : displayval
		}

		dragobj.setAttribute("title", (displayval_override!=undefined)?displayval_override : displayval)
	},
	
	mousedown:	function(evtobj) {

	},
	
	mouseup:	function(evtobj) {

		if (df.gadgets.slider.gadget_slider==undefined) {return}

//console.log("mouseup",df.gadgets.slider.gadget_slider)	

		if (df.gadgets.slider.gadget_slider) {
			var slider = df.gadgets.slider.gadget_slider
			if (slider.mouseup) {slider.mouseup()}
		}

	}
}

df.customdoms["df-slider"]={dominit:df.gadgets.slider.initSlider}

df.attachevent({target:window, name:"load", handler:df.gadgets.slider.init })

df.attachevent({target:window, name:"mouseup", handler:df.gadgets.slider.mouseup })

