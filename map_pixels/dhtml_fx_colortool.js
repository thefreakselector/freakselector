/*
Code by Damien Otis 2004-2010
License found at http://www.dhtmldf.net/license.html
API Docs at http://www.dhtmldf.net/
*/

dhtmlfx.colortool = {
	width:90,
	height:90,
	top:100,
	left:100,
	color:"0000ff",
	
	rainbow:["ff0000","ff8000","ffff00","80ff00","00ff00","00ff80","00ffff","0080ff","0000ff","8000ff","ff00ff","ff0080","ff0000"],
	
	colorobjs:{},
	
	keyevent:	false,
	
	init:	function(colorobj) {	
		var df = dhtmlfx
	try{
		if (df.colortool.spectrun==undefined) {df.colortool.spectrum=df.gradient(df.colortool.rainbow,360)}

		if (colorobj.target!=undefined) {
			if (colorobj.id==undefined) {
				colorobj.id = "dhtmlfx_colortool_"+df.testid(colorobj.target)
			}
		} else {
			if (colorobj.id==undefined) {
				colorobj.id = "dhtmlfx_colortool_"+df.unique()
			}
			colorobj.target = df.newdom({
				tagname:"div",
				attributes:{
					id:colorobj.id,
					style:"display:block;top:0px;left:0px;position:absolute;display:none;"
				}
			})
		}
		colorobj.target = df.dg(df.testid(colorobj.target))

		if (df.colortool.colorobjs[colorobj.id]==undefined) {
			df.colortool.colorobjs[colorobj.id]=colorobj
		} else {
			colorobj = df.colortool.colorobjs[colorobj.id]
		}

		if (colorobj.zindex==undefined) {colorobj.zindex=600}
		if (colorobj.connector_zindex==undefined) {colorobj.connector_zindex = colorobj.zindex-10}
		df.mouse.dragz = colorobj.zindex+10
		
		colorobj.width = colorobj.width||df.colortool.width
		colorobj.height = colorobj.height||df.colortool.height

		colorobj.livetarget = colorobj.target||df.dg(colorobj.id)

		colorobj.color = df.hexcolor(colorobj.livetarget.getAttribute("value")||colorobj.livetarget.innerHTML||colorobj.color||df.colortool.color)
		
		colorobj.top = (colorobj.top==undefined)?(df.getbot(colorobj.livetarget)+20):colorobj.top
		colorobj.left = (colorobj.left==undefined)?(df.getleft(colorobj.livetarget)+40):colorobj.left
		colorobj.offsetleft = (colorobj.offsetleft==undefined)?parseInt(colorobj.offsetleft,10):0
		colorobj.offsettop = (colorobj.offsettop==undefined)?parseInt(colorobj.offsettop,10):0

		df.addcss({".rainbow":"{width:25px;height:1px;overflow:hidden;}"})

		var newdom
		
		colorobj.div = newdom = df.newdom({tagname:"div",attributes:{"id":colorobj.id,"mousefx":"{floating:true}","style":"display:block;position:absolute;top:"+(colorobj.top+colorobj.offsettop)+"px;left:"+(colorobj.left+colorobj.offsetleft)+"px;width:"+(colorobj.width+34)+"px;height:"+(colorobj.height+20)+"px;z-index:"+(colorobj.zindex||9999899)+";"}})
//Hue
		var pickerHTML =[]
		pickerHTML.push('<div id="color_'+colorobj.id+'" style="z-index:10;width:30px;height:'+colorobj.height+'px;position:absolute;top:0px;left:0px;" mousefx="{floating:\'nomove\',mousedown:df.colortool.colordown,mousemove:df.colortool.colormove,colorid:\''+colorobj.id+'\'}"'+df.noselectHTML+'>')
		colorobj.huegradient = df.gradient(df.colortool.rainbow,360)
		for (var i=0;i<colorobj.huegradient.length;i++) {
			pickerHTML.push('<div id="rainbow_'+colorobj.id+'_'+i+'" '+((i==0)?' mousefx="{colorid:\''+colorobj.id+'\'}" ':'')+' class="rainbow" style="background-color:#'+colorobj.huegradient[Math.round(i*(360/colorobj.height))]+'"></div>')		
		}		
		pickerHTML.push('</div>')

//Saturation		
	 	pickerHTML.push('<div id="picker_'+colorobj.id+'" style="position:absolute;left:33px;top:px;width:'+colorobj.width+'px;height:'+colorobj.height+'px;z-index:10">')
	 	var dither = colorobj.dither||3
		var sat_gradient = colorobj.width
		for (var x=0;x<sat_gradient;x+=dither) {
			var thisopacity = Math.round((100/sat_gradient)*(sat_gradient-x))
			pickerHTML.push('<div style="position:absolute;z-index:5000;width:'+dither+'px;height:'+colorobj.height+'px;top:0px;left:'+(x)+'px;overflow:hidden;background-color:#ffffff;'+df.getopacitycss(thisopacity)+';"'+df.noselectHTML+'></div>')
		}
//Brightness
		var lum_gradient = colorobj.height
		for (var y=0;y<lum_gradient;y+=dither) {
			var thisopacity = Math.round((100/lum_gradient)*y)
			pickerHTML.push('<div style="position:absolute;z-index:5000;width:'+colorobj.width+'px;height:'+dither+'px;left:0px;top:'+(y)+'px;overflow:hidden;background-color:#000000;'+df.getopacitycss(thisopacity)+';"'+df.noselectHTML+'></div>')
		}		
		pickerHTML.push('</div>')

//controls
		pickerHTML.push('<div id="pickertop_'+colorobj.id+'" style="z-index:20;width:'+colorobj.width+'px;height:'+colorobj.height+'px;position:absolute;top:0px;left:34px;" mousefx="{floating:\'nomove\',mousedown:df.colortool.pickdown,mousemove:df.colortool.pickmove,colorid:\''+colorobj.id+'\'}" '+df.noselectHTML+'></div>')
		pickerHTML.push('<div id="result_'+colorobj.id+'" style="z-index:10;width:28px;height:15px;position:absolute;top:'+(colorobj.height+3)+'px;left:0px;border:1px solid black;"'+df.noselectHTML+'></div>')
		pickerHTML.push('<input id="hexcolor_'+colorobj.id+'" onclick="df.autoselect(this)" type="text" style="z-index:10;position:absolute;top:'+(colorobj.height+3)+'px;left:34px;width:40px;height:11px;font-family:arial;font-size:11px;color:black;background-color:white;border:1px solid black;padding:2px;"/>')
		pickerHTML.push('<div id="marker_'+colorobj.id+'" style="z-index:15;position:absolute;top:0px;left:0px;width:5px;height:5px;overflow:hidden;border:1px solid #cacaca;"'+df.noselectHTML+'><div style="width:3px;height:3px;overflow:hidden;border:1px solid black;"'+df.noselectHTML+'></div></div>')
		pickerHTML.push('<div id="colormarker_'+colorobj.id+'" style="position:absolute;top:0px;left:-12px;z-index:200;font-family:arial;font-size:14px;color:black;"'+df.noselectHTML+'>\u25ba</div>')
		pickerHTML.push('<div onclick="window.df.colortool.done(\''+colorobj.id+'\')" style="cursor:pointer;position:absolute;top:'+(colorobj.height+3)+'px;left:'+(colorobj.width-8)+'px;z-index:30;height:11px;width:40px;background-color:#cacaca;padding-bottom:3px;padding-top:1px;text-align:center;border:1px solid black;font-family:arial;font-size:11px;"'+df.noselectHTML+'>Done</div>')

		newdom.innerHTML = pickerHTML.join("")

		if (df.shell!=undefined) {
			df.shell.init({target:newdom,id:"colortool_bgtop_"+colorobj.id,curve:20,color:"bababa",zindex:1,inside:false})
			df.shell.init({target:newdom,id:"colortool_bgbot_"+colorobj.id,curve:28,color:"000000",zindex:0,opacity:30,inside:false})
		} else {
			newdom.style.backgroundColor="#bababa"
			newdom.style.border="8px solid #bababa"
		}

		if (df.connector!=undefined&&colorobj.noconnector!=true) {
			colorobj.connector = df.connector.init({source:colorobj.div,target:colorobj.target,color:"aaaaaa",size:20,targetsize:Math.min(colorobj.target.offsetHeight,colorobj.target.offsetWidth),zindex:colorobj.connector_zindex})
		}


		colorobj.livetarget.maxLength = 6
		df.dg("hexcolor_"+colorobj.id).maxLength = 6
		colorobj.livetarget.setAttribute("maxLength",6)
		df.dg("hexcolor_"+colorobj.id).setAttribute("maxLength",6)

//setup colors
		colorobj.hsv = df.colortool.rgbtohsv(colorobj.color)

		df.colortool.updateui(colorobj)

//----------------------------
		if (df.keys!=undefined) {
			if (colorobj.livetarget.tagName.toLowerCase()=="input") {

				df.attachevent({
					name	: "keyup",
					target	: colorobj.livetarget,
					handler	: df.colortool.keyup
				})
				df.attachevent({
					name	: "keydown",
					target	: colorobj.livetarget,
					handler	: df.colortool.keydown
				})
				colorobj.livetarget.setAttribute("colorid",colorobj.id)
			}

			df.attachevent({
				name	: "keyup",
				target	: df.dg("hexcolor_"+colorobj.id),
				handler	: df.colortool.keyup
			})
			df.attachevent({
				name	: "keydown",
				target	: df.dg("hexcolor_"+colorobj.id),
				handler	: df.colortool.keydown
			})
			
			
			df.dg("hexcolor_"+colorobj.id).setAttribute("colorid",colorobj.id)
		}
		
		return colorobj
	}catch(e){df.debug.debugwin({title:"colortool.init",color:"red",message:e.description+"<br>"+e.message})}
	},

//---------------------------------------

	keydown:	function(event) {
		var df = dhtmlfx
		var keyfilter = df.keys.keyfilter(event,{accept:['0','1','2','3','4','5','6','7','8','9','\x41','\x42','\x43','\x44','\x45','\x46','\x23','\x24','\x27','\x08','\x25','\x2e','\x09','\x08'],reject:[],action:[]})
		
		if (keyfilter==false) {
			df.mouse.killevent(event)
			df.colortool.keyevent = false
			return false
		}
		df.colortool.keyevent = true
		return true
	},
	
	keyup:	function(event) {
		var df = dhtmlfx
	try{
	
	
		df.colortool.keyevent = true
		var keyfilter = df.keys.keyfilter(event,{accept:['0','1','2','3','4','5','6','7','8','9','\x41','\x42','\x43','\x44','\x45','\x46'],reject:[],action:[]})
		if (keyfilter==false) {
alert('y')		
			df.mouse.killevent(event)
			df.colortool.keyevent = false
			return false
		}
		
		if (df.keys.evtsrc.getAttribute("value").length>6) {
alert('z')		
			df.mouse.killevent(event)
			df.colortool.keyevent = false
			return false		
		}
		
		//df.keys.evtsrc.setAttribute("value") = df.keys.evtsrc.getAttribute("value").substr(0,6)

		var colorid = String(df.keys.evtsrc.getAttribute("colorid"))
		
		if (colorid!="null") {
			var colorobj = df.colortool.colorobjs[colorid]

			colorobj.color = String(df.keys.evtsrc.getAttribute("value")+"000000").substr(0,6)
			colorobj.hsv = df.colortool.rgbtohsv(colorobj.color)

			df.colortool.updateui(colorobj)
	
			df.colortool.keyevent = false
		}

	}catch(e){df.debug.debugwin({title:"df.colortool.keyup",message:e.description+"<br>"+e.message,color:"red"})}	
	},

//---------------------------------------

	updateui:	function(colorobj) {
		var df = dhtmlfx
	try{
		var fixcolor = df.colortool.hsvtorgb(colorobj.hsv)

//		if (df.colortool.keyevent == false) {
			try{df.dg("hexcolor_"+colorobj.id).setAttribute("value",fixcolor.toUpperCase())}catch(e){}
//		}
		try{df.dg("marker_"+colorobj.id).style.backgroundColor = "#"+fixcolor}catch(e){}
		try{df.dg("result_"+colorobj.id).style.backgroundColor = "#"+fixcolor}catch(e){}		

		try{df.dg("marker_"+colorobj.id).style.left = Math.round((colorobj.hsv.sat*(colorobj.width/100))+30)+"px"}catch(e){}
		try{df.dg("marker_"+colorobj.id).style.top = Math.round(colorobj.height-(colorobj.hsv.val*(colorobj.height/100))-4)+"px"}catch(e){}

		df.dg("colormarker_"+colorobj.id).style.top = Math.round(((colorobj.hsv.hue/360)*colorobj.height)-8)+"px"
		df.dg("picker_"+colorobj.id).style.backgroundColor = "#"+df.colortool.spectrum[Math.round(colorobj.hsv.hue)]
		if (df.colortool.keyevent == false) {
			if (colorobj.livetarget!=null) {
				if (colorobj.livetarget.tagName.toLowerCase()=="input"||colorobj.livetarget.getAttribute("value")!=null) {
					try{colorobj.livetarget.setAttribute("value",fixcolor)}catch(e){}
				} else {
					try{colorobj.livetarget.innerHTML = fixcolor}catch(e){}
				}
			}
		}
		if (colorobj.callback!=undefined) {
			try{clearTimeout(df.colortool.updatetmr)}catch(e){}
			df.colortool.updatetmr = setTimeout("window.df.colortool.do_update('"+colorobj.id+"','"+fixcolor+"')",30)
		}

	}catch(e){df.debug.debugwin({title:"df.colortool.updateui",color:"red",message:e.description+"<br>"+e.message})}
	},

//---------------------------------------
//Hue Y
	colormove:	function(x,y,evt,evtsrc,colorid) {
		var df = dhtmlfx
		try{clearTimeout(this.colormove_tmr)}catch(e){}
		this.colormove_tmr = setTimeout(function(){
	try{
		var colorid = df.json.deserialize(df.mouse.dragobj.getAttribute("mousefx")).colorid||colorid
		var colorobj = df.colortool.colorobjs[colorid]
 
		var colory = (y+df.mouse.dragoffy)

		if (colory<0) {colory=0}
		if (colory>colorobj.height) {colory=colorobj.height}
				
		colorobj.hsv.hue = colory*(360/colorobj.height)

		df.colortool.updateui(colorobj)

	}catch(e){df.debug.debugwin({title:"colormove",color:"red",message:e.description+"<br>"+e.message})}
	},65)
	
	},

	colordown:	function(domobj,evtsrc,evt) {
		var df = dhtmlfx
		df.colortool.colormove(0,df.mouse.handleoffy-df.mouse.dragoffy,evt,domobj)
	},

//==================================
//Saturation X, Brightnes Y
	pickmove:	function(x,y,evt,evtsrc,colorid) {
		var df = dhtmlfx
		
		try{clearTimeout(this.colormove_tmr)}catch(e){}
		this.colormove_tmr = setTimeout(function(){
	try{
		var colorid = df.json.deserialize(df.mouse.dragobj.getAttribute("mousefx")).colorid||colorid
		var colorobj = df.colortool.colorobjs[colorid]

		var colorx = (x+df.mouse.dragoffx)-34
		var colory = (y+df.mouse.dragoffy)

		if (colorx<0) {colorx=0}
		if (colorx>colorobj.width) {colorx=colorobj.width}
		if (colory<0) {colory=0}
		if (colory>colorobj.height) {colory=colorobj.height}

		colorobj.hsv.sat = colorx*(100/colorobj.width)
		colorobj.hsv.val = 100-(colory*(100/colorobj.height))

		df.colortool.updateui(colorobj)

	}catch(e){df.debug.debugwin({title:"pickmove",color:"red",message:e.description+"<br>"+e.message})}
	})
	
	},

	pickdown:	function(domobj,evtsrc,evt) {
		var df = dhtmlfx
		df.colortool.pickmove(df.mouse.handleoffx-df.mouse.dragoffx+34,df.mouse.handleoffy-df.mouse.dragoffy,evt,domobj)
	},

//==================================

	rgbtohsv:	function(hexcolor) {
		var df = dhtmlfx
		hexcolor = df.hexcolor(hexcolor)
		var red = df.getdec(hexcolor.substr(0,2))*(100/255)
		var grn = df.getdec(hexcolor.substr(2,2))*(100/255)
		var blu = df.getdec(hexcolor.substr(4,2))*(100/255)
	
		var hsv={hue:0,sat:0,val:0}
		
		hsv.val = Math.max(red,grn,blu)
		
		if (hsv.val==0) {return hsv}
		
		red /= hsv.val
		grn /= hsv.val
		blu /= hsv.val
		
		var rgb_min = Math.min(red,grn,blu)
		var rgb_max = Math.max(red,grn,blu)
		
		hsv.sat = rgb_max - rgb_min
		
		if (hsv.sat == 0) {
			hsv.hue = 0
			return hsv
		}
		
		red = (red - rgb_min) / (rgb_max - rgb_min)
		grn = (grn - rgb_min) / (rgb_max - rgb_min)
		blu = (blu - rgb_min) / (rgb_max - rgb_min)
		rgb_min = Math.min(red,grn,blu)
		rgb_max = Math.max(red,grn,blu)
		
		if (rgb_max == red) {
			hsv.hue = 60 * (grn - blu)
			if (hsv.hue < 0) {
				hsv.hue += 360
			}
		} else if (rgb_max == grn) {
			hsv.hue = 120 + 60 * (blu - red)
		} else {
			hsv.hue = 240 + 60 * (red - grn)
		}
		
		hsv.sat *= 100
		
		hsv.val = hsv.val
		
		return hsv
	},

	hsvtorgb:	function(hsvobject) {
		var df = dhtmlfx
	try{
		var red,grn,blu
		
		if (hsvobject.sat==0) {
			red=grn=blu= df.gethex(Math.round((hsvobject.val*(255/100))))
			return red+grn+blu
		}

		var val = hsvobject.val / 100
		var sat = hsvobject.sat / 100
		var hue = (hsvobject.hue/360)*6
		if (hue==6) {hue=0}

		var x = Math.floor(hue)
		var y = hue-x
		var x1= val * (1 - sat)
		var x2= val * (1 - sat * y)		
		var x3= val * (1 - sat * (1 - y))

		switch(x) {
			case 0:
				red = val
				grn = x3
				blu = x1
				break;
			case 1:
				red = x2
				grn = val
				blu = x1
				break;
			case 2:
				red = x1
				grn = val
				blu = x3
				break
			case 3:
				red = x1
				grn = x2
				blu = val
				break
			case 4:
				red = x3
				grn = x1
				blu = val
				break
			default :
				red = val
				grn = x1
				blu = x2		
		}


		return df.gethex(Math.round(red*255))+df.gethex(Math.round(grn*255))+df.gethex(Math.round(blu*255))

	}catch(e){df.debug.debugwin({title:"df.colortool.hsvtorgb",message:e.description+"<br>"+e.message,color:"red"})}
	},

	updatetmr:0,
	
	do_update:	function(colorid,fixcolor) {
		var df = dhtmlfx
	try{
		try{clearTimeout(df.colortool.updatetmr)}catch(e){}
		var colorobj = df.colortool.colorobjs[colorid]
		colorobj.newcolor = fixcolor
		try{colorobj.callback(colorobj)}catch(e){}
	}catch(e){df.debug.debugwin({title:"df.colortool.do_update",color:"red",message:e.description+"<br>"+e.message})}
	},

	done:	function(colorid) {
		var df = dhtmlfx
		try{df.dg(colorid).style.display="none"}catch(e){}
		var thiscolorobj = df.colortool.colorobjs[colorid]
		try{thiscolorobj.livetarget.blur()}catch(e){}
		if (df.connector!=undefined&&thiscolorobj.connector!=undefined) {
			df.connector.removeconnector(thiscolorobj.connector)
		}

		if (thiscolorobj.done!=undefined) {
			try{thiscolorobj.done(thiscolorobj)}catch(e){}
		}
	}

//==================================
}
