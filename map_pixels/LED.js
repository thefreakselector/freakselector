

LED = {

//==================================================================================

	frame:	[],

	grid_width:0,
	grid_height:0,

//==================================================================================

	start:	function(map){
	
	},
	
//==================================================================================

	setPixel:	function (x,y,r,g,b,a,context) {
		var frameidx = this.getCanvasIndex(x,y)
		var frame = context || this.frame
		frame[frameidx]   = String.fromCharCode(Math.floor(b))
		frame[frameidx+1] = String.fromCharCode(Math.floor(g))
		frame[frameidx+2] = String.fromCharCode(Math.floor(r))
		if (a) {
			frame[frameidx+3] = String.fromCharCode(Math.floor(a))
		}
	},
	setPixelHex:	function (x,y,hex,context) {
		var frameidx = this.getCanvasIndex(x,y) //((y*grid_width)+x)*4
		var r = df.getdec(df.hexcolor(hex).substr(0,2))
		var g = df.getdec(df.hexcolor(hex).substr(2,2))
		var b = df.getdec(df.hexcolor(hex).substr(4,2))
		var frame = context || this.frame
		frame[frameidx]   = String.fromCharCode(Math.floor(b))
		frame[frameidx+1] = String.fromCharCode(Math.floor(g))
		frame[frameidx+2] = String.fromCharCode(Math.floor(r))
	},

	getCanvasIndex:	function(x,y){
		return ((Math.floor(y)*LED.grid_width)+Math.floor(x))*4
	},
	
//==================================================================================

	sliderList:	function(slidertypes) {

		var sliders = []

		for (var i=0;i<slidertypes.length;i++) {

			sliders.push(
				{
					tagname	: "div",
					attributes:{
						style	: "clear:both;position:relative;padding:3px;margin:4px;border:1px solid black;border-radius:5px;background-color:#2a2a4a;font-family:arial;font-size:12px;"
					},
					children:[
						{
							tagname	: "div",
							attributes	: {
								html	: slidertypes[i].name,
								style	: "font-size:20px;margin-bottom:10px;position:relative;top:5px;left:5px;"
							}
						},
						thisvalue = {
							tagname	: "div",
							attributes	: {
								id	: "slidervalue_"+slidertypes[i].id,
								html	: parseFloat(slidertypes[i].startvaluetext||slidertypes[i].startvalue||0).toFixed(2),
								style	: "font-size:20px;position:absolute;top:5px;left:-5px;text-align:right;width:100%;"
							}
						},
						{
							tagname	: "df-slider",
							attributes	: {
								id		: "slider_"+slidertypes[i].id,
								name		: slidertypes[i].name,
								className	: slidertypes[i].classname||"slider",
								min		: (slidertypes[i].min!=undefined)?slidertypes[i].min:0,
								max		: (slidertypes[i].max!=undefined)?slidertypes[i].max:1,
								startvalue	: slidertypes[i].startvalue||0,
								height		: (slidertypes[i].height!=undefined)?slidertypes[i].height:35,
								width		: (slidertypes[i].width!=undefined)?slidertypes[i].width:285,
								onchange	: slidertypes[i].handler,
								onmousedown	: slidertypes[i].mousedown,
								onmouseup	: slidertypes[i].mouseup,
								style		: slidertypes[i].style||"",
								showvalue	: thisvalue.attributes.id

							}
						}
					]
				}

			)
		}

		return sliders
	}

//==================================================================================
}