/*
Code by Damien Otis 2004-2011
License found at http://www.dhtmldf.net/license.html
API Docs at http://www.dhtmldf.net/
*/

/*
new coding conventions

function names	: camelCase
variables	: separated_by_underscores
constants	: _starts_with_underscore



TODO:
make back-end version of dhtmlfx
using df.newdom and df.amin and others to interact with dom on the front-end, using ajax/comet
use jscript.net and node.js implementations to allow dhtmlfx to do comet to the front-end to pass json ojbects to dhtmlfx functions
runat:df.server["servername"]
runat:df.client


df.server = {
	mode	: "server",
	window	: {},
	document: {}
}


df.client = {
	mode	: "cleint",
	window	: window,
	document: document
}


*/



//------------------------------------------------------------------------------------------------------------
//Windows Script Host - allows execution by cscript.exe or wscript.exe
//DO NOT DELETE
//This is for use when DFX is run from WSH (for whatever reason)
/*
try{if (this.window==undefined) {this.window={}}}catch(e){}
try{this.document = document||{}}catch(e){}
if (window.document==undefined) {
	window.document = {
		body	: {
			offsetWidth	: 0,
			offsetHeight	: 0
		},
		documentElement	: {
			clientWidth	: 0,
			clientHeight	: 0
		},
		getElementsById	: function(){return {}},
		getElementsByTagName: function(){return []}
	};
	this.document = window.document
	this.navigator = "WSH"
	this.location = "WSH://WSH"
	this.setTimeout = function(func,delay) {
		func()
	}
	window.isWSH = true
}
*/

//==============================================================================================================
window.dhtmlfx = {
	nav		: String(navigator.userAgent).toLowerCase(),
	platform	: navigator.platform,
	noanim		: false,	//disables animation, only calls last report if present, and callback, immediately
	browser		: "",
	bver		: "",
	capability	: 1,

	period		: 33,
	fxtmr		: 0,

	fxarray		: {},

	fxmarray	: {},
	preloadimages	: {},
	fxinproc	: 0,
	objidx		: 0,
	fxcommand	: 0,
	uniquetc	: 0,//unique timecode
	uniqueid	: 0,//unique id

	blankimg	: "img/spacer.gif", // for PNG functions - must be a blank, transparent gif (100% see-through, any size)

	//constants
	rads		: 0.0174532925,	//19943295
	golden		: 1.6180339887,
	ms_in_day	: 86400000,
	current		: "&",

	linear		: 0,
	bounce		: 1,
	slidein		: 2,
	slideout	: 3,
	smoothmove	: 4,
	lookup		: 5,
	loop		: 100,	//legacy

	colortype	: 1,
	pixeltype	: 2,
	opacitytype	: 3,

	desktop		: 1,
	mobile		: 2,

	touch		: false,

//---------------------------------------
//main function to start or end effects on an element ID, CSS style sheet item or DOM object reference

// df.anim({"target":"test1Div","steps":20,"slope":df.slidein,"callback":ranker.testfunc,"styles":[{"style":"backgroundColor","start":"ffffff","end":"000000","report":ranker.reporttest},{"style":"top","start":10,"end":100"}]})

/*
 df.anim( {
 		"target"	: "test1Div",		//string (DOM ID), DOM Object
 		"steps"		: 20,			//0 to n (typical 3 to 40), default is 0 and will set properties to end value
 		"slope"		: df.slidein,		//df.linear,df.bounce,df.slidein,df.slideout
 		"callback"	: testobj.testfunc,     //or function(animobj){  }, or [testobj.reportfunc1,testobj.reportfunc2],
 		"report"	: testobj.reportfunc,   //or function(animobj){  }, or [testobj.reportfunc1,testobj.reportfunc2],
 		"styles"	: {
 			"backgroundColor":{
 				"min"	: "ffffff",
 				"max"	: "000000",
 				"start":df.current+"ffea11"  // appending "ff0000" will add "ff0000" to the current color. The highest color value that can result from this is "ffffff" (white).
 			},
 			"top":{
 				"min"	: 10,
 				"max"	: 100,
 				"start":df.current+"-50"	// df.current can be adjusted by appending a numeric value inside a string. df.current = "&" so this would result in "&-50", startval:"&-50" could also be used.
 			}
 		}
 	} );
*/

// df.animx({"target":"#test1Div","steps":20,"slope":df.slidein,"callback":ranker.testfunc,"styles":{"backgroundColor":{"start":"ffffff","end":"000000","report":ranker.reporttest},{"style":"top","start":10,"end":100"}}})
// df.anim('test1Div',20,df.slidein,ranker.testfunc,'backgroundColor,000000,ffffff,ranker.reporttest')

//old anim function, for legacy code. (can be removed when dhtmlfx plugins and ranker are converted)
	anim	:	function() {

		if (arguments.length==1) {return dhtmlfx.animx(arguments[0])}

		var testerr=false

		var loop = false

		var steps	= parseInt(arguments[1],10);
		var features	= parseInt(arguments[2],10);
		if(arguments[2]==true){features=df.slidein}
		if(arguments[2]==false){features=df.slidein}
		if (features>100) {
			loop = true
			features=features-100
		}
		if (features==100) {
			loop = true
			features = df.linear
		}
		if(arguments[2]==df.loop) {
			features=df.linear
			loop=true
		}

		var notifyfunc	= arguments[3];
		var stepidx	= steps;

		var animobj	= {"target":arguments[0],steps:steps,callback:notifyfunc,styles:{},loop:loop}

		for(var argi=4;argi<arguments.length;argi++) {
			var thisstyle = arguments[argi]
			var styleobj = {}

			var sargs	= thisstyle.split(",")
			var cssselector = sargs[0]
			styleobj.min	= sargs[1]
			styleobj.max	= sargs[2]
			styleobj.report	= String(sargs[3])
			if (styleobj.report=="undefined") {delete styleobj.report}
			styleobj.slope	= features

			animobj.styles[cssselector] = styleobj
		}

//df.debug.debugwin({title:"animobj.target="+animobj.target,message:df.json.serialize(animobj)})



		return df.animx(animobj)
	},

//animx will be renamed to anim once the legacy anim function can be removed
//animation/tween function.
//
//df.animx({"target":"#test1Div","steps":20,"callback":ranker.testfunc,"styles":{"backgroundColor":{"start":"ffffff","end":"000000","report":ranker.reporttest}}})
//target is optional. if omitted then a uniqueid is assigned as the fxId in fxarray. for it to have any use it must be used in conjunction with report or callback
//steps is the number of 'frames' between min and max.
//callback is a functuon reference that gets called at the end of the animation cycle (stepidx==steps). only one argument is passed, the entire animobject for this tween
//styles is an object that contains the styles to be tweened. the hashes in the object should be CSS "camelCase" names if the target is a DOM object's CSS value
//  styles can be colors as well, tweens will work between colors
//  the hashes could also be non-css names, but this should only be done in the case where the target is not a DOM object.
//  opacity is always named 'opacity' and ranges from 0 to 100.
//  min and max values can be set to df.current to take the current CSS value of the style being tweened
//  It is also possible to specify an offset from the current value using a number in string format. df.current+"10" or df.current+"-10" are valid.
//  slope specifies the tween style. valid slope values are df.linear, df.slidein, df.slideout, df.smoothmove, df.bounce. slope can be an array or a number (refernce to a number as in df.linear). if the style is looped, the slopes will be rotated in a circular way
//  report is a function reference that is similar to callback, except it happens on every step of the tween. animobject is passed to the function so the report function can act on the tween data for the style
//  loop : true will loop the animation until the animobject.loop is set to false or the animobject is deleted from fxarray
//
//  df.fxarray holds all current tweens. tweens can be removed by deleting the animobject by its fxId   ex: delete df.fxarray["someDivID"]
//
//  slope can be a function, which gets passed animobj,stylename - this allows for custom slope algorithms, or lookup tables
//


//min and max are start and end. [might need to convert those or overload.]
//

	animx : function(animobj) {
	var df = dhtmlfx
	//figures out the target of the tween. can be DOM object, css selector (in style sheet), or if no target then an artificial one is created (no target)
	try{

		//safari can't handle nullanim??  if this line is removed
		//then an undefined target will cause safari to freeze (high cpu/high memory)
		if (df.browser=="safari"&&animobj.target==undefined) {return}

		if (df.objtype(animobj.target)=="array") {
			for (var i=0;i<animobj.target.length;i++) {
				var arrayanim = df.copyobject(animobj)
				arrayanim.target = animobj.target[i]
				df.animx(arrayanim)
			}
			return
		}

		if (animobj.target==undefined) {
			animobj.targetobj = undefined
			animobj.target="nullanim_"+df.unique()
		} else {
			if (String(animobj.target).indexOf("nullanim")==-1) {
				if (animobj.target.tagName!=undefined) {
					animobj.targetobj = animobj.target
					if (animobj.targetobj.id=="") {
						animobj.targetobj.id = "df_id"+df.unique()
					}
					animobj.target = animobj.targetobj.id
				} else {
					if (animobj.target.style!=undefined) {
						animobj.targetobj = animobj.target
						animobj.target = animobj.target.selectorText
					} else {
						if (animobj.target.constructor==Object) {
							animobj.targetobj = animobj.target
							if (animobj.targetobj.id!=undefined) {
								animobj.target = animobj.targetobj.id
							} else {
								animobj.targetobj.id = "df_id"+df.unique()
								animobj.target = animobj.targetobj.id
							}
						} else {
							if (String(animobj.target).substr(0,1)!="#"&&String(animobj.target).substr(0,1)!=".") {
								if (animobj.target.constructor==Number) {
									animobj.targetobj = animobj.target
								} else {
									var thisobj=df.dg(animobj.target)
									if (thisobj==null) {return}
									animobj.targetobj = thisobj
								}
							} else {
								animobj.targetobj = df.getstyle(animobj.target)
							}
						}
					}
				}
			} else {
				animobj.targetobj = undefined
			}
			if (animobj.targetobj==undefined||animobj.targetobj==null) {return}
		}


		if (animobj.remove==true||animobj.steps==0) {
			try{delete df.fxarray[animobj.target]}catch(e){};
			return
		};

		if (animobj.styles==undefined) {animobj.styles={}}

	//	animobj.steps = parseInt(animobj.steps,10);

//-----------

		for(var stylename in animobj.styles) {
			var thisanimstyle = animobj.styles[stylename]


//			var thisanimstyle = animobj.styles[stylename]
//			thisanimstyle.steps = parseInt( ( (animobj.styles[stylename].steps==undefined)?animobj.steps||0:animobj.styles[stylename].steps||0),10)
//			thisanimstyle.step = (df.noanim)?(animobj.styles[stylename].steps||animobj.steps):0
//			thisanimstyle = df.calcanim(animobj,stylename,animobj.styles[stylename])
//			if (animobj.loop!=undefined) {thisanimstyle.loop = animobj.loop}


			thisanimstyle.step = (df.noanim)?(thisanimstyle.steps||animobj.steps):0
			if (thisanimstyle.lookup==undefined && animobj.targetobj!=undefined) {

				thisanimstyle.steps = parseInt( ( (thisanimstyle.steps==undefined)?animobj.steps||0:thisanimstyle.steps||0),10)
				var csstext = animobj.targetobj.style.cssText
				var testarr = csstext.split(";")
				for (var i=0;i<testarr.length;i++) {
					var csstest = testarr[i]
					if (csstest=="") {continue}
					var teststyle = df.trim(csstest.split(":")[0])
					var testvalue = df.trim(csstest.split(":")[1])
					if (df.trim(teststyle) != stylename){continue}
					if (testvalue.indexOf("important")!=-1){
						thisanimstyle.important = true	//probably need to extend this into fxproc, and set all styles at once to make this conform to !important
					}
					//testarr[i] = teststyle+":"+df.trim(testvalue)//.replace(/important/g,"").replace(/\!/g,"")
					break
				}

				//animobj.targetobj.style.cssText = testarr.join(";")

				thisanimstyle = df.calcanim(animobj,stylename,thisanimstyle)

			} else if (animobj.targetobj!=undefined) {
				thisanimstyle.steps	= thisanimstyle.lookup.length
				thisanimstyle.slope	= df.lookup
				thisanimstyle.min	= thisanimstyle.min||0
				thisanimstyle.max	= thisanimstyle.max||thisanimstyle.lookup.length
			} else {
				thisanimstyle = df.calcanim(animobj,stylename,thisanimstyle)
			}

			if (animobj.loop!=undefined) {thisanimstyle.loop = animobj.loop}


		}
		try{delete animobj.loop}catch(e){}

		if (df.fxarray[animobj.target]!=undefined){
			for(var stylename in animobj.styles) {
				if (animobj.styles[stylename].remove==true||animobj.styles[stylename].steps==0) {
					delete df.fxarray[animobj.target].styles[stylename];
					continue
				}
				df.fxarray[animobj.target].styles[stylename] = animobj.styles[stylename]
			}
		} else {
			df.fxarray[animobj.target] = animobj
		}


//		animobj.step=(df.noanim)?(animobj.steps):0


		if (df.fxtmr==0) {
			var nofx=true
			for (var selector in df.fxarray){nofx=false;break}

			if (nofx==false) {
				try{window.chronos.clearTimeout(df.fxtmr)}catch(e){}
				df.fxtmr = window.chronos.setTimeout(function(){df.fxproc()},10);
			} else {
				window.chronos.clearTimeout(df.fxtmr)
				df.fxtmr = 0
			}
		}

		return df.fxarray[animobj.target]

	}catch(e){df.debug.debugwin({title:"df.anim",color:"red",message:e.description+"<br>"+e.message})}
	},

	animadd:	function(animobj,newstyle) {

	},
//--------------------------------------------------------------------
//calculate animation vectors

	calcanim:	function(animobj,stylename,thisstyle) {
	//calculates the tween vectors based on min, max, slope and steps
	//this is not meant to be called by user scripts
try{
	var df = dhtmlfx
//			if (stylename==undefined||thisstyle.max==undefined) {return}

			if (thisstyle.steps==undefined&&animobj.steps!=undefined){thisstyle.steps=animobj.steps}
			thisstyle.steps = (thisstyle.steps==undefined)?1:thisstyle.steps

			if(String(stylename).toLowerCase().indexOf("color")!=-1){
				thisstyle.type=df.colortype
			}else{
				if(String(stylename).toLowerCase().indexOf("opacity")!=-1){
					thisstyle.type=df.opacitytype
				} else {
					thisstyle.type=df.pixeltype
				}
			}

			if (String(thisstyle.min).indexOf("%")!=-1) {
				thisstyle.ispercent=true
				thisstyle.min = parseInt(thisstyle.min,10)
			}
			if (String(thisstyle.max).indexOf("%")!=-1) {
				thisstyle.ispercent=true
				thisstyle.max = parseInt(thisstyle.max,10)
			}

			if(thisstyle.slope==undefined){
				var thisslope = thisstyle.slope = df.linear
			} else if (thisstyle.slope.constructor == Function) {
				return thisstyle
			} else {
				var thisslope = parseInt((thisstyle.slope[0]||thisstyle.slope),10)
			}

			if (thisslope==df.bounce) {
				thisstyle.radstep = (180*df.rads)/thisstyle.steps
				thisstyle.stepoffset = 0
			}
			if (thisslope==df.smoothmove) {
				thisstyle.radstep = (180*df.rads)/thisstyle.steps
				thisstyle.stepoffset = (df.rads*270)
			}
			if (thisslope==df.slidein) {
				thisstyle.radstep = (90*df.rads)/thisstyle.steps
				thisstyle.stepoffset = (df.rads*0)
			}
			if (thisslope==df.slideout) {
				thisstyle.radstep = (90*df.rads)/thisstyle.steps
				thisstyle.stepoffset = -(df.rads*90)
			}

			if(thisstyle.type==df.colortype) {

				if (String(thisstyle.min).substr(0,1)==df.current) {
					var currentval = df.hexcolor(df.trim(df.getComputedStyle(animobj.targetobj,stylename).replace(/important/g,"").replace(/!/g,"")))
					if (thisstyle.min.length>6) {
						var addval = df.hexcolor(String(thisstyle.min).substr(1,6))
						currentval = df.newcolor(currentval,addval)
					}
					thisstyle.minval = currentval
				} else {
					thisstyle.minval = df.hexcolor(String(thisstyle.min))
				}

				if (String(thisstyle.maxval).substr(0,1)==df.current) {
					var currentval = df.hexcolor(df.trim(df.getComputedStyle(animobj.targetobj,stylename).replace(/important/g,"").replace(/!/g,"")))
					if (thisstyle.maxval.length>6) {
						var addval = df.hexcolor(String(thisstyle.maxval).substr(1,6))
						currentval = df.newcolor(currentval,addval)
					}
					thisstyle.maxval = currentval
				} else {
					thisstyle.maxval = df.hexcolor(String(thisstyle.max))
				}

				if (thisstyle.start==df.current) {
					if (thisstyle.follow==true) {
						thisstyle.startval = df.hexcolor(thisstyle.minval)||'000000'
						delete thisstyle.follow
					} else {
						if (animobj.targetobj==undefined) {
							thisstyle.startval = df.hexcolor(thisstyle.minval)||'000000'
						} else {
							thisstyle.startval = df.hexcolor(df.trim(df.getComputedStyle(animobj.targetobj,stylename).replace(/important/g,"").replace(/!/g,"")))
						}
					}
				} else {
					thisstyle.startval = df.hexcolor(thisstyle.start||thisstyle.minval)||'000000'
				}

				delete thisstyle.start

				//thisstyle.maxval = df.hexcolor(thisstyle.maxval)

				thisstyle.startr=parseInt(thisstyle.startval.substr(0,2),16)
				thisstyle.startg=parseInt(thisstyle.startval.substr(2,2),16)
				thisstyle.startb=parseInt(thisstyle.startval.substr(4,2),16)
				if (thisslope>0) {
					thisstyle.slopesizer=(parseInt(thisstyle.maxval.substr(0,2),16)-thisstyle.startr)
					thisstyle.slopesizeg=(parseInt(thisstyle.maxval.substr(2,2),16)-thisstyle.startg)
					thisstyle.slopesizeb=(parseInt(thisstyle.maxval.substr(4,2),16)-thisstyle.startb)
					return thisstyle
				} else {
					thisstyle.stepr=((parseInt(thisstyle.maxval.substr(0,2),16)-thisstyle.startr)/thisstyle.steps)
					thisstyle.stepg=((parseInt(thisstyle.maxval.substr(2,2),16)-thisstyle.startg)/thisstyle.steps)
					thisstyle.stepb=((parseInt(thisstyle.maxval.substr(4,2),16)-thisstyle.startb)/thisstyle.steps)
					return thisstyle
				}
			} else {
//this section needs refactoring
				if (animobj.targetobj!=undefined) {
					if (thisstyle.type==df.opacitytype) {
						if (df.browser=="ie") {
							try{
								currentval = parseInt(df.trim(df.getComputedStyle(animobj.targetobj,"filter").replace(/important/g,"").replace(/!/g,"")).split("opacity=")[1].split(")")[0]  ,10)// ) do not match because of split  //  parseInt(String(animobj.targetobj.style.filter).split("opacity=")[1].split(")")[0],10)
							}catch(e){
								currentval = 100
							}

						} else {
							//var opacitystyle = animobj.targetobj.style.opacity
							var opacitystyle = parseFloat(df.trim(df.getComputedStyle(animobj.targetobj,"opacity").replace(/important/g,"").replace(/!/g,"")))
							if (opacitystyle==undefined||opacitystyle==null) {opacitystyle=1}
							currentval = Math.round(opacitystyle*100)
						}
					} else {
						var currentval = df.trim(df.getComputedStyle(animobj.targetobj,stylename).replace(/important/g,"").replace(/!/g,""))  //df.computedstyle(animobj.targetobj,stylename) //
					}
				} else {currentval=0}

				if (String(thisstyle.min).substr(0,1)==df.current) {
					if (thisstyle.type==df.opacitytype) {
						if (df.browser=="ie") {
							try{
								currentval = parseInt(df.trim(df.getComputedStyle(animobj.targetobj,"filter").replace(/important/g,"").replace(/!/g,"")).split("opacity=")[1].split(")")[0]  ,10)// ) do not match because of split  //  parseInt(String(animobj.targetobj.style.filter).split("opacity=")[1].split(")")[0],10)
							}catch(e){
								currentval = 100
							}

						} else {
							var opacitystyle = parseFloat(df.trim(df.getComputedStyle(animobj.targetobj,"opacity").replace(/important/g,"").replace(/!/g,""))) //animobj.targetobj.style.opacity
							if (opacitystyle==undefined||opacitystyle==null) {opacitystyle=1}
							currentval = Math.round(opacitystyle*100)
						}
					} else {
						if (currentval.indexOf("%")!=-1) {thisstyle.ispercent=true;currentval = parseInt(currentval,10)}
						if (animobj.isfloat) {
							currentval = df.trim(df.getComputedStyle(animobj.targetobj,stylename).replace(/important/g,"").replace(/!/g,""))
						} else {
							currentval = parseInt(df.trim(df.getComputedStyle(animobj.targetobj,stylename).replace(/important/g,"").replace(/!/g,"")),10)
						}
					}

					//alert("min="+currentval)
					var addval = 0
					if (thisstyle.min.length>1) {
						addval = parseInt(String(thisstyle.min).substr(1),10)
					}

					thisstyle.minval = currentval+addval
				} else {
					thisstyle.minval = parseInt(thisstyle.min,10)
				}

				if (String(thisstyle.max).substr(0,1)==df.current) {
					if (currentval.indexOf("%")!=-1) {thisstyle.ispercent=true;currentval = parseInt(currentval,10)}
					if (animobj.isfloat) {
						currentval = parseFloat(df.trim(df.getComputedStyle(animobj.targetobj,stylename).replace(/important/g,"").replace(/!/g,""))) //animobj.targetobj.style[stylename]
					}else{
						currentval = parseInt(df.trim(df.getComputedStyle(animobj.targetobj,stylename).replace(/important/g,"").replace(/!/g,"")),10)
					}

					var addval = 0
					if (thisstyle.max.length>1) {
						addval = parseInt(String(thisstyle.max).substr(1),10)
					}
//	alert("currentval="+currentval+"\naddval="+addval+"\nmaxval="+(currentval+addval))
					thisstyle.maxval = (currentval+addval)
				} else {
					thisstyle.maxval = parseInt(thisstyle.max,10)
				}

				if (thisstyle.type==df.opacitytype) {
					if (thisstyle.min>0&&thisstyle.min<1) {thisstyle.min*=10}//some developers might set this from 0.0 to 1.0
					if (thisstyle.max>0&&thisstyle.max<1) {thisstyle.max*=10}
					if (thisstyle.min<0) {thisstyle.min=0}
					if (thisstyle.max>100) {thisstyle.max=100}
				}

				if (thisstyle.start==df.current) {
					if (thisstyle.follow==true) {
						thisstyle.startval = parseInt(thisstyle.minval,10)||0
						delete thisstyle.follow
					} else {
						if (animobj.targetobj==undefined) {
							thisstyle.startval = parseInt(thisstyle.minval,10)||0
						} else {
							thisstyle.startval = parseInt(df.trim(df.getComputedStyle(animobj.targetobj,stylename).replace(/important/g,"").replace(/!/g,"")),10)
						}
					}
				} else {
					if (thisstyle.start==undefined) {
						thisstyle.startval = parseInt(thisstyle.minval,10)||0
					} else {
						thisstyle.startval = parseInt(thisstyle.start,10)||0
					}
				}

				delete thisstyle.start

				thisstyle.maxval = parseInt(thisstyle.maxval,10)

				if(thisslope>0){
					thisstyle.slopesize = thisstyle.maxval-thisstyle.startval
//console.log("thisstyle.slopesize",thisstyle.slopesize,thisstyle.maxval,thisstyle.startval)
				} else {
					thisstyle.slopesize = ((thisstyle.maxval-thisstyle.startval)/thisstyle.steps)
					//thisstyle.step = Math.abs(Math.round(((thisstyle.maxval-thisstyle.startval)/thisstyle.steps)))
				}
			}

			return thisstyle
}catch(e){df.debug.debugwin({title:"calcanim",color:"red",message:e.description+"<br/>"+e.message})}
	},

//--------------------------------------------------------------------
//*** main process loop, do not call this directly for any reason.***

	fxproc : function() {
	try{
		var df = dhtmlfx


		for (var selector in df.fxarray) {
			var thisdf = df.fxarray[selector]

			for (var stylename in thisdf.styles) {
	try{
				var thisdfstyle = thisdf.styles[stylename]
				var thiscx = 1

//				if (thisdfstyle.slope.constructor==Function) {
//					thisdfstyle.slope(animobj,stylename)
//				}
				var thisslope = parseInt(thisdfstyle.slope[0]||thisdfstyle.slope||-1,10)

				if (thisdfstyle.radstep!=undefined) {

					var radstep = Math.sin((thisdfstyle.radstep*thisdfstyle.step)+thisdfstyle.stepoffset)

					if(thisslope==df.bounce||thisslope==df.slidein) {
						var thiscx = ((radstep+0)/1)
					}
					if(thisslope==df.smoothmove) {
						var thiscx = ((radstep+1)/2)
					}
					if(thisslope==df.slideout){
						var thiscx = ((radstep+1)/1)
					}
				}

				if (thisdfstyle.type===df.colortype) {

					if (thisdfstyle.slope.constructor==Function) {

						thisdfstyle.currentval = thisdfstyle.slope(thisdfstyle)

					} else {

						if (thisdfstyle.radstep==undefined) {
							var thisr = df.gethex(Math.round(thisdfstyle.startr+(thisdfstyle.stepr*thisdfstyle.step)))
							var thisg = df.gethex(Math.round(thisdfstyle.startg+(thisdfstyle.stepg*thisdfstyle.step)))
							var thisb = df.gethex(Math.round(thisdfstyle.startb+(thisdfstyle.stepb*thisdfstyle.step)))
						} else {
							var thisr = df.gethex(Math.round(thiscx*thisdfstyle.slopesizer)+thisdfstyle.startr)
							var thisg = df.gethex(Math.round(thiscx*thisdfstyle.slopesizeg)+thisdfstyle.startg)
							var thisb = df.gethex(Math.round(thiscx*thisdfstyle.slopesizeb)+thisdfstyle.startb)
						}

						thisdfstyle.currentval = thisr+thisg+thisb
					}

					if (thisdfstyle.report!=undefined) {
						if (thisdfstyle.report.constructor == Function) {
							var noreport = thisdfstyle.report(thisdf)
						}

	//if report is an array, it is assumed that it is an array of references to functions
	//and it will try to call each function, in order
						if (thisdfstyle.report.constructor == Array) {
							for (var ir=0;ir<thisdfstyle.report.length;ir++) {
								var noreport = thisdfstyle.report[ir](thisdf)
							}
						}
		//this is for legacy dhtml_fx
						if (thisdfstyle.report.constructor==String) {
							var noreport = eval(thisdfstyle.report)(thisdfstyle.currentval)
						}

					}

					if (thisdf.targetobj!=undefined&&noreport!==false){
						if (thisdfstyle.important==true) {
							df.setcssText(thisdf.targetobj,stylename,"#"+thisdfstyle.currentval,true)
						} else {
							try{df.dg(thisdf.targetobj.id).style[stylename]="#"+thisdfstyle.currentval}catch(e){}//+((thisdfstyle.important==true)?"!important":"")
						}
					}

				} else {
					if (thisdfstyle.slope.constructor==Function) {
						thisdfstyle.currentval = thisdfstyle.slope(thisdfstyle)
					} else {


						if (thisdfstyle.radstep==undefined) {
							if (thisdfstyle.slope.constructor==Function) {
								var thispx = thisdfstyle.slope(thisdf,stylename)
							} else {
								if (thisslope==df.lookup) {
									var thisidx = Math.round(((thisdfstyle.max-thisdfstyle.min)/thisdfstyle.steps)*thisdfstyle.step)
									var thispx = thisdfstyle.lookup[thisidx]
								} else {
									if (thisdfstyle.isfloat) {
										var thispx = (thisdfstyle.slopesize*thisdfstyle.step)+thisdfstyle.startval
									} else {
										var thispx = Math.round((thisdfstyle.slopesize*thisdfstyle.step)+thisdfstyle.startval)
									}
								}
							}
						} else {

//--------------------------------------------------------------------------------
//df.lookup slope type specifies an array of values as the data for the function
//this can be used to animate paths using custom data.
//
//example:
//  {lookup:[1,2,3,4,5,6,7,8,9]}

								if (thisdfstyle.isfloat) {
									var thispx = (thiscx*thisdfstyle.slopesize)+thisdfstyle.startval
								} else {
									var thispx = Math.round(thiscx*thisdfstyle.slopesize)+thisdfstyle.startval
								}
						}

						thisdfstyle.currentval = thispx
					}

					if (thisdfstyle.report!=undefined) {
						if (thisdfstyle.report.constructor == Function) {
							var noreport = thisdfstyle.report(thisdf)
						}

	//if report is an array, it is assumed that it is an array of references to functions
	//and it will try to call each function, in order
						if (thisdfstyle.report.constructor == Array) {
							for (var ir=0;ir<thisdfstyle.report.length;ir++) {
								var noreport = thisdfstyle.report[ir](thisdf)
							}
						}
		//this is for legacy dhtml_fx
						if (thisdfstyle.report.constructor==String) {
							var noreport = eval(thisdfstyle.report)(thisdfstyle.currentval)
						}

					}

//,thisdfstyle.important  - if true, must set styles in bulk, need a function that takes a style name, value, and domobject, and updates the value using cssText
					if (thisdf.targetobj!=undefined&&noreport!==false){
						if (thisdfstyle.type==df.opacitytype) {
							if (thisdfstyle.important) {
								var opacitycss = df.opacityCSS(thispx)
								df.setcssText(thisdf.targetobj,opacitycss.split(":")[0],opacitycss.split(":")[1],true)
							} else {
								df.setopacity(thisdf.targetobj.id,thispx)	//,thisdfstyle.important
							}
						} else {
							var valrange = (thisdfstyle.ispercent)?"%":"px"
							if (thisdfstyle.important) {
								df.setcssText(thisdf.targetobj , stylename  , thispx + valrange , true)
							} else {
								try{df.dg(thisdf.targetobj.id).style[stylename] = thispx + valrange}catch(e){}//+((thisdfstyle.important==true)?"!important":"")
							}
						}
					}
				}



	}catch(e){df.debug.debugwin({title:"df.fxproc",color:"red",message:e.description+"<br>"+e.message+"<br><br>"+thispx+"<br>"+((df.json!=undefined)?df.json.serialize(df.fxarray):'')+"<br>"+thisdf.target+"<br>"+stylename})}


	//df.dg("objdiv").innerHTML = df.json.serialize(df.fxarray)
		//----------------
		//Report every iteration
			if (thisdf.report!=undefined) {

				if (thisdf.report.constructor == Function) {
					thisdf.report(thisdf)
				}
				if (thisdf.report.constructor == Array) {
					for (var ir=0;ir<thisdf.report.length;ir++) {
						thisdf.report[ir](thisdf)
					}
				}
//this is for legacy dhtml_fx
				if (thisdf.report.constructor==String) {
					eval(thisdf.report)(thisdf)
				}

			}

			//if connector object is present, then try to update the connectors
			//if this object is moving by top or left only..
			if (df.connector!=undefined) {
				if (stylename=="top"||stylename=="left") {
					try{df.connector.mousemove(df.getleft(thisdf.targetobj),df.gettop(thisdf.targetobj),thisdf.targetobj)}catch(e){}
				}
			}
		//----------------
		//callback at end of cycle

			if ( (thisdfstyle.step+1) >thisdfstyle.steps) {

				var killloop = false

				if (thisdfstyle.callback!=undefined) {
					if (thisdfstyle.callback.constructor==String) {
//this is for legacy dhtmlfx
						try{eval(thisdfstyle.callback)}catch(e){}
						//killloop = true
					} else {
						try{
							killloop = thisdfstyle.callback(thisdf)||false
						}catch(e){
							try{df.debug.debugwin({title:"df.anim callback failed: "+selector+"/"+stylename,color:"red",message:e.description+"<br/>"+e.message})}catch(e){}
						}
					}
				}

				if (thisdfstyle.loop==true&&killloop!=true) {
					//for (var selectorloop in thisdf.styles) {
						var maxtemp = thisdfstyle.maxval
						thisdfstyle.max = thisdfstyle.minval
						thisdfstyle.min = maxtemp
						thisdfstyle.follow=true  // follow is used to loop anim events, it isnt an input parameter
					//}
					thisdfstyle.step = 0
//if (String(thisdf.target).indexOf("nullanim_")!=-1) {delete thisdf.target}
//alert(df.json.serialize(thisdf))
				//	delete thisdf.minval
				//	delete thisdf.maxval
					//df.animx(thisdf)
					try{thisdfstyle.slope.push(thisdfstyle.slope.shift())}catch(e){}
					thisdfstyle = df.calcanim(thisdf,stylename,thisdfstyle)

				} else {
					//if (killloop==true) {
						delete df.fxarray[selector].styles[stylename]
					//}
				}


			} else {
				thisdfstyle.step++
			}
		}

		var hasstyles=false
		testhasstyles:for (var testselector in thisdf.styles){hasstyles=true;break testhasstyles}

		//----------------
//after all styles are exhausted, use the callback
			if (hasstyles==false&&thisdf.callback!=undefined) {
				if (thisdf.callback.constructor==String) {
//this is for legacy dhtmlfx
					try{eval(thisdf.callback)}catch(e){}
					killloop = true
				} else {
					try{
						killloop = thisdf.callback(thisdf)||true
					}catch(e){
						try{df.debug.debugwin({title:"df.anim callback failed: "+selector,color:"red",message:e.description+"<br/>"+e.message})}catch(e){}
					}
				}
			}
			if (hasstyles==false) {
				delete df.fxarray[selector]
			}

		//----------------
		}


	//df.debug.debugwin({title:"test"})

		var nofx=true
		for (var selector in df.fxarray){nofx=false;break}
		if (nofx==false) {
			df.fxtmr = window.chronos.setTimeout(df.fxproc,df.period)
		} else {
			df.fxtmr = 0
		}


		return
	}catch(e){df.debug.debugwin({title:"df.fxproc",color:"red",message:e.description+"<br>"+e.message})}
	},

//==============================================================================================
//This does not belong in dhtmlfx.vector because it is used as an envelope for dhtmlfx.anim
	bezier:	function(thisdfstyle) {
		var thiscurve = thisdfstyle.curve
		thiscurve.percent = thisdfstyle.step/thisdfstyle.steps
		return df.getBezierPercent(thiscurve)
	},

	Bez1:	 function(b){ return (b*b*b)},
	Bez2:	 function(b){ return (3*b*b*(1-b))},
	Bez3:	 function(b){ return (3*b*(1-b)*(1-b))},
	Bez4:	 function(b){ return ((1-b)*(1-b)*(1-b))},
	getBezierPercent:	function(bezobj) {
		var	df		= window.dhtmlfx	,
			percent		= bezobj.percent,
			startPoint	= bezobj.startPoint,
			endPoint	= bezobj.endPoint,
			ctrl1		= bezobj.ctrl1,
			ctrl2		= bezobj.ctrl2,
			precision	= bezobj.precision

		df.objvars(this,arguments)

		if (precision==undefined) {precision=3}

		if(!ctrl2 && !ctrl1) {
			var ctrl2 = {x:(startPoint.x + 3*(endPoint.x-startPoint.x)/4), y:(startPoint.y + 3*(endPoint.y-startPoint.y)/4) }
		}
		if(!ctrl2) {
			var ctrl2 = ctrl1
		}
		if(!ctrl1) {
			var ctrl1 = {x:(startPoint.x + (endPoint.x-startPoint.x)/4), y:(startPoint.y + (endPoint.y-startPoint.y)/4)}
		}
		var thispos = {x:0,y:0}
		thispos.x = parseFloat((startPoint.x * df.Bez1(percent) + ctrl1.x * df.Bez2(percent) + ctrl2.x * df.Bez3(percent) + endPoint.x * df.Bez4(percent)).toFixed(precision));
		thispos.y = parseFloat((startPoint.y * df.Bez1(percent) + ctrl1.y * df.Bez2(percent) + ctrl2.y * df.Bez3(percent) + endPoint.y * df.Bez4(percent)).toFixed(precision));
		return thispos;
	},

//---------------------------------------
//helper functions
	domcache:{},
	domcaching:false,
	domcachingEnable:	function() {
		dhtmlfx.dg = function(thisid) {
			try{
				return dhtmlfx.domcache[thisid] || (dhtmlfx.domcache[thisid] = document.getElementById(thisid) || (dhtmlfx.domcache[thisid]=null) )
			}catch(e){return null}
		}
		dhtmlfx.domcaching = true
	},
	domcache_clear:	function(thisdom) {
		if (thisdom==undefined) {return}
		var removedoms = thisdom.getElementsByTagName("*")
		for (var i=0;i<removedoms.length;i++) {
			if (removedoms[i].id) {delete dhtmlfx.domcache[removedoms[i].id]}
		}
	},
	domcache_garbageCollect:	function() {
		for (var selector in dhtmlfx.domcache) {
			if (dhtmlfx.domcache[selector]==null||(dhtmlfx.domcache[selector]||{}).offsetParent==null) {
				delete dhtmlfx.domcache[selector]
			}
		}
	},

	dg:		function (thisid) {
		try{
			return document.getElementById(thisid)
		}catch(e){
			return null
		}
	}
	,
	opacityfactor:100,
	setopacity:	function(domobj,opv,important){
	try{
		var df = dhtmlfx
		domobj = df.dg(df.testid(domobj))
		try{
			var imp = (important==true)?"!important":""
			if((df.browser=="ie")&&(df.platform.indexOf("Win")!=-1)){
				thisfilter = String(domobj.style.filter);
				thisfilter = thisfilter.replace(/alpha\(opacity\=.*?\)/g,"");
				thisfilter = (thisfilter+" ").replace(/\s/g,"");
				thisfilter += "alpha(opacity="+parseInt(Math.round((opv)),10)+")"+imp;
				domobj.style.filter = thisfilter
				if (df.bver<9&&parseInt(Math.round((opv*((df.opacityfactor==1)?100:1)),10))==((df.opacityfactor==1)?100:1)&&important!=true) {
					domobj.style.filter=""
				};
				return
			};
			if((df.browser=="ie")&&(df.platform=="mac")){return};
			if(df.browser=="safari"||df.browser=="chrome"){
				domobj.style.opacity = (opv/df.opacityfactor)+""+imp;
				return
			};
			if(df.browser=="ff"){
				if(opv>99){opv=df.opacityfactor};
//				domobj.style.MozOpacity=(opv/df.opacityfactor);
				if (df.bver>3.1) {
					domobj.style.opacity=(opv/df.opacityfactor)+imp;
				} else {
					domobj.style.MozOpacity=(opv/df.opacityfactor)+imp;
				}
				return
			}
			if(df.browser=="opera"){
				domobj.style.opacity = (opv/df.opacityfactor)+imp;
				return
			}
			domobj.style.opacity=(opv/df.opacityfactor)+imp;

		}catch(e){}
	}catch(e){df.debug.debugwin({title:"df.setopacity",message:e.description+"<br>"+e.message,color:"red"})}
	}
	,
	setrotate:	function(domobj,rot) {
		try{domobj.style.WebkitTransform="rotate("+rot.toFixed(2)+"deg)"}catch(e){}
		try{domobj.style.MozTransform="rotate("+rot.toFixed(2)+"deg)"}catch(e){}
		try{domobj.style.transform="rotate("+rot.toFixed(2)+"deg)"}catch(e){}
	},

	roundedCSS:	function(var1,var2,var3,var4,important) {
		var round=""
		if (arguments.length==2) {
			round = var1+"px "+var1+"px "+var1+"px "+var1+"px"
			if (arguments[1]==true){round+=" !important"}

		} else {
			round = var1+"px "+var2+"px "+var3+"px "+var4+"px"
			if (important==true){round+=" !important"}
		}

		return ';-moz-border-radius:'+round+';-webkit-border-radius:'+round+';border-radius:'+round+';'
	}
	,
	shadowCSS:	function(var1,var2,var3,color,important) {
		if (arguments.length==2||arguments.length==3) {
			var shadow = var1+"px "+var1+"px "+var1+"px #"+df.hexcolor(var2)
			if (arguments[2]==true){shadow+=" !important"}
		} else {
			var shadow = var1+"px "+var2+"px "+var3+"px #"+df.hexcolor(color)
			if (important==true){shadow+=" !important"}
		}
		return ";-moz-box-shadow:"+shadow+";-webkit-box-shadow:"+shadow+";box-shadow:"+shadow+";"
	},

	opacityCSS:	function(opv,important){return dhtmlfx.getopacitycss(opv,important)},
	getopacitycss:	function(opv,important){
		try{
		var df = dhtmlfx
			if (parseInt(Math.round((opv),10))==100&&important!=true) {return ''};
			var imp=""
			if (important==true) {imp="!important"}
			if((df.browser=="ie")&&(df.platform.indexOf("Win")!=-1)){
				return "filter:alpha(opacity="+parseInt(Math.round((opv)),10)+")"+imp;
			}
			if(df.browser=="safari"||df.browser=="chrome"){return 'opacity:'+(opv/100)+imp};
			if(df.browser=="ff"){
				if(opv>99){opv=100};
				if (df.bver>3.0) {
					return 'opacity:'+(opv/100)+imp
				} else {
					return '-moz-opacity:'+(opv/100)+imp
				}
			}
			if(df.browser=="opera"){return 'opacity:'+(opv/100)+imp}

			if((df.browser=="ie")&&(platform=="mac")){return ''};

			return 'opacity:'+(opv/100)+imp;
		}catch(e){return ''}
	}
	,
	getopacity:	function(domobj) {
		var df = dhtmlfx
		var thisdom = df.dg(df.testid(domobj))
		if (thisdom==null) {return undefined}

		if (df.browser=="ie") {
			var opac=String(thisdom.style.filter).toLowerCase()
			if (opac=="") {opac="opacity="}
			var thisopacity = parseInt(opac.split("opacity=")[1].replace(/[^0-9]/g,""),10)||undefined
		} else {
			var thisopacity = Math.round(parseFloat(thisdom.style.opacity)*100)||undefined
		}
		return thisopacity

	},
	ischild:	function (thisobj,idtype){if(thisobj.tagName=="INPUT"||thisobj.tagName=="TEXTAREA"||thisobj.tagName=="SELECT"){return null};do{try{tobj=String(thisobj.id)}catch(e){tobj=""};try{if(tobj.indexOf(idtype)!=-1){break};thisobj=thisobj.offsetParent;}catch(e){break}}while(tobj!="BODY");return thisobj}
	,
	getleft:	function (obj){
		var c=0;
		try{
			do {
				c+=obj.offsetLeft// + (obj.offsetParent.scrollLeft||obj.offsetParent.documentElement.scrollLeft||0)
				c-=(obj.style.marginLeft.indexOf("%")==-1)?parseInt(obj.style.marginLeft||0,10):((parseInt(obj.style.marginLeft||0,10)/100)*(obj.parentNode.offsetWidth/2))
			} while (obj=obj.offsetParent)
		}catch(e){}
		return c
	}
	,
	gettop:		function (obj){
		var c=0;
		try{
			do {
				c+=obj.offsetTop //+ (obj.offsetParent.scrollTop||obj.offsetParent.documentElement.scrollTop||0)
				c-=(obj.style.marginTop.indexOf("%")==-1)?parseInt(obj.style.marginTop||0,10):((parseInt(obj.style.marginTop||0,10)/100)*(obj.parentNode.offsetHeight/2))
			} while (obj=obj.offsetParent)
		}catch(e){}
		return c//+df.scrolltop()
	}
	,
	calcscrolltop:		function (obj){
		var c=0;
		try{
			do {
				c+=obj.scrollTop //+ (obj.offsetParent.scrollTop||obj.offsetParent.documentElement.scrollTop||0)
				obj=obj.offsetParent
			} while (obj.tagName!="body")
		}catch(e){}
		c += (document.documentElement.scrollTop+document.body.scrollTop)
		return c
	}
	,
	calcscrollleft:		function (obj){
		var c=0;
		try{
			do {
				c+=obj.scrollLeft //+ (obj.offsetParent.scrollTop||obj.offsetParent.documentElement.scrollTop||0)
				obj=obj.offsetParent
			} while (obj.tagName!="body")
		}catch(e){}
		c += (document.documentElement.scrollLeft+document.body.scrollLeft)
		return c
	}
	,
	getbot:		function (thisobj){
		var df = dhtmlfx
		return df.gettop(thisobj)+thisobj.offsetHeight
	}
	,
	getright:	function (thisobj){
		var df = dhtmlfx
		return df.getleft(thisobj)+thisobj.offsetWidth
	}
	,

	fixpng:		function (scaling){
		var df = dhtmlfx
		if((df.browser=="ie")&&(df.bver<7)){
			if(event.srcElement.src.indexOf(df.blankimg)!=-1){return};
			event.srcElement.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+event.srcElement.src+"', sizingMethod='"+((scaling==undefined||scaling==false)?"image":"scale")+"')";
			event.srcElement.src=df.blankimg
		}
	}
	,
	fixpngdom:	function (domobj,scaling){
		var df = dhtmlfx
		if((df.browser=="ie")&&(df.bver<7)){
			var thisdomobj = df.dg(df.testid(domobj))
			if(thisdomobj.src.indexOf(df.blankimg)!=-1){return};
			thisdomobj.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+thisdomobj.src+"', sizingMethod='"+((scaling==undefined||scaling==false)?"image":"scale")+"')";
			thisdomobj.src=df.blankimg
		}
	}
	,
	fixpng_bg:	function (thisdom){
		var df = dhtmlfx
		if((df.browser=="ie")&&(df.bver<7)){
			var thisdomobj = df.dg(df.testid(thisdom))
			if(thisdomobj.background.indexOf(df.blankimg)!=-1){return};
			thisdomobj.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+thisdomobj.background+"', sizingMethod='scale')";
			thisdomobj.background=""
		}
	}
	,
	fixpng_bg_style:	function (thisdom){
		var df = dhtmlfx
		if((df.browser=="ie")&&(df.bver<7)){
			var thisdomobj = df.dg(df.testid(thisdom))
			var thisbgimg = String(df.computedstyle(thisdomobj,"backgroundImage")).replace(/url\(/g,"").replace(/[\)\"]/g,"")
			if(thisbgimg.indexOf(df.blankimg)!=-1){return};
			thisdomobj.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+thisbgimg+"', sizingMethod='scale')";
			thisdomobj.style.backgroundImage="url("+df.blankimg+")"
		}
	}
	,
	fixpng_cssbg:	function (cssselector){
		var df = dhtmlfx
		if((df.browser=="ie")&&(df.bver<7)){
			thiscss=df.getstyle(cssselector);
			if(thiscss.style.backgroundImage.indexOf(df.blankimg)!=-1){return};
			thiscss.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+thiscss.style.backgroundImage.replace(/url\(/g,"").replace(/\)/g,"")+"', sizingMethod='scale')";thiscss.style.backgroundImage="url("+df.blankimg+")"
		}
	}
	,
	backgroundImagePNG:	function (imagename){
		var df = dhtmlfx
		if(df.browser=="ie"&&df.bver<7){
			return "background-image:url("+df.blankimg+");FILTER:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+imagename+"', sizingMethod='scale');";
		} else {
			return "background-image:url("+imagename+");";
		}
	}
	,
	gethex:	function (decnum) {var dechex="0"+decnum.toString(16);return dechex.substr(dechex.length-2)}
	,
	getdec:	function (hexnum) {return parseInt(hexnum,16)}
	,
	hexcolor:	function (mozcolor) {
		var df = dhtmlfx
		if (mozcolor==undefined) {return undefined}
		mozcolor = mozcolor.toLowerCase()
		if (mozcolor.indexOf("#")!=-1) {
			mozcolor = mozcolor.replace(/#/g,"")
			if (mozcolor.length==3) {
				var mozarr=mozcolor.split("")
				mozcolor = mozarr[0]+mozarr[0]+mozarr[1]+mozarr[1]+mozarr[2]+mozarr[2]
			}
			return mozcolor
		} else if (mozcolor.indexOf("rgb(")!=-1) {
			var fixcolor=mozcolor.replace(/[^0-9\,]/g,"").split(",");
			return parseInt(fixcolor[0],16)+parseInt(fixcolor[1],16)+parseInt(fixcolor[2],16)
//			return df.gethex(parseInt(fixcolor[0],10))+df.gethex(parseInt(fixcolor[1],10))+df.gethex(parseInt(fixcolor[2],10))
		} else {

			if (df.colorlookup[mozcolor]) {
				return df.colorlookup[mozcolor]
			} else {
				if (mozcolor.length==3) {
					var mozarr=mozcolor.split("")
					mozcolor = mozarr[0]+mozarr[0]+mozarr[1]+mozarr[1]+mozarr[2]+mozarr[2]
					return mozcolor
				} else {
					return mozcolor
				}
			}
		}
	}
	,
	hextorgba:	function(hexcolor,alpha){
		var df = dhtmlfx
		var thishex = df.hexcolor(hexcolor)
		if (hexcolor.replace(/#/g,"").length>6 && alpha==undefined) {
			alpha = parseInt(hexcolor.substr(6,2),16)
		}
		return "rgba("+parseInt(thishex.substr(0,2),16)+","+parseInt(thishex.substr(2,2),16)+","+parseInt(thishex.substr(4,2),16)+","+alpha+")"
	}
	,
	colorlookup:{aqua:"00ffff",azure:"f0ffff",beige:"f5f5dc",bisque:"ffe4c4",black:"000000",blue:"0000ff",brown:"a52a2a",chocolate:"d2691e",coral:"ff7f50",cornflowerblue:"6495ed",cornsilk:"fff8dc",crimson:"dc143c",cyan:"00ffff",darkblue:"00008b",darkcyan:"008b8b",darkgray:"a9a9a9",darkgrey:"a9a9a9",darkgreen:"006400",darkorange:"ff8c00",darkred:"8b0000",dodgerblue:"1e90ff",gold:"ffd700",gray:"808080",grey:"808080",green:"008000",honeydew:"f0fff0",indigo:"4b0082",ivory:"fffff0",khaki:"f0e68c",lavender:"e6e6fa",lightblue:"add8e6",lightcyan:"e0ffff",lightgray:"d3d3d3",lightgrey:"d3d3d3",lightgreen:"90ee90",lightpink:"ffb6c1",lightskyblue:"87cefa",lightyellow:"ffffe0",lime:"00ff00",limegreen:"32cd32",magenta:"ff00ff",maroon:"800000",midnightblue:"191970",navy:"000080",olive:"808000",olivedrab:"6b8e23",orange:"ffa500",orangered:"ff4500",orchid:"da70d6",pink:"ffc0cb",plum:"dda0dd",powderblue:"b0e0e6",purple:"800080",red:"ff0000",royalblue:"4169e1",salmon:"fa8072",sandybrown:"f4a460",seagreen:"2e8b57",sienna:"a0522d",silver:"c0c0c0",skyblue:"87ceeb",springgreen:"00ff7f",steelblue:"4682b4",tan:"d2b48c",teal:"008080",tomato:"ff6347",turquoise:"40e0d0",violet:"ee82ee",white:"ffffff",whitesmoke:"f5f5f5",yellow:"ffff00",yellowgreen:"9acd32"}
	,
	newcolor:	function (thiscolor,amount) {
		var df = dhtmlfx
		if (thiscolor==null||thiscolor==""||thiscolor==undefined) {return "000000"}
		var newbgcolor = df.hexcolor(thiscolor);
		var newr = (df.getdec(newbgcolor.substr(0,2))+amount)||0
		var newg = (df.getdec(newbgcolor.substr(2,2))+amount)||0
		var newb = (df.getdec(newbgcolor.substr(4,2))+amount)||0
		if (newr>255) {newr=255};if(newr<0) {newr=0}
		if (newg>255) {newg=255};if(newg<0) {newg=0}
		if (newb>255) {newb=255};if(newb<0) {newb=0}
		return df.gethex(newr) + df.gethex(newg) + df.gethex(newb);

//		if (newr>255) {newr=255};if(newr<0) {newr=0}
//		if (newg>255) {newg=255};if(newg<0) {newg=0}
//		if (newb>255) {newb=255};if(newb<0) {newb=0}

//		if (newr>255) {return thiscolor};if(newr<0) {return thiscolor}
//		if (newg>255) {return thiscolor};if(newg<0) {return thiscolor}
//		if (newb>255) {return thiscolor};if(newb<0) {return thiscolor}
//		var bgcolor = df.gethex(newr) + df.gethex(newg) + df.gethex(newb);

//		if (bgcolor.indexOf("-")!=-1) {return thiscolor} else {return bgcolor}
	},
	gradient:	function(gradientsource,gradientlength) {
	//returns an array of 24 or 32 bit gradient colors (hex values)
	//takes an input array of 24 or 32 bit colors (hex values)
		var df = dhtmlfx
		var returnarr = []
		var colorsteps = gradientlength/(gradientsource.length-1)
		var stepidx = 0
		for (var i=0;i<gradientsource.length;i++) {
			if (i+1>gradientsource.length-1){break}

			var startcol = gradientsource[i]
			var endcol = gradientsource[i+1]

			var startr = df.getdec(startcol.substr(0,2))
			var startg = df.getdec(startcol.substr(2,2))
			var startb = df.getdec(startcol.substr(4,2))
			try{var starta = df.getdec(startcol.substr(6,2))}catch(e){}

			var endr = df.getdec(endcol.substr(0,2))
			var endg = df.getdec(endcol.substr(2,2))
			var endb = df.getdec(endcol.substr(4,2))
			try{var enda = df.getdec(endcol.substr(6,2))}catch(e){}

			var deltar = (endr-startr)/colorsteps
			var deltag = (endg-startg)/colorsteps
			var deltab = (endb-startb)/colorsteps
			var deltaa = 1
			try{deltaa = (enda-starta)/colorsteps}catch(e){}

			if ((stepidx+colorsteps)>gradientlength) {
				colorsteps = gradientlength-stepidx
			}

			for (var c=0;c<colorsteps;c++) {
				var thiscolor = df.gethex(Math.round(startr+(deltar*c)))+df.gethex(Math.round(startg+(deltag*c)))+df.gethex(Math.round(startb+(deltab*c)))
				if (!isNaN(starta)) {thiscolor+=df.gethex(Math.round(starta+(deltaa*c)))}
				returnarr.push(thiscolor)
				stepidx++
			}
		}
		return returnarr
	}
	,
	colorinvert:	function(thiscolor) {
		var df = dhtmlfx
		var fromcolor = df.hexcolor(thiscolor)
		var ir = df.gethex(255-df.getdec(fromcolor.substr(0,2)))
		var ig = df.gethex(255-df.getdec(fromcolor.substr(2,2)))
		var ib = df.gethex(255-df.getdec(fromcolor.substr(4,2)))
		return ir+ig+ib
	}
	,
	rgbtocmyk:	function(rgbval) {
		var df = dhtmlfx

		rgbval = df.hexcolor(rgbval)
		var r = df.getdec(rgbval.substr(0,2))
		var g = df.getdec(rgbval.substr(2,2))
		var b = df.getdec(rgbval.substr(4,2))

		var c = 1 - ( r / 255 )
		var m = 1 - ( g / 255 )
		var y = 1 - ( b / 255 )
		var vark = 1

		if ( c < vark ) { vark = c }
		if ( m < vark ) { vark = m }
		if ( y < vark ) { vark = y }
		if (vark == 1 ) {
			c = 0
			m = 0
			y = 0
		} else {
			c = ( c - vark ) / ( 1 - vark )
			m = ( m - vark ) / ( 1 - vark )
			y = ( y - vark ) / ( 1 - vark )
		}
		var k = vark
		c = df.gethex(Math.round((255/100)*(c*100)))
		m = df.gethex(Math.round((255/100)*(m*100)))
		y = df.gethex(Math.round((255/100)*(y*100)))
		k = df.gethex(Math.round((255/100)*(k*100)))

		return c+m+y+k
	}
	,
	now:	function() {return Date.parse(new Date())}
	,
	testid:	function (thisid){
		var df = dhtmlfx

		if (thisid.constructor==String) {
			return thisid
		} else {
			if (thisid.id=="") {
				var returnid = String("dhtmlobj"+df.objidx);
				thisid.setAttribute("id",returnid);
				df.objidx++
				return returnid
			} else {
				return thisid.id
			}
		}
	}
	,
	unique:	function() {
		var df = dhtmlfx
		var thisnow = df.now();
		if (thisnow==df.uniquetc) {
			df.uniqueid++;
		} else {
			df.uniquetc = thisnow;
			df.uniqueid=0;
		}
		return df.uniquetc+""+df.uniqueid
	}
	,
	nullfunc:	function(){}
	,
	copydom:	function(source,target,unique,deep) {
		var df = dhtmlfx
		//if (arguments[0].source!=undefined) {
		//	var source = arguments[0].source
		//	var target = arguments[0].target
		//	var unique = arguments[0].unique
		//	var replace = arguments[0].replace
		//}
	try{
//		if (arguments.length==1&&df.objecttype=="object") {
//			var source = arguments[0].source
//			var target = arguments[0].target
//			var unique = arguments[0].unique
//			var replace = arguments[0].replace
//		}
var err=0
		source = df.dg(df.testid(source))
		if (deep==undefined) {var deep=true}

		var copyobject = source.cloneNode(deep);
err=1
		var uniqueid=""
		if (unique==true) {uniqueid=df.unique()}
		if (target!=undefined) {
			var thistarget = df.dg(df.testid(target))
			thistarget.appendChild(copyobject)
		}
err=2
		copyobject.setAttribute("id",df.testid(copyobject)+"_copy"+uniqueid)

err=3
		//this fixes copying of VML and canvas elements
		if (df.vector!=undefined) {
			if (df.browser=="ie") {
				if (
					source.getElementsByTagName("oval").length>0||
					source.getElementsByTagName("line").length>0||
					source.getElementsByTagName("shape").length>0
				) {
err=3.1
					copyobject.innerHTML = source.innerHTML
				}
			} else {
err=3.4
				var canvases = copyobject.getElementsByTagName("canvas")
err=3.5
				if (copyobject.tagName.toLowerCase()=="canvas"){
					copyobject.getContext('2d').drawImage(df.dg(copyobject.id.replace(/\_copy.*?$/g,"")),0,0)
				}
err=3.6
				for (var i=0;i<canvases.length;i++) {
					try{canvases[i].getContext('2d').drawImage(df.dg(canvases[i].id.replace(/\_copy.*?$/g,"")),0,0)}catch(e){}
				}
err=3.7
			}
		}

		var dragchildren = copyobject.getElementsByTagName("*");
		for (var i=0;i<dragchildren.length;i++) {
			try{
				if (dragchildren[i].id!=""){
					dragchildren[i].setAttribute("id",String(dragchildren[i].id)+"_copy"+uniqueid)
				}
			}catch(e){}
		}

		return copyobject

	}catch(e){df.debug.debugwin({title:"copydom",color:"red",message:e.description+"<br>"+e.message+"<br>err="+err})}
	},

//-------------------------------------------------------
//a chunked for-next loop that unblocks the UI using an interval timer
//it processes N number of items before delaying X milliseconds and then continuing until the array end is reached
/*
		df.chunk({
			array		: document.getElementsByTagName("*"),
			chunksize	: 10,
			timeout		: 1000,
			func		: function(thisitem){
				df.debug.debugwin({title:thisitem.tagName,message:"",color:""})
			},
			callback	: df.heatmap.play_loop
		})
*/
	forchunk:	function(varinit,varlen,varinc,func,callback,timeout,chunksize) {
	try{
		var df = dhtmlfx
		if (timeout==undefined) {timeout=20}
		if (chunksize==undefined) {chunksize=200}

		var slices = varlen/varinc
		var thisslice = varinit

		var thisinterval = window.chronos.setInterval(function() {

			var slice = 0
			var maxslice = 200
			if (maxslice>slices) {maxslice=slices}
			do {
				func(thisslice)
				thisslice+=varinc
				slices--
				slice++
			} while (slice<maxslice&&slices>0&((varinit<varlen)?thisslice<varlen:thisslice>varlen))

			if ((varinit<varlen)?thisslice>=varlen:thisslice<=varlen) {
				window.chronos.clearTimeout(thisinterval)
				if (callback!=undefined) {
					callback(thisslice)
				}
			}

		},timeout)
	}catch(e){df.debug.debugwin({title:"forchunk",message:e.description+"<br>"+e.message,color:"red"})}
	},
//-------------------------------------------------------
	customdoms:[


	],

	newdom:		function(newdomobj) {
	try{
	//tagname,target,content,attributes

		var df = dhtmlfx
		if (newdomobj.target==undefined) {
			newdomobj.target = (newdomobj.noappend)?document:document.body
			var targetid = df.testid(newdomobj.target)
		} else {
			var targetid = df.testid(newdomobj.target)
		}

		newdomobj.attributes = newdomobj.attributes || newdomobj.attr

		try{
			if (newdomobj.attributes) {
				var thisid = newdomobj.attributes["id"]||String("dhtmlobj"+(df.objidx++))
			} else {
			 	var thisid = String("dhtmlobj"+(df.objidx++))
			}
		}catch(e){}

		var thistarget = newdomobj.target||df.dg(targetid)

		var ownerdoc = thistarget.ownerDocument || document
		var thisnewdom = df.dg(thisid)
		if (thisnewdom != null) {
			thisnewdom.parentNode.removeChild(thisnewdom)
		}
		//if (thisnewdom==null) {
			var thisnewdom = ownerdoc.createElement(newdomobj.tagname||newdomobj.tagName);//document
			thisnewdom.id = thisid
		//}

		for (var selector in newdomobj.attributes) {
			var thisatt = newdomobj.attributes[selector]
			if (thisatt===undefined) {continue}

			if (selector=="style") {
				if (newdomobj.attributes.wipeCSS) {
					df.wipeCSS(thisnewdom,true,thisatt)
				} else {
					var stylearr = thisatt.split(";")
					var stylearrlen = stylearr.length;
					for (var isx=0;isx<stylearrlen;isx++) {
						try{
							if (stylearr[isx]=="") {continue}
							var stylesplit = stylearr[isx].split(":")
							var styleselector = stylesplit[0]
							var stylevalue = stylesplit[1]
							thisnewdom.style[df.styleToCamel(styleselector)] = stylevalue
						}catch(e){
							try{console.log("attempt to set style failed: "+styleselector+" = "+stylevalue)}catch(e){}
						}
					}
				}

				continue
			}

			if (selector=="class"||selector=="className") {
				if (df.browser=="ie") {
					thisnewdom.className = thisatt
				} else {
					thisnewdom.setAttribute("class",thisatt)
				}
				continue
			}
			if (selector=="innerHTML"||selector=="html") {
				thisnewdom.innerHTML = thisatt
				continue
			}
			try{
				if (df.objtype(thisatt)=="function") {
					thisnewdom[selector] = thisatt
				} else {
					thisnewdom.setAttribute(selector,thisatt)
				}
			}catch(e){
				try{
					thisnewdom[selector] = thisatt
				}catch(e){
					//df.debug.debugwin({title:"df.newdom setAttribute",message:"selector="+selector,color:"red"})
				}
			}

		}


		if (newdomobj.children && newdomobj.children.length!=undefined && newdomobj.children.length>0) {



			//thisnewdom.innerHTML = ""
			for (var idx=0;idx<newdomobj.children.length;idx++) {
				var newchild = newdomobj.children[idx]
				if (newchild.parentNode==undefined) {
					if (newchild.tagname==undefined) {continue}
					newchild.target = thisnewdom
					df.newdom(newchild)
				} else {
					thisnewdom.appendChild(newchild)
				}
			}
		}

		if (newdomobj.events) {
			for (var idx=0;idx<newdomobj.events.length;idx++) {
				var evtobj = newdomobj.events[idx]
				evtobj.domobj = thisnewdom
				df.attachevent(evtobj)
			}
		}

		if (String(newdomobj.noselect)=="true") {
			if (String(newdomobj.tagname).toLowerCase()!="textarea" && String(newdomobj.tagname).toLowerCase()!="input") {
				df.noselectDOM(thisnewdom)
			} else {
				if (String(newdomobj.tagname).toLowerCase()!="textarea" && String((newdomobj.attributes||{}).type).toLowerCase()!="text") {
					df.noselectDOM(thisnewdom)
				} else {
					if (df.browser=="ff") {
						thisdom.style.MozUserSelect="text"
					}
				}
			}
		}

		if (newdomobj.noappend!=true && df.dg(thisid)==null) {
			try{thistarget.appendChild(thisnewdom)}catch(e){}
		}

		if (df.customdoms[newdomobj.tagname]) {
			df.customdoms[newdomobj.tagname].dominit(thisnewdom)
		}

	//	if (newdomobj.wipecss==true) {
			//setTimeout(function(){df.wipeCSS(thisnewdom,false,true)},10)
	//		df.wipeCSS(thisnewdom,true,true)
	//	}

		if (df.domcaching) {
			df.domcache[thisid]=thisnewdom
		}
//might need a way to access the object
//		df.domobjects[thisnewdom.id] = newdomobj

		return thisnewdom
	}catch(e){df.debug.debugwin({title:"df.newdom",message:e.description+"<br>"+e.message,color:"red",error:e})}
	},

	objtype:	function(thisobj) {
	try{
		var df = dhtmlfx
		try{var objtype = String(typeof thisobj).toLowerCase()}catch(e){var objtype="unknown"}
		try{var thisconstructor = String(thisobj.constructor)}catch(e){var thisconstructor="unknown"}
		if (thisconstructor.toLowerCase().indexOf("array")!=-1){objtype="array"}
		if (thisconstructor.toLowerCase().indexOf("object")!=-1){objtype="object"}
		if (thisconstructor.indexOf("Date()")!=-1){objtype="date"}
		if (thisconstructor.indexOf("RegExp()")!=-1){objtype="regex"}
		if (thisconstructor.indexOf("Function")!=-1){objtype="function"}
		if (thisobj==undefined) {objtype="undefined"}
		if (thisobj==null) {objtype="null"}
		if (objtype=="object") {if (thisobj.tagName!=undefined) {objtype="domobject"}}
		if (objtype=="number") {if (isNaN(thisobj)==true) {objtype="NaN"}}
		return objtype
	}catch(e){df.debug.debugwin({title:"df.objtype",message:e.description+"<br>"+e.message,color:"red"})}
	},

	blockiframes:	function(thisdom) {
	try{
		var df = dhtmlfx
		var targetbody=false
		if (thisdom==undefined) {thisdom=document.body;targetbody=true}
		var getiframes = thisdom.getElementsByTagName("iframe")

		for (var i=0;i<getiframes.length;i++) {
			if (getiframes[i].style.position!="absolute") {
				getiframes[i].style.position="relative"
			}
			if (isNaN(parseInt(getiframes[i].style.zIndex,10))) {
				getiframes[i].style.zIndex = 500
			}
			var thisid = df.testid(getiframes[i])

			var targetdom = ((targetbody==true)?getiframes[i].parentNode:thisdom)

			var thistop = getiframes[i].offsetTop
			var thisleft = getiframes[i].offsetLeft
			var thiswidth = getiframes[i].offsetWidth
			var thisheight = getiframes[i].offsetHeight
			var thisblock = df.newdom({
				tagname:"div",
				target:targetdom,
				attributes:{
					id:"dhtmlfx_"+thisid+"_iframeblockdiv",
					style:"background-color:white;position:absolute;top:"+thistop+"px;left:"+thisleft+"px;width:"+thiswidth+"px;height:"+thisheight+"px;z-index:"+(parseInt(getiframes[i].style.zIndex,10)+10)+";"+df.getopacitycss(0)
				}
			})
		}
	}catch(e){df.debug.debugwin({title:"blockiframes",color:"red",message:e.description+"<br>"+e.message})}
	},

	iframeZindexPush:	function() {
		var doms = document.getElementsByTagName("iframe")
		for (var i=0;i<doms.length;i++) {
			var thisdom = doms[i]
			if (thisdom.style.position=="") {
				thisdom.style.position="relative"
			}
			if (thisdom.style.zIndex=="") {
				thisdom.style.zIndex = 1
			}
		}
	},

	unblockiframes:	function(thisdom) {
	try{
		var df = dhtmlfx
		if (thisdom==undefined) {thisdom=document.body}
		try{var domid = df.testid(thisdom)}catch(e){return}
		var parentdomid = domid.replace(/\_copy$/,"")
		try{
		var getiframes = df.dg(parentdomid).getElementsByTagName("div")
			for (var i=getiframes.length-1;i>0;i--) {
				if (getiframes[i].id.indexOf("iframeblockdiv")!=-1) {
					df.removedom(getiframes[i])
				}
			}
		}catch(e){}

		try{
			var getiframes = df.dg(parentdomid+"_copy").getElementsByTagName("div")
			for (var i=getiframes.length-1;i>0;i--) {
				if (getiframes[i].id.indexOf("iframeblockdiv")!=-1) {
					df.removedom(getiframes[i])
				}
			}
		}catch(e){}

	}catch(e){df.debug.debugwin({title:"unblockiframes",color:"red",message:e.description+"<br>"+e.message+"<br>err="+err})}
	},

	toggle:	function(thisdom) {
	try{
		var df = dhtmlfx
		var thisdom = df.dg(df.testid(thisdom))

		if (thisdom.style.display=="block") {
			thisdom.style.display="none"
			return false
		} else {
			thisdom.style.display="block"
			return true
		}
	}catch(e){}
	},

//=========================================================================================
//=========================================================================================
//Styles

	//not working yet?
	//supposed to find all css rules that have the matching selector
	//returns an array of ?
	findstyle:	function (cssselector){
		var rulesarr=[]
	try{
		var df = dhtmlfx
		sheets=document.styleSheets;

	//df.debug.debugwin({title:"stylesheets",message:df.json.serialize(sheets)})
		for (var iss=0;iss<sheets.length;iss++){
			var getrules=[];
			try{
				if(df.browser=="ie"){
					getrules=document.styleSheets[iss].rules;
				}else{
					getrules=document.styleSheets[iss].cssRules;
				}
			}catch(e){continue}

			var thisrules = df.getrules(getrules,cssselector)
			if (thisrules.length>0) {rulesarr.concat(thisrules)}
		}
		return rulesarr
	}catch(e){
		return rulesarr
	}
	}
	,
//-----------------------------------------------

//does not work on <link> elements? WTF?!?!
	getrules:	function(getrulesin,cssselector) {
		var rulesarr=[]
		cssselector = cssselector.toLowerCase()
		for (var irl=0;irl<getrulesin.length;irl++){
			try{
				var ruleitem = getrulesin.item(irl)
				if (ruleitem.selectorText.toLowerCase().indexOf(cssselector)!=-1 ){
					rulesarr.push(ruleitem)
				}
			}catch(e){continue}
		}
		return rulesarr
	},
//-----------------------------------------------

//does not work on <link> elements? WTF?!?!
	getrule:	function(getrulesin,cssselector) {
		for (var irl=0;irl<getrulesin.length;irl++){
			try{
				if (getrulesin.item(irl).selectorText.toLowerCase()==cssselector.toLowerCase()){
					return getrulesin.item(irl)
				}
			}catch(e){continue}
		}
		return undefined
	},

//-----------------------------------------------

	getstyle:	function (cssselector){
	try{
		var df = dhtmlfx
		sheets=document.styleSheets;

	//df.debug.debugwin({title:"stylesheets",message:df.json.serialize(sheets)})

		for (var iss=0;iss<sheets.length;iss++){
			var getrules=[];
			try{
				if(df.browser=="ie"){
					getrules=document.styleSheets[iss].rules;
				}else{
					getrules=document.styleSheets[iss].cssRules;
				}
			}catch(e){continue}

			var thisrule = df.getrule(getrules,cssselector)
			if (thisrule!=undefined) {return thisrule}
		}
		return undefined
	}catch(e){
		return undefined
	}
	}
	,
//-----------------------------------------------

	setstyle	:	function (cssselector,cssstyle,cssvalue){
		var df = dhtmlfx
		var setstyleobj = df.getstyle(cssselector);
		setstyleobj.style[cssstyle] = cssvalue
	}
	,

/* ********************************************************************************
@function	: setcssText
@path		: df.setcssText
@description	:
Uses the cssText dom element property to set a single style. cssText is a string that contains the entire inline style for the dom object.
this function splits the css string apart, searches for the value and replaces it, or adds a new style to the string.
This is useful because trying to set !important on a style of a dom element doesn't work by using style.top or style.backgroundColor, etc.
It seems that the only way to set !important dynamically is to use cssText. This is really more of a DOM workaround than useful code.
If browsers would allow style.top or style["top"] to set values with !important, this function wouldn't be neccessary.
@author		: Damien Otis
@returns	: undefined
@requires	: n/a
@aliases	: n/a
@alias		: n/a
@argument	: domobj DOM Object,stylename String,value String,important Boolean
@example	:

	var thisdom = DF.newdom({tagname:"div"})
	DF.setcssText(thisdom,"top","100px",true)

*/
	setcssText:	function(domobj,stylename,value,important){
//console.log("setcssText",domobj,stylename,value,important)
		var csstext = domobj.style.cssText
		var testarr = csstext.split(";")
		var nocss = true
		for (var i=0;i<testarr.length;i++) {
			var csstest = testarr[i]
			if (csstest=="") {continue}
			var teststyle = df.trim(csstest.split(":")[0])
			if (df.trim(teststyle) != stylename){continue}
			testarr[i] = stylename + ":" + df.trim(value)+((important==true)?"!important":"")
			nocss = false
			break
		}
		if (nocss) {
			testarr.push(stylename + ":" + df.trim(value)+((important==true)?"!important":""))
		}

		domobj.style.cssText = testarr.join(";")
/*
		if (df.browser=="ie") {
			domobj.style.cssText = testarr.join(";")
		} else {
			domobj.style.setAttribute("cssText",testarr.join(";"))
		}
*/
	}
	,
//-----------------------------------------------

	addcss:		function (cssobj,important) {
		try{
			var df = dhtmlfx

			var thishead = document.getElementsByTagName("head")[0]

			if (df.fxstyles==undefined) {
				var newcss = document.createElement("style");
				newcss.setAttribute("type","text/css");
				thishead.appendChild(newcss);
				df.fxstyles = newcss
			} else {
				newcss = df.fxstyles
			}

			if (df.browser!="ff") {
var err=0
//				if (!window.createPopup) {
//					newcss.appendChild(document.createTextNode(selector + cssobj[selector]+"\n"));
//					return
//				}


				var thisstyle = document.styleSheets[document.styleSheets.length - 1];
err=1
				for (var selector in cssobj) {
					var cssstyle = cssobj[selector]
err=2
					if (cssstyle.indexOf("{")==-1) {cssstyle = "{"+cssstyle}
					if (cssstyle.indexOf("}")==-1) {cssstyle = (cssstyle+";}").replace(/\;\;/g,";")}

					if (important) {
						cssstyle = (cssstyle+";").replace(/\;\;/g,";").replace(/\!important/g,"").replace(/\;/g,"!important;")
					}
err=3
					if (thisstyle.insertRule) {
err=4
						try{
err=5
							thisstyle.insertRule(selector + cssstyle)
err=6
						}catch(e){
							try{
err=7
								thisstyle.insertRule(selector + cssstyle , thisstyle.cssRules.length)
err=8
							}catch(e){
								try{
err=9
									thisstyle.insertRule(selector + cssstyle , 0)
err=10
								}catch(e){
									try{

										for (var selector in cssobj) {
											var cssstyle = cssobj[selector]
											if (cssstyle.indexOf("{")==-1) {cssstyle = "{"+cssstyle}
											if (cssstyle.indexOf("}")==-1) {cssstyle = cssstyle+"}"}
						err=13.1
											newcss.appendChild(document.createTextNode(selector + cssstyle+"\n"));
						err=14.1
										}
										return newcss

									}catch(e){
											df.debug.debugwin({title:"df.addcss insertRule fail 1",message:selector + cssstyle+"<br>"+e.description+"<br>"+e.message+"<br>err="+err,color:"red"})
									}
								}
							}
						}
					} else {
err=11
						try {
							thisstyle.addRule(selector,cssstyle)
						} catch(e) {
							try{

								for (var selector in cssobj) {
									var cssstyle = cssobj[selector]
									if (cssstyle.indexOf("{")==-1) {cssstyle = "{"+cssstyle}
									if (cssstyle.indexOf("}")==-1) {cssstyle = cssstyle+"}"}
				err=13.2
									newcss.appendChild(document.createTextNode(selector + cssstyle+"\n"));
				err=14.2
								}
								return newcss

							}catch(e){
									df.debug.debugwin({title:"df.addcss insertRule fail 2",message:selector + cssstyle+"<br>"+e.description+"<br>"+e.message+"<br>err="+err,color:"red"})
							}
						}
err=12
					}
				}
				return newcss
			} else {
				for (var selector in cssobj) {
					var cssstyle = cssobj[selector]
					if (cssstyle.indexOf("{")==-1) {cssstyle = "{"+cssstyle}
					if (cssstyle.indexOf("}")==-1) {cssstyle = (cssstyle+";}").replace(/\;\;/g,";")}
					if (important) {
						cssstyle = (cssstyle).replace(/\!important/g,"").replace(/\;/g,"!important;")
					}
console.log("cssstyle",cssstyle)

err=13
					newcss.appendChild(document.createTextNode(selector + cssstyle+"\n"));
err=14
				}
				return newcss
			}

		}catch(e){try{df.debug.debugwin({title:"df.addcss",color:"red",message:e.description+"<br>"+e.message+"<br>cssobj="+df.json.serialize(cssobj)+"<br>err="+err})}catch(e){}}
	}
	,
//jsDoc	- code.google.com/p/jsdoc-toolkit/wiki/TagReference
/*************************************
@function	: domstyle
@description	: Takes a string of CSS and applies them to a dom node via script. It converts style-format to camelCase.
@author		: Damien Otis
@returns	: undefined
@requires	: n/a
@aliases	: n/a
@alias		: false
@argument	: id String, style String
@example	:

	dhtmlfx.newdom({
		tagname	: "div",
		attr	: {
			id	: "mydiv"
		}
	})

	dhtmlfx.domstyle(mydiv,"position:absolute;left:30px;top:100px;width:100px;height:100px;border:1px solid black;background-color:#ff0000")

	dhtmlfx.domstyle("domid","font-size:20px;font-weight:700;color:white;")

*/
	domstyle:	function(id,style) {
		var df = dhtmlfx

		var thisid = df.testid(id)
		var newdiv = df.dg(thisid)

		var stylearr=style.split(";")
		for (var i=0;i<stylearr.length;i++) {
			var thisstyle=stylearr[i].split(":")
			var stylecamel = thisstyle[0].split("-")
			var camelname=""
			for (var ic=0;ic<stylecamel.length;ic++) {
				if (ic>0) {
					camelname+=stylecamel[ic].substr(0,1).toUpperCase()+stylecamel[ic].substr(1)
				} else {
					camelname+=stylecamel[ic]
				}
			}
			newdiv.style[camelname] = thisstyle[1]
//					df.debug.debugwin({title:camelname,message:thisstyle[1]})
		}
	}
	,

	wipeallCSS:	function(domobj,getall,cssmods){
		var df = dhtmlfx
		df.wipeCSS(domobj,getall,cssmods,callback)
		var doms = domobj.getElementsByTagName("*")
		for (var i=0;i<doms.length;i++) {
			df.wipeCSS(doms[i],getall,cssmods,callback)
		}
	}
	,

/*************************************
@function	: wipeallCSS
@description	: removes all computed css styles from a DOM object. returns an object, with array of default css style keys pairs 'defaults',and an array of css style key pairs 'styles' which are the styles before wiping out the computed styles
@author		: Damien Otis
@returns	: {csstext:csstext,defaults:defaults,styles:styles,primary:primary,secondary:secondary,tertiary:tertiary}
@requires	: n/a
@aliases	: n/a
@alias		: false
@argument	: domobj HtmlElement , getall , cssmods , callback
@example	:

	dhtmlfx.wipeCSS(HtmlElement,true,"position:absolute",)

//probably needs to support argument as an object
	df.wipeCSS({
		target	: menudoms[i],
		alldoms	: true,
		style	: "position:relative;top:0px;left:0px;float:left;margin:20px;padding:10px;display:block;cursor:pointer;font-size:14px;height:18px;font-family:arial;clear:both;text-align:center;color:black;text-decoration:none;",
		hover	: "",
		clicked	: "",
		etc	: "..."
	})
*/

	wipeCSS: function(domobj,getall,cssmods) {
		var df = dhtmlfx
//console.log("---------------------------------------------------------------------------------------")
//console.log("WIPECSS",domobj,getall,cssmods)
		var width_height = (df.browser=="ff") ? "-moz-max-content" : (df.browser=="ie")?"auto":"intrinsic"
		var webkit = (df.browser=="safari"||df.browser=="chrome")?true:false
		var timeout=0

		try{clearTimeout(df.wipeCSSgc_tmr)}catch(e){}

		if (df.wipeCSSiframe==undefined) {
			df.wipeCSSiframe = df.newdom({tagname:"iframe",attributes:{src:"about:blank",style:"position:absolute;top:-400px;left:-400px;width:200px;height:200px;"}})
		}
		if (df.wipeCSSifmbody==undefined) {
			df.wipeCSSifmbody = (df.browser=="ie") ? df.wipeCSSiframe.contentWindow.document.appendChild(df.wipeCSSiframe.contentWindow.document.createElement("body")) : df.wipeCSSiframe.contentDocument.body
		}

		var defaults , styles , cssmodsobj = {}

//console.log("domobj",domobj)

		//add existing styles on DOM element so we don't over-write them
		if (df.browser=="ie") {
			var domstyles = domobj.style.getAttribute("cssText");
		} else {
			var domstyles = domobj.getAttribute("style");
		}
		if (domstyles==null) {domstyles=""}

//console.log("domstyles ",domstyles)

//		if (cssmods!=undefined) {
			var cssmodsplit = (domstyles+';'+(cssmods||"")).split(";")//"cursor:pointer;display:block;float:none;font-size:16px;font-family:arial;clear:both;text-align:left;margin:5px;color:black;text-decoration:none;"
			for (var i=0;i<cssmodsplit.length;i++){
				var stylekey = df.trim(cssmodsplit[i].split(":")[0])
				var styleval = df.trim(cssmodsplit[i].split(":")[1]).replace(/important/g,"").replace(/\!/g,"")	//we'll add !important back later anyway
				if (stylekey=="") {continue}
//console.log("cssmodsobj",stylekey,styleval)
				cssmodsobj[stylekey] = styleval
			}
//		}
//console.log('cssmodsobj',df.getNewObj(cssmodsobj))
//		do{//it's a dangerous game we play
//			df.wipeCSSifmbody.innerHTML="done"
//			timeout++
//		} while (df.wipeCSSifmbody.innerHTML!="done"&&timeout<1000)
//		if (timeout>=100) {return}

//		df.waitfor(function(){if (df.wipeCSSinit==true) {return true};df.wipeCSSifmbody.innerHTML="done";if (df.wipeCSSifmbody.innerHTML=="done"){return true}else{return false}},function(){
			df.wipeCSSinit = true
			df.wipeCSSifmbody.innerHTML=""

			//if (df.wipeCSSiefrmbody==undefined && df.browser=="ie") {
			//	df.wipeCSSifmbody = df.wipeCSSiefrmbody = df.wipeCSSifmbody.appendChild(df.wipeCSSifmbody.createElement('body'))
			//}

			var objobj = { tagname:domobj.tagName , target:df.wipeCSSifmbody }
			if (domobj.getAttribute("type")!=null) {objobj.attributes={type:domobj.getAttribute("type")}}
			var defaultdom = df.newdom( objobj )
//console.log("df.wipeCSSiframe",df.wipeCSSiframe)
//console.log("df.wipeCSSifmbody",df.wipeCSSifmbody)
//console.log("defaultdom",defaultdom)

			defaults = df.getComputedStyles( defaultdom , true )

			styles = df.getComputedStyles( domobj , true )

			for (var selector in styles){
				if (selector.substr(0,6)=="border"&&selector!="border"){
					if (defaults[selector]==styles[selector]) {
						delete styles[selector]
					}
				}
			}
			for (var selector in defaults){
				if (selector.substr(0,6)=="border"){delete defaults[selector]}
			}

			var starr=[]

//console.log("defaults=",df.json.serialize(df.getNewObj(defaults)))
//console.log("styles=",df.json.serialize(df.getNewObj(styles)))
//console.log("cssmodsobj=",df.json.serialize(df.getNewObj(cssmodsobj)))
//console.log("defaults",df.json.serialize(defaults))

			var primary = []
			var secondary = []
			var tertiary = []

			for (var sx in cssmodsobj) {
				defaults[sx] = cssmodsobj[sx].replace(/important/g,"").replace(/\!/g,"")
			}

			for ( var sx in defaults ) {
				if (sx=="undefined"||sx=="null"||sx==undefined) {continue}
//console.log("test",getall==true , defaults[sx]!=styles[sx] , cssmodsobj[sx]!=undefined)
				if (getall==true || defaults[sx]!=styles[sx] || cssmodsobj[sx]!=undefined ) {
					if (cssmodsobj[sx]!=undefined) {
						defaults[sx] = cssmodsobj[sx].replace(/important/g,"").replace(/\!/g,"")
					} else if (sx=="width") {
						defaults[sx] = width_height;
					} else if (sx=="height") {
						defaults[sx] = "auto";
					}

					if (webkit) {
						if (sx=="-webkit-text-fill-color"){//&&styles.color!=undefined
							continue
						}
					}

//console.log("defaults[sx] ",sx,' ',defaults[sx])

					if (cssmodsobj[sx]!=undefined) {
//console.log("primary",sx,':',defaults[sx])
						primary.push( sx + ":" + defaults[sx] ) //defaults[sx] is correct, it gets replaced above
					} else 	if (styles[sx]!=undefined && (styles[sx])!=defaults[sx]) {
//console.log("    secondary",sx,':',defaults[sx]," - ",styles[sx])
						secondary.push( sx + ":" + defaults[sx] )
					} else {
//console.log("        tertiary",sx,':',defaults[sx])
						tertiary.push( sx + ":" + defaults[sx] )  //always have to have all defaults or hover css and such will disrupt styles
					}
				} else {
					delete defaults[sx]
				}
			}

//			if (df.browser=="safari"||df.browser=="chrome") {
//				var cssarr = tertiary.concat(secondary,primary)
//				var cssarr = primary.concat(secondary,tertiary)
//			} else {
				var cssarr = tertiary.concat(secondary,primary)
//			}

			var csstext = cssarr.join("!important;")+"!important;"

//console.log('csstext  ',csstext )

			if (df.browser=="ie") {
				domobj.style.setAttribute("cssText", csstext);
			} else {
				domobj.setAttribute("style",csstext );
			}
//		})
//console.log("defaults mixed",df.json.serialize(defaults))
		//remove the iframe after 200 milliseconds.
		//this leaves the iframe element open for mutliple concurrent calls to wipeCSS.
		//after 200ms, the iframe is removed from the DOM
		df.wipeCSSgc_tmr = setTimeout(function(){
			delete df.wipeCSSifmbody;
			delete df.wipeCSSinit
			df.removedom(df.wipeCSSiframe);
			delete df.wipeCSSiframe;
		},200)

		return {csstext:csstext,defaults:defaults,styles:styles,primary:primary,secondary:secondary,tertiary:tertiary}

	},
////df.setcssText(thisdf.targetobj,stylename,"#"+thisdfstyle.currentval,true)
	isCSSImportant:	function(domobj,stylename){
		df.getComputedStyles(domobj,stylename)



	},

//-----------------------------------------------

	getComputedStyle:	function(domobj,stylename) {
		var cssarr = dhtmlfx.getComputedStyles(domobj,false)
		for (var selector in cssarr) {
			if (selector == stylename) {
				return cssarr[selector]
			}
		}
		return ""
	},

	getComputedStyles:	function(domobj,cameltostyle,selectmod) {
	try{

		var df = dhtmlfx
		var cstyle
		var instyle
		var webkit = (df.browser=="safari"||df.browser=="chrome"||df.browser=="webkit")


		if (df.browser=="ie") {
			cstyle = domobj.currentStyle		//document.defaultView
			instyle = domobj.style.getAttribute("cssText")
		} else {
			cstyle = (document.defaultView||domobj.ownerDocument.defaultView).getComputedStyle(domobj, (selectmod!=undefined)?selectmod:null ) //domobj.ownerDocument.defaultView.getComputedStyle(domobj,null)
			instyle = String(domobj.style.cssText)
		}

		var styleobj={}

		for (var selector in cstyle) {

			if (webkit) {
				if (isNaN(selector)) {continue}
				var styleval =  cstyle.getPropertyValue(String(cstyle[selector]))
				var thisselector = cstyle[selector]
			} else {
				if (!isNaN(selector)) {continue}
				var styleval = cstyle[selector]
				var thisselector = selector
			}
//console.log("selector ",cstyle[selector]," value ",cstyle.getPropertyValue(String(cstyle[selector])) )

			if (styleval==null || styleval=="" || df.objType(styleval)=="function" ) {continue}


//			if (thisselector.toLowerCase()=="font") {continue}
//			if (thisselector.toLowerCase()=="length") {continue}
//			if (thisselector.toLowerCase()=="quotes") {continue}
//			if (thisselector=="cssFloat"||thisselector=="styleFloat"){
//				thisselector="float"
//			}

			styleobj[ (cameltostyle==true) ? df.camelToStyle(thisselector) : thisselector  ] = (webkit==true) ? cstyle.getPropertyValue(thisselector) : styleval

		}


		if (instyle!="") {
			var instylearr = String(instyle).split(";")
			for (var i=0;i<instylearr.length;i++) {
				if (instylearr[i]=="") {continue}
				var fixstyle = instylearr[i].split(":")
				styleobj[ (cameltostyle==true) ? df.trim(fixstyle[0]) : df.styleToCamel(df.trim(fixstyle[0])) ] = df.trim(fixstyle[1])
			}
		}


		return styleobj

	}catch(e){df.debug.debugwin({title:"df.getComputedStyles",message:e.description+"<br>"+e.message,color:"red"})}
	},

//-----------------------------------------------

	camelToStyle:	function(camelcase) {
		return String(String(camelcase).replace(/([A-Z])/g,"-$1")).toLowerCase()
	},

//-----------------------------------------------

	styleToCamel:	function(style) {
try{
		if (style==""||style==undefined||style==null) {return}
		do {
			var matches = style.match(/-([a-zA-Z]{1,1})/)
			if (matches!=null) {
				style = style.replace(matches[0],String(matches[1]).toUpperCase())
			}
		} while (matches!=null)

		var fixfirst = style.split("")
		fixfirst[0]=fixfirst[0].toLowerCase()
		style = fixfirst.join("")

		return style
}catch(e){
console.log(e,"style=",style,"fixfirst=",fixfirst)
}
	},

//-----------------------------------------------

	computedstyle:	function(domobj,style) {
//	try{
		var df = dhtmlfx
		var cstyle
		if (df.browser=="ie") {
			if (style=="opacity") {
				return parseInt(domobj.currentStyle.filter.split("opacity=")[1].split(")")[0],10)||100
			} else {
				cstyle = domobj.currentStyle||{}
			}
		} else {
			cstyle = document.defaultView.getComputedStyle(domobj,null)||{}

			if (style=="opacity") {
				return parseFloat(cstyle[style])*100
			}
		}
		return cstyle[style]
//	}catch(e){df.debug.debugwin({title:"df.computedstyle",message:e.description+"<br>"+e.message,color:"red"})}
	}
	,

//=========================================================================================
//=========================================================================================
//colors

	//finds the background color of an object, even if it has none, goes up the tree up to HTML and if no background color is set, then it returns 'ffffff'
	absoluteBgColor:	function(thisdom) {

		do {
			var bgcolor = df.computedstyle(thisdom,"backgroundColor")

			if (bgcolor!="transparent") {break}

			thisdom = thisdom.parentNode

		} while (thisdom.tagName.toLowerCase()!="html")

		if (bgcolor=="transparent") {bgcolor="ffffff"}

		return df.hexcolor(bgcolor)

	},

//=====================================================================================
//DOM manipulation
	pathToDOM:	function(path) {
		try{var pathsplit = path.split(".")}catch(e){return ""}
		var thisobj = window
		for (var i=1;i<pathsplit.length;i++) {
			try{thisobj = thisobj[pathsplit[i]]}catch(e){}
		}
		return (thisobj||"")
	},

	removedom:	function(thisnode) {
	try{

//possibly add event detachment here
		var df = dhtmlfx
		try{if (thisnode==undefined) {thisnode = df.mouse.dragobj}}catch(e){return}
		if (thisnode==undefined||thisnode==null) {return}
		var thisid = df.testid(thisnode)
		thisnode = df.dg(thisid);
		if (df.domcaching) {
			df.domcache_clear(df.domcache[thisid])
			delete df.domcache[thisid]
		}
		try{
			thisnode.removeNode(true);
		}catch(e){
			try{thisnode.parentNode.removeChild(thisnode)}catch(e){}
		}

		//if (df.browser=="ie") {
		//	try{thisdomobject.removeNode(true)}catch(e){}
		//} else {
		//	try{thisdomobject.parentNode.removeChild(thisdomobject)}catch(e){}
		//}
	}catch(e){df.debug.debugwin({title:"df.removedom",message:e.description+"<br>"+e.message,color:"red"})}
	},

	currentwindowheight:0,
	windowheight	:	function() {
		var df = dhtmlfx
		return df.currentwindowheight
//		return document.documentElement.clientHeight||window.document.body.offsetHeight
	},
	getwindowheight:	function() {
		var df = dhtmlfx
		try{
			df.currentwindowheight = (document.documentElement.clientHeight||window.document.body.offsetHeight)
		}catch(e){
			df.currentwindowheight = document.documentElement.offsetHeight //IE5.5
		}
		return df.currentwindowheight
	},
	currentwindowwidth:0,
	windowwidth	:	function() {
		var df = dhtmlfx
		return df.currentwindowwidth
		//return document.documentElement.clientWidth||window.document.body.offsetWidth
	},
	getwindowwidth:	function() {
		var df = dhtmlfx
		try{
			df.currentwindowwidth = (document.documentElement.clientWidth||window.document.body.offsetWidth)
		}catch(e){
			df.currentwindowwidth = document.documentElement.offsetWidth //IE5.5
		}
		return df.currentwindowwidth
	}
	,
	scrolltop	:	function() {return (document.body.scrollTop||document.documentElement.scrollTop)}
	,
	scrollleft	:	function() {return (document.body.scrollLeft||document.documentElement.scrollLeft)}
	,

	//this has been depreciated in favor of df.attachevent, references not yet removed from codebase
	addevent:	function(evttype,domobj,callback) {
	try{
		var df = dhtmlfx
		var thisdom = df.dg(df.testid(domobj))

		try{
			document.addEventListener(evttype,callback,false)
		}catch(e){
			document.attachEvent("on"+evttype,callback)
		}
	}catch(e){df.debug.debugwin({title:"df.addevent",message:e.description+"<br>"+e.message,color:"red"})}
	},

	loadscript:	function(url) {
		var thishead = document.getElementsByTagName("head")[0]
		var newobj = document.createElement("script");
		newobj.setAttribute("type","text/javascript");
		newobj.src=url
		thishead.appendChild(newobj);
	},

	dependancy:	function(loadedobject) {
		var df = dhtmlfx
		var dependancies = loadedobject.requires
		for (var i=0;i<dependancies.length;i++) {
			var thislibrary = scriptpath+"\\"+dependancies[i]+".js"
			df.loadscript(thislibrary)
		}
	},

	preload		:	function(filename) {
		var df = dhtmlfx
		if (df.preloadimages[filename]==undefined) {
		//	df.preloadimages[filename]=new Image();
		//	df.preloadimages[filename].src=filename;
			setTimeout(function(){df.preloadimages[filename]=new Image();df.preloadimages[filename].src=filename},10);
		}
	},

	autoselect : function (obj) {
		var df = dhtmlfx
		try{
		if (obj && (obj.tagName=="TEXTAREA"||(obj.tagName=="INPUT"&&String(obj.type).toLowerCase()=="text"))) {obj.select();return}
		if (df.browser!="ie") {
			var sel = window.getSelection();
			var range = document.createRange();
			range.selectNodeContents(obj);
			sel.removeAllRanges();
			sel.addRange(range);
		} else { // IE
			document.selection.empty();
			var range = document.body.createTextRange();
			range.moveToElementText(obj);
			range.select();
		}
		}catch(e){alert("autoselect:"+e.message)}
	}
//javascript:alert(df.json.serialize(document.getElementsByTagName("head")[0].childNodes[22].childNodes[1],true))
	,

	getfunctionname:	function(thisfunc) {
		try{return thisfunc.toString().match(/function\s.*?\(/g)[0].substr(9).replace(/\(/g,"")}catch(e){return thisfunc}
	},

	browsers: {
		"chromeframe":{
			browser:"chrome",
			rendering:"webkit",
			jsengine:"v8"
		},
		"msie":{
			browser:"ie",
			rendering:"trident",
			jsengine:"jscript"
		},
		"trident":{
			browser:"ie",
			rendering:"trident",
			jsengine:"jscript"
		},
		"firefox":{
			browser:"ff",
			rendering:"gecko",
			jsengine:"spidermonkey"
		},
		"seamonkey":{
			browser:"ff",
			rendering:"gecko",
			jsengine:"spidermonkey"
		},
		"khtml":{
			browser:"safari",
			rendering:"webkit",
			jsengine:"squirrelfish"
		},
		"gecko":{
			browser:"ff",
			rendering:"gecko",
			jsengine:"spidermonkey"
		},
		"minefield":{
			browser:"ff",
			rendering:"gecko",
			jsengine:"spidermonkey"
		},
		"iceweasel":{
			browser:"ff",
			rendering:"gecko",
			jsengine:"spidermonkey"
		},
		"shiretoko":{
			browser:"ff",
			rendering:"gecko",
			jsengine:"spidermonkey"
		},
		"safari":{
			browser:"safari",
			rendering:"webkit",
			jsengine:"squirrelfish"
		},
		"opera":{
			browser:"opera",
			rendering:"opera",
			jsengine:"opera"
		},
		"chrome":{
			browser:"chrome",
			rendering:"webkit",
			jsengine:"v8"
		}
	},

	capabilities:{
		'max_url_length':'mul'
	},
	capability:	function(capability) {
		var df = dhtmlfx
		return df.capabilities[df.browser][df.platform][String(df.bver).replace(/\./g,"_")][df.capabilities[capability]]
	},

	capabilities:	{
		"ie":{
			desktop:{
				"5_5":{mul:2048},
				"6":{mul:2048},
				"7":{mul:2048},
				"8":{mul:2048}
			},
			mobile:{
				"5":{mul:2048},
				"6":{mul:2048},
				"7":{mul:2048}
			}
		},

		"ff":{
			desktop:{
				"1_2":{mul:14000},
				"1_3":{mul:14000},
				"1_4":{mul:14000},
				"2_0":{mul:14000},
				"3_0":{mul:14000},
				"3_1":{mul:14000},
				"3_5":{mul:14000}
			},
			mobile:{
			}
		},
		"gecko":{
			desktop:{
				"1_2":{mul:14000},
				"1_3":{mul:14000},
				"1_4":{mul:14000},
				"2_0":{mul:14000},
				"3_0":{mul:14000},
				"3_1":{mul:14000},
				"3_5":{mul:14000}
			},
			mobile:{
			}
		},

		"safari":	{
			desktop:{
				"2_0":{mul:14000},
				"3_0":{mul:14000},
				"3_1":{mul:14000},
				"4_0":{mul:14000}
			},
			mobile:{
			}
		},

		"chromeframe":	{
			desktop:{
				"1":{mul:14000},
				"2":{mul:14000}
			},
			mobile:{
				"1":{mul:14000},
				"2":{mul:14000}
			}
		},

		"chrome":	{
			desktop:{
				"1":{mul:14000},
				"2":{mul:14000}
			},
			mobile:{
				"1":{mul:14000},
				"2":{mul:14000}
			}
		},

		"opera":	{
			desktop:{
				"8":{mul:14000},
				"9":{mul:14000},
				"10":{mul:14000}
			},
			mobile:{
				"8":{mul:14000},
				"9":{mul:14000},
				"10":{mul:14000}
			}
		}
	},

	fps	: function(fps){

		df.current_fps = fps;

		dhtmlfx.period = Math.ceil(dhtmlfx.periodfloat = 1000/fps)
	},

	init:	function() {
		var df = dhtmlfx

		df.attachevent({domobj:window,name:"resize",handler:function(){df.getwindowwidth();df.getwindowheight()}});

		df.fps(df.current_fps||30)

//set window width and height, this is tracked by an onresize window event
		df.getwindowwidth()
		df.getwindowheight()

//detect browser
		df.navigator = String(navigator.userAgent).toLowerCase()

		for (var selector in df.browsers) {
			if (df.navigator.indexOf(selector)!=-1) {
				df.browser = String(df.browsers[selector].browser)
				break;
			}
		}

		if (df.browser=="ff") {
			df.bver = parseFloat(df.navigator.match(/firefox\/[0-9]{1,}\.[0-9]{1,}/)[0].replace(/[^0-9\.]/g,""))   //parseFloat(df.navigator.match(/firefox\/[0-9\.]*\D/)[0].replace(/[^0-9\.]/g,""))
			if (df.navigator.indexOf("fennec")!=-1) {df.capability = df.mobile}
			return
		}

		if (df.browser=="ie") {
			var bver = ((df.navigator.match(/msie\s[0-9\.]*\D/)||[])[0]||"").replace(/[^0-9\.]/g,"") || ((df.navigator.match(/rv\:(.*?)[^0-9]/)||[])[1]||"").replace(/[^0-9\.]/g,"")
			df.bver = parseFloat(bver || 0)
			if (df.navigator.indexOf("mobile")!=-1) {df.capability = df.mobile}
			if (df.bver<7) {try{document.execCommand('BackgroundImageCache',false,true)}catch(e){}}
			return
		}

		if (df.browser=="opera") {
			df.bver = parseFloat(df.navigator.match(/opera\/[0-9\.]*\D/)[0].replace(/[^0-9\.]/g,""))
			if (df.navigator.indexOf("mobi")!=-1) {df.capability = df.mobile}
			return
		}

		if (df.browser=="safari") {
			if (df.navigator.indexOf("mobile")!=-1) {df.touch=true}
			if (df.navigator.toLowerCase().indexOf("phantom")!=-1) {
				df.bver = phantom.version.major
				return
			}
			df.bver = parseFloat(((df.navigator.match(/version\/[0-9\.]*\D/)||[])[0]||"0").replace(/[^0-9\.]/g,"").replace(/\./,"x").replace(/\./,"").replace(/\x/,"."))
			if (df.navigator.indexOf("iphone")!=-1) {df.capability = df.mobile}
			return
		}

		if (df.browser=="chrome") {
			try{
				df.bver = parseFloat(df.navigator.match(/chrome\/[0-9\.]*\D/)[0].replace(/[^0-9\.]/g,"").replace(/\./,"x").replace(/\./,"").replace(/\x/,"."))
			}catch(e){
				//chromeframe needs its own thing here.. not shure what yet. maybe its too early to know.
				df.bver = 3
			}
			return
		}

		//assume webkit if browser not detected. webkit is most likely to be ported to other platforms.
		df.browser="safari"
		df.bver = 3
		df.unknown_browser = true


	},

	noselectHTML: ' onmouseover="this.style.MozUserSelect=\'-moz-none\';if (this.style.cursor==\'\'||this.style.cursor==\'default\') {this.style.cursor=\'default\'};" onselectstart="return false" unselectable="on" ',
	noselectDOM: function(thisdom,children) {
	try{
		var df = dhtmlfx
		var cursorcomputedstyle = df.computedstyle(thisdom,"cursor")
		if (cursorcomputedstyle == "text") {

			if (df.browser=="ff") {
				thisdom.style.MozUserSelect="text"
			}

			return
		}

		if (df.browser=="ff") {
			thisdom.style.MozUserSelect="-moz-none"
		} else {
			try{thisdom.style.webkitUserSelect = "none"}catch(e){}
		}

		df.attachevent({name:"selectstart",domobj:thisdom,handler:df.returnfalse})
//		try{thisdom.addEventListener("selectstart",df.returnfalse,false)}catch(e){thisdom.attachEvent("onselectstart",df.returnfalse)}

		if (df.browser=="ie") {
			thisdom.setAttribute("unselectable","on")
		}

		if (cursorcomputedstyle!="pointer"&&thisdom.style.cursor!="pointer") {
			thisdom.style.cursor="default"
		}

		if (children==true) {
			var doms = thisdom.getElementsByTagName("*")
			for (var i=0;i<doms.length;i++) {
				df.noselectDOM(doms[i])
			}
		}

	}catch(e){df.debug.debugwin({title:"df.noselectDOM",message:e.description+"<br>"+e.message,color:"red"})}
	},
	returnfalse:function(){return false},

	decimalpoints:	function(number,decpoints) {

		var thisnum = String(number)
		var decp = thisnum.indexOf(".")+1
		if (decp==0) {return parseFloat(number)}
		return parseFloat( thisnum.substr(0,decp+decpoints) )

	},

	numbercommas:	function(number) {

		var numbercomma = String(number)
		var numarr=[]
		var comidx=0
		for (var i=numbercomma.length-1;i>0;i--) {
			numarr.push(numbercomma[i])
			comidx++
			if (comidx==3) {numarr.push(',');comidx=0}
		}

		return numbercomma
	},

	//call this to turn and object argument into local function variables
	// example:
	//	function test(){
	//		df.objvars(this,arguments)
	//
	//		console.log(var1,var2)
	//
	//		return var1+" "+var2
	//	}
	//
	//	test({var1:"hello",var2:"world"})
	//
	//
	//> hello world
	//
	objvars:	function(thisthis,arguments) {
		if (arguments.length==1 && df.objType(arguments[0])=="object"){
			for (var selector in arguments[0]) {
				thisthis[selector] = arguments[0][selector]
			}
		}
	},

	getdomain:	function(thislocation) {
	try{
		if (thislocation==undefined) {var thislocation = String(location)}
		var thisdomain = String(thislocation).split("//")[1].split("/")[0].split(".")//.slice(1).join(".")
		var returndomain = thisdomain.slice(thisdomain.length-2).join(".")
		return returndomain

	}catch(e){return ""}
	},

	topzindex2:	function(tagname) {
		return (parseInt(df.zindexorder(tagname).pop()||{}.zindex||1,10)+1)||0
	},

	topzindex:	function(tagname) {
		var df = dhtmlfx
		df.objvars(this,arguments)
		var thiszindex = df.zindexorder(tagname).pop()
		if (thiszindex==undefined) {
			return 0
		} else {
			return parseInt(thiszindex.zindex,10)
		}
	},

	zindexorder:	function(tagname){
		var df = dhtmlfx
		df.objvars(this,arguments)
		if (tagname==undefined) {tagname="*"}
		var doms = document.getElementsByTagName(tagname)
		var domlen = doms.length
		var zindexes = []
		var topmost = 0
		for (var i=0;i<domlen;i++) {
			var thisz = df.computedstyle(doms[i],"zIndex")
			if (thisz!="auto") {
				if (String(thisz).indexOf("e")!=-1) {
					thisz = topmost+10
					doms[i].style.zIndex = thisz
					topmost = parseInt(thisz,10)||topmost
				}
			}

			if (thisz!=""&&thisz!="auto") {
				var thisdom = df.testid(doms[i])
				zindexes.push({domid:thisdom,zindex:thisz})
				topmost = Math.max(thisz,topmost)
			}
		}
		var zindexes = df.sortobject(zindexes,"zindex","asc")
		return zindexes
	},


	sortobject:	function(thisarray,objectpath,newdir) {
	//sorts an array of object literals, based on the objectpath input
	//newdir== "asc" or "desc"

		var d1=1;var d2=-1;
		if (newdir=="desc") {d1=-1;d2=1}
		var thispath = objectpath.split('.');
		thisarray.sort(function(a, b) {
				var asrc = a;
				var bsrc = b;
				for (var i=0;i<thispath.length;i++) {
					asrc = asrc[thispath[i]];
					bsrc = bsrc[thispath[i]];
				}
				var srca = parseFloat(asrc)||asrc;
				var srcb = parseFloat(bsrc)||bsrc;

				if((srca===undefined||srca==="")&&(srcb!=undefined&&srcb!="")){return d1};
				if((srcb===undefined||srcb==="")&&(srca!=undefined&&srca!="")){return d2};

				if(srca>srcb){return d1};
				if(srca<srcb){return d2};

				return 0;
			}
		);

		return thisarray
	},

	sortKeys:	function(thisobj,sortfunc) {

		var newobjarr = {}
		var objkeys = []
		for (var selector in thisobj) {
			objkeys.push(selector)
		}

		if (sortfunc) {
			objkeys.sort(sortfunc)
		} else {
			objkeys.sort()
		}

		for (var i=0;i<objkeys.length;i++) {
			newobjarr[objkeys[i]] = thisobj[objkeys[i]]
		}

		return newobjarr
	},

	cleardom:	function(thisdom) {
		var df = window.dhtmlfx
		var thisid = df.testid(thisdom)
		df.dg(thisid).innerHTML = ''
		if (df.domcaching) {
			df.domcache_garbageCollect()
		}
	},

//=========================================================================
	attachevent:	function(eventobj) {//  df.attachevent({name:"mousedown",handler:someobject.somefunction,domobj:df.dg("someelement")})
	try{
	//event name is either a string or an array, use an array to attach multiple events to the same handler

		var df = dhtmlfx

		if (eventobj.name==undefined) {return}
		if (eventobj.handler==undefined) {return}

		//if (eventobj.domobj==undefined){eventobj.domobj=eventobj.thisdom||window}

		eventobj.domobj = eventobj.domobj||eventobj.target||eventobj.thisdom||window||document

		if (df.objtype(eventobj.domobj)=="string") {
			eventobj.domobj = df.dg(df.testid(eventobj.domobj))
		}

		if (df.objtype(eventobj.name)=="string") {
			var thisname = eventobj.name
			eventobj.name = [thisname]
		}

		var gohandle = function (evt) {
			evt.srcdata	= eventobj;
			evt.src		= eventobj.domobj
			evt.srcdom	= evt.srcElement||evt.target||undefined;
			eventobj.handler(evt)
		}


		for (var i=0;i<eventobj.name.length;i++) {

			var thisname = eventobj.name[i]

			if (thisname=="mousewheel"){
				thisname = (df.browser=="safari"||df.browser=="opera"||df.browser=="chrome")?"mousewheel":"DOMMouseScroll"
				if (df.browser=="ie") {thisname = "mousewheel"}
			}


			if (df.browser=="ie") {
				eventobj.domobj.attachEvent("on"+thisname,	gohandle)
			} else {
				try{
					eventobj.domobj.addEventListener(thisname,		gohandle,eventobj.bubble||false)
				}catch(e){
					try{
						eventobj.domobj.attachEvent("on"+thisname,		gohandle)
					}catch(e){
						return false
					}
				}
			}
		}

		return true
	}catch(e){df.debug.debugwin({title:"df.attachevent",message:e.description+"<br>"+e.message,color:"red"})}
	},

	killevent	: function(evt) {
	try{
		var df = dhtmlfx
		df.eventCancelBubble(evt)
		if (window.event) {window.event.returnValue = false}
		if (evt.preventDefault) {evt.preventDefault()}
		if (evt.preventCapture) {evt.preventCapture()}
		evt.stopped = true
	}catch(e){df.debug.debugwin({title:"df.killevent",message:e.description+"<br>"+e.message,color:"red"})}
	},
	eventCancelBubble:	function(evt) {
		if (window.event&&window.event.cancelBubble) {window.event.cancelBubble=true}
		if (evt.preventBubble) {evt.preventBubble()}
		if (evt.stopPropagation) {evt.stopPropagation()}
	},
//=========================================================================

/*
//this code is probably going to be replaced by the new dom caching scheme
	enabledomcaching:	function() {
	//experimental and will probably be removed
	//problem: it does not work if DOM elements are removed or rewritten
	try{
		var df = dhtmlfx
		df.dg = function(thisid,nocache) {
		try{
			//Private function
			if (thisid=="") {
				try{df.debug.debugwin({title:"no dom id from:",message:df.debug.getcaller(df.dg.caller),color:"red"})}catch(e){}
				return null
			}
			var getnewdom = function(thisid){
				if (nocache==true) {
					var thisdom = document.getElementById(thisid)
				} else {
					var thisdom = document.getElementById(thisid)
					if (thisdom!=null) { df.domcache[thisid] = thisdom }
				}
				return thisdom
			}

			var finddom = df.domcache[thisid]

			if (finddom!=undefined) {
				if (finddom.parentNode==null) {
//				df.debug.debugwin({title:"thisid",message:thisid,color:"orange"})
					return getnewdom(thisid)
//					var thisdom = df.domcache[thisid] = document.getElementById(thisid)
//					return thisdom

				} else {
					return finddom
				}
			} else {
				try{
					return getnewdom(thisid)
					//var thisdom = df.domcache[thisid] = document.getElementById(thisid)
					//return thisdom
				}catch(e){
					//try{delete df.domcache[thisid]}catch(e){}
					return null
				}
			}


		}catch(e){try{df.debug.debugwin({title:"df.dg.enabledomcaching",message:e.description+"<br>"+e.message,color:"red"})}catch(e){}}
		}


	}catch(e){df.debug.debugwin({title:"enabledomcaching",message:e.description+"<br>"+e.message,color:"red"})}
	},
*/

	getquerystring:	function(qs) {
		if (qs==undefined) {
			var qs = String(location).split("?")[1]
		}
		if (qs==undefined) {return {}}
		var qso = qs.split("&")

		if (qso.length==0) {return {}}

		qsobject = {}

		for (var i=0;i<qso.length;i++) {
			qsobject[qso[i].split("=")[0]] = qso[i].split("=")[1]
		}
		return qsobject
	},

	classdoms:	function(domobj,target) {
	try{
	//creates a tree of dom nodes (divs) one inside another that have a classname set for each element in the parent of the source domobj
	//so that a copy can be created of a dom node, with all the parent classes applied to it
		var classarr=[]
		domobj = domobj.parentNode
		do {
				if (df.browser=="ie"){
					var thisclass = domobj.className
				} else {
					var thisclass = domobj.getAttribute("class")
				}
				if (thisclass!=null&&thisclass!=undefined) {
					classarr.push({tagname:domobj.tagName,classname:thisclass})
				}


			domobj = domobj.parentNode
			if (domobj.tagName==undefined) {
				break
			}
			if (domobj.tagName.toLowerCase=="body") {break}
		} while (domobj.tagName.toLowerCase!="body")
		if (classarr.length>0) {
		try{var thisclass = classarr.pop()}catch(e){return {}}
		if (thisclass==undefined||thisclass==null||thisclass=="") {return {}}
			if (thisclass.tagname.toLowerCase()=="html"||thisclass.tagname.toLowerCase()=="body") {
				var thisclass = classarr.pop()
			}
			if (thisclass==undefined||thisclass==null||thisclass=="") {return {}}

			var firstdom = df.newdom({tagname:thisclass.tagname,target:target,attributes:{"class":thisclass.classname}})
			var lastdom = firstdom
			for (var i=classarr.length-1;i>-1;i--) {
				if (classarr[i].tagname.toLowerCase()=="body"||classarr[i].tagname.toLowerCase()=="html") {continue}
				if (classarr[i].classname=="") {continue}
				var newdom = df.newdom({tagname:classarr[i].tagname,target:lastdom,attributes:{"class":classarr[i].classname}})
				var lastdom = newdom
			}
		} else {
			return {}
		}

		return {outer:firstdom,inner:lastdom}
	}catch(e){df.debug.debugwin({title:"df.classdoms",message:e.description+"<br>"+e.message,color:"red"})}
	},

	parentelement:	function(domobj,tagname) {
	try{
		do {
			if (domobj.tagName.toLowerCase()==tagname.toLowerCase()) {
				return domobj
			}
			domobj = domobj.parentNode
			if (domobj==null||domobj==undefined) {return null}
			if (domobj.tagName.toLowerCase()=="body") {return null}
		} while (domobj!=null&&domobj!=undefined)
	}catch(e){df.debug.debugwin({title:"df.parentelement",message:e.description+"<br>"+e.message,color:"red"})}
	},

	cloneattributes:	function(domobj,targetobj) {
	try{
		for (var selector in domobj) {
			if (selector=="id"){continue}
			targetobj.setAttribute(selector,domobj[selector])
		}
	}catch(e){df.debug.debugwin({title:"df.cloneproperties",message:e.description+"<br>"+e.message,color:"red"})}
	},

//	waitfor:	function(functest,callback,frequency,retries,fail_callback,fail_data) {
	waitfor:	function(functest,callback,errorcount,failover,failmessage) {
//tests a function and if return is false it waits and tests again until it is true and then it executes the callback function
//df.waitfor(function(){return df.mouse!=undefined},function(){alert("df.mouse is loaded")})
		var df = dhtmlfx
		if (errorcount==undefined) {errorcount=0}
		try{
			if (functest()!=true) {
				window.chronos.setTimeout(function(){df.waitfor(functest,callback,errorcount)},100)
			} else {
				try{
					callback()
				}catch(e){df.debug.debugwin({title:"df.waitfor callback error",message:String(functest)+"<br>"+String(callback)+"\n"+e.description+"<br>"+e.message,color:"red"})}
			}
		} catch(e){
			errorcount++
			if (errorcount>100) {
				if (failover!=undefined) {try{failover(failmessage)}catch(e){}}
				try{df.debug.debugwin({title:"df.waitfor timeout",message:String(functest)+"<br>"+String(callback)+"\n"+e.description+"\n"+e.message})}catch(e){}
			} else {
				window.chronos.setTimeout(function(){df.waitfor(functest,callback,errorcount)},100)
			}
		}
	},

	time:{
		now:function(){return Date.parse(new Date())},

		time:function(dateobj){
			var now = 0
			now += (dateobj.centuries||0) * (100*365*24*60*60*1000)
			now += (dateobj.decades || 0) * (10*365*24*60*60*1000)
			now += (dateobj.years	|| 0) * (365*24*60*60*1000)
			//no months - which months?
			now += (dateobj.weeks	|| 0) * (7*24*60*60*1000)
			now += (dateobj.days	|| 0) * (24*60*60*1000)
			now += (dateobj.hours	|| 0) * (60*60*1000)
			now += (dateobj.minutes	|| 0) * (60*1000)
			now += (dateobj.seconds	|| 0) * (1000)
			now += (dateobj.ms	|| 0)
			return now
		}
	},

/*
	fortimer:	function(start,thislength,step,thisfunc,chunksize,callback,timeout) {
		start=start||0
		timeout=timeout||33
		chunksize=chunksize||1
		var resultarray=[]
		var thistmr = setInterval(function(){
			resultarray.push(thisfunc(thisvar))
			console.log(thisvar)
			thisvar+=step
			if ((step>0&&thisvar>=thislength)||(step<0&&thisvar<=thislength)) {
				clearInterval(thistmr)
				try{if (callback!=undefined) {callback(resultarray)}}catch(e){}
			}
		},timeout)
	},
*/
	addnamespace:	function(thisobject,objects) {
		for (var selector in objects) {
			thisobject[selector] = objects[selector]
		}
	},

	swapnodes:	function(domobj1,domobj2) {
		var domtemp = domobj1.cloneNode(1)
		var parent = domobj1.parentNode
		var parent2 = domobj2.parentNode;
		domobj2 = parent2.replaceChild(domtemp,domobj2)
		parent.replaceChild(domobj2,domobj1)
		parent2.replaceChild(domobj1,domtemp)
		domtemp = null
	},

	trim:	function(thistext) {
		return String(thistext).replace(/^\s{1,}/g,"").replace(/\s{1,}$/g,"")
	},

	domain_name:	String(location).split("/")[2].split(":")[0],

//--------------------------------------------------------------------------------------------------------------

	getElementsBy:	function(attribute,value,tagname,partial,parentobj) {
		if (parentobj==undefined) {parentobj = document}
		if (tagname==undefined) {tagname="*"}
		var doms = parentobj.getElementsByTagName(tagname)
		var domslen = doms.length
		var domarr=[]
		if (value!=undefined&&value!="*") {
			if (partial==true) {
				for (var i=0;i<domslen;i++) {
					var thisdom = doms[i]
					if (thisdom.getAttribute(attribute).indexOf(value)!=-1) {
						domarr.push(thisdom)
					}
				}
			} else {
				for (var i=0;i<domslen;i++) {
					var thisdom = doms[i]
					if (thisdom.getAttribute(attribute)==value) {
						domarr.push(thisdom)
					}
				}
			}
		} else {
			for (var i=0;i<domslen;i++) {
				var thisdom = doms[i]
				if (thisdom.getAttribute(attribute)!=null) {
					domarr.push(thisdom)
				}
			}
		}
		return domarr
	},

//--------------------------------------------------------------------------------------------------------------
	copyobject:	function(thisobj) {
		var newobj = {}
		for (var selector in thisobj) {
			var objtype = df.objtype(thisobj[selector])
			if (objtype=="object") {newobj[selector]=df.copyobject(thisobj[selector]);continue}
			if (objtype=="array") {newobj[selector]=df.copyarray(thisobj[selector]);continue}
			newobj[selector] = thisobj[selector]
		}
		return newobj
	},
//--------------------------------------------------------------------------------------------------------------
	copyarray:	function(thisarr) {
		var newarr=[]
		for (var i=0;i<thisarr.length;i++){
			newarr.push(thisarr[i])
		}
		return newarr
	},

//--------------------------------------------------------------------------------------------------------------

	objectLength:	function(thisobj) {
		var objlen = 0
		for (var selector in thisobj) {
			objlen++
		}
		return objlen
	},

//--------------------------------------------------------------------------------------------------------------

	/*
		tableToArray

		Converta a table element into an array of arrays (columns x rows)

		<table>
			<tr>
				<td>a</td><td>1</td>
			</tr>
			<tr>
				<td>b</td><td>2</td>
			</tr>
			<tr>
				<td>c</td><td>3</td>
			</tr>
			<tr>
				<td>d</td><td>4</td>
			</tr>
			<tr>
				<td>e</td><td>5</td>
			</tr>
		</table>

		-becomes-

		[["a","1"],["b","2"],["c","3"],["d","4"],["e","5"]]

	*/
	tableToArray:	function(thistab,trim,stripheader,striphtml) {

		var tab = []

		var rows = thistab.getElementsByTagName("tr")

		for (var ir=(stripheader)?1:0;ir<rows.length;ir++) {
			var thisrow		= rows[ir]
			var thistabarr	= tab[tab.push([])-1]
			var cols		= thisrow.getElementsByTagName("td")

			for (var ic=0;ic<cols.length;ic++) {
				var thiscell = cols[ic].innerHTML
				if (trim) {
					thiscell = thiscell.replace(/\s{1,}/g," ")
					thiscell = thiscell.replace(/^\s{1,}/g,"")
					thiscell = thiscell.replace(/\s{1,}$/g,"")
					thiscell = thiscell.replace(/[\r\n\t]/g,"")
				}
				if (striphtml) {
					thiscell = thiscell.replace(/\<.*?\>/g,"")
				}

				thistabarr.push(thiscell)
			}
		}

		return tab
	},

//---------------------------------------------------------
	getNewObj:	function (curobj) {
		if (curobj==undefined||curobj==null) {return curobj}
		var objtype = df.objType(curobj)
		if (curobj.tagName!=undefined) {return curobj}
		if (curobj==null||curobj==undefined) {return curobj}
		if (objtype=="object")		{return df.copyObject(curobj)}
		if (objtype=="array")		{return df.copyArray(curobj)}
		return curobj
	},

	copyObject:	function (thisobj) {
		var newobj = {}
		for (var selector in thisobj) {
			newobj[selector] = df.getNewObj(thisobj[selector])
		}
		return newobj
	},

	copyArray:	function (thisarray) {
		var newarr = []
		var arrlen = thisarray.length
		for (var i=0;i<arrlen;i++) {
			newarr.push(df.getNewObj(thisarray[i]))
		}
		return newarr
	},


	objType:	function (curobj) {
		if (curobj==null||curobj==undefined) {return "unknown"}
		var objtype = curobj.constructor || curobj.prototype
		objtype = (objtype==Array)?"array":(objtype==Function)?"function":(objtype==Object)?"object":typeof curobj||"unknown"
		if (objtype=="array"&&curobj.size!=undefined) {objtype="assoarray"}
		return objtype
	},
//---------------------------------------------------------
	arraymax:	function(array) {
		return Math.max.apply(Math , array)
	},
	arraymin:	function(array) {
		return Math.min.apply(Math , array)
	}


};


window.df=window.df||window.dhtmlfx;
window.DF=window.DF||window.dhtmlfx;

window.dhtmlfx.init();

//=========================================================

if (window.isWSH) {
	this.dhtmlfx = window.dhtmlfx
};


if (dhtmlfx.debug==undefined) {
	dhtmlfx.debug = {
		debugwin	:	function(err){
			if (window.isWSH){WScript.Echo("********ERROR******  > "+err.title+" - "+err.message)}
			if (window.console) {console.log(err)}
		}
	}
}


if (window.console==undefined) {
window.console = {
	"log":function(){},
	"warn":function(){},
	"debug":function(){},
	"error":function(){}
};
}

/*
WScript.Echo(dhtmlfx.gradient(["ff0000","0000ff"],200))
dhtmlfx.anim({styles:{height:{min:0,max:11200,steps:500,report:function(animobj){WScript.Echo(animobj.styles.height.currentval)},slope:dhtmlfx.smoothmove}}})
*/





//=======================================================================================================================
//chronos is included to test it's functionality. chronos tries to make timers work more smoothly. not sure if I keep it.
//animation seems to slow a bit under chronos. It's smooth but maybe more even in it's timer execution.
/** @license Copyright (c) 2011 Nick Fitzgerald
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * http://media.fitzgeraldnick.com/code-samples/chronos-test.html
 *
 */


window["chronos"] = (function () {


    // ## Variables and Utilities

    // Counter which keeps incrementing to give each task a unique id.
    var taskIdCounter = 0;

    // Store all active tasks in this object.
    var tasks = {};

    // Keep track of whether the main task runner has been initialized or not.
    var initialized = false;

    // The interval at which we check for tasks to run. It follows that the
    // smallest timeout interval you can give a task is this value.
    var INTERVAL = 50;

    // Return a function which calls `fn` as if `args` had been passed in as
    // arguments directly. Don't need to worry about return values because this
    // is only used asynchrnously, and don't need to worry about new arguments
    // because we know there will be no more.
    function curry (args, fn) {
        return args.length === 0
            ? fn
            : function () {
                fn.apply(null, args);
            };
    }

    var keys = typeof Object.keys === "function"
        ? Object.keys
        : function (obj) {
            var ks = [], k;
            for ( k in obj ) {
                if ( obj.hasOwnProperty(k) ) {
                    ks.push(k);
                }
            }
            return ks;
        };

    function slice(ary, n) {
        return Array.prototype.slice.call(ary, n);
    }

    // Round n to the nearest multiple of INTERVAL.
    function roundToNearestInterval (n) {
        var diff = n % INTERVAL;
        return diff < INTERVAL / 2
            ? n - diff
            : n + INTERVAL-diff;
    }


    // ## Tasks
    //
    // A task is a function to be executed after a timeout. We abstract away the
    // implementation of a task with a constructor and functions to perform each
    // operation we might wish to perform on a task. The closure compiler will
    // inline most of these functions for us.

    // Constructor for tasks.
    function makeTask (repeats, ms, fn) {
        return {
            next: ms,
            timeout: ms,
            repeats: repeats,
            fn: fn,
            lastTimeRan: +new Date()
        };
    }

    // Decrement the ammount of time till this task should be run next and
    // returns how many milliseconds are left till the next time it should be
    // run.
    function decrementTimeTillNext (task) {
        return task.next -= (+new Date()) - task.lastTimeRan;
    }

    // Return true if the task repeats multiple times, false if it is a task to
    // run only once.
    function taskRepeats (task) {
        return task.repeats;
    }

    // Execute the given task.
    function runTask (task) {
        task.lastTimeRan = +new Date();
        return task.fn();
    }

    // Reset the countdown till the next time this task is executed.
    function resetTimeTillNext (task) {
        return task.next = task.timeout;
    }


    // ## Task Runner
    //
    // The task runner is the main function which runs the tasks whose timers
    // have counted down, resets the timers if necessary, and deletes tasks
    // which only run once and have already been run.

    function taskRunner () {
        var i = 0,
            tasksToRun = keys(tasks),
            len = tasksToRun.length;

        // Make sure that the taskRunner's main loop doesn't block the browser's
        // UI thread by yielding with `setTimeout` if we are running for longer
        // than 50 ms.
        function loop () {
            var start;
            for ( start = +new Date;
                  i < len && (+new Date()) - start < 50;
                  i++ ) {
                if ( tasks[tasksToRun[i]]
                     && decrementTimeTillNext(tasks[tasksToRun[i]]) < INTERVAL / 2 ) {
                    runTask(tasks[tasksToRun[i]]);
                    if ( taskRepeats(tasks[tasksToRun[i]]) ) {
                        resetTimeTillNext(tasks[tasksToRun[i]]);
                    } else {
                        delete tasks[tasksToRun[i]];
                    }
                }
            }

            if ( i < len ) {
                setTimeout(loop, 10);
            } else {
                setTimeout(taskRunner, INTERVAL);
            }
        }
        loop();
    }

    // If the task runner is not already initialized, go ahead and start
    // it. Otherwise, do nothing.
    function maybeInit () {
        if ( ! initialized ) {
            lastTimeRan = +new Date();
            setTimeout(taskRunner, INTERVAL);
            initialized = true;
        }
    }

    // Registering a task with the task runner is pretty much the same whether
    // you want it to run once, or multiple times. The only difference is
    // whether it runs once or multiple times, so we abstract this out from the
    // public set* functions. Returns a task id.
    function registerTask (repeats, fn, ms, args) {
        var id = taskIdCounter++;
        tasks[id] = makeTask(repeats,
                             roundToNearestInterval(ms),
                             curry(args, fn));
        maybeInit();
        return id;
    }

    // Remove a task from the task runner. By enforcing that `repeats` matches
    // `tasks[id].repeats` we make timeouts and intervals live in seperate
    // namespaces.
    function deregisterTask (repeats, id) {
        return tasks[id]
            && tasks[id].repeats === repeats
            && delete tasks[id];
    }


    // ## Public API
    //
    // The arguments and return values of the functions exposed in the public
    // API exactly match that of their respective timer functions defined on
    // `window` by the HTML 5 specification. The only exception is
    // `minimumInterval`, which is specific to Chronos.

    return {

        "setTimeout": function (fn, ms /*, args... */) {
            var args = slice(arguments, 2);
            return registerTask(false, fn, ms, args);
        },

        "setInterval": function (fn, ms /*, args... */) {
            var args = slice(arguments, 2);
            return registerTask(true, fn, ms, args);
        },

        "clearTimeout": function (id) {
            deregisterTask(false, id);
        },

        "clearInterval": function (id) {
            deregisterTask(true, id);
        },

        // Get or set the minimum interval which the task runner checks for
        // tasks to execute.
        "minimumInterval": function (newInterval) {
            return arguments.length === 1 && typeof newInterval === "number"
                ? INTERVAL = newInterval
                : INTERVAL;
        }

    };

}());


/*
// This code was written by Tyler Akins and has been placed in the
// public domain.  It would be nice if you left this header intact.
// Base64 code from Tyler Akins -- http://rumkin.com
*/

(function(){
var ua = navigator.userAgent.toLowerCase();
if (ua.indexOf(" chrome/") >= 0 || ua.indexOf(" firefox/") >= 0 || ua.indexOf(' gecko/') >= 0) {
	window.dhtmlfx.StringMaker = function () {
		this.str = "";
		this.length = 0;
		this.append = function (s) {
			this.str += s;
			this.length += s.length;
		}
		this.prepend = function (s) {
			this.str = s + this.str;
			this.length += s.length;
		}
		this.toString = function () {
			return this.str;
		}
	}
} else {
	window.dhtmlfx.StringMaker = function () {
		this.parts = [];
		this.length = 0;
		this.append = function (s) {
			this.parts.push(s);
			this.length += s.length;
		}
		this.prepend = function (s) {
			this.parts.unshift(s);
			this.length += s.length;
		}
		this.toString = function () {
			return this.parts.join('');
		}
	}
}
})()

dhtmlfx.base64keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

dhtmlfx.encode64 = function(input) {
	var output = new dhtmlfx.StringMaker();
	var chr1, chr2, chr3;
	var enc1, enc2, enc3, enc4;
	var i = 0;

	while (i < input.length) {
		chr1 = input.charCodeAt(i++);
		chr2 = input.charCodeAt(i++);
		chr3 = input.charCodeAt(i++);

		enc1 = chr1 >> 2;
		enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		enc4 = chr3 & 63;

		if (isNaN(chr2)) {
			enc3 = enc4 = 64;
		} else if (isNaN(chr3)) {
			enc4 = 64;
		}

		output.append(dhtmlfx.base64keyStr.charAt(enc1) + dhtmlfx.base64keyStr.charAt(enc2) + dhtmlfx.base64keyStr.charAt(enc3) + dhtmlfx.base64keyStr.charAt(enc4));
   }

   return output.toString();
}

dhtmlfx.decode64 = function(input) {
	var output = new dhtmlfx.StringMaker();
	var chr1, chr2, chr3;
	var enc1, enc2, enc3, enc4;
	var i = 0;

	// remove all characters that are not A-Z, a-z, 0-9, +, /, or =
	input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

	while (i < input.length) {
		enc1 = keyStr.indexOf(input.charAt(i++));
		enc2 = keyStr.indexOf(input.charAt(i++));
		enc3 = keyStr.indexOf(input.charAt(i++));
		enc4 = keyStr.indexOf(input.charAt(i++));

		chr1 = (enc1 << 2) | (enc2 >> 4);
		chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
		chr3 = ((enc3 & 3) << 6) | enc4;

		output.append(String.fromCharCode(chr1));

		if (enc3 != 64) {
			output.append(String.fromCharCode(chr2));
		}
		if (enc4 != 64) {
			output.append(String.fromCharCode(chr3));
		}
	}

	return output.toString();
}
