/*
custom slider

two ways to instantiate:

inline:
	<df-onoff id="slider_green" name="Green" style="float:left;" class="onoff" on="true" off="false" startvalue="on" size="35" onchange="slide_green"></df-slider>



javascript:

	df.newdom({
		tagname		:"df-onoff",
		target		: scrollwin,
		attributes:{
			id			: "testonoff",
			name		: "test on off",
			style		: "float:left;",
			class		: "onoff",
			on			: true,
			off			: false,
			startvalue	: "on",
			width		: 55,
			height		: 55,
			onchange	: function(val){
				self.scroll_speed = (val * 10)-5
			}
		}

	})


*/
df.gadgets=df.gadgets||{}
df.gadgets.onoff = {

	image_src_on	: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAqCAYAAADBNhlmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjRCRkFEQTRDQTNFNjExRTE4QUQ2RUE3QjJEQjUyMzA0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjRCRkFEQTREQTNFNjExRTE4QUQ2RUE3QjJEQjUyMzA0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NEJGQURBNEFBM0U2MTFFMThBRDZFQTdCMkRCNTIzMDQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NEJGQURBNEJBM0U2MTFFMThBRDZFQTdCMkRCNTIzMDQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7AHq5yAAAP6UlEQVR42kxZW49k11X+9rnXqaquvl+mPfFMYptxJhnHiRMIiqUElIhIKEEgCA/hiQdL8MIvQIIHhHhAXJ7yAFKiIPECBiQeIFIgYAWFxAqOkWU7M5PxeK7dU13dXVXnus/ZfGvtaqB7arqqzqm9116X7/vWKvO5n3/55t7u/gYMEJgQ4JMwDOAcXwch+q7n8w4B3wOf97xgjDx4Dx89r7merwPDzwNRGIEfRc/35LWThRDodVmbN/O6v9cZo9cDI9e5V8/1uw6ttbyjx2x2Oou40y43HY/yHLbtMByOYZxB0zVIwoSGhUjiEG3DhQIaKdvxTxjFvK/jMv71xRPavjJWjO/FHpgw1M/JqUMeQH5616KjMUEQoe1aPZAcrqgWqKsWs7MZzs4XcRSFYXt2fobj46l6Ik4SORrkVz0jp7S9Hh6rjQN5T365u3hWDHJ97z2ku/PTYnTgdA3oWn5NWUOv0VjxlkTBcb++t/5godHoVWWJMDBtFMeRWi/LtLXF8rxGFicaws6Kp4x6pSgrLsitenFWp0b3CNUkCbP3rdN/cIGGkQvD0GHGxHoYH2KnRoZBjzRLuYFB10qKOKSjBAkfsk8YSUoZRAmNsZJbi0Y/G230GK0naMsONQ1OtmJvxJJhGlgEXaghc6FTk0zaM0QdqoIeaVu0TBO6gabLoSWXYkRRRyMAW7Zo5gwnX7SVRdzTObXkMo2PHZJpgEvZFRqXqlflJ+KymkP1vMT+tUOsPb0mLmGUGO4BjeMBuDSiNIDtW9iGBpU9ltWc79aa5AOeftRxI4ZJCqIzHR0TIaAXQkYiMhmyvRSJS5HzwPN6CVOxKJqKoaEz6g6GDzfv8OTmCaNkcfETmUBCaVkgLapmiaEbIvtsgOXjEl1S0u2pr+KYdluH4bUW+VqM2S0m9B0mes3k1grlfZabSN7JoXl/OAqQP5Vh6/kY1fIc5+/Qs/k6Nmi8bTqUC0vPdti4nOP8ezWa/3Irs5iLmu8MMavEQ4m8zTd2Dzbx/nce4NGPT+h23pTQk0yV+UmFxw+O8cGPH+Dw01tI9kKMDieMZsNcSWVJrtGxyAYaEZc0amzHUN75hwXufvsx7v3oFPlmir2nN7l0zJTgrjbG+ksNnvr4Du6+doLFtMDWwUAQbeVB42HBSULGMfIkR1PUMBMgO2Bo1yyNneLh3Rmu37iOydo6Tr/XoWBKmJQ5ySRZMjQdQ28UvSTveWC+39T0q0no8YwVmCMZVzi6fxftZI4PvXAJkv/NkUF13GNCw9M00YNi5SzxZCQwoAXGuA/y1Odd5jTJh9ccyuUjYG+Oz/zmSxhPtlkcBayzzDnJU2Y+c3XCcrd8bdtGoyHVJmnTCHYSXuJ0gMxFyD+c4rB5Gi6dI5jcx+CDmzBvjoBZppWcpkPko0xBVcngIgdhrAKtJDWkOkPZHCgfEzRdg2s3Pob1K2s04owLVQRwHiYRrwsxtFqvXVMSVWpFEtms75xWIk1EmBCiqh5ZKwiQoTkDmrJA+eiIr1PuO8RgyGLBnMXaIh2nnqn4Gyn+BlLqIfMpUsph/WNxfIxHx2e48cVrSDeHBM45r9EAWqWhbJkGUY8kj5gvLa+1SmPRYKDF1DetFoJSZ8J7oobVyvtYIM4KuI/UyMf3H2JnLUeaiw2ONNcq3HkKYojlDYl7qBQFDZ8A9/3b7+PKx65juEPqM5UaECQaUaUpYwT5ew0piZGGRbqovB8mgWcLLmr0PRrKw2i0eLdtaXRE/K138O6//QTBSzOczkrUxFP5rLM+ir6K6bWmbRUaLDe29FJrG3LyBg6vHCgGGlZzlHHTsFX6Co1Hf7glvSdCgqGII2UDCbkAeZQIfMUaam+4MKAIAoOk7ZXimtMhtkcHOD9/gqpi+jDICZnFhEY1gHC1Mrd4wQpn0rf1mUXKfLj2wvNIBswPVqphzolhAY0MIpUqPBBhhAsGYjANDCMxMtQwmMjnT9gGyrdipCX9kbboNXo4NRpOobPtnU1sfjLFYKfX8ERJqIdxGimmDJyXTl4c9Dg+miIPxnC7VDL0XDphVUVCjq3yqnCnyVqGQnhZPic8HCiwGhpo4kArUEVBrEJLaU6YK4jFYEIT7xOpJeouJ3Jc+cURpv9N7wYN85YOcivdIVLMuQv+ZkjoxcoukQZe0aSUVHJqE4V6OpPR01WB+R2i/ozuJ0KEA+hGveCXSCxWa2edHtZ13vNd3St1CheLN4Ms1HRxhKlwHGG8NsGTN5bMNyOwonQp3hN9GkjuSP6pohFe3UoQD3PyKW+IMn54E4ONMaJhjHvfWuLVX5/im79whG98fobv/wkxkZsb8SZlVzQm5k0CjPdD3Hx1ga9/4Qjnt8HXKR85vv/nC7z2++ThyIteMSjJmW/FEO2cXM9Kl5zviaGhoInR811ovBDzGQHqp0rE2yT2OOcCRPdRhJ2DfZy9HuMfX5nh4EWLr/ztAF/4wwz/8UdP8O9/MEdMfZGsBZqraU6Vwr89PTS7VeJf//iERrQYrPHALKhMBKukQexD3JOLE6bRs7/FfN8r6O1O8VWKT9gksLS2X0FMNatRzBZwm1TTJOBYFmMY6naBt79xjsufafHp30uw/7MBPvFKhBdemeDNvzzB2U1uvKHCkAurWkR+EGLjxQFu//0Mt75F4UGE2P9IiP2rAeJYewMvhlk8Hd00D85VdolQwwoGVXGblRSPWAEd86M+Zn5RzRbLBY5nP8F0fo8k/x7e/Jf7ePbLEUYb0rMIXju88NWh5suMqiY2AshGK5iZg/oJGfJKgutf3cDf/c6U2EdJNwtQzo0E0YtbDwiMKv8rmXetXeEm1yBmSo8SRNovOC2ClGIhTAV/WAy8uWZFlbMllg+FgmbMMaiEDxWnmHO5HI+l2gVK8UpCrNBYmFxggod4+XfXUby9xA+/Joo8YTSMqnRFj2BVYMYp1ElhtDXhS593vqUQ0JEbFahJ7raqGV4FO/gKp9vXuCCBd3bTS3tisYbx7E6jh8u3QnqFSU4ha5lDDX8D0uXyqMXW0yF+6a8u4b1XS6pxi80PMKS1oja9E5EweBibUP0YpcFa1dXKcqkNbQvltHTzgEJ0sJOimUrzkWkIuoaVuW3x3OfHeP0vajx6S9Qyw7Xs8d0/qxHtJNj+aEzxKSkYeO+ICpG+o49RTA0uf26Aqy8PmavaEyqV9ZR3w60DTHYnSLIBqTJQaLKSd5FZtaC96EFFRIWZwWaG/cMc+YPLOHd3NOmVF+mxT/z2Ot795zm++XMP8dyvjPDgjRqLtx1+9dV1cjQ13Umnm0gUgoFBcUwVdbtn4RmMdge4/CWDo+8UqMgYPe+heqC0OgSbXuJuiHy+SU3OXI4qDa8xverUyGkuRCK8aHGIsx/mOC5vKVVJKfWU8WbpsP5sjK/8zQb+82sNbn17ga2rKb78TxPsfYpcPudZSqsetJm4F5hcDXHjNwZqfFPQSFbvlV8eqGpS1nJSaaHqUEGSm1+fojiRhitSLu/Yl6se7JgXYmRHPrK2RvWkIIaRPyU3uUDHbi2aCAR17C8ifPFPL60mDdDCqk/5nIcIAw8z7SmrlCff/+QA+z9N+mJSOaZJR6MvfYlIwWqtTz0VOsqxJBxR/0Grv6MEb2reqCgBX8WiGgLjcTAM2R9sbKlhNYtF8sVSZEpj1NBDhnnTliT8pRQENy3o+X6gyQ3RHZ3cy2uLDpZe7eZSAGwtWbl9Ia2rUQN1MCAauRNplzOwd8n5VotHRis9QVqxWUiks52+qWYTjrY2Nun5DnffYe8gpyFUdKXTCu1admNSDKQ310p+8jKLxYoRBJeenOe0aReYyWgAPWbpRWkxeVBb8wCtr2BpzpuixNHDW3j3tbvYeZH9SBv5iUUPhbJQZJu4UYnd+V50c28N5e1zPHz/IdZ3KMWfH7I7k9XYdNPDAVuAJIN62YS9wpChrLKyWOxhyblIWUKVt/TYNFJDJm1AaxRWiCtYnj7Be28co1s8wM/8mkQu0PSR4hBVbunhSKwUl2qxwG9czxvt7qfHJ9hiTmW7ouOoPthzxCQPawOltEh6+l4HLSwGyk2Cu9OxlWenlWLysxdF7kALQ8Rqx/wrFme4+/A2VRMBvhxjmPe4xzyUFkRqQP5G0n0JxYWiiI2fq8RsB9c2BpReFcpzaS8TnUGIpw2rstMZDfvl2GmVypwmagPtPbiQbo7e/d8wCb7CBXx7axS2ajbtRXmMYK3gviyUmJ1f0qpwEae1rafCyM/7vMKVjWzVKsWt7w0RbAILUlzXMdST2Mv0JbEqJ6WT7LtQhKw8eCjmmOjIiBKNZc2ianT4I/K9h+/wvDZkyEvBxzO01QPsXc9xNmXqUKwKi8exTw9t5ERRO60aP5+RxK2KCl1F4BVYeC7Hkq3nyZ2CpxsizlgkLCrLxbJRqp1YmiXMyZw5GGFhJTVEqBKyWimW2Ou63tOjjuJYIE2/QOvuY3S4ge3yEGvM8SjPkWUVG/ixoKSnXxUxZhWGwE9Vu4oRKOj+WYuTH7AQghQn71CnLc8xnIywe7iJjMUwv11iEcrCMpsh+pPW6tL6cLKLS9IASeRVSSztBgVFXTWYzk5QtFNc/dRlxIMt2BnzuzjhnnQS75E8FmWvoz4ZPHXaADkfYnJfWZU60CmmC4YzRJbGPrfTDiezKWS69qGP7GJ3O0djSV3L1SSB4cioIftY+JjagrQX0XtOPsyUKc4r3H/4SLu+Sx94CuZ0gjOCfDulgmEqTWdzLAt2dVniU04s1PFb72fToTy4uHRmz3z2ErJhuhrf9r6ZphuEdbb3tsnXl5GPR8jyAUIqmT6i104I+ALIKnLZ0LNjkpGyVP/pbIYH799EFTfY4OekMarnVodLDYth/5kJ03aA9fUcxe78/83HK6nNTis3oUFiaDmrMOr2ceOjz1B2MTxDhpl4Yqj9WNPapblGm0pgaLDBlrHoS1Q/CjQsrrWqjM5dofdE9OBwZw3PH7yEDwsikfYm2Rh5MtLBUsM29Na9H8O93mA0jnWA1Tadj6qMPoRJJCGlB17OCrz13bf8VGA1XZLKi6WzC0LVaaIyYC4QxOHtv2bOaHvo8VBq1upcBqt2FrpREMQXH9G8FGfI6E/mklLxUrVjGi4D9FB4erWagp+MGOSrB2WVi3nKSnJLCdql9cqm673rRYa1fjwBi9Ww3Z9JFnUClJFPGWmKjH6tUfuvMi6gkTnftb0OzWVwL9OJ6XKmhkpdCI4KE4kH42W1RF23vg8NPPqv6lwN99MHpzSkE3usDOr8IczqGwC5rN+HRB6kOxv4qXvn1UjQ+97ZW+kjJ6K+bYkCtQdmAemOEsx/P+MoD6vqqCorGfSh7Rs10v3vJFYGPU5VrltNIFYyQ/sHXAwA+tX3IYEfeVx8lRCYlcc06SPP23oyrL68MT4NzMXXKNoE6z2rPWf/I8AAqJ3iVwXONlMAAAAASUVORK5CYII=",
	image_src_off	: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAqCAYAAADBNhlmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjM1OTY4RjhEQTNFNjExRTE5OTVEODYyMEVGNkFCNDNGIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjM1OTY4RjhFQTNFNjExRTE5OTVEODYyMEVGNkFCNDNGIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzU5NjhGOEJBM0U2MTFFMTk5NUQ4NjIwRUY2QUI0M0YiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzU5NjhGOENBM0U2MTFFMTk5NUQ4NjIwRUY2QUI0M0YiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4/LHTeAAAPL0lEQVR42kSZzaul2VXG1/t5vu65t25V3aqybieNSakxtgElaGLjICSISGLQgSgOAqIOBCf+AQ4cZKIRxZFxIEFERRAHikg7aRAC0tBqOtpJFFOaqq6qW7fOvefr/X63v2ftU50qDnXO+7H32ms961nPWpV86tM/+V9379w7tcQsTTIzvmRZaiHwO81sHEa+D5Zyzfg+ciNJ9OEZPiP3wsjvNOF9szzLjVdt5Jp+By1kqd/X2jzM/fhsSBK/nya6z14j6w+DdX3PE6OtVlernJ3usOnyaD63vhtstlz6Qn3fWV6UlmSZlUVmXScjMFJ3WT0vCqzoWYbNdHmUHalM4B8ZkHCeEI3PM3/PBg6QxzXC2FnPgTOc0g8dhgUrONy+2lpbdXZ1vbLr9bbI8yzrrtfXdnFxaQOnmeSljSxu2iBEz8iLdjhxxmkz/pUnA1ZlMsbkydHfMV0NSXwGT5uMtJfei8brO76zfiQyQe/K073lrJNliUevriqt3eGI3MOhQDS4dzPU7JN4uDmUDR56sz0e1ULmzh99sz5GT6vbSzMs/uSX3/B3dYSX95LxEAGMn+a570UgOFCwBdGaJwX28FueZvO8JIxydbNvffO71ttyMrNmDG7IRBjh+45VEr7PWIhIWSEvOGaCY7They2sxlhjfMozg8NCVwf3bLCqV6SFa7xcpNbgoEnb2xR7rnl1dusOXiz9vv7kMkMY2tWN/cCdW/ZDM24O8s5gU56ZKmxsno6chn87sLPV4tXecsAc8Dq/3KAxKNgxGuZQmESIlIUtcMSMtc7mExvA2I77LR7b8WjLCx1J8hyDv3a1A5+9vfyT+wnZqFM2dq01uP0L4OF522DUYHmaewYovAnGfLzjOs98GzMe8k6bcBqeUbL01ntYFcYCUxesfY/s/5Au7Pf2P+Arl1e51/DuyGEHDvcRkvANHnmTPQpBJ/luouVkySEJUkfROSf8i3Vtb2/3Nu86Xk9swrZrMPi0HuyT88JeXyS2JAwTsu4Iy1s7hD1kFg6YnQV5PNge099oBntzX9tXm9ZmGHx7cWQ9HmswpuFwvzTN7FPzqf01KHuBY27Ml3hxOHjwYO2gMPLCdDaz6+cXdto09iEWlwHvtbU9JA7npzfsEjr6c2GvamzCv1NwtRd3hRgSYVAHlU+VGh2hTaGknlPmXW/bprZ7rPdRjEzLiT3hvYe8XJQZB8oI9yHZkpjtuXhKWSPglmTOhBPN5BGS4UfhpwAfBSDwc+ev2tHR0lJgYKM8i98GQiocjjFzh7F3/GgL0Y6So5GZJYFl3T2YC3w/qTZ2drm2O8c37euTue3Sub8/m03tCKMHIJEdWMExaEncBIhYkXTKHFmO5ypLMfL8zj07mwD43cZGPDsde/dQ5jw0WMEhh76B6FuDRW0/yqOpH3pMIfs+t47nBrA7llOr+IjiN5tnYPy+pbMFZF56MtYceILHvVLxN5eVWSrsZF4NFnxSFhPm/qNu7ccw7qSYkuZ7K4bWcgxWaLNh9BOqRhQY1IPXsduxeO7ZWo+VVTwfiqVlrNkNHCCB0siAkOTWTo9tu7mw6/3akukSh/SsSUarqoQpHo3EDyy6yLbOq6OfdCR0j0n37wVz83IGnXRWcm9qMcsyGcgnOeCl5FpJJOpA+VL2c9iJOFFYBQ5O4hgwg3bqIPhwLy2sPrplb+0ae63a2Zp/ByIpfu3FAkk4ZDGZ2AJeubdXkcYTJSFMy9LuLU9swolmhHCBe2cH8AoCLiJ4NpEBPGOhcU8mHCTFZQNGagPdFx4LlUkvmwPVAr7kWkICjfPUumZvdV1ZMlk476bZoZLxrEjON2tV7njpGozdxBsPzk5tOgG0UIsydcrJpuLF4FXVE6Rtr8HX7v1rBVBJWFiVRV4Tn41e7kRFqUNoCLH+imOllhbzhX10e2X3cE5eEiUStU9i0ulgubl8SlxO6fQb+G+fTezmNAfA8J1ndmJlTzJwfyYjCfkgLLrnIg4XrDHBuC50vnoHZ9UYlbsoAPgYIyoL3E9C7qVPmSoF89nF0r5OMsloMcYYpYWvk3thlweyNDIXJS+l3ElAlAC+HHNwAT/isROM/A5uf0RoPwymPmgdmDK/f+E0QvGPisIPc8bnf8ncrVgBA6/w3oIEmTtMRg/flHAfw73vcIgMw0uVQFdHvYc5zzGiblvXbRk8dcpCc4y9EnFP53ZrktqcE6z5/cWgj7iIdElb+zLXfq2/shXG3Z/c59osij6Jjn5rf9Nc2uvZPTLoiHdg6r6yj0NjfxZqT4ZM3IcXG6DUikE4/AQ3vcC4pTxO0uRR3aZeTdbg78N4ZVMe23sQZkEGT5vOjgDzlwnBF7HtF9pL+0K7tj/OZ/br03O7Sfn6ac5s+cJ+flhxb2Mr1nwADHYh4u5XupX9MAdrufYAfIe8cBzKgy2HLKcz+42usr9DgPxzH2WctIGglytzXTCyVoPlNTRxSp6PMGR3/YJMrexyv7Hf6VP7WLOzv6oes0JjP4NNp5PEfrs4ts90ly4SPtvv7WfbbQQQG70JlgGV/UF94R6Jknpu7xSFq540BMebq4tdZXMglCYTFx6qblJKTtTqCzJpOGGpFfipmUNlJam/r3f2Lr+vw9x+Fc+pzMVV5Zmd/T7Xn4wSl539UbqwJxi9Ihqfp94eaRdw9JvpkZ0QkWOM/KmxtTMlSzp6pgsQPe8XqfgPfs1UPfSbJOO3JH9Uvl5WEN1gUpYDWRej6k0qsbrwIZ0Wxve12llUGtZpDRZ9Gwy+DRR07cZwZZ8btv79K7CoFTPH7kfalb1CxCoJ3iR1XnX9R0hlbIszpEVFR8ETKYlNz/BSnis51Px4MkbxeUsY4MVvhpe63W/Zv6k88uBtaULk15eqR/ZbNV6WLGKdf0qm/vhXh0v7xI4uDdwG1n5YLDxivr56H++6yNxUqjxxiZck8dBpiA2Ew2PBczeULOKoQ5NUc+uD8NMHyMAvZXP71+Dcbt/KCvvLbGm/ODZ2z6KenKaH1k1eVn4IQIiANgeLROcYTylxOi43cj4sMYOGyjJ6Xbyod5W0g7cT+n7oxPR3Do/dPF5aRYkTOYrJKzU1GPi70IPlU/uR8tx+Ir9t35/f9QT4PWhm66yKfFciYH8fO0sMCe7ZLJvaU5LjCq+0SYSwSuHs9gfs1fNzehNeQh9uoRvBJSW8GYnjvXbw1jL3Zn3AwHco4t++gigIqSiPDgFeCvYZPPOP/do+Qc19C2M+DZd9o3vPzqkMqqu/3D6yT3Z79+QeA1e8e8Y7nx/WdgwE9mz2nANuMm+3bCRECZ47OpqRHJ39IU3bv4/yLIUhiSrf9eAgQ6QwpGIQDY8knULs6AZxV3DGsA0vvQaN/O24wXOdk6quv/DubrA/rZ55A/QsMW9XG4y8ixF/0m1slxXee3RZTCo5uMdT3sGAy1wNGV6r2FdqZ6J4OoxHVSCpjt6zWG8ucPMF9FLRCi4cgxG04iyVtCMIOUBBbRLrcPARx0il4VCH8JkmCUQipFPbYlyfHASAxZHKoBZDghSZv9MLF8/tLr/fVamEg8N4aNIk+QfI0acC6vUFdEmsvranq42dLBbUlQRvpHaN+DzyEUZUK7n3wFQDwoZ+9todxyCp98Kj9285hsmYxOHS+ydz+e9GUxSePnlq39hc2cdObtob7unmUHZ12kyacvQL3puw/flyahPEwMV2a5cIhwbg18norq8xbg02NgpHok0KitwAocJf/E6KOXV3SXIkPpEQznq1teJKDOpS9cLoTzJXmbxePbdHT/7P/mG3c0mn8AdPkMTt6YFfLoEwHoSmOC1DHFwSXjHEFbVxjdcKPNEAA3Gk/DJJZaDaBPoUaMZ8QjXE6dYBu8NhHONGytcCPg+oQVdv0uGtar+zerOxZwPNFYqmpFdpr9YYJY6K0cjVtA+g2iuKJk3gJsWoJRmW9ZJTrdVwWRzNoYxVnnhGIrXVgaARqeal1AqioiZ5WoVQmZqmXpHSQ2tbi2bS6MlKHSE1/jZqOlBlSg0IposofLGlPfSx+XgQrD6V4nON7mm4dhtu+h68U0MdFzwrma5TpxgjI4s0aj/V0HxSOn4nwiCHUYhlQJcmTvijRWJsua4JUM8h9nVtYfXMXsGonahFfUuWuuRLDlj2UieK8XS2xEOzQanUPpowpFFhW5Z/qBkfBk+lQsRjJIyGSBO+l2DKQ+J9SucyXlCRlwZJJA00FRk1Q6IgjT+6xmbrS7tPJVmd3vMh6BHPSSDfRFT49C+NIiaP3ouyXUkiotRw6AVG/gu6rUzO7L858SWY0NjiZHFsskeDI0n0RYjZ29Ma6F2d2rMcQwtRTZY7/w0+8Rpsh1qawbev3T63Bsa4RECE7doa9SKp+czQGWEwFxO59wkKrzLZMZM4T11h7H+G1Ouz977QUQUt9BjwYD6zG9OJq5CxG33QWYm7VE/xVhFiuHPRDZ+G9V7ADBsEsSYR5Xxpj2mWHhGRi5DZAw7fkphVDWJ9Zmjfnc14XWdRlboJySEcfO5kbm9oOqATsHnJyV/VZACKeWU+t7PZ3DKNKeDJ+xqr8eyK+40wJmWkUpJLQRtJltqOru1itbKn0nklHrXUcapyVlKVXl8ubCCL79w6tTv7vVVI6jgfr5WMWIpglKrwKSvg/+bN+/Z9JwOZKYVS0tgMdktZWTU+QO98gBbslO8/Di0Z3lHJ23O4ikrQwpuh7sBvYWsRMt76waMjRAFZjmHFzaUVN1gRzqzpob/z7rfs72GBu8cz+5rYRBOJw1+vJALklOxbYcBbjx/7xEChdypTieMlYUqVI/GZIXk5xNHEV16sXeC6dtP0P8TZs7I6OcxbPWaeLLV/z9YVHn7hYqFHYTe7Oo5f9gvbV7VDI8RJ40Gw+ogh9Wx+tt7GReTiw6DcVZ5vPLrrJSY6FUuEqQuLEPWj6lni0/3Bh+2FFNIwOIRUKIr08N8bSkiNkjXl58cMvSgmSDY7fkctqGL8shYXO/qOhu5NC6Vp7JMPee7YjNOHcJDlibu+SGKWaf4SR3lJ1KnOESHO9mSkD9yjvE99kpYeJPkQm3P1M11tbROJWTyoMV78/5lQ5HVdP6srAKNhI+5+eVo7hKbvghOr60YtHmWGZ/P76j9E9SElLE7QDEZrZN5zxPYgSyMFOa0ldvjPm2j4geniHoKR9o17rv5fgAEALXGWpX3n+GYAAAAASUVORK5CYII=",
	
	init:	function() {

		var sliders = document.getElementsByTagName("df-onoff")
		for (var i=0;i<sliders.length;i++) {
			df.gadgets.slider.initGadget(sliders[i])
		}
	},
	
	initGadget:	function(thisgadget) {
//console.log("thisgadget",thisgadget)
		var thisw = parseInt(thisgadget.getAttribute("width"),10)||undefined
		var thish = parseInt(thisgadget.getAttribute("height"),10)||undefined

//console.log("thisw=",thisw,"  thish=",thish)

		if (thish==undefined&&thisw!=undefined){thish=thisw}
		if (thisw==undefined&&thish!=undefined){thisw=thish}

		var onchange
		var inline = false
		if (df.objtype(thisgadget.getAttribute("onchange")) == "string"){
			onchange = thisgadget.getAttribute("onchange")
			inline = true
		} else {
			onchange = thisgadget.onchange
		}
		
		var image_on = thisgadget.getAttribute("image_on")||undefined
		var image_off= thisgadget.getAttribute("image_off")||undefined
		var srcdata = JSON.parse(thisgadget.getAttribute("srcdata")||"{}")
		
		if (thisgadget.getAttribute("name")!=null) {

			
			var nameholder = df.newdom({
				tagname:	"div",
				attributes:	{
					id	: thisgadget.id+"_nameholder",
					className	: "df_onoff_nameplate",
					style	: "cursor:pointer;color:black;font-size:20px;font-family:arial;position:"+(thisgadget.style.position)+";"+((thisgadget.style.left)?'left:'+thisgadget.style.left+';':'')+((thisgadget.style.top)?'top:'+thisgadget.style.top+';':'')+"display:inline-block;background-color:#cacaca;border-radius:5px;"+((thisgadget.getAttribute("namestyle")!=null)?thisgadget.getAttribute("namestyle"):"")
				}
			})
			thisgadget.parentNode.insertBefore(nameholder,thisgadget)
			nameholder.appendChild(thisgadget)
			var textholder = df.newdom({
				tagname	: "div",
				target	: nameholder,
				attributes	: {
					id	: thisgadget.id+"_nametext",
					style	: "float:left;display:inline-block;position:relative;padding-left:5px;padding-right:5px;vertical-align:top;",
					html	: thisgadget.getAttribute("name")
				}
			})
			thisgadget.style.position=""
			thisgadget.style.left=""
			thisgadget.style.top=""
			thisgadget.style.float="left"
		}

		var gadgetDOM = df.gadgets.onoff.draw({
			target		: thisgadget,
			onchange	: onchange,
			inline		: inline,
			value		: thisgadget.getAttribute("value")||"false",
			width		: thisw,
			height		: thish,
			image_on	: image_on,
			image_off	: image_off,
			srcdata		: srcdata
		})	
//console.log(gadgetDOM)

		if (thisgadget.getAttribute("name")!=null) {
			if (thisw!=undefined) {
				var textholder_w = textholder.clientWidth
				var textholder_h = textholder.clientHeight
				var gadgetDOM_w = thisw
				var gadgetDOM_h = thish
				textholder.style.marginTop = Math.round((gadgetDOM_h/2)-(textholder_h/2))+"px"
				nameholder.style.padding="5px"
			
			} else {
			
				gadgetDOM.onload = function(){
					var textholder_w = textholder.clientWidth
					var textholder_h = textholder.clientHeight
					var gadgetDOM_w = gadgetDOM.offsetWidth
					var gadgetDOM_h = gadgetDOM.offsetHeight
	//console.log(gadgetDOM_w,gadgetDOM_h,textholder_w,textholder_h)

					textholder.style.marginTop = Math.round((gadgetDOM_h/2)-(textholder_h/2))+"px"
					nameholder.style.padding="5px"
				}
			}
			
			df.attachevent({
				target	: nameholder,
				name	: "click",
				handler	: function(evt) {
					gadgetDOM.click()
				}
			})
		}

	},

	draw:	function(gadget) {



		var thisdom = df.newdom({
			tagname	:"img",
			target	: gadget.target,
			attributes:{
				src			: ((gadget.value==="true")?gadget.image_on||this.image_src_on:gadget.image_off||this.image_src_off),
				style		: "cursor:pointer;",
				width		: gadget.width || 22,
				height		: gadget.height|| 22,
				image_on	: gadget.image_on	|| this.image_src_on,
				image_off	: gadget.image_off	|| this.image_src_off
			},
			events:[
				{
					name	: "click",
					handler	: function(evtobj){
					
						var newval = !(String(evtobj.target.parentNode.getAttribute("value"))==="true")

						var returnval
						if (gadget.onchange){
							returnval = gadget.onchange(newval,evtobj, gadget)
						}

						newval = (returnval!=undefined)?returnval:newval
					
						if (newval===false){
							evtobj.target.src = evtobj.target.getAttribute("image_off")		//df.gadgets.onoff.image_src_on
						} else {
							evtobj.target.src = evtobj.target.getAttribute("image_on")		//df.gadgets.onoff.image_src_off
						}

						evtobj.target.parentNode.setAttribute("value",String(newval))
						
						df.killevent(evtobj)

					}

				}
			]
		})
		
		gadget.target.getState = function(){
			return (gadget.target.getAttribute("value")==="true")
		}
		gadget.target.setState = function(val){
			gadget.target.setAttribute("value", (val===true) ? "true" : "false" )

			if (val===false){
				thisdom.src = gadget.target.getAttribute("image_off")		//df.gadgets.onoff.image_src_on
			} else {
				thisdom.src = gadget.target.getAttribute("image_on")		//df.gadgets.onoff.image_src_off
			}
			
		}
			 
		return thisdom
	}
}

df.customdoms["df-onoff"]={dominit:df.gadgets.onoff.initGadget}

df.attachevent({target:window, name:"load", handler:df.gadgets.onoff.init })

