/*
    ///////////////////////////////////////////////////////////////////////////////
  // Audio input
  //
  // Returns number of available audio inputs
  long enumerateAudioInputs();
  // Returns name of audio input.
  std::string audioInputName(long device);
  // Initializes device for use.
  bool initializeAudioInput(long device);
  // Sets autoscale. A running average of the peak is kept, and the output is scaled
  // such that the average peak is mapped to targetMax.
  // The maximim amplication applied to the signal is maxAmplification. 1.0 for this param disables the feature.
  // alphaRise and alphaFall are the coefficients for the running average. Range is [0,1].
  // Smaller alpha values mean that the running average responds slower.
  // If alphaRise > alphaFall, the running average responds quicker if the volume increases than if it falls, and vice versa.
  // Reasonable initial settings may be (0.1, 0.005, 100, 0.5) or (0.01, 0.01, 100, 0.2)
  void setAudioInputAutoScale(float alphaRise, float alphaFall, float maxAmplification, float targetMax);
  // Starts audio capture, e.g. sampleRate 22050, buffersPerSecond 30
  bool startAudioInput(long sampleRate, long buffersPerSecond);
  // Stops audio capture.
  void stopAudioInput();
  // Returns latest audio data, or empty string if no new data is available.
  std::string getAudioData();
  // Fires onnewaudiodata event
  FB_JSAPI_EVENT(newaudiodata, 0, ());

*/
(function(){

var self = {
  name    : "Audio Input",
  has_audio : true, //it doesn't really have audio, but it should cause other audio sources to stop if this is solo'd
  has_eqData  : true,
  icon    : "Audio Input.png",

  start:  function(){

    navigator.getUserMedia = navigator.getUserMedia ||
                             navigator.webkitGetUserMedia ||
                             navigator.mozGetUserMedia;

    self.music = {
      juice:{
        frameCount    : 0,
        volume      : 0,
        freqBands   : [],
        freqBands16   : [],
        avgFreqBands  : [],
        relFreqBands  : [],
        longAvgFreqBands: [],
        avgRelFreqBands : [],
        waveData    : [],
        eqData      : [],
        freqData    : []
      },
      eqData:[],
      peakData:{left:0.0,right:0.0},
      waveformData:{left:[],right:[]},
      waveDataL:[],
      waveDataR:[]
    }

    var numFreqBands = 3;
    for (var i=0;i<numFreqBands;i++) {
      self.music.juice.avgFreqBands[i] = 0;
      self.music.juice.longAvgFreqBands[i] = 0;
    }

  },


  end:  function(){


  },

  //=============================================
  //audio and video plugins need these functions
  play: function(idx) {

  },
  pause:  function() {

  },
  next: function() {

  },
  last: function() {

  },
  mute: function(){

  },
  unmute: function() {

  },
  volume: function(vol){

  },

  //----------------------------------------------------------------------------------------

  settings_to_save: [
  ],

  //----------------------------------------------------------------------------------------

  processMicrophoneBuffer: function(evt){


    var microphone_output_buffer = event.inputBuffer.getChannelData(0); // just mono - 1 channel for now

console.log(">>>>>>>>microphone_output_buffer:",microphone_output_buffer)

  },

  //----------------------------------------------------------------------------------------

  BUFF_SIZE_RENDERER: 16384,

  interface:  function(){

    self.input_devices = [];

    if (navigator.getUserMedia) {

      navigator.mediaDevices.enumerateDevices()
        .then(gotDevices)
        .catch(errorCallback);

      function gotDevices(deviceInfos) {
console.log("deviceInfos:",deviceInfos)
        var input_options = [];

        for (var i = 0; i !== deviceInfos.length; ++i) {
          var deviceInfo = deviceInfos[i];

          if (deviceInfo.kind === 'audioinput') {

            self.input_devices.push(deviceInfo);

            var this_option = df.newdom({
              tagname : "option",
              attributes:{
                html  : deviceInfo.label,
                value : deviceInfo.deviceId//,
              }
            })

            input_options.push(this_option);
            
            if (self.current_audio_device && deviceInfo.deviceId === self.current_audio_device.deviceId) {
              this_option.setAttribute("selected",true)
            };
          } 
        }

        var select = df.newdom({
          tagname:"select",
          target:LED.pluginwindow,
          attributes:{
            style:"font-size:30px;width:870px;height:45px;position:absolute;top:10px;left:10px;"
          },
          children: input_options,
          events:[
            {
              name  : "change",
              handler : function(evtobj) {


                self.current_audio_device = self.input_devices[evtobj.target.selectedIndex];

console.log(">>>>current_audio_device:",self.current_audio_device);

                self.getUserMedia();

              }
            }
          ]
        })

      }
      
      //----------------------------------------------------------------------------------------

      function errorCallback(){
        console.log("Error:",arguments)
      }

      //----------------------------------------------------------------------------------------

    } else {
       console.log("getUserMedia not supported");
    }

    //==========================================================================================
    //test interface
    var testwidth = 256

    var testholder = df.newdom({
      tagname :"div",
      target  : LED.pluginwindow,
      attributes:{
        id    : "testdiv",
        style : "overflow:hidden;position:absolute;top:100px;left:232px;width:"+testwidth+"px;height:100px;border:1px solid white;background-color:#caeaff;z-index:60000",
        html  : ""
      }
    })

    self.testcanvas_ctx = df.newdom({
      tagname : "canvas",
      target  : testholder,
      attributes:{
        style : "width:"+testwidth+"px;height:100px;",
        width : 256,
        height  : 100
      }
    }).getContext("2d")

    df.newdom({
      tagname   :"df-onoff",
      target    : LED.pluginwindow,
      attributes:{
        id      : "testonoff1",
        name    : "Test On/Off",
        style   : "position:absolute;top:100px;left:10px;",
        class   : "onoff",
        value   : self.testrender_enabled,
        width   : 55,
        onchange  : function(val){
          self.testrender_enabled = val
        }
      }
    })
  },

  testrender_enabled:false,
  testRender: function() {

    var juice = self.music.juice
    var testreverse = false

    self.testcanvas_ctx.fillStyle = "#00ff00"
    self.testcanvas_ctx.fillRect(0,0,256,100)
    self.testcanvas_ctx.fillStyle = "#000000"

    for (var y = 0;y<256;y++) {
      var newval = Math.round(juice.eqData[y]*100)
      newval = Math.min(newval,100)
      self.testcanvas_ctx.fillRect(y,100-newval,1,newval-100)
    }
  },

  //--------------------------------------------------------------------------------------------------------

  getUserMedia: function(){
    navigator.mediaDevices.getUserMedia({
      audio: true, 
      deviceId: {
        exact: self.current_audio_device.deviceId
      },
      groupId: {
        exact: self.current_audio_device.groupId
      } 
    }).then( 
      function(stream) {

        console.log(">>>>stream: ",stream);

        self.gain_node = LED.audioContext.createGain();
        self.gain_node.connect( LED.audioContext.destination );

        self.microphone_stream = LED.audioContext.createMediaStreamSource(stream);
        self.microphone_stream.connect(self.gain_node); 

        self.script_processor_node = LED.audioContext.createScriptProcessor(self.BUFF_SIZE_RENDERER, 1, 1);
        self.script_processor_node.onaudioprocess = self.processMicrophoneBuffer;

        self.microphone_stream.connect(self.script_processor_node);

        self.gain_node.gain.value = 0

        self.script_processor_analysis_node = LED.audioContext.createScriptProcessor(512, 1, 1);
        self.script_processor_analysis_node.connect(self.gain_node);

        self.analyser_node = LED.audioContext.createAnalyser();
        self.analyser_node.smoothingTimeConstant = 0;
        self.analyser_node.fftSize = 512;

        self.microphone_stream.connect(self.analyser_node);

        self.analyser_node.connect(self.script_processor_analysis_node);

        var buffer_length = self.analyser_node.frequencyBinCount;

        self.array_freq_domain = new Uint8Array(buffer_length);
        self.array_time_domain = new Uint8Array(buffer_length);

        console.log("buffer_length " + buffer_length);

        self.script_processor_analysis_node.onaudioprocess = self.onAudioProcess;

      }
    ).catch(
      function(err) {
        console.log("The following error occurred: " + err.name);
      }
    )

  },

  //--------------------------------------------------------------------------------------------------------

  onAudioProcess: function() {

    // get the average for the first channel
    self.analyser_node.getByteFrequencyData(self.array_freq_domain);
    self.analyser_node.getByteTimeDomainData(self.array_time_domain);

    var peakData = (function(){
      var peak = 0;
      for (var i=0;i<self.array_time_domain.length;i++){
        peak = peak + self.array_time_domain[i]
      }
      peak = peak / self.array_time_domain.length
      return {left: peak, right: peak}
    })();

    var stereo_freq_array = []//new Uint8Array(buffer_length * 2);
    var stereo_time_array = []//new Uint8Array(buffer_length * 2);

    for (var i=0;i<self.array_freq_domain.length;i++){
      stereo_freq_array[i+stereo_freq_array.length] = self.array_freq_domain[i]/255;
      stereo_freq_array[i] = self.array_freq_domain[i]/255;
    }
    for (var i=0;i<self.array_time_domain.length;i++){
      stereo_time_array[i+self.array_time_domain.length] = self.array_time_domain[i]/255;
      stereo_time_array[i] = self.array_time_domain[i]/255;
    }

    self.music.eqData   = stereo_freq_array;
    self.music.peakData   = peakData
    self.music.waveformData = stereo_time_array;

    if (self.microphone_stream.playbackState == self.microphone_stream.PLAYING_STATE) {
      self.analyzeSound()
    }
  },

  //--------------------------------------------------------------------------------------------------------

  analyzeSound: function() {

    var juice = self.music.juice;

    var numFreqBands = 3;
    var freqBandInterval = 256 / numFreqBands;

    var eqData    = juice.eqData    = self.music.eqData;
    var peakData  = juice.peakData  = self.music.peakData;
    var waveData  = juice.waveData  = self.music.waveformData;

    if (waveData.length == 512) {
      juice.waveDataL = waveData.slice(0, 256);
      juice.waveDataR = waveData.slice(256);
      self.music.waveDataL = juice.waveDataL
      self.music.waveDataR = juice.waveDataR
    } else {
      juice.waveDataL = juice.waveDataR = juice.waveData;
    }

    if (eqData.length == 512) {
      juice.eqDataL = eqData.slice(0, 256);
      juice.eqDataR = eqData.slice(256);
    } else {
      juice.eqDataL = juice.eqDataR = juice.eqData;
    }

    var freqBands     = juice.freqBands;
    var freqBands16     = juice.freqBands16;
    var avgFreqBands    = juice.avgFreqBands;
    var longAvgFreqBands  = juice.longAvgFreqBands;
    var relFreqBands    = juice.relFreqBands;
    var avgRelFreqBands   = juice.avgRelFreqBands;

    for (var i=0;i<numFreqBands;i++) {
      freqBands[i] = 0;
    }

    for (var i=0;i<128;i++) {
      freqBands[(i/freqBandInterval/2)>>0] += eqData[i];
    }

    for (var i=0;i<16;i++)
      freqBands16[i] = 0;
    for (var i=0;i<128;i++) {
      freqBands16[(i/8)>>0] += eqData[i];
    }

    juice.frameCount++;

    for (var i=0;i<numFreqBands;i++) {

      if (freqBands[i] > avgFreqBands[i])
        avgFreqBands[i] = avgFreqBands[i]*0.2 + freqBands[i]*0.8;
      else
        avgFreqBands[i] = avgFreqBands[i]*0.5 + freqBands[i]*0.5;

      if (juice.frameCount < 50)
        longAvgFreqBands[i] = longAvgFreqBands[i]*0.900 + freqBands[i]*0.100;
      else
        longAvgFreqBands[i] = longAvgFreqBands[i]*0.992 + freqBands[i]*0.008;

      if (Math.abs(longAvgFreqBands[i]) < 0.001) {
        relFreqBands[i] = 1.0;
      } else {
        relFreqBands[i]  = freqBands[i] / longAvgFreqBands[i];
      }

      if (Math.abs(longAvgFreqBands[i]) < 0.001) {
        avgRelFreqBands[i] = 1.0;
      } else {
        avgRelFreqBands[i]  = avgFreqBands[i] / longAvgFreqBands[i];
      }
    }

    juice.position = juice.frameCount/100 //self.position;

    if (self.testrender_enabled == true && self.has_interface == true) {
      self.testRender()
    }
  }

  //--------------------------------------------------------------------------------------------------------

}


LED.plugins.push(self)

})();

















