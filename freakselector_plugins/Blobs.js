(function(){

var self = {
	autostart: true,
	
	name		: "Blobs",
	//icon		: "Raster Bars.png",

	//fullscreen	: true,

	haspreview:true,

	start:	function(){

		self.TAU = Math.PI * 2;

		self.tempCanvas = document.createElement("canvas");
		self.tempCtx = self.tempCanvas.getContext("2d");

		self.colors = {r:255, g:0, b:0}
		self.cycle = 0;

    self.canvas_width = LED.grid_width;
    self.canvas_height= LED.grid_height;
    self.tempCanvas.width = LED.grid_width
    self.tempCanvas.height= LED.grid_width

		self.makeBlobs()
	},

	points:[],

	direction_variability:0,

	makeBlobs:	function() {
		self.points = []
		for (var i=0;i<self.BLOB_COUNT;i++){
			self.points.push({
				x : Math.random() * LED.grid_width,
				y : Math.random() * LED.grid_width,
				vx: (Math.random() * self.velocity) - (self.velocity/2),
				vy: (Math.random() * self.velocity) - (self.velocity/2),
				size:Math.floor(Math.random() * self.BLOB_SIZE) + self.BLOB_SIZE,
				colors:{
					r:Math.max(Math.round(Math.random()*255),20),
					g:Math.max(Math.round(Math.random()*255),20),
					b:Math.max(Math.round(Math.random()*255),20),
					cycle:0
				},
				directional:{
					variability: (Math.random() * self.direction_variability),
					xd:0,
					yd:0,
					xaccum:0,
					yaccum:0
				}
			});
		};

	},

	end:	function(){

	},

	play:	function(){
		self.paused = false
	},

	pause:	function(){
		self.paused = true
	},


	render: function(){
    if (self.canvas_width !== LED.grid_width || self.canvas_height !== LED.grid_height){
      self.canvas_width = LED.grid_width;
      self.canvas_height= LED.grid_height;
      self.tempCanvas.width = LED.grid_width
      self.tempCanvas.height= LED.grid_width
    }

    var this_ctx = /*self.fullscreen === true ? self.tempCtx :*/ LED.composite_ctx;

		if (self.wipe_screen == true) {
			this_ctx.clearRect(0, 0, LED.grid_width, LED.grid_height);
		}

		for (var i=0;i<self.points.length;i++) {

			var point = self.points[i]

			point.vx = point.vx + (((Math.random() * 2)-1) * point.directional.variability)
			point.vy = point.vy + (((Math.random() * 2)-1) * point.directional.variability)
			point.x += point.vx;
			point.y += point.vy;


			if(point.x > LED.grid_width + point.size) {
				point.x = -point.size;
			}
			if(point.x < -point.size) {
				point.x = LED.grid_width + point.size;
			}
			if(point.y > LED.grid_height + point.size) {
				point.y = -point.size;
			}
			if(point.y < -point.size) {
				point.y = LED.grid_height + point.size;
			}



			this_ctx.beginPath();
			var grad = this_ctx.createRadialGradient(point.x, point.y, 1, point.x, point.y, point.size);

			var R = Math.round((point.colors.r * self.color_variability) + (self.colors.r * (1-self.color_variability)));
			var G = Math.round((point.colors.g * self.color_variability)+(self.colors.g * (1-self.color_variability)));
			var B = Math.round((point.colors.b * self.color_variability)+(self.colors.b * (1-self.color_variability)));
			var newcolor = 'rgba(' + R +',' + G + ',' + B

			grad.addColorStop(0, newcolor + ','+self.opacity+')');
			grad.addColorStop(self.opacity, newcolor + ',0)');

			this_ctx.fillStyle = grad;
			this_ctx.arc(point.x, point.y, point.size, 0, self.TAU);
			this_ctx.fill();
		}


//		var imageData = self.tempCtx.getImageData(0, 0, LED.grid_width, LED.grid_height);
		var imageData = this_ctx.getImageData(0, 0, LED.grid_width, LED.grid_height);

		var pix = imageData.data;
		var i = 3;
		while ((i += 4) < pix.length) {
			if(pix[i] < self.THRESHOLD) {
				pix[i] /= self.metabalize1;
				pix[i] %= (self.THRESHOLD / self.metabalize2);
			}
		}
		for (var i=0;i<pix.length;i+=4){
			pix[i]	 = Math.round(pix[i]   * 1)//self.opacity)
			pix[i+1] = Math.round(pix[i+1] * 1)// self.opacity)
			pix[i+2] = Math.round(pix[i+2] * 1)// self.opacity)
		}
		LED.composite_ctx.putImageData(imageData, 0, 0);


		self.colorCycle();

	},


	colorCycle:	function(){
		self.cycle += self.cycle_speed;
		self.cycle %= 100;
		self.colors.r = ~~(Math.sin(0.3 * self.cycle + 0) * 127 + 128);
		self.colors.g = ~~(Math.sin(0.3 * self.cycle + 2) * 127 + 128);
		self.colors.b = ~~(Math.sin(0.3 * self.cycle + 4) * 127 + 128);
	},

//----------------------------------------------------------------

	BLOB_SIZE			: 11,
	BLOB_COUNT			: 10,
	THRESHOLD			: 138,
	velocity			: 1.56,
	cycle_speed			: .07,
	metabalize1			: 188,
	metabalize2			: 128,
	color_variability	: 0,
	opacity				: 1,
	wipe_screen			: true,

	settings_to_save	: [
		"BLOB_SIZE",
		"BLOB_COUNT",
		"THRESHOLD",
		"velocity",
		"metabalize1",
		"metabalize2",
		"cycle_speed",
		"color_variability",
		"opacity",
		"wipe_screen",
		"direction_variability"
	],

	settingsUpdate:	function() {

		self.setBlobSize()
		self.makeBlobs()
	},

	setBlobSize:	function() {

		for (var i=0;i<self.points.length;i++){
			self.points[i].size	= Math.floor(Math.random() * self.BLOB_SIZE) + self.BLOB_SIZE
		};
	},

	setVelocity:	function(){

		for (var i=0;i<self.points.length;i++){
			self.points[i].size	= Math.floor(Math.random() * self.BLOB_SIZE) + self.BLOB_SIZE
			self.points[i].vx = (Math.random() * self.velocity) - (self.velocity/2)
			self.points[i].vy = (Math.random() * self.velocity) - (self.velocity/2)
		};
	},

	interface:	function(){

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "wipescreen",
				name		: 'Wipe Screen',
				namestyle	: "width:183px;",
				style		: "position:absolute;top:110px;left:690px;",
				className	: "onoff",
				width		: 25,
				value		: self.wipe_screen,
				onchange	: function(val){
					self.wipe_screen = val
				}
			}
		})


		var sliders=[]

		var slidertypes = [
			{
				id:"blobsize",
				name:"Blob Size",
				min:.0,
				max:55,
				startvalue:self.BLOB_SIZE,
				handler:function (val){
					self.BLOB_SIZE = val
					self.setBlobSize()
				}
			},
			{
				id:"blobcount",
				name:"Blob Count",
				startvalue:self.BLOB_COUNT,
				min:0,
				max:256,
				handler:function(val){
					self.BLOB_COUNT = val
					self.makeBlobs()
				}
			},
			{
				id:"threshold",
				name:"Thershold",
				min:0,
				max:300,
				startvalue:self.THRESHOLD,
				handler:function(val){
					self.THRESHOLD = val
				}
			},
			{
				id:"velocity",
				name:"Velocity",
				min:0,
				max:20,
				startvalue:self.velocity,
				handler:function(val){
					self.velocity = val
					self.setVelocity()
				}
			},
			{
				id:"direction_variability",
				name:"Direction Variability",
				min:0,
				max:1,
				startvalue:self.direction_variability,
				handler:function(val){
					self.direction_variability = val
					self.makeBlobs()
				}
			},
			{
				id:"metabalize1",
				name:"Metabalize 1",
				min:0,
				max:200,
				startvalue:self.metabalize1,
				handler:function(val,evtsrc){
					self.metabalize1 = val
				}
			},
			{
				id:"metabalize2",
				name:"Metabalize 2",
				min:0,
				max:200,
				startvalue:self.metabalize2,
				handler:function(val,evtsrc){
					self.metabalize2 = val
				}
			},
			{
				id:"colorspeed",
				name:"Color Speed",
				min:0,
				max:1,
				startvalue:self.cycle_speed,
				handler:function(val,evtsrc){
					self.cycle_speed = val
				}
			},
			{
				id:"colorvariablility",
				name:"Color Variability",
				min:0,
				max:1,
				startvalue:self.color_variability,
				handler:function(val,evtsrc){
					self.color_variability = val
				}
			},
			{
				id:"opacity",
				name:"Opacity",
				min:0,
				max:1,
				startvalue:self.opacity,
				handler:function(val,evtsrc){
					self.opacity = val
				}
			}


		]

		self.setBlobSize()
		self.makeBlobs()

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		for (var i=0;i<slidertypes.length;i++) {

			sliders.push(
				{
					tagname	: "div",
					attributes:{
						style	: "position:relative;padding:3px;margin:4px;border:1px solid black;border-radius:5px;background-color:#2a2a4a;font-family:arial;font-size:12px;"
					},
					children:[
						{
							tagname	: "div",
							attributes	: {
								html	: slidertypes[i].name,
								style	: "font-size:20px;margin-bottom:10px;position:relative;top:5px;left:5px;"
							}
						},
						thisvalue = {
							tagname	: "div",
							attributes	: {
								id		: "slidervalue_"+slidertypes[i].id,
								html	: (slidertypes[i].startvalue||0).toFixed(2),
								style	: "font-size:20px;position:absolute;top:5px;left:-5px;text-align:right;width:100%;"
							}
						},
						{
							tagname	: "df-slider",
							attributes	: {
								id			: "slider_"+slidertypes[i].id,
								name		: slidertypes[i].name,
								className	: slidertypes[i].classname||"slider",
								min			: (slidertypes[i].min!=undefined)?slidertypes[i].min:0,
								max			: (slidertypes[i].max!=undefined)?slidertypes[i].max:1,
								startvalue	: slidertypes[i].startvalue||0,
							//	height		: (slidertypes[i].height!=undefined)?slidertypes[i].height:35,
								width		: (slidertypes[i].width!=undefined)?slidertypes[i].width:285,
								onchange	: slidertypes[i].handler,
								style		: slidertypes[i].style||"",
								showvalue	: thisvalue.attributes.id

							}
						}
					]
				}

			)
		}

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:100%;margin-left:-710px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:560px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})

	}

}

LED.plugins.push(self)

})();
