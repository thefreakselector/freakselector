(function(){


var self = {
	name		: "Oscilloscope",
//	icon		: "Center Fade.png",

//	fullscreen	: true,

//	haspreview:true,


	start:	function(){

	}
	,
	end:	function(){

	},

	play:	function(){
		self.paused = false
	},
	pause:	function(){
		self.paused = true
	},
	
	color : "00ff00",

//--------------------
	mode: 0,

	render: function(){
		if (!LED.eqData_source || !LED.eqData_source.music || !LED.eqData_source.music.juice){
			return
		}
		var juice = LED.eqData_source.music.juice

		var eqDataL = juice.waveDataR
		var ctx = LED.composite_ctx;

		//ctx.clearRect(0, 0, LED.grid_width, LED.grid_height);

	
		ctx.strokeStyle = "#"+self.color;
		ctx.lineWidth = 1;
		ctx.beginPath();
		
		var grid_spacing = Math.floor(256/LED.grid_width);
		var xx = 0;
		for (var i=0;i<256;i+=grid_spacing){
			var y_avg = 0;
			for (var ix=0;ix<grid_spacing;ix++){
				y_avg += Math.round(eqDataL[i+ix] * (LED.grid_height));
			}
			
			if (self.mode === 0){
				var yy = (y_avg / grid_spacing) 
				ctx.moveTo(xx,LED.grid_height/2)
				ctx.lineTo(xx, Math.min(LED.grid_height/2 - (yy/2) , LED.grid_height/2));
				ctx.moveTo(xx,LED.grid_height/2)
				ctx.lineTo(xx, Math.max(LED.grid_height/2 + (yy/2) , LED.grid_height/2));
			}
			if (self.mode === 1){
				var yy = (y_avg / grid_spacing) 
				ctx.moveTo(xx,LED.grid_height/2)
				ctx.lineTo(xx, LED.grid_height/2 + yy);
			}
			if (self.mode === 2){
				var yy = (y_avg / grid_spacing) 
				ctx.moveTo(xx,LED.grid_height)
				ctx.lineTo(xx, LED.grid_height - yy);
			}
			if (self.mode === 3){
				var yy = (y_avg / grid_spacing)/2 //Math.round(eqDataL[i] * (LED.grid_height/2));
				var linetop = (LED.grid_height/2) - yy
				ctx.moveTo(xx,LED.grid_height/2)
				ctx.lineTo(xx, linetop);				
			}

			xx++
		}
		ctx.stroke();		

	},



	showAnalyzedSound: function(){
	
	
	},


	settings_to_save:[
	],

	interface:	function(){


		self.colorpicker1_div = df.newdom({
			tagname:"div",
			target:LED.pluginwindow,
			attributes:{
				style:"position:absolute;top:20px;left:20px;"
			}
		})

		self.colorpicker1 = new df.ColorPicker({
			target: self.colorpicker1_div,
			width : 256,
			height: 50,
			startvalue:self.color,
			callback:function(new_color){
				self.color = new_color
			}
		})

			var procwin = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "bpmSelectWin",
					style	: "position:absolute;top:5px;left:658px;z-index:5000;color:white;background-color:#7a7a7a;padding:5px;overflow:hidden;border-radius:5px;"
				}
			})


			var namestyle = "width:180px;font-size:20px;margin-bottom:3px;"
      var radio_off = LED.radio_off_image;
      var radio_on  = LED.radio_on_image;
			var radio_w = 32
			var radio_h = 32

			var proc0 = df.newdom({
				tagname		:"df-onoff",
				target		: procwin,
				attributes:{
					id		: "testonoff0",
					name		: "Mode 0",
					namestyle	: namestyle,
					style		: "position:relative;",
					class		: "onoff",
					value		: (self.mode === 0),
					width		: radio_w,
					height		: radio_h,
					image_on	: radio_on,
					image_off	: radio_off,
					onchange	: function(val){

						if (val==false) {return true}

						proc0.setState(false)
						proc1.setState(false)
						proc2.setState(false)
						proc3.setState(false)

						self.mode = 0

					}
				}
			})

			df.newdom({tagname:"br",target:procwin})

			var proc1 = df.newdom({
				tagname		:"df-onoff",
				target		: procwin,
				attributes:{
					id		: "testonoff1",
					name		: "Mode 1",
					namestyle	: namestyle,
					style		: "position:relative;",
					class		: "onoff",
					value		: (self.mode === 1),
					width		: radio_w,
					height		: radio_h,
					image_on	: radio_on,
					image_off	: radio_off,
					onchange	: function(val){

						if (val==false) {return true}

						proc0.setState(false)
						proc1.setState(false)
						proc2.setState(false)
						proc3.setState(false)

						self.mode = 1

					}
				}
			})

			df.newdom({tagname:"br",target:procwin})



			var proc2 = df.newdom({
				tagname		:"df-onoff",
				target		: procwin,
				attributes:{
					id		: "testonoff2",
					name		:  "Mode 2",
					namestyle	: namestyle,
					style		: "position:relative;",
					class		: "onoff",
					value		: (self.mode === 2),
					width		: radio_w,
					height		: radio_h,
					image_on	: radio_on,
					image_off	: radio_off,
					onchange	: function(val){

						if (val==false) {return true}

						proc0.setState(false)
						proc1.setState(false)
						proc2.setState(false)
						proc3.setState(false)

						self.mode = 2

					}
				}
			})

			df.newdom({tagname:"br",target:procwin})

			var proc3 = df.newdom({
				tagname		:"df-onoff",
				target		: procwin,
				attributes:{
					id		: "testonoff3",
					name		: "Mode 3",
					namestyle	: namestyle,
					style		: "position:relative;",
					class		: "onoff",
					value		: (self.mode === 3),
					width		: radio_w,
					height		: radio_h,
					image_on	: radio_on,
					image_off	: radio_off,
					onchange	: function(val){

						if (val==false) {return true}

						proc0.setState(false)
						proc1.setState(false)
						proc2.setState(false)
						proc3.setState(false)

						self.mode = 3


					}
				}
			})



	}

}

	LED.plugins.push(self)

})();
