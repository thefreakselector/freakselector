(function(){

var self = {

	name		: "Waterfall",
	//icon		: "Video Input.png",

	render:	function(){
		self.draw()
		//LED.composite_ctx.drawImage(self.canvas, 0, 0,LED.grid_width,LED.grid_height)
	},

	start:	function(){

	},

	end:	function() {

	},

	settings_to_save:	[
		"water_count",
		"water_bounce",
		"water_acc",
		"water_opacity",
		"water_radius",
		"water_color_red",
		"water_color_green",
		"water_color_blue",
		"horizontal",
		"bounces"
	],

	water_count		: 550,
	water_bounce	: -.51,
	water_acc		: 0.05,
	water_opacity	: 0.1,
	water_radius	: 0.75,

	water_color_red:0,
	water_color_green:150,
	water_color_blue:255,

	horizontal		: true,

	bounces			: 4,

	//----------------
	audio_kick:0,

	speed: .25,

	interface:	function() {


//================================================================================================================
//================================================================================================================

// Get the canvas DOM element along with it's 2D context
// which is basically what we do our work on

var width = LED.grid_width
var height = LED.grid_height

//Flocking Behaviour

var ctx = LED.composite_ctx

var w =width
var h = height

//================================================

var particles = []
var count = self.water_count
var bounce = self.water_bounce
var acc = self.water_acc;
var Particle

var init = self.init = function() {

	Particle = function() {

		this.radius = self.water_radius;
		this.hits = 0;

		if (self.horizontal == true) {
			this.x = Math.random() * (w/1.5);
			this.y = Math.random() * (h);
		} else {
			this.x = Math.random() * w;
			this.y = Math.random() * (h/1.5);
		}

		if (self.horizontal == true) {
			this.vy = Math.random() * .1;
			this.vx =this.radius + Math.random() * 2;
		} else {
			this.vy = this.radius +Math.random() * 2;
			this.vx = Math.random() * .1;
		}

		var red		= (self.water_color_variability > 0) ? Math.round( (self.water_color_red * (1-self.water_color_variability) ) + ((Math.random() * self.water_color_variability * 255) * (self.water_color_variability)) ) : Math.round(self.water_color_red)
		var green	= (self.water_color_variability > 0) ? Math.round( (self.water_color_green * (1-self.water_color_variability) ) + ((Math.random() * self.water_color_variability * 255) * (self.water_color_variability)) ) : Math.round(self.water_color_green)
		var blue	= (self.water_color_variability > 0) ? Math.round( (self.water_color_blue * (1-self.water_color_variability) ) + ((Math.random() * self.water_color_variability * 255) * (self.water_color_variability)) ) : Math.round(self.water_color_blue)

		this.draw = function() {
			var opacity = self.water_opacity
			ctx.fillStyle = "rgba("+red+", "+green+", "+blue+", "+opacity+")";
			ctx.beginPath();
			ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, false);
			ctx.closePath();
			ctx.fill();
		};
	}

	count = self.water_count
	bounce = self.water_bounce
	acc = self.water_acc;

	//Fill the insects into flock
	particles = []
	for(var i = 0; i < count; i++) {
		particles.push(new Particle());
	}
}

var paintCanvas = function() {
	ctx.fillStyle = "black";
	ctx.fillRect(0, 0, w, h);
}

var draw = self.draw = function() {
//	paintCanvas();

	//Draw particles
	for(var j = 0; j < particles.length; j++) {
		var p = particles[j];
		p.draw();

		p.y += (p.vy) * self.speed;
		p.x += (p.vx) * self.speed;


		//Acceleration in acrion
		if (self.horizontal == true) {
			p.vx += acc += self.audio_kick;
		} else {
			p.vy += acc += self.audio_kick;
		}

		//Detect collision with floor
		if(p.y > h) {
			p.y = h - p.radius;
			p.vy *= bounce;
			p.hits++;
		} else if(p.y < p.radius) {
			p.y = p.radius;
			p.vy *= bounce;
			p.hits++;
		}

		//Detect collision with walls
		if(p.x > w) {
			p.x = w - p.radius;
			p.vx *= bounce;
			p.hits++;
		} else if(p.x < p.radius) {
			p.x = p.radius;
			p.vx *= bounce;
			p.hits++;
		}

		//Regenerate particles
		if(p.hits > self.bounces) {
			//console.log("hits = " + p.hits);
			particles[j] = new Particle()
		}
	}
}

init()


//================================================================================================================
//================================================================================================================
//	bird_count:	90,
//	bird_speed: 3,
//	bird_size : 3,


		var sliders=[]

		var slidertypes = [
			{
				id:"water_count",
				name:"Count",
				startvalue:self.water_count,
				min:0,
				max:1000,
				handler:function(val){
					self.water_count = val
					init()
				}
			},
			{
				id:"water_speed",
				name:"Speed",
				startvalue:self.speed,
				min:0,
				max:1,
				handler:function(val){
					self.speed = val * 2
					init()
				}
			},
			{
				id:"water_bounce",
				name:"Bounce",
				startvalue:-(self.water_bounce),
				min:0,
				max:2,
				handler:function(val){
					self.water_bounce = -val
					init()

				}
			},
			{
				id:"water_acc",
				name:"Acceleration",
				startvalue:self.water_acc,
				min:0,
				max:1,
				handler:function(val){
					self.water_acc = val
					init()

				}
			},
			{
				id:"water_opacity",
				name:"Opacity",
				startvalue:self.water_opacity,
				min:0,
				max:1,
				handler:function(val){
					self.water_opacity = val
					init()

				}
			},
			{
				id:"water_radius",
				name:"radius",
				startvalue:self.water_radius,
				min:0,
				max:5,
				handler:function(val){
					self.water_radius = val
					init()

				}
			},
			{
				id:"water_color_variability",
				name:"Color Variability",
				startvalue:self.water_color_variability||0,
				min:0,
				max:1,
				handler:function(val){
					self.water_color_variability = val
					init()

				}
			},

			{
				id:"water_color_red",
				name:"Red",
				startvalue:self.water_color_red,
				min:0,
				max:255,
				handler:function(val){
					self.water_color_red = val
					init()

				}
			},
			{
				id:"water_color_green",
				name:"Green",
				startvalue:self.water_color_green,
				min:0,
				max:255,
				handler:function(val){
					self.water_color_green = val
					init()

				}
			},
			{
				id:"water_color_blue",
				name:"Blue",
				startvalue:self.water_color_blue,
				min:0,
				max:255,
				handler:function(val){
					self.water_color_blue = val
					init()

				}
			},
			{
				id:"water_bounces",
				name:"Bounces",
				startvalue:self.bounces,
				min:0,
				max:19,
				handler:function(val){
					self.bounces = Math.round(val+1)
					return self.bounces

				}
			}


		]

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		var sliders = LED.sliderList(slidertypes)


		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})

//==========================================================================================


}

};

LED.plugins.push(self)


})();
