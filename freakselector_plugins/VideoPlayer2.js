(function(){


var self = {

	name		: "Video Player",
	icon		: "Video Player.png",
	has_audio	: true,		//this can toggle???
	fullscreen	: true,
  sort_top: true,

  auto_start: false,

/*
to encode:

ffmpeg2theora -v 10 -V 1500 --optimize -x 96 -y 48 -a 8 -A 96 -o "01 Farbrausch - fr041 Debris (Breakpoint 2007).ogv" "01 Farbrausch - fr041 Debris (Breakpoint 2007).mp4"


*/

	playback_speed: .1,
	opacity: 1,
	looping	: true,
	//---------------------------------------------------

	start:	function(startsong){


		if (self.videoplayer==undefined) {

			self.videoplayer = df.newdom({
				tagname:"video",
				target:LED.contextHolder,
				attributes:{
					preload:"metadata",
					style:"width:"+((LED.grid_width/LED.grid_height)*80)+"px;height:80px;z-index:60000;",
					className:"previewcontext"
				}

			})

			self.videoplayer.addEventListener('loadedmetadata', function(evt) {
			    console.log("loadmetadata",self.videoplayer.duration);
			});
			self.videoplayer.addEventListener('durationchange', function(evt) {
			    console.log("durationchange",self.videoplayer.duration);
			});


			df.attachevent({
				name:"loadedmetadata",
				domobj:self.videoplayer,
				handler:function(evtobj){

					self.videoplayer.currentTime = 99999999
					setTimeout(function(){
						self.videoplayer.loop = self.looping
						self.videoplayer.play()
						self.videoplayer.playbackRate = (self.playback_speed * 10)
						self.play_state = "playing"
					},100)
				}
			})

			self.getDefaultSettings();

		}



	},

	//---------------------------------------------------

  getDefaultSettings: function(cb){

  	//alert('q')
			df.ajax.send({
			  url : "getSetting/"+self.name,
			  verb:"get",
			  callback:function(ajaxobj){
			    var settings = JSON.parse(ajaxobj.text);

					self.playback_speed =  settings.playback_speed;

					self.volume_level = settings.volume_level;

					self.selectVideo(encodeURIComponent(settings.videoUrl), true);

					self.treeService.currently_playing = settings.videoUrl.split('/').pop();

			    if (cb){cb()}
			  }
			})

  },

	//---------------------------------------------------

	filetypes	: {
		"ogv":1,
    "webm":1,
    "mp4": 1
	},

	//---------------------------------------------------

	end:	function(){
		self.pause()
	},

	//=============================================
	//audio and video plugins need these functions
	play:	function(idx) {
		try{
			if (self.videoplayer){
				self.videoplayer.play()
			}
		}catch(e){}
	},
	pause:	function() {
		try{
			self.videoplayer.pause()
		}catch(e){}
	},
	stop:	function() {
		try{
			self.videoplayer.pause()
			self.videoplayer.currentTime = 0
		}catch(e){}
	},

	next:	function() {
		self.treeService.next();
	},

	last:	function() {
		self.treeService.last();
	},


	fading:true,

	fileSelect: function(file){
		self.selectVideo(file)
	},

	//==========================================================================================

	selectVideo:	function(video, faded) {

		self.current_video = video;

console.log('>>>>>>>>>>>>>>>>>>>>',video)

		var filename = "getStream/"+video //(unescape(video)).replace(/\//g,"\\").replace(/\:\\/g,"/").replace(/\:/g,"").replace(/\\/g,"/").replace(/\+/g,"%2B")

		if (self.fading == true && faded!=true) {
			LED.fadeOut(function(){
				self.selectVideo(video,true)
			})

		} else {
			self.videoplayer.pause()

			self.videoplayer.src = filename	//TOP_DEMOS/

			if (self.fading==true) {
				df.waitfor(function(){return (LED.fade_inprogress==false)},function(){LED.fadeIn()})
			}
		}
	},

	//==========================================================================================

	setMute:	function(mute){
		self.mute = mute
	},
	getMute:	function() {
		return self.mute
	},

	setVolume:	function(vol){
					// 0 - 1
	},
	getVolume:	function(){
		return 1	// 0 - 1
	},

	getPosition:	function(){
		return self.position || 0
	},
	setPosition:	function(milliseconds){
		self.position = milliseconds || 0
	},

	//==========================================================================================

	toggleAudio:	function(){
		self.has_audio = !self.has_audio
		//self.has_audio		??
	},

	volume_level:0,

	render: function(context){
		LED.composite_ctx.globalAlpha = self.opacity;
		LED.composite_ctx.drawImage(self.videoplayer,0,0,LED.grid_width,LED.grid_height);
		LED.composite_ctx.globalAlpha = 1;

		self.updatePosition()

		self.videoplayer.volume = (LED.car_volume * LED.master_volume * self.volume_level)


	},

	//==========================================================================================

	interface:	function(){

		self.listholder = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes	: {
				id		: "listholder",
				className	: "buttonlist",
				style	: "left:5px;width:570px;height: 480px;"
			},
			children	: [
				{
					tagname	: "ul",
					attributes:	{
						"id"	: "playlist"
					}
				}
			]
		})

		if(!self.treeService){
			self.treeService = new LED.treeService({
				media_type: self.name,
				listholder: self.listholder,
				fileSelect: self.fileSelect,
				filetypes : self.filetypes
			})
		} else {
			self.treeService.listholder = self.listholder
			self.treeService.renderTree()
		}


//==========================================================================================
//media . playbackRate [ = value ]

		var slidertypes = [
			{
				id:"progress_slider",
				name:"Position",
				startvalue: self.position,
				min:0,
				max:1,
				handler:function(val){
					var duration = self.videoplayer.duration
					var buffered = self.videoplayer.buffered.end(0)

					var time = self.secondsToTime(duration-(duration*val))

					self.newposition = (duration*val)
console.log("val",self.newposition ,self.videoplayer.currentTime)

					return "-"+time

				},
				mousedown:function(){
					self.positionmoving = true
					self.newposition = undefined
				},
				mouseup:function(){

					if (self.newposition!=undefined) {

							setTimeout(function(){

console.log("mouseup",self.newposition )
							  self.videoplayer.currentTime = self.newposition
								df.dg("slider_progress_slider").getElementsByTagName("input")[0].value = ((self.newposition / self.videoplayer.duration)*100)
								self.newposition = undefined
								self.positionmoving = false
							},50)

					}

				}
			}
			,

			{
				id:"video_speed_slider",
				name:"Playback Speed",
				startvalue: .1,
				min:0,
				max:1,
				handler:function(val){
					self.playback_speed = val;
					self.videoplayer.playbackRate = (val * 10)
				}
			}
			,

			{
				id:"video_volume_slider",
				name:"Volume",
				startvalue:self.volume_level,
				min:0,
				max:1,
				handler:function(val){
					self.volume_level = val
					self.videoplayer.volume = (LED.car_volume * LED.master_volume * self.volume_level)
				}
			},

			{
				id:"video_opacity_slider",
				name:"Opacity",
				startvalue: self.opacity,
				min:0,
				max:1,
				handler:function(val){
					self.opacity = val;
					return val.toFixed(2);
				}
			}

		]

		var sliders = LED.sliderList(slidertypes)

		self.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:587px;top:6px;color:white;"
				},
				children	: sliders
		})


		self.bufferedDiv = df.newdom({
			tagname	: "div",
			target	: df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0],
			attributes:{
				style:"width:"+df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0].offsetWidth+"px;height:"+df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0].offsetHeight+"px;background-color:red;z-index:50;position:absolute;overflow:hidden;opacity:.3"
			}

		})

		var namestyle = "width:260px;font-size:30px;margin-bottom:3px;margin-left:10px;"
    var radio_off = LED.radio_off_image;
    var radio_on  = LED.radio_on_image;

		df.newdom({tagname:"br",target:self.positionDiv})

		var advancebutton = df.newdom({
			tagname		:"df-onoff",
			target		: self.positionDiv,
			attributes:{
				id		: "advanceonoff",
				name		: "Auto Advance",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.auto_advance==true),
				width		: 40,
				height		: 40,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){
					if (val==true) {
						loopbutton.setState(false)
						self.looping = false
						self.videoplayer.loop = false
					}

					self.auto_advance = val
				}
			}
		})


		df.newdom({tagname:"br",target:self.positionDiv})

		var loopbutton = df.newdom({
			tagname		:"df-onoff",
			target		: self.positionDiv,
			attributes:{
				id			: "looponoff",
				name		: "Loop Video",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: self.looping,
				width		: 40,
				height		: 40,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){
					if (val==true) {
						advancebutton.setState(false)
						self.auto_advance = false
					}
					self.looping = val
					self.videoplayer.loop = self.looping
				}
			}
		})


		df.newdom({tagname:"br",target:self.positionDiv})

		var fadebutton = df.newdom({
			tagname		:"df-onoff",
			target		: self.positionDiv,
			attributes:{
				id			: "fadeonoff",
				name		: "Fading",
				namestyle	: namestyle+";margin-top:10px;",
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.fading==true),
				width		: 40,
				height		: 40,
				//image_on	: radio_on,
				//image_off	: radio_off,
				onchange	: function(val){
					self.fading = val
				}
			}
		})


	//==========================================================================================


	//==========================================================================================
//	setTimeout(function(){

		var fadebutton = df.newdom({
			tagname		:"input",
			target		: LED.pluginwindow,
			attributes:{
				id			: "savedefaultvideo",
				style		: "position: absolute;top:500px;left:290px;width:290px;height:41px;font-size:20px;line-height:10px;background-color:#2a2a2a",
				name		: "Set Default Video",
				type		: "button",
				class		: "bigbutton2",
				value		: "Set Default Video",
				onclick	: function(val){
//					self.fading = val
//alert( self.current_video )
					self.saveDefaultVideo();
				}
			}
		})

		var select_folders = df.newdom({
			tagname		:"input",
			target		: LED.pluginwindow,
			attributes:{
				id		: "musicplayer_select_folders",
				className: "bigbutton2",
				value		: "Select Folders",
				type		: "button",
				style		: "position: absolute;top:500px;left:0px;width:285px;height:41px;font-size:20px;line-height:10px;",
				onclick	: function(val){
					LED.selectFolders(self.name, function(folders){
						self.treeService.refresh(folders);
					})
				}
			}
		})


	//},100);

//==========================================================================================


	},

  saveDefaultVideo: function(cb){

  	var savedData = {
  		videoUrl: self.current_video,
  		playback_speed: self.playback_speed,
  		volume_level : self.volume_level
  	}

    df.ajax.send({
      url : "saveSetting/"+self.name,
      verb:"post",
      formdata:JSON.stringify( savedData ),
      callback:function(ajaxobj){
        //console.log("save pixelmap",ajaxobj.text)
        if (cb){cb()}
      }
    })
  },

	updatePosition:	function() {

		var fadeout_time = ((1/LED.fade_speed) * LED.fade_timeout)

		if (self.has_interface != true || self.videoplayer.buffered.length==0) {return}

//console.log(self.videoplayer.currentTime,self.videoplayer.duration,self.videoplayer.buffered.end(0))

/*
		var scroll_gadget_width = self.positionDiv.getElementsByClassName("df_slider_gadget")[0].offsetWidth
		var race_width = self.positionDiv.getElementsByClassName("df_slider_race")[0].offsetWidth - scroll_gadget_width

		var duration = self.videoplayer.duration
		var buffered = self.videoplayer.buffered.end(0)

		if (buffered < duration) {
			self.bufferedDiv.style.display="block"
			self.bufferedDiv.style.left = Math.round((race_width+scroll_gadget_width) *  (buffered / duration ) )+"px"

		} else {
			self.bufferedDiv.style.display="none"
		}
*/

		var duration = self.videoplayer.duration
		var buffered = self.videoplayer.buffered.end(0)

		self.completed = (self.videoplayer.currentTime / duration)

		var time = self.secondsToTime(duration-(duration * self.completed))


		if (self.positionmoving!=true) {

		df.dg("slider_progress_slider").getElementsByTagName("input")[0].value = (self.videoplayer.currentTime / duration)*100

		//self.positionDiv.getElementsByClassName("df_slider_gadget")[0].style.left = Math.round(race_width*self.completed)+"px"

			df.dg("slidervalue_progress_slider").innerHTML = "-"+time //(self.completed).toFixed(2)
		}

		//var queue = window.soundManager.sounds

		var time_remaining = ((duration-(duration*self.completed))*1000)

		if (self.auto_advance == true) {
			if ( time_remaining <  fadeout_time && time_remaining != 0 && LED.fade_inprogress == false) {
				self.next()
			}
		}

	},

	secondsToTime:	function (seconds) {

		// multiply by 1000 because Date() requires miliseconds
		var date = new Date(0,0,0,0,0,0,seconds * 1000);
		var hh = date.getHours();
		var mm = date.getMinutes();
		var ss = date.getSeconds();
		// This line gives you 12-hour (not 24) time
		if (hh > 12) {hh = hh - 12;}
		// These lines ensure you have two-digits
		if (hh < 10) {hh = "0"+hh;}
		if (mm < 10) {mm = "0"+mm;}
		if (ss < 10) {ss = "0"+ss;}
		// This formats your string to HH:MM:SS
		var t = ((hh!="00")?hh+":":"")+mm+":"+ss;
		return t
	}

//====================================================================================================

};

//====================================================================================================



LED.plugins.push(self)


})();
