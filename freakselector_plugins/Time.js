(function(){


var self = {
	name		: "Time",
	icon		: "Time.png",

	fontSize	: "43px",
	fontFace	: "Arial",
	fontWeight	: "",
	color		: "#ffff2f",

	startstop	: false,

	fontobj		: {
		fontstyle	: "15px 'Fixedsys Regular'",
		fillcolor	: "#ffffff",
		x			: 3,
		y			: 16
	},
	
	start:	function(){
		
		self.vcrimage = new Image()
		self.vcrimage.src = "images/vcr_1200.png"

	}
	,
	end:	function(){

	}
	,
	
	flashtimer:0,
	
	render: function(context){

		if (self.startstop != true) {return}
		
		var colon = ""
		
		var context = LED.composite_ctx
		
		if (self.timemode == 0) {

//			var thistext = "12:00"
			
			self.flashtimer++
			if (self.flashtimer<15) {

			context.drawImage(self.vcrimage,0,0,LED.grid_width,LED.grid_height)


			} else {
				if (self.flashtimer>30) {
					self.flashtimer = 0
				}
			}

		} else if (self.timemode == 1) {

			self.flashtimer++
			if (self.flashtimer<15) {
				colon = ":"			
			} else {
				if (self.flashtimer>30) {
					self.flashtimer = 0
				}
				colon = " "
			}

			var thistext = String(new Date()).split(" ")[4].split(":").slice(0,2).join(":")
console.log(thistext)
			var thishour = thistext.split(":")[0]
//			var thishour = parseInt(thistext.split(":")[0],10)
	//					console.log(thishour)
		//	thishour = ((String(thishour).substr(0,1)==="0") ? String(thishour).substr(1,1) : String(thishour) )

			var ampm = "AM"

			if (parseInt(thishour,10)>12) {
				thishour-=12
				thistext = String(thishour)+":"+thistext.split(":").slice(0).join(":")
				ampm = "PM"
			} else if (parseInt(thishour,10) == 0) {
				thistext = "12:"+(thistext.split(":").slice(1).join(":"))
			} else {
				thistext = thishour+":"+thistext.split(":").slice(1).join(":")
			}
			
			thistext = thistext.replace(/\:/g,colon)

			context.font = self.fontobj.fontstyle//self.fontobj.fontsize+"px "+LED.fontlist[self.fontobj.fontfaceidx]
			context.fillStyle = "#ffffff"
			context.fillText(thistext,self.fontobj.x,self.fontobj.y)
			//context.font = (self.fontobj.fontsize-3)+'px '+LED.fontlist[self.fontobj.fontfaceidx]
		//	context.fillText(ampm,self.fontobj.x,self.fontobj.y+self.fontobj.fontsize)
		}

	},

	interface:	function(){

		var procwin = df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "processWindow",
				style	: "position:absolute;top:5px;left:10px;z-index:5000;color:white;background-color:#7a7a7a;padding:5px;overflow:hidden;border-radius:5px;"
			}/*,
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "processes",
						style	: "clear:both;position:relative;"
					},
					children	: processes

				}
			]*/
		})
		
		var namestyle = "width:303px;font-size:30px;margin-bottom:3px;"
    var radio_off = LED.radio_off_image;
    var radio_on  = LED.radio_on_image;
		var radio_w = 48
		var radio_h = 48
			
		var proc1 = df.newdom({
			tagname		:"df-onoff",
			target		: procwin,
			attributes:{
				id		: "testonoff1",
				name		: "VCR 12:00",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.timemode==0),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){
				
					if (val==false) {return true}
					
					proc1.setState(false)
					proc2.setState(false)
					
					self.timemode = 0
					

				}
			}
		})
		
		df.newdom({tagname:"br",target:procwin})

		var proc2 = df.newdom({
			tagname		:"df-onoff",
			target		: procwin,
			attributes:{
				id			: "testonoff2",
				name		: "Current Time",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.timemode==1),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){

					if (val==false) {return true}

					proc1.setState(false)
					proc2.setState(false)
					
					self.timemode = 1

				}
			}
		})
		


//============================================================

		var startwin = df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "startWindow",
				style	: "position:absolute;top:5px;left:358px;z-index:5000;color:white;background-color:#7a7a7a;padding:5px;overflow:hidden;border-radius:5px;"
			}/*,
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "processes",
						style	: "clear:both;position:relative;"
					},
					children	: processes

				}
			]*/
		})

			
		var timeon = df.newdom({
			tagname		:"df-onoff",
			target		: startwin,
			attributes:{
				id		: "stoponoff",
				name		: "Start",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.startstop==true),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){
									
					timeon.setState(true)
					timeoff.setState(false)
					
					self.startstop = true
					

				}
			}
		})
		
		df.newdom({tagname:"br",target:procwin})

		var timeoff = df.newdom({
			tagname		:"df-onoff",
			target		: startwin,
			attributes:{
				id			: "startonoff",
				name		: "Stop",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.startstop==false),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){

					timeon.setState(false)
					timeoff.setState(true)
					
					self.startstop = false

				}
			}
		})
		
	}

}

	LED.plugins.push(self)	
})();
