(function(){


var self = {

	name		: "BPM Tap",
	//icon		: "Full Eq.png",
	uses_eqData	: true,

	start:	function(){
		self.count = 0;
	}
	,
	end:	function(){

	},
	

	count:0,
	msecsFirst:0,
	msecsPrevious:0,

	ResetCount:	function() {
	  
	//  document.TAP_DISPLAY.T_AVG.value = "";
	//  document.TAP_DISPLAY.T_TAP.value = "";
	//  document.TAP_DISPLAY.T_RESET.blur();
	},
	
	bmpCount:	function() {
	
	
/*<!-- Original:  Derek Chilcote-Batto (dac-b@usa.net) -->
<!-- Web Site:  http://www.mixed.net -->
<!-- Rewritten by: Rich Reel all8.com -->*/

	var timeSeconds = new Date;
	var msecs = timeSeconds.getTime();
	
	if ((msecs - self.msecsPrevious) > 1000 * 2) {
		self.count = 0;
	}

	if (self.count == 0) {
	//    document.TAP_DISPLAY.T_AVG.value = "First Beat";
	//    document.TAP_DISPLAY.T_TAP.value = "First Beat";
	self.msecsFirst = msecs;
	self.count = 1;
	} else {
		self.bpmAvg = 60000 * self.count / (msecs - self.msecsFirst);
		//document.TAP_DISPLAY.T_AVG.value = Math.round(bpmAvg * 100) / 100;
		//document.TAP_DISPLAY.T_WHOLE.value = Math.round(bpmAvg);
		self.count++;
		//document.TAP_DISPLAY.T_TAP.value = count;
	}
	self.msecsPrevious = msecs;
	
	self.bmpresult.innerHTML = (self.bpmAvg||0).toFixed(2)

	LED.BPM = (self.bpmAvg||0).toFixed(2)
  

//document.onkeypress = TapForBPM;

//  End -->	
	
	},
	
	interface:	function(){
		
//==========================================================================================
	
		self.tapdiv = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:30px;left:30px;width:300px;height:300px;border:2px solid black;background-color:white;border-radius:5px;"
			},
			events:[
					{
						name:"mousedown",
						handler:function(evt){
							self.bmpCount()
							df.anim({target:self.tapdiv,styles:{backgroundColor:{min:"#ff0000",max:"#ffffff",slope:df.slidein,steps:10}}})

						}						
					}
			],
			children:[
				{
					tagname	: "div",
					attributes:{
						html	:"TAP HERE<br>to count BPM",
						style	: "margin-top:114px;font-family:arial;font-size:30px;text-align:center;"
					}
				}
			]
		})
	

		self.bmpresult = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:30px;left:400px;padding:10px;font-family:arial;font-size:50px;border-radius:5px;border:2px solid black;background-color:#cacaca",
				html	: (self.bpmAvg||0).toFixed(2)
			}
		})
		
		df.keys.addkeystroke({key:13,callback:self.hitKey})
		df.keys.addkeystroke({key:32,callback:self.hitKey})

	},
	
	hitKey:	function(evt) {

		self.bmpCount()
		//df.anim({target:self.tapdiv,styles:{backgroundColor:{min:"#ff0000",max:"#ffffff",slope:df.slidein,steps:2}}})
		
		
		return false
		
	},
	
	removeInterface:	function() {
		df.keys.removekeystroke({key:13,callback:self.hitKey})
		df.keys.removekeystroke({key:32,callback:self.hitKey})
		
	}

}

	LED.plugins.push(self)

})();
