(function(){


var self = {
	name		: "Image Viewer",
	icon		: "Image Viewer.png",
	fullscreen	: true,


	start:	function(){
				
		if (self.tempcanvas==undefined) {
			self.tempcanvas = df.newdom({
				tagname:"canvas",
				target:LED.contextHolder,
				attributes:{
					className:"previewcontext"
				//	style:"display:none;"
				}
			})
		}

		self.tempcanvas_ctx = self.tempcanvas.getContext("2d")

		
		self.image = new Image()
		self.image.onload = function() {

			self.aspect = self.image.width / self.image.height;
			
			self.tempcanvas.style.width = (self.aspect*LED.grid_height)+"px";
			self.tempcanvas.style.height= LED.grid_height+"px";
			self.thiswidth  = self.image.width;
			self.thisheight = self.image.height;
			self.thisx = 0;
			self.thisy = 0;
			
		}

		self.image.src = "Graphics/burning_man_1990.jpg"
		
	}
	,
	end:	function(){

	},
	
	xd:-3,
	yd:-2,
	wd:-2,
	hd:0,
	render: function(){


		self.tempcanvas_ctx.clearRect(0, 0, self.thiswidth*2,self.thisheight*2);

		if (self.thisx!=undefined) {
			self.thisx += self.xd
			self.thisy += self.yd

//console.log(self.thisx,self.thisy,LED.grid_width,self.thiswidth)

			
			if (self.thisx>0) {self.thisx=0;self.xd=-self.xd}
			if (self.thisy>0) {self.thisy=0;self.yd=-self.yd}
			if (self.thisx+self.thiswidth-(LED.grid_width*4)<0) {self.xd=-self.xd}
			if (self.thisy+self.thisheight-(LED.grid_height*4)<0) {self.yd=-self.yd}
			
			
			/*
			self.thiswidth += self.wd
			self.thisheight += self.wd*self.aspect
			
			if (
				self.thiswidth<LED.grid_width/2		||
				self.thiswidth>self.image.width*4	||
				self.thisheight<LED.grid_height/2	||
				self.thisheight>self.image.height*4
			) {
				self.wd=-self.wd
			}
			if (
				self.thisx < -(self.thiswidth-LED.grid_width)/2	||	
				self.thisx > 0
			) {
				self.xd = (self.xd>0)?Math.random()*-3:Math.random()*3;
				self.thisx += self.xd
			}
			
			if (
				self.thisy < -(self.thisheight-LED.grid_height)/2	||
				self.thisy > 0
			) {

 				self.yd =  (self.yd>0)?Math.random()*-3:Math.random()*3;
				self.thisy += self.yd
			}
			*/

			self.tempcanvas_ctx.drawImage(self.image,self.thisx,self.thisy,self.thiswidth,self.thisheight)

			LED.composite_ctx.drawImage(self.tempcanvas, 0, 0,LED.grid_width,LED.grid_height)
		}


	},

	interface:	function(){
	
		df.newdom({
			tagname:"input",
			target: LED.pluginwindow,
			attributes:{
				type	: "button",
				value	: "Rotate 90 degrees",
				style	: "position:absolute;top:10px;left:500px;width:200px;height:35px;font-family:arial;font-size:20px;"
			},
			events:[
				{
					name	: "click",
					handler	: function(evt) {
						
						self.tempcanvas_ctx.rotate(-Math.PI / 2);

					}

				}
			]

		})
		
	}

}

	LED.plugins.push(self)
})();

