(function(){

var self = {
	name		: "Sound Effects",
	icon		: "Sound Effects.png",
	has_audio	: true,

  sort_top: true,

	volume_level:1,

	looping:	false,

	//=============================================

	start:	function(startsong){

	},

	fileSelect: function(file){
		self.selectSong(file)
	},

	//=============================================

	filetypes	: {
		"mp3" : true,
		"wav" : true,
		"ogg" : true
	},

	end:	function(){

		self.stop()

		try{clearInterval(self.positionInterval)}catch(e){}

		if (self.music) {
			self.music.whileplaying = null;
			self.music.unload()
			soundManager.destroySound(self.music.sID)
			delete self.music
		}

	},

	//=============================================
	//audio and video plugins need these functions
	play:	function(idx) {
		if (self.music.playState==0 || self.music.paused == true) {
			soundManager.play(self.music.sID)
			try{clearInterval(self.positionInterval)}catch(e){}
			self.positionInterval = setInterval(function(){
					self.updatePosition(self.music)
			},250)

		}
	},
	pause:	function() {
		soundManager.pause(self.music.sID)
		try{clearInterval(self.positionInterval)}catch(e){}
	},
	stop:	function() {
		//soundManager.stopAll();
		if (self.current_sound){soundManager.stop(self.current_sound)}
		try{clearInterval(self.positionInterval)}catch(e){}
	},

	setMute:	function(mute){
		self.mute = mute
	},
	getMute:	function() {
		return self.mute
	},

	setVolume:	function(vol){
					// 0 - 1
	},
	getVolume:	function(){
		return 1	// 0 - 1
	},

	getPosition:	function(){
		return self.position || 0
	},
	setPosition:	function(milliseconds){
		self.position = milliseconds || 0
	},

	//=============================================

//	render: function(context){
//	},


	interface:	function(){

		self.listholder = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes	: {
				className	: "buttonlist",
				style	: "left:5px;width:570px;"
			},
			children	: [
				{
					tagname	: "ul",
					attributes:	{
						"id"	: "playlist"
					}
				}
			]
		})

		if(!self.treeService){
			self.treeService = new LED.treeService({
				media_type: self.name,
				listholder: self.listholder,
				fileSelect: self.fileSelect,
				filetypes : self.filetypes
			})
		} else {
			self.treeService.listholder = self.listholder
			self.treeService.renderTree()
		}

/*
		var progress_outer = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes:	{
				style	: "position:absolute;top:50px;left:450px;width:110px;height:10px;border:1px solid orange;"
			}
		})
		self.progress_inner = df.newdom({
			tagname	: "div",
			target	: progress_outer,
			attributes:{
				style	:"position:absolute;top:0px;left:0px;width:10px;height:10px;background-color:yellow;"
			}
		})
*/

//==========================================================================================

		var slidertypes = [
			{
				id:"progress_slider",
				name:"Position",
				startvalue:self.position,
				min:0,
				max:1,
				handler:function(val){
					var music = self.music

					var duration = music.duration
					if (self.music.bytesLoaded < self.music.bytesTotal) {
						duration = music.durationEstimate
					}

					var totalseconds = duration/1000

					var time = self.secondsToTime(totalseconds-(totalseconds*val))

					self.newposition = (totalseconds*val)

					return "-"+time

				},
				mousedown:function(){
					self.positionmoving = true
					self.newposition = undefined
				},
				mouseup:function(){
					self.positionmoving = false
					if (self.newposition!=undefined) {
						self.music.setPosition(self.newposition*1000)
					}

				}
			},
			{
				id:"sfx_volume_slider",
				name:"Volume",
				startvalue:self.volume_level,
				min:0,
				max:1,
				handler:function(val){
					self.volume_level = val
					self.music.setVolume((LED.master_volume * self.volume_level)*100);

				}
			}
		]

		var sliders = LED.sliderList(slidertypes)

		self.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:587px;top:6px;color:white;"
				},
				children	: sliders
		})


		//df.dg("slider_progress_slider").getElementsByClassName("df_slider_gadget")[0].style.zIndex = 100

		self.bufferedDiv = df.newdom({
			tagname	: "div",
			target	: df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0],
			attributes:{
				style:"width:"+df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0].offsetWidth+"px;height:"+df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0].offsetHeight+"px;background-color:red;z-index:50;position:absolute;overflow:hidden;opacity:.3"
			}

		})


		df.newdom({tagname:"br",target:self.positionDiv})

		var proc3 = df.newdom({
			tagname		:"df-onoff",
			target		: self.positionDiv,
			attributes:{
				id			: "looponoff",
				name		: "Loop Sound",
			//	namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.looping==true),
				width		: 50,
				height		: 50,
				//image_on	: radio_on,
				//image_off	: radio_off,
				onchange	: function(val){
					self.looping = val
				}
			}
		})



	//==========================================================================================

		var select_folders = df.newdom({
			tagname		:"input",
			target		: self.positionDiv,
			attributes:{
				id		: "musicplayer_select_folders",
				className: "bigbutton2",
				value		: "Select Folders",
				type		: "button",
				style		: "position: absolute;top: 470px;left: 13px;width: 267px;height: 58px;background-color: #000000;color: white;",
				onclick	: function(val){
					LED.selectFolders(self.name, function(folders){
						self.treeService.refresh(folders);
					})
				}
			}
		})


//==========================================================================================

	},

	selectSong:	function(song) {

		var item = song

//		soundManager.stopAll();

		if (self.music) {
			self.music.whileplaying = null;
			self.music.unload()
			soundManager.destroySound(self.music.sID)
			delete self.music
		}

		var music = soundManager.createSound({
			id				: escape(item),
			url				: "getAudio/" + item,
			autoLoad		: true,
			stream			: true,
			autoPlay		: true,
			useEQData		: true,
			useWaveformData	: true,
			usePeakData		: true,
			whileplaying	: self.analyzeSound,
			onfinish:	function(){
				if (self.looping == true) {
					self.play()
				}
			},
			onplay			: function(){//onload
try{
				try{self.bufferedDiv.style.left="0px"}catch(e){}

				self.current_sound = escape(item)

				self.juice = {
					frameCount		: 0,
					volume			: 0,
					freqBands		: [],
					freqBands16		: [],
					avgFreqBands	: [],
					relFreqBands	: [],
					longAvgFreqBands: [],
					avgRelFreqBands	: [],
					waveData 		: [],
					eqData 			: [],
					freqData 		: []
				}


				var numFreqBands = 3;
				for (var i=0;i<numFreqBands;i++) {
					self.juice.avgFreqBands[i] = 0;
					self.juice.longAvgFreqBands[i] = 0;
				}
				self.music = this

				self.play_state = "playing"

				try{clearInterval(self.positionInterval)}catch(e){}
				self.positionInterval = setInterval(function(){
						self.updatePosition(self.music)
				},250)

}catch(e){
console.warn("music start error")
console.error(e)
self.next()
}

			}
		});
	},



	analyzeSound:	function() {

		var sound_vol = LED.Analog3 > 0 ? LED.Analog3 : 1;

		self.music.setVolume(( sound_vol * /*LED.car_volume * LED.master_volume * */self.volume_level)*100);

	},

	updatePosition:	function(music) {

		if (self.has_interface != true) {return}


//		self.progress_inner.style.left = Math.round(100*completed)+"px"


//		var scroll_gadget_width = self.positionDiv.getElementsByClassName("df_slider_gadget")[0].offsetWidth
//		var race_width = self.positionDiv.getElementsByClassName("df_slider_race")[0].offsetWidth - scroll_gadget_width

		var duration = music.duration
		if (self.music.bytesLoaded < self.music.bytesTotal) {
			duration = music.durationEstimate

			self.bufferedDiv.style.display="block"
			self.bufferedDiv.style.left = Math.round((race_width+scroll_gadget_width) *  (self.music.bytesLoaded / self.music.bytesTotal ) )+"px"

		} else {
			self.bufferedDiv.style.display="none"
		}


		self.completed = (music.position/duration)

		if (self.positionmoving!=true) {

//			self.positionDiv.getElementsByClassName("df_slider_gadget")[0].style.left = Math.round(race_width*self.completed)+"px"
			df.dg("slider_progress_slider").getElementsByTagName("input")[0].value = (self.completed * 100)

			var totalseconds = duration/1000

			var time = self.secondsToTime(totalseconds-(totalseconds*self.completed))

			df.dg("slidervalue_progress_slider").innerHTML = "-"+time //(self.completed).toFixed(2)
		}

		//var queue = window.soundManager.sounds


	},

	secondsToTime:	function (seconds) {

		// multiply by 1000 because Date() requires miliseconds
		var date = new Date(0,0,0,0,0,0,seconds * 1000);
		var hh = date.getHours();
		var mm = date.getMinutes();
		var ss = date.getSeconds();
		// This line gives you 12-hour (not 24) time
		if (hh > 12) {hh = hh - 12;}
		// These lines ensure you have two-digits
		if (hh < 10) {hh = "0"+hh;}
		if (mm < 10) {mm = "0"+mm;}
		if (ss < 10) {ss = "0"+ss;}
		// This formats your string to HH:MM:SS
		var t = ((hh!="00")?hh+":":"")+mm+":"+ss;
		return t
	}
}

LED.plugins.push(self);

})();
