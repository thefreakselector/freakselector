(function(){


var self = {

	name		: "Full Eq",
	icon		: "Full Eq.png",
	uses_eqData	: true,

	start:	function(){
	
		self.spec_top = df.gradient(["00ff00","ffff00","ff0000"],Math.round(LED.grid_height/2))
		self.spec_bot = df.gradient(["ff0000","ffff00","00ff00"],Math.round(LED.grid_height/2))
		
	}
	,
	end:	function(){

	},
	lastrender:"",
	

	
	drawTop:	function(eqdata,x) {
		
		var split = eqdata.length / LED.grid_width
		var splitlen = split
		var data = []
		var accum = 0
		for (var i=0;i<eqdata.length;i++) {
			accum+=eqdata[i]
			if (i>split) {
				data.push(accum/splitlen)
				split+=splitlen
				accum=0
			}
		}

		var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

		for (var x=1;x<data.length;x++) {
			
			var heightval = Math.floor((LED.grid_height/2) * data[x])

			for (var y=0;y<heightval;y++) {
				
				var idx = LED.getCanvasIndex(LED.grid_width-x,((LED.grid_height/2)-y))
				var thistop = self.spec_top[Math.min(y,self.spec_top.length-1)]
				imagedata.data[idx]   = parseInt(thistop.substr(0,2),16)
				imagedata.data[idx+1] = parseInt(thistop.substr(2,2),16)
				imagedata.data[idx+2] = parseInt(thistop.substr(4,2),16)
				imagedata.data[idx+3] = 255
				
			}
			
		}
		
		LED.composite_ctx.putImageData(imagedata,0,0)
		
		
	
	},
	
	drawBottom:	function(eqdata,x) {
		var split = eqdata.length / LED.grid_width
		var splitlen = split
		var data = []
		var accum = 0
		for (var i=0;i<eqdata.length;i++) {
			accum+=eqdata[i]
			if (i>split) {
				data.push(accum/splitlen)
				split+=splitlen
				accum=0
			}
		}

		var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

		for (var x=1;x<data.length;x++) {
			
			var heightval = Math.floor((LED.grid_height/2) * data[x])

			for (var y=0;y<heightval;y++) {
				var thistop = self.spec_top[Math.min(y,self.spec_top.length-1)]
				var idx = LED.getCanvasIndex(LED.grid_width-x,y+(LED.grid_height/2))
				imagedata.data[idx]   = parseInt(thistop.substr(0,2),16)
				imagedata.data[idx+1] = parseInt(thistop.substr(2,2),16)
				imagedata.data[idx+2] = parseInt(thistop.substr(4,2),16)
				imagedata.data[idx+3] = 255
			}
		}
		LED.composite_ctx.putImageData(imagedata,0,0)
	},
	
	
	accentuate:	function(eqdata) {


		for (var y = 0;y<eqdata.length;y++) {

			var newval = eqdata[y]//Math.round( (eqdata[y])* (1-(y/eqdata.length)) )
			newval = newval * self.accentuate_value
			//newval += (eqdata[(y-1)||0] || 0)/2
			eqdata[y] = newval

		}
		
		return eqdata
	},

		
	render: function(context){

		var outwidth = LED.grid_width
		var outheight= LED.grid_height
		
		try{var juice = LED.eqData_source.music.juice}catch(e){var juice}
		if (!juice) {return};

		if (!juice.eqDataL){return}

		var thisdataL = self.accentuate(juice.eqDataL.slice(0))	//self.accentuate(juice.eqDataL.slice(0))
		var thisdataR = self.accentuate(juice.eqDataR.slice(0))	//self.accentuate(juice.eqDataR.slice(0))
		self.drawTop(thisdataL)
		self.drawBottom(thisdataR)



	},
	
	settings_to_save:[
		"accentuate_value"
	],

	accentuate_value : 5,
	
	interface:	function(){
		
		var slidertypes = [
			{
				id:"accentuate",
				name:"Amplify",
				startvalue:self.accentuate_value,
				min:0,
				max:50,
				handler:function(val){
					if (val<1) {val = 1}
					var newval = val
					self.accentuate_value = newval
				}
			}
		]
		
		//------------------------------------------------------------------------------------------------------
		
		var thisvalue
		

		var sliders = LED.sliderList(slidertypes)

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:530px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:340px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})
		
		
		
//==========================================================================================



/*		
		df.newdom({
			tagname:"div",
			target:LED.pluginwindow,
			attributes:{
				style:'font-family:"wingdings 3";font-size:30px;cursor:pointer;',
				html:"P"
			},
			events:[
				{
					name:"click",
					handler:function(evtobj){
						setTimeout(function(){
							LED.stopPlugin(self)
							delete LED.registry["Full EQ"]
							for (var i=0;i<LED.plugins.length;i++) {
								if (LED.plugins[i].name==self.name) {
									LED.plugins.splice(i,1)
									break
								}
							}
							var newscript = document.createElement("script")
							document.getElementsByTagName("head")[0].appendChild(newscript)
							newscript.src = "freakselector_plugins/FullEQ.js"
							
							setTimeout(function(){
								LED.drawPluginList()
							
							},1000)
						},100)
					}
				}
			]
		
		})
*/

	}

}

	LED.plugins.push(self)
})();



