(function(){

var self = {

  name    : "Displays",
  icon    : "Settings.png",

  sort_top: true,

  always_render: true,
  auto_start: true,
  render_last : true,

	split_screen: true,

  settings: {},

  //--------------------------------------------------------------------------------------------------

  start:  function(){

    if (!self.canvas){
      self.canvas = df.newdom({
        tagname:"canvas",
        target  : LED.contextHolder,
        attributes:{
          id  : "display_temp_canvas",
          style : "position:absolute;top:0px;left:0px;"
        }
      })
      self.ctx = self.canvas.getContext('2d');
    }

    self.canvas.width = LED.grid_width;
    self.canvas.height = LED.grid_height;

    self.createMask();

    if (self.settings.pixelmap === undefined){
      self.getPixelMap(function(){
        self.start()
        self.interface()
      });
    }

  },

  //--------------------------------------------------------------------------------------------------

  getPixelMap: function(cb){

    df.ajax.send({
      url : "getSetting/pixelmap",
      verb:"get",
      callback:function(ajaxobj){
        var settings = JSON.parse(ajaxobj.text);
        if (settings.pixelmap === undefined) {
          self.settings = {pixelmap:'pixelmap_arbitrary-30x13.pmap.json'};
          self.savePixelMap();
          //self.init();
        } else {
          self.settings = settings
        }
        //self.interface()
        if (cb){cb()}
      }
    })

  },

  //--------------------------------------------------------------------------------------------------

  savePixelMap: function(cb){
    df.ajax.send({
      url : "saveSetting/pixelmap",
      verb:"post",
      formdata:JSON.stringify(self.settings),
      callback:function(ajaxobj){

        console.log("save pixelmap",ajaxobj.text)
        if (cb){cb()}
      }
    })
  },

  drawPixels: function(){
  	var pixel_window = df.newdom({
  		tagname: "div",
  		target: LED.pluginwindow,
  		attributes: {
  			id: "pixelwindow",
  			style: "position: absolute;top:0px;left:0px;width:550px;height:550px;background-color:white;z-index:99999;"
  		}
  	})

  	var ww = 0;
  	var hh = 0;


  	var pixw = 3;
  	var pixh = 3;
  	for (var screen in LED.pixelmap){
  		var map = LED.pixelmap[screen];
  		for (var i=0;i<map.length;i++){
  			if (map[i].x > ww) {ww = map[i].x}
  			if (map[i].y > hh) {hh = map[i].y}
			}
		}

  	var pw = 550-pixw;
  	var ph = 550-pixh;

  	self.pixelboxes = []

  	for (var screen in LED.pixelmap){
  		var map = LED.pixelmap[screen];

  		for (var i=0;i<map.length;i++){
  			var x = map[i].x / ww;
  			var y = map[i].y / hh;
  			var xx = x * pw
  			var yy = y * ph
  			self.pixelboxes.push(df.newdom({
  				tagname: 'div',
  				target:pixel_window,
  				attributes: {
  					style:'position:absolute;top:'+yy+'px;left:'+xx+';width:'+pixw+'px;height:'+pixh+'px;background-color:black;'
  				}
  			}));
  		}
  	}

  	self.animatePixels()
  },

  //--------------------------------------------------------------------------------------------------

  pixelanimidx: 0,
  animatePixels: function(){
  	try{
  	self.pixelboxes[self.pixelanimidx].style.backgroundColor = '#ff0000';
  	self.pixelboxes[self.pixelanimidx].style.border = '2px solid #ff0000';

  	self.pixelanimidx++
  	if (self.pixelanimidx > self.pixelboxes.length - 1) {
  		self.pixelanimidx = 0;
  	}
  	console.log(self.pixelanimidx)
  	setTimeout(self.animatePixels,100);
  }catch(e){
  	console.log(e)
  }
  },

  //--------------------------------------------------------------------------------------------------

  hidePixels: function(){
  	df.removedom(df.dg("pixelwindow"))
  },

  //--------------------------------------------------------------------------------------------------

  createMask: function(){
    var nopixel = []
    console.log("grid",LED.pixelmap)

    for (var grid in LED.pixelmap) {
      var thisgrid = LED.pixelmap[grid]
      for (var i=0;i<thisgrid.length;i++) {
        if (nopixel[thisgrid[i].x] == undefined) {
          nopixel[thisgrid[i].x] = []
        }
        nopixel[thisgrid[i].x][thisgrid[i].y] = true
      }
    }

    self.ctx.fillStyle = '#202020';

    self.has_mask = false;
    nopixel_abort: for (var x=0;x<LED.grid_width;x++){
      for (var y=0;y<LED.grid_height;y++){
        if (nopixel[x] === undefined){
          self.ctx.fillRect(x, y, 1, 1);
          self.has_mask = true;
          continue
        }
        if (nopixel[x][y] === undefined){
          self.ctx.fillRect(x, y, 1, 1);
          self.has_mask = true;
        }
      }
    }

  },

  //--------------------------------------------------------------------------------------------------

  settings_to_save  : [
  ],

  //--------------------------------------------------------------------------------------------------

  fileSelect: function(data){
console.log(data)
  },

  //--------------------------------------------------------------------------------------------------

  screens: 2,
  //overlays a 'mask' on the display area for pixels that are not part of the grid.
  render: function(){

    if (self.screens === 4 && self.settings && self.settings.pixelmap && self.settings.pixelmap.indexOf('jensbikebasket')!==-1 ){
      var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

      self.ctx.putImageData(imagedata, 0, 0);

      var panel_x =16;

      LED.composite_ctx.fillStyle = "#000000";
      LED.composite_ctx.fillRect(0,0,LED.grid_width,LED.grid_height);

      var panel = "right";
      var this_panel = self.panels[panel];
      LED.composite_ctx.drawImage(self.canvas, panel_x, 0, this_panel.x_size, LED.grid_height )
      panel_x += this_panel.x_size;

      var panel = "front";
      var this_panel = self.panels[panel];
      LED.composite_ctx.drawImage(self.canvas, panel_x, 0, this_panel.x_size, LED.grid_height )
      panel_x += this_panel.x_size;


      var panel = "left";
      var this_panel = self.panels[panel];
      LED.composite_ctx.drawImage(self.canvas, panel_x, 0, this_panel.x_size, LED.grid_height )
      panel_x += this_panel.x_size;


      var panel = "back";
      var this_panel = self.panels[panel];
      LED.composite_ctx.drawImage(self.canvas, panel_x, 0, this_panel.x_size, LED.grid_height )
      panel_x = -this_panel.x_size;

      LED.composite_ctx.drawImage(self.canvas, -14, 0, this_panel.x_size, LED.grid_height )

    } else if (self.screens === 2 && self.settings && self.settings.pixelmap && self.settings.pixelmap.indexOf('jensbikebasket')!==-1 ){
      var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

      self.ctx.putImageData(imagedata, 0, 0);

      var panel_x = 25;

      LED.composite_ctx.fillStyle = "#000000";
      LED.composite_ctx.fillRect(0,0,LED.grid_width,LED.grid_height);

      LED.composite_ctx.drawImage(self.canvas, panel_x, 0, 50, LED.grid_height )
      panel_x += 50

      LED.composite_ctx.drawImage(self.canvas, panel_x, 0, 50, LED.grid_height )
      panel_x += 50

      var panel = "back";
      var this_panel = self.panels[panel];

      LED.composite_ctx.drawImage(self.canvas, -25, 0, 50, LED.grid_height )


    } else if (self.canvas && self.show_mask){
      LED.composite_ctx.drawImage(self.canvas, 0, 0,LED.grid_width, LED.grid_height)
    }

//    self.showEstimated()

  },

  //-------------------------------------------------------------------------------------------------------
/*
  showEstimated: function(){
    var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

    var light_diffusion_radius_x = 3;
    var light_diffusion_radius_y = 3;
    var r_max = Math.max(light_diffusion_radius_x, light_diffusion_radius_y);
    
    for (var i=0;i<LED.pixelmap.length;i++){
      var gx = LED.grid_width * LED.pixelmap[i].x;
      var gy = LED.grid_height * LED.pixelmap[i].y;
      
      for (var q = 0;q<r_max;q++){
        for (var deg = 0;deg < 360; deg+=1){
          var rad = deg * Math.PI / 180;
          var px = gx + (Math.cos(rad)*q)
          var py = gy + (Math.sin(rad)*q)
        }
      }
    }

  },
*/
  //-------------------------------------------------------------------------------------------------------

  interface:  function(){

    //--------------------------------------------------------------------------------------------------
    console.log("LED.pixelmap>>>",LED.pixelmap)

    self.panel_holder = df.newdom({
      tagname: 'div',
      target: LED.pluginwindow,
      attributes:{
        className: 'buttonlist',
        style: 'position:absolute;top:70px;left:6px;height:420px;width:590px;overflow-y:scroll;'
      }
    })

    self.panel_doms = [];
    self.panels = {}

    var panels = 0;

    var all_total_pixels = 0;
    for (panel in LED.pixelmap) {
      var this_panel = LED.pixelmap[panel]
      panels++
      if (panels > 1){self.has_multiscreen = true}

      var x_points = {};
      var y_points = {};
      for (var i=0;i<this_panel.length;i++){
        x_points[this_panel[i].x] = true;
        y_points[this_panel[i].y] = true;
      }
      var x_size = 0;
      var y_size = 0;
      for (var i in x_points){x_size++}
      for (var i in y_points){y_size++}
      self.panels[panel] = self.panels[panel] || {}
      self.panels[panel].x_size = x_size;
      self.panels[panel].y_size = y_size;

      var panel_width = x_size;
      var panel_height= y_size;

      var panel_grid_count = panel_width * panel_height;

      all_total_pixels += this_panel.length;

      var panel_name = df.newdom({
        tagname:'div',
        attributes:{
          html: panel,
          style: ''
        }
      });

      var panel_size = df.newdom({
        tagname:'div',
        attributes:{
          html: panel_width + ' x ' + panel_height + ' (w/h)',
          style: 'font-size:24px;font-weight:500;line-height:30px;margin-left:20px;'
        }
      });

      var panel_pixels = df.newdom({
        tagname:'div',
        attributes:{
          html: this_panel.length + " actual pixels",
          style: 'font-size:24px;font-weight:500;line-height:30px;margin-left:20px;'
        }
      });0

      var pixel_grid_count = df.newdom({
        tagname:'div',
        attributes:{
          html: panel_grid_count + " grid pixels",
          style: 'font-size:24px;font-weight:500;line-height:30px;margin-left:20px;'
        }
      });

      var grid_info_holder = df.newdom({
        tagname:'div',
        target:self.panel_holder,
        attributes:{
          style: 'position:relative;width:98%;font-family:arial;font-size:26px;font-weight:700;margin-top:5px;padding:5px;background:white;border:#cacaca;border-radius:4px;'
        },
        children:[panel_name, panel_size, panel_pixels, pixel_grid_count]
      });

      self.panel_doms.push(grid_info_holder)

      var block_width  = grid_info_holder.offsetWidth;
      var block_height = grid_info_holder.offsetHeight;

      var grid_height = block_height-10;
      var grid_width  = ( (panel_width/panel_height) * grid_height);

      if (grid_width > (block_width/2)){
        grid_width = block_width/2
        grid_height = (panel_height/panel_width) * grid_width;
      }

      var grid_layout = df.newdom({
        tagname: 'canvas',
        attributes:{
          width: panel_width,
          height: panel_height,
          style: [
            'position:absolute',
            'top:5px',
            'left: 100%',
            'margin-left: -'+( grid_width + 5 )+'px',
            'border:1px solid #cacaca',
            'background-color:black',
            'width:'+grid_width+'px',
            'height:'+grid_height+'px'
            ].join(';')
        }
      })

      grid_info_holder.appendChild(grid_layout);

      var this_panel_ctx = grid_layout.getContext('2d');
      for (var ix=0;ix<this_panel.length;ix++){
        var this_pixel = this_panel[ix];
        if (self.has_mask) {
          this_panel_ctx.fillStyle = '#20f020';
        } else {
          this_panel_ctx.fillStyle = (this_pixel.x & 1) ? ((this_pixel.y & 1) ? '#20f020' : '#206020') : (this_pixel.y & 1) ? '#206020' : '#20f020';
        }
        this_panel_ctx.fillRect(0, 0, x_size, y_size);
      }
    }

    df.newdom({
      tagname: 'div',
      target: LED.pluginwindow,
      attributes:{
        html: 'Total Pixels: '+all_total_pixels,
        style: 'position:absolute;top:503px;left:7px;width:590px;font-family:arial;font-size:26px;font-weight:700;margin-top:5px;padding:5px;background:white;border:#cacaca;border-radius:4px;'
      }
    })

    //--------------------------------------------------------------------------------------------------

    var current_display_setting = df.newdom({
      tagname: 'div',
      target: LED.pluginwindow,
      attributes:{
        html: self.settings.pixelmap || 'not selected',//localStorage.getItem('pixelmap') || 'not selected',
        style: 'position:absolute;top:6px;left:6px;width:570px;font-family:arial;font-size:27px;background-color:white;padding:14px;border:1px solid #cacaca;border-radius:5px;'
      }
    })

    var select_folders = df.newdom({
      tagname   :"input",
      target    : LED.pluginwindow,
      attributes:{
        id    : "musicplayer_select_folders",
        className: "bigbutton2",
        value   : "Select Profile",
        type    : "button",
        style   : "position: absolute;top: 0px;left: 613px;width: 267px;height: 58px;background-color: #000000;color: white;",
        onclick : function(val){

          confirm("This will restart the FreakSelector. Okay?",function(){

            lastfolder = self.settings.lastfolder//localStorage.getItem('freakselector_displaysettings_lastfolder') || undefined;

            LED.selectFolders(self.name, function(path_and_file){

              console.log(">>>>>>>path_and_file:",path_and_file);
//Object {path: "D:\\/FreakSelector/pixelmaps", file: "pixelmap_mini_tophat.pmap.json"}
              //localStorage.setItem('pixelmap',path_and_file.file);
              self.settings.pixelmap = path_and_file.file;
              if (path_and_file.path){
                self.settings.lastfolder = path_and_file.path;
              }

              self.savePixelMap()

//              if (path_and_file.path){
  //              localStorage.setItem('freakselector_displaysettings_lastfolder',path_and_file.path)
    //          }

              current_display_setting.innerHTML = path_and_file.file;
              LED.loadPixelMap(path_and_file.file);
              location.reload()

            }, {
              fileselector:true,
              filetypes:['.pmap.json'],
              startfolder: lastfolder
            });

          })

        }
      }
    })

    //--------------------------------------------------------------------------------------------------
    if (self.has_mask){
      self.show_mask = false;
      df.newdom({
        tagname   :"df-onoff",
        target    : LED.pluginwindow,
        attributes:{
          id      : "pixelmaponoff",
          name    : "Pixel Mask",
          style   : "position:absolute;top:100px;left:700px;",
          class   : "onoff",
          value   : self.show_mask,
          width   : 40,
          onchange  : function(val){
            self.show_mask = val;
          }
        }
      })
    }
    //--------------------------------------------------------------------------------------------------

    df.newdom({
      tagname   :"df-onoff",
      target    : LED.pluginwindow,
      attributes:{
        id      : "showpixels",
        name    : "Pixels",
        style   : "position:absolute;top:160px;left:800px;",
        class   : "onoff",
        value   : self.show_pixels,
        width   : 40,
        onchange  : function(val){

        	self.show_pixels = val;

        	if (val === true) {
        		self.drawPixels();
        	} else {
        		self.hidePixels();
        	}

        }
      }
    })

    //--------------------------------------------------------------------------------------------------
/*
    if (self.has_multiscreen){
      if (self.split_screen === undefined){
        self.split_screen = true;
      }
      df.newdom({
        tagname   :"df-onoff",
        target    : LED.pluginwindow,
        attributes:{
          id      : "splitscreenonoff",
          name    : "Split Screen",
          style   : "position:absolute;top:200px;left:700px;",
          class   : "onoff",
          value   : self.split_screen,
          width   : 40,
          onchange  : function(val){
            self.split_screen = val;
          }
        }
      })
    }
*/
    //--------------------------------------------------------------------------------------------------
    if (self.has_multiscreen){

			var procwin = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "bpmSelectWin",
					style	: "position:absolute;top:305px;left:658px;z-index:5000;color:white;background-color:#7a7a7a;padding:5px;overflow:hidden;border-radius:5px;"
				}
			})

			var namestyle = "width:180px;font-size:20px;margin-bottom:3px;"
			var radio_off = LED.radio_off_image;
			var radio_on  = LED.radio_on_image;
			var radio_w = 32
			var radio_h = 32

			var screens1 = df.newdom({
				tagname		:"df-onoff",
				target		: procwin,
				attributes:{
					id		: "testonoff0",
					name		: "4 Screens",
					namestyle	: namestyle,
					style		: "position:relative;",
					class		: "onoff",
					value		: (self.screens === 4),
					width		: radio_w,
					height		: radio_h,
					image_on	: radio_on,
					image_off	: radio_off,
					onchange	: function(val){

						if (val==false) {return true}

						screens1.setState(false)
						screens2.setState(false)
						screens4.setState(false)

						self.screens = 4
						//self.startAutoTimer()

					}
				}
			})

			df.newdom({tagname:"br",target:procwin})

			var screens2 = df.newdom({
				tagname		:"df-onoff",
				target		: procwin,
				attributes:{
					id		: "testonoff1",
					name		: "2 Screens",
					namestyle	: namestyle,
					style		: "position:relative;",
					class		: "onoff",
					value		: (self.screens == 2),
					width		: radio_w,
					height		: radio_h,
					image_on	: radio_on,
					image_off	: radio_off,
					onchange	: function(val){

						if (val==false) {return true}

						screens1.setState(false)
						screens2.setState(false)
						screens4.setState(false)

						self.screens = 2
//						self.startAutoTimer()
					}
				}
			})

			df.newdom({tagname:"br",target:procwin})



			var screens4 = df.newdom({
				tagname		:"df-onoff",
				target		: procwin,
				attributes:{
					id		: "testonoff2",
					name		:  "1 Screen",
					namestyle	: namestyle,
					style		: "position:relative;",
					class		: "onoff",
					value		: (self.screens == 1),
					width		: radio_w,
					height		: radio_h,
					image_on	: radio_on,
					image_off	: radio_off,
					onchange	: function(val){

						if (val==false) {return true}

						screens1.setState(false)
						screens2.setState(false)
						screens4.setState(false)

						self.screens = 1
						//self.startAutoTimer()

					}
				}
			})

    }

  //--------------------------------------------------------------------------------------------------


  }
}

LED.plugins.push(self);

})();
