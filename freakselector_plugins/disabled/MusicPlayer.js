LED.plugins.push({
	name		: "Music Player",
	icon		: "Music Player.png",
	has_audio	: true,
	has_eqData	: true,


	volume_level	: 1,

	start:	function(startsong){

		var self = this


		self.goback = []

//		self.buildTree()

		self.getTree("Media/Music",function(){

//			self.getTree("E:%5Cmusic2",function(){

				self.interface()

//			})
		})
	},


	selectFolder:	function(folder) {
		var self = this

		self.getTree(folder,function(){
			self.renderTree()
		})
	},
	//=============================================
	filetypes	: [
		"mp3",
		"wav",
		"ogg"
	],

	buildTree:	function() {
		var self = this
		df.ajax.send({
			url	: "get_drives.asp",
			callback	: function(ajaxobj) {

				var drives = df.json.deserialize(ajaxobj.text)

				var tree = {
					folders	: [],
					files	: []
				}

				for (var selector in drives) {
					if (selector == "C") {continue}
					if (drives[selector] == "GREENCLOUD_BACKUP"){continue}
					tree.folders[selector+":\\"+drives[selector]] = []
				}

				self.tree = tree

				self.interface()

			}
		})
	},


	goback:[],


	getTree:	function(thispath,thiscallback) {
		var self = this

		if (thispath==undefined) {
			self.buildTree()
			this.renderTree()
			return
		}

		self.goback.push(thispath)

		df.ajax.send({
			url : "gettree.aspx?thispath="+thispath,
			callback:function(data) {

//				self.tree.folders[thispath] = df.json.deserialize(data.text)
				self.tree = df.json.deserialize(data.text)

				for (var i=self.tree.files.length-1;i>-1;i--) {
					var keepfile = false
					for (var ix=0;ix<self.filetypes.length;ix++) {
						if (self.tree.files[i].substr(self.tree.files[i].length-3).toLowerCase() == self.filetypes[ix].toLowerCase()) {
							keepfile = true
							break
						}
					}
					if (keepfile != true) {
						self.tree.files.splice(i,1)
					}
				}

				if (thiscallback!=undefined) {thiscallback()}

			}
		})
	},


	renderTree:	function() {

		var self = this

		var playlist = df.dg("playlist")

		var folders = this.tree.folders

		if (self.listholder!=undefined) {self.listholder.innerHTML=""}

		var password
		var foldername

		if (self.goback.length>0) {
			var thisname = unescape(this.goback[this.goback.length-1])

			password = undefined
			foldername = thisname.split("\\").pop()

			if (foldername.substr(0,1) == "," && foldername.indexOf("-")!=-1 ) {
				thisname = foldername.substr(foldername.indexOf("-")+1)
			}

			df.newdom({
				tagname	: "li",
				target	: self.listholder,
				attributes:{
					className: "folderlist",
					style	: "width:480px;",
					html	: ' <span style="color:#ffffff;">[<span style="font-size:25px;font-weight:700;">&#x21EA;</span> Back] <span style="float:right;">'+thisname.replace(/\//g,"\\")+'</span></span>'
				},
				events:	[
					{
						name	: "mouseup",
						handler	: function(evtobj) {
							self.goback.pop()
							self.selectFolder(self.goback.pop())
						},
						selector		: selector
					}
				]
			})
		}

		var toplevel = (self.goback.length==0)

		for (var selector in folders) {
			var thisname = unescape(selector.replace(/_/g," "))

			password = undefined
			foldername = thisname.split("\\").pop()
			if (foldername=="") {foldername = thisname.split("\\").shift()}

			if (foldername.substr(0,1) == "," && foldername.indexOf("-")!=-1 ) {
				password = foldername.split(",")[1].split("-")[0]
				foldername = foldername.substr(foldername.indexOf("-")+1)
			}

			var clickfolder = selector

			if (toplevel == true) {
				foldername = thisname
				clickfolder = escape(clickfolder.split("\\")[0]+"/")
			}

			df.newdom({
				tagname	: "li",
				target	: self.listholder,
				attributes:{
					className: "folderlist"+((self.currently_playing&&self.currently_playing.indexOf(selector)!=-1)?" playingfolder":""),
					style	: "width:480px;",
					html	: foldername.replace(/\//g,"\\")
				},
				events:	[
					{
						name	: "mouseup",
						handler	: function(evtobj) {
							var thispassword = evtobj.srcdata.password
							var thisfolder = evtobj.srcdata.clickfolder

							if (thispassword!=undefined) {
								prompt({
									message	:"Password protected",
									callback: function(val){

										if (val == thispassword) {
											self.selectFolder(thisfolder)

										}
									},
									bgcolor	: "#ff0000",
									color	: "#ffffff",
									ispassword:true
								})
								return
							}

							self.selectFolder(thisfolder)
						},
						clickfolder		: clickfolder,
						password		: password
					}
				]
			})
		}

		var vids = this.tree.files
		var current_file

		for (var i=0;i<self.tree.files.length;i++) {

			var thisfile = (unescape(self.tree.files[i])).replace(/\:\\/g,"/").replace(/\:/g,"").replace(/\\/g,"/")
			var thisfilebutton = df.newdom({
				tagname	: "li",
				target	: self.listholder,
				attributes:{
					className: "folderbutton"+((self.currently_playing==thisfile)?" playing":""),
					style	: "width:480px;",
					html	: thisfile.split("/").pop().replace(/\//g,"\\") //unescape(thisfile.replace(/_/g," ")).substr(unescape(thisfile.replace(/_/g," ")).lastIndexOf("\\")+1)
				},
				events:	[
					{
						name	: "mouseup",
						handler	: function(evtobj) {
							var music = evtobj.srcdata.thisfile //self.tree.files[evtobj.srcdata.idx]
							self.selectSong(music)
							self.currently_playing = evtobj.srcdata.thisfile//self.tree.files[evtobj.srcdata.idx]
							self.currently_playing_idx = evtobj.srcdata.idx
							self.renderTree()
						},
						idx		: i,
						thisfile: thisfile
					}
				]
			})
			if (self.currently_playing==thisfile) {
				current_file = thisfilebutton
			}
		}

		if (current_file!=undefined){
			current_file.scrollIntoView()
		}

	}
	,



	end:	function(){

		var self = this

		try{clearInterval(self.positionInterval)}catch(e){}



		//soundManager.stopAll();

		if (self.music) {
			self.music.stop()
			self.music.whileplaying = null;
			self.music.unload()
			soundManager.destroySound(self.music.sID)
			delete LED.registry["Music Player"].music
		}
		self.currently_playing = undefined
		self.goback = []
		self.tree = undefined

	},

	//=============================================
	//audio and video plugins need these functions
	play:	function(idx) {
		if (LED.registry["Music Player"].music.playState==0 || LED.registry["Music Player"].music.paused == true) {
			soundManager.play(LED.registry["Music Player"].music.sID)
			try{clearInterval(self.positionInterval)}catch(e){}
			self.positionInterval = setInterval(function(){
					LED.registry["Music Player"].updatePosition(LED.registry["Music Player"].music)
			},1000)

		}
	},
	pause:	function() {
		soundManager.pause(LED.registry["Music Player"].music.sID)
		try{clearInterval(self.positionInterval)}catch(e){}
	},
	stop:	function() {
		this.music.stop() //soundManager.stopAll();
		try{clearInterval(this.positionInterval)}catch(e){}
	},
	next:	function() {
		var self = this
		var vids = this.tree.files
		self.currently_playing_idx++
		if (self.currently_playing_idx > this.tree.files.length-1) {self.currently_playing_idx = 0}
		var thisfile = (unescape(self.tree.files[self.currently_playing_idx])).replace(/\:\\/g,"/").replace(/\:/g,"").replace(/\\/g,"/")
		self.currently_playing = thisfile
		self.selectSong(unescape(self.currently_playing))
		self.renderTree()

	},
	last:	function() {
		var self = this
		var vids = this.tree.files
		self.currently_playing_idx--
		if (self.currently_playing_idx < 0) {self.currently_playing_idx = this.tree.files.length-1}
		self.currently_playing = vids[self.currently_playing_idx]
		self.selectSong(unescape(self.currently_playing).substr(3))
		self.renderTree()
	},


	setMute:	function(mute){
		this.mute = mute
	},
	getMute:	function() {
		return this.mute
	},

	setVolume:	function(vol){
					// 0 - 1
	},
	getVolume:	function(){
		return 1	// 0 - 1
	},

	getPosition:	function(){
		return this.position || 0
	},
	setPosition:	function(milliseconds){
		this.position = milliseconds || 0
	},

	//=============================================

//	render: function(context){
//	},


	interface:	function(){

		var self = this

		if (this.tree==undefined) {return}

		self.listholder = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes	: {
				id		: "listholder",
				className	: "buttonlist",
				style	: "left:5px;width:570px;"
			},
			children	: [
				{
					tagname	: "ul",
					attributes:	{
						"id"	: "playlist"
					}
				}
			]
		})
/*
		var progress_outer = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes:	{
				style	: "position:absolute;top:50px;left:450px;width:110px;height:10px;border:1px solid orange;"
			}
		})
		this.progress_inner = df.newdom({
			tagname	: "div",
			target	: progress_outer,
			attributes:{
				style	:"position:absolute;top:0px;left:0px;width:10px;height:10px;background-color:yellow;"
			}
		})
*/

//==========================================================================================

		var slidertypes = [
			{
				id:"progress_slider",
				name:"Position",
				startvalue:this.position,
				min:0,
				max:1,
				handler:function(val){
					var music = self.music

					var duration = music.duration
					if (self.music.bytesLoaded < self.music.bytesTotal) {
						duration = music.durationEstimate
					}

					var totalseconds = duration/1000

					var time = self.secondsToTime(totalseconds-(totalseconds*val))

					self.newposition = (totalseconds*val)

					return "-"+time

				},
				mousedown:function(){
					self.positionmoving = true
					self.newposition = undefined
				},
				mouseup:function(){
					self.positionmoving = false
					if (self.newposition!=undefined) {
						self.music.setPosition(self.newposition*1000)
					}

				}
			},
			{
				id:"music_volume_slider",
				name:"Volume",
				startvalue:self.volume_level,
				min:0,
				max:1,
				handler:function(val){
					self.volume_level = val
					self.music.setVolume((LED.master_volume * self.volume_level)*100);

				}
			}
		]

		var sliders = LED.sliderList(slidertypes)

		this.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:587px;top:6px;color:white;"
				},
				children	: sliders
		})


		df.dg("slider_progress_slider").getElementsByClassName("df_slider_gadget")[0].style.zIndex = 100
		this.bufferedDiv = df.newdom({
			tagname	: "div",
			target	: df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0],
			attributes:{
				style:"pointer-events:none;width:"+df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0].offsetWidth+"px;height:"+df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0].offsetHeight+"px;background-color:red;z-index:50;position:absolute;overflow:hidden;opacity:.3"
			}

		})



//==========================================================================================


		this.renderTree()
	},

	selectSong:	function(song) {
		var self = this

		var item = song
console.log(song)

		if (self.music){self.music.stop()} //soundManager.stopAll();

		if (self.music) {
			self.music.whileplaying = null;
			self.music.unload()
			soundManager.destroySound(self.music.sID)
			delete LED.registry["Music Player"].music
		}

		var music = soundManager.createSound({
			id				: escape(item),
			url				: item,
			autoLoad		: true,
			stream			: true,
			autoPlay		: true,
			useEQData		: true,
			useWaveformData	: true,
			usePeakData		: true,
			whileplaying	: LED.registry["Music Player"].analyzeSound,
			onfinish:	function(){

				self.next()
			},
			onplay			: function(){//onload
try{
				try{self.bufferedDiv.style.left="0px"}catch(e){}
				this.juice = {
					frameCount		: 0,
					volume			: 0,
					freqBands		: [],
					freqBands16		: [],
					avgFreqBands	: [],
					relFreqBands	: [],
					longAvgFreqBands: [],
					avgRelFreqBands	: [],
					waveData 		: [],
					eqData 			: [],
					freqData 		: []
				}

				var numFreqBands = 3;
				for (var i=0;i<numFreqBands;i++) {
					this.juice.avgFreqBands[i] = 0;
					this.juice.longAvgFreqBands[i] = 0;
				}
				LED.registry["Music Player"].music = this

				self.play_state = "playing"

				try{clearInterval(self.positionInterval)}catch(e){}
				self.positionInterval = setInterval(function(){
						LED.registry["Music Player"].updatePosition(LED.registry["Music Player"].music)
				},1000)

}catch(e){
console.warn("music start error")
console.error(e)
self.next()
}

			}
		});
	},



	analyzeSound:	function() {

		var juice = this.juice;

		var numFreqBands = 3;
		var freqBandInterval = 256 / numFreqBands;

		var eqData		= juice.eqData		= this.eqData;
		var peakData	= juice.peakData	= this.peakData;
		var waveData	= juice.waveData	= this.waveformData;

		if (waveData.length == 512) {
			juice.waveDataL = waveData.slice(0, 256);
			juice.waveDataR = waveData.slice(256);
			this.waveDataL = juice.waveDataL
			this.waveDataR = juice.waveDataR
		} else {
			juice.waveDataL = juice.waveDataR = juice.waveData;
			this.waveDataL = this.waveDataR = juice.waveData;
		}

		if (eqData.length == 512) {
			juice.eqDataL = eqData.slice(0, 256);
			juice.eqDataR = eqData.slice(256);
		} else {
			juice.eqDataL = juice.eqDataR = juice.eqData;
		}

		var freqBands			= juice.freqBands;
		var freqBands16			= juice.freqBands16;
		var avgFreqBands		= juice.avgFreqBands;
		var longAvgFreqBands	= juice.longAvgFreqBands;
		var relFreqBands		= juice.relFreqBands;
		var avgRelFreqBands		= juice.avgRelFreqBands;

		for (var i=0;i<numFreqBands;i++) {
			freqBands[i] = 0;
		}

		for (var i=0;i<128;i++) {
			freqBands[(i/freqBandInterval/2)>>0] += eqData[i];
		}

		for (var i=0;i<16;i++)
			freqBands16[i] = 0;
		for (var i=0;i<128;i++) {
			freqBands16[(i/8)>>0] += eqData[i];
		}

		juice.frameCount++;

		for (var i=0;i<numFreqBands;i++) {

			if (freqBands[i] > avgFreqBands[i])
				avgFreqBands[i] = avgFreqBands[i]*0.2 + freqBands[i]*0.8;
			else
				avgFreqBands[i] = avgFreqBands[i]*0.5 + freqBands[i]*0.5;

			if (juice.frameCount < 50)
				longAvgFreqBands[i] = longAvgFreqBands[i]*0.900 + freqBands[i]*0.100;
			else
				longAvgFreqBands[i] = longAvgFreqBands[i]*0.992 + freqBands[i]*0.008;

			if (abs(longAvgFreqBands[i]) < 0.001) {
				relFreqBands[i] = 1.0;
			} else {
				relFreqBands[i]  = freqBands[i] / longAvgFreqBands[i];
			}

			if (abs(longAvgFreqBands[i]) < 0.001) {
				avgRelFreqBands[i] = 1.0;
			} else {
				avgRelFreqBands[i]  = avgFreqBands[i] / longAvgFreqBands[i];
			}
		}

		juice.position = this.position;

		LED.registry["Music Player"].music.setVolume((LED.master_volume * LED.registry["Music Player"].volume_level)*100);

	},

	updatePosition:	function(music) {
		var self = this

		if (this.has_interface != true) {return}


//		this.progress_inner.style.left = Math.round(100*completed)+"px"

		var scroll_gadget_width = this.positionDiv.getElementsByClassName("df_slider_gadget")[0].offsetWidth
		var race_width = this.positionDiv.getElementsByClassName("df_slider_race")[0].offsetWidth - scroll_gadget_width

		var duration = music.duration
		if (self.music.bytesLoaded < self.music.bytesTotal) {
			duration = music.durationEstimate

			this.bufferedDiv.style.display="block"
			this.bufferedDiv.style.left = Math.round((race_width+scroll_gadget_width) *  (self.music.bytesLoaded / self.music.bytesTotal ) )+"px"

		} else {
			this.bufferedDiv.style.display="none"
		}


		this.completed = (music.position/duration)

		if (self.positionmoving!=true) {

			this.positionDiv.getElementsByClassName("df_slider_gadget")[0].style.left = Math.round(race_width*this.completed)+"px"

			var totalseconds = duration/1000

			var time = this.secondsToTime(totalseconds-(totalseconds*this.completed))

			df.dg("slidervalue_progress_slider").innerHTML = "-"+time //(this.completed).toFixed(2)
		}

		//var queue = window.soundManager.sounds


	},

	secondsToTime:	function (seconds) {

		// multiply by 1000 because Date() requires miliseconds
		var date = new Date(0,0,0,0,0,0,seconds * 1000);
		var hh = date.getHours();
		var mm = date.getMinutes();
		var ss = date.getSeconds();
		// This line gives you 12-hour (not 24) time
		if (hh > 12) {hh = hh - 12;}
		// These lines ensure you have two-digits
		if (hh < 10) {hh = "0"+hh;}
		if (mm < 10) {mm = "0"+mm;}
		if (ss < 10) {ss = "0"+ss;}
		// This formats your string to HH:MM:SS
		var t = ((hh!="00")?hh+":":"")+mm+":"+ss;
		return t
	}
})