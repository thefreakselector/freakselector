(function(){

var self = {

	name		: "Interactive",
	//icon		: "Video Input.png",
	//fullscreen	: true,
	uses_camera	: true,
	
	grayframes:[],
	
	coloridx	: 0,

	getGridIndex:	function(x,y){
		return ((Math.floor(y)*(360/8))+Math.floor(x))
	},
	
	auto_attenuation:0,
		
	render:	function(){
		
		var clridx = self.coloridx
		var thiscolor = self.gradient[clridx]||"000000"

		self.coloridx++
		if (self.coloridx >= self.gradient.length){self.coloridx=0}

		var rcol = df.getdec(thiscolor.substr(0,2))
		var gcol = df.getdec(thiscolor.substr(2,2))
		var bcol = df.getdec(thiscolor.substr(4,2))

		if (self.grayframes.length>0 && LED.motion_data && LED.motion_data.length == 0) {
			self.grayframes.shift()
		} else if (LED.motion_data && LED.motion_data.length > 0) {
			self.grayframes.push({
				rcol	: rcol,
				gcol	: gcol,
				bcol	: bcol,
				grid	: LED.motion_grid
			})
		}
		if (self.grayframes.length>self.maxframes) {
			self.grayframes.shift()
		}

		var griddata = []


		for (var y=0;y<(240/8);y++) {
			for (var x=0;x<(360/8);x++) {				
				var ix			= LED.getCanvasIndex(x,y)				
				griddata[ix]	= 0
				griddata[ix+1]	= 0
				griddata[ix+2]	= 0
				griddata[ix+3]	= 255
			}
		}

		if (self.grayframes.length>0) {

var brt = 0
			for (var i=0;i<self.grayframes.length;i++) {

				for (var y=0;y<(240/8);y++) {
					for (var x=0;x<(360/8);x++) {				
						var gray	= self.grayframes[i].grid.charCodeAt(self.getGridIndex(x,y))
						if (gray < self.threshold){gray=0}
						var ix		= LED.getCanvasIndex(x,y)
							griddata[ix]	+= ((gray * self.grayframes[i].rcol) / self.grayframes.length) * (i/self.grayframes.length)

							griddata[ix+1]	+= ((gray * self.grayframes[i].gcol) / self.grayframes.length) * (i/self.grayframes.length)
						
							griddata[ix+2]	+= ((gray * self.grayframes[i].bcol) / self.grayframes.length) * (i/self.grayframes.length)
							
							brt+= griddata[ix]+griddata[ix+1]+griddata[ix+2]
							
						griddata[ix+3]	= 255
					}
				}
			}

		}/* else {
			for (var y=0;y<(240/8);y++) {
				for (var x=0;x<(360/8);x++) {				
					var ix			= LED.getCanvasIndex(x,y)				
					griddata[ix]	= 0
					griddata[ix+1]	= 0
					griddata[ix+2]	= 0
					griddata[ix+3]	= 255
				}
			}

		}*/
		
		if (brt && brt>8000) {
			self.auto_attenuation+=.1
			if (self.auto_attenuation>1) {
				self.auto_attenuation = 1
			}
		} else {
			self.auto_attenuation -=.1
			if (self.auto_attenuation<0) {
				self.auto_attenuation = 0
			}		
		}
		
		LED.registry.Milkdrop.attenuation = self.auto_attenuation
		
		
//		var imagedata = self.tempcanvas.getContext("2d").getImageData(0,0,LED.grid_width,LED.grid_height)
//		for (var i=0;i<griddata.length;i++){Math.min(255,imagedata.data[i] += griddata[i])}
//		LED.composite_ctx.putImageData(imagedata, 0, 0);

		var ctxdata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)	
		for (var i=0;i<ctxdata.data.length;i++){
			ctxdata[i] = Math.min(255,ctxdata.data[i] += griddata[i])
		}
		LED.composite_ctx.putImageData(ctxdata, 0, 0);
		
		/*
		if (LED.motion_data&&LED.motion_data.length>0) {
		
			var ctx = self.tempcanvas.getContext("2d")
			
			var pointlen = LED.motion_data.length
			
			for (var i=0;i<pointlen;i++) {
				var thispoint = LED.motion_data[i]
				// Begin Drawing Path
				ctx.beginPath();
				// Background color for the object that we'll draw
				ctx.fillStyle = self.gradient[self.coloridx]//"#ff0000";
				// Draw the arc
				// ctx.arc(x, y, radius, start_angle, end_angle, anticlockwise)
				// angle are in radians
				// 360 degrees = 2p radians or 1 radian = 360/2p = 180/p degrees
				ctx.arc(thispoint.x, thispoint.y, thispoint.amplitude/15 , 0, Math.PI*2, false);	//thispoint.amplitude/5
				// Close Drawing Path
				ctx.closePath();
				// Fill the canvas with the arc
				ctx.fill();
			}
		}
		
*/



	//	LED.composite_ctx.drawImage(self.tempcanvas, 0, 0,LED.grid_width,LED.grid_height)

	},

	render_working:	function(){
		
		var thiscolor = self.gradient[self.coloridx]
		
		var rcol = df.getdec(thiscolor.substr(0,2))
		var gcol = df.getdec(thiscolor.substr(2,2))
		var bcol = df.getdec(thiscolor.substr(4,2))
		

		if (self.grayframes.length>0 && LED.motion_data && LED.motion_data.length == 0) {
			self.grayframes.shift()
		} else if (LED.motion_data && LED.motion_data.length > 0) {
			self.grayframes.push(LED.motion_grid)
		}
		if (self.grayframes.length>self.maxframes) {
			self.grayframes.shift()
		}

		var griddata = []

		if (self.grayframes.length>0) {
			for (var y=0;y<(240/8);y++) {
				for (var x=0;x<(360/8);x++) {				
					var gray		= self.grayframes[0].charCodeAt(self.getGridIndex(x,y))				
					var ix			= LED.getCanvasIndex(x,y)				
					griddata[ix]	= (rcol * (gray/255))
					griddata[ix+1]	= (gcol * (gray/255))
					griddata[ix+2]	= (bcol * (gray/255))
					griddata[ix+3]	= 255
				}
			}

			for (var i=0;i<self.grayframes.length;i++) {
				for (var y=0;y<(240/8);y++) {
					for (var x=0;x<(360/8);x++) {				
						var gray		= self.grayframes[i].charCodeAt(self.getGridIndex(x,y))				
						var ix			= LED.getCanvasIndex(x,y)				
						griddata[ix]	= griddata[ix]   + (rcol * (gray/255) /2)
						griddata[ix+1]	= griddata[ix+1] + (gcol * (gray/255)/2)
						griddata[ix+2]	= griddata[ix+2] + (bcol * (gray/255)/2)
						griddata[ix+3]	= 255
					}
				}
			}

			//var griddata = []
			for (var y=0;y<(240/8);y++) {
				for (var x=0;x<(360/8);x++) {				
					var gray		= LED.motion_grid.charCodeAt(self.getGridIndex(x,y))				
					var ix			= LED.getCanvasIndex(x,y)				
					griddata[ix]	= griddata[ix]   + (rcol * (gray/255))
					griddata[ix+1]	= griddata[ix+1] + (gcol * (gray/255))
					griddata[ix+2]	= griddata[ix+2] + (bcol * (gray/255))
					griddata[ix+3]	= 255				
				}
			}
		} else {
			for (var y=0;y<(240/8);y++) {
				for (var x=0;x<(360/8);x++) {				
					var ix			= LED.getCanvasIndex(x,y)				
					griddata[ix]	= 0
					griddata[ix+1]	= 0
					griddata[ix+2]	= 0
					griddata[ix+3]	= 255
				}
			}

		}
		
		
		var imagedata = self.tempcanvas.getContext("2d").getImageData(0,0,LED.grid_width,LED.grid_height)
		for (var i=0;i<griddata.length;i++){Math.min(255,imagedata.data[i] += griddata[i])}
		LED.composite_ctx.putImageData(imagedata, 0, 0);
		
		/*
		if (LED.motion_data&&LED.motion_data.length>0) {
		
			var ctx = self.tempcanvas.getContext("2d")
			
			var pointlen = LED.motion_data.length
			
			for (var i=0;i<pointlen;i++) {
				var thispoint = LED.motion_data[i]
				// Begin Drawing Path
				ctx.beginPath();
				// Background color for the object that we'll draw
				ctx.fillStyle = self.gradient[self.coloridx]//"#ff0000";
				// Draw the arc
				// ctx.arc(x, y, radius, start_angle, end_angle, anticlockwise)
				// angle are in radians
				// 360 degrees = 2p radians or 1 radian = 360/2p = 180/p degrees
				ctx.arc(thispoint.x, thispoint.y, thispoint.amplitude/15 , 0, Math.PI*2, false);	//thispoint.amplitude/5
				// Close Drawing Path
				ctx.closePath();
				// Fill the canvas with the arc
				ctx.fill();
			}
		}
		
*/

		self.coloridx++
		if (self.coloridx >= self.gradient.length){self.coloridx=0}


	//	LED.composite_ctx.drawImage(self.tempcanvas, 0, 0,LED.grid_width,LED.grid_height)

	},

	
	start:	function(){
		
		if (self.tempcanvas == undefined) {
			self.tempcanvas = df.newdom({
				tagname	: "canvas",
				target	: LED.contextHolder,
				attributes	: {
					width	: LED.grid_width,
					height	: LED.grid_height
				}
			})
			
			self.gradient = df.gradient(["ff0000","ffff00","00ff00","00ffff","0000ff","ff0000"],40)
		}
		
	},
	
	end:	function() {
	
	},
	
	interface:	function() {
	
//==========================================================================================
		var sliders=[]
		
		var slider_tmr
		var slidertypes = [
		
			
			{
				id:"interactive_threshold",
				name:"Threshold",
				min:0,
				max:1,
				startvalue:self.threshold/255,
				handler:function(val){
					self.threshold = Math.round(val*255)
					return Math.round(val*255)
				}
			},
			{
				id:"interactive_maxframes",
				name:"Max Frames",
				min:0,
				max:1,
				startvalue:(self.maxframes/100),
				handler:function(val){
					self.maxframes = Math.round(val*100)
					self.grayframes = []
					
					return self.maxframes
				}
			},
			{
				id:"interactive_gradient",
				name:"Gradient Size",
				startvalue:self.gradient_size,
				min:0,
				max:128,
				handler:function(val){
					self.gradient_size = val					
					var newval = Math.round(val)
					self.gradient = df.gradient(["ff0000","ffff00","00ff00","00ffff","0000ff","ff0000"],newval)					
					return newval
				}
			}
			
			
		]
		
		//------------------------------------------------------------------------------------------------------
		
		var thisvalue
		
		var sliders = LED.sliderList(slidertypes)


		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})
		
//==========================================================================================


	},
	
	settings_to_save	: [
		"threshold",
		"maxframes",
		"gradient_size"
	],
	
	threshold		: 10,
	maxframes		: 20,
	gradient_size	: 40


}

	LED.plugins.push(self)
})();
