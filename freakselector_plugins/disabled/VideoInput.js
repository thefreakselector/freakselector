/*
	// Video capture. framerate may be ignored by the device.
	bool setVideoCaptureConfig(long device, long capwidth, long capheight, long framerate, long outputwidth, long outputheight);
	// Will start calling the event handler onnewcaptureimage and pass the image as a base64-encode string with the desired format
	bool startVideoCapture(const std::string& format);
	void stopVideoCapture();
	// Returns base64-encoded image in desired format. Returns empty string if no new image available. Do not call if event-based capture is
	// in progress!
	std::string getCaptureImage(const std::string& format);




iMicro Webcam:
160x120
176x144
320x240
352x288
640x480
800x600
1024x768
1280x1024

    ///////////////////////////////////////////////////////////////////////////////
	// Video capture
	//

	// Video capture. framerate may be ignored by the device.
	bool setVideoCaptureConfig(long device, long capwidth, long capheight, long framerate, long outputwidth, long outputheight);

	// Frees device
	void clearVideoCaptureConfig();

	// Equivalent to clear.. followed by set.. with same parameters as before. Useful to restart a frozen device.	
	bool resetVideoCaptureConfig();

	// Shows the settings dialog window for the configured video capture device.
	void showVideoCaptureSettingsDialog();

	// Sets video capture settings. All params are from 0..1, default is typically 0.5
	void setVideoCaptureSettings(float brightness, float contrast, float hue, float saturation);

	// Will start calling the event handler onnewcaptureimage and pass the image as a base64-encode string with the desired format
	bool startVideoCapture(const std::string& format);

	void stopVideoCapture();

	// Returns base64-encoded image in desired format. Returns empty string if no new image available. Do not call if event-based capture is
	// in progress!	
	std::string getCaptureImage(const std::string& format);

	// Fires onnewcaptureimage event	
	FB_JSAPI_EVENT(newcaptureimage, 1, (const std::string&));

*/

(function(){



var self = {

	name		: "Video Input",
	icon		: "Video Input.png",
	//fullscreen	: true,
	
	running		: false,
	
	start:	function(){
		
		if (self.current_video_device==undefined) {
			self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)
			self.openVideoPort()
			self.running = true
		}
	}
	,
	frames	: [],
	
	openVideoPort:	function() {
	
		if (self.current_video_device === undefined) {

			//-----------------------------------------------------------

			LED.LEDplugin.addEventListener("newcaptureimage", self.newframe, false);

			LED.LEDplugin.addEventListener("newinterestpoints", self.newMotionFrame, false);

			//-----------------------------------------------------------
			if (self.pointcanvas === undefined){
				self.pointcanavs = df.newdom({
					tagname	: "canvas",
					target	: LED.contextHolder,
					attributes:{
						id	: "pointcanvas",
						width	: self.camera_width/8,
						height	: self.camera_height/8
					}
				})
			}
			//-----------------------------------------------------------
			self.video_capture_width  = Math.round(self.camera_width  / 1.2 );
			self.video_capture_height = Math.round(self.camera_height / 1.2 );

			if (self.set_interest_point_tracking == true) {
				LED.LEDplugin.setVideoCaptureConfig(0, self.camera_width, self.camera_height, 0, self.camera_width/8, self.camera_height/8)
			} else {
				LED.LEDplugin.setVideoCaptureConfig(0, self.camera_width, self.camera_height, 0, self.video_capture_width || LED.grid_width, self.video_capture_height || LED.grid_height)
			console.log(">>>>",0, self.camera_width, self.camera_height, 0, self.video_capture_width || LED.grid_width, self.video_capture_height || LED.grid_height)
			}
			
console.log(self.camera_width, self.camera_height)
console.log(self.video_capture_width, self.video_capture_height)
			//-----------------------------------------------------------

			LED.LEDplugin.setVideoCaptureFrameStride(self.setVideoCaptureFrameStride)
			LED.LEDplugin.startVideoCapture("RGB")
			self.current_video_device = 0

			LED.LEDplugin.setInterestPointTracking(self.set_interest_point_tracking); //false calls newcaptureimage, true calls newinterestpoints

		}

		if (self.grid_full === undefined){

			self.grid_full_holder = df.newdom({
				tagname: "div",
				target: LED.LEDwindow,
				attributes:{
					width: self.video_capture_width,
					height: self.video_capture_height,
					style: "position:absolute;top:45px;left:690px;width:200px;height:150px;"
				}				
			})

			self.grid_full = df.newdom({
				tagname: "canvas",
				target: self.grid_full_holder,
				attributes:{
					width: self.video_capture_width,
					height: self.video_capture_height,
					style: "display:none;width:100%;height:100%;background-color:black;"
				},
				events:[
					{
						name: "click",
						handler: function(evt){
							self.vid_full_screen = !self.vid_full_screen;
		
							if (self.vid_full_screen){
								self.grid_full.webkitRequestFullScreen(document.body.ALLOW_KEYBOARD_INPUT)
							} else {
								document.webkitCancelFullScreen(document.body.ALLOW_KEYBOARD_INPUT)
							}

						}
					}
				]
			})

			self.grid_full_ctx = self.grid_full.getContext("2d");

			self.grid_scaled = df.newdom({
				tagname: "canvas",
				target: LED.LEDwindow,
				attributes:{
					width: LED.grid_width,
					height: LED.grid_height,
					style: "display:none;position:absolute;top:10px;left:690px;width:"+LED.grid_width+"px;height:"+LED.grid_height+"px;border:1px solid black;"
				}
			})
			self.grid_scaled_ctx = self.grid_scaled.getContext("2d");
			self.grid_scaled_ctx.scale(LED.grid_width / self.video_capture_width, LED.grid_height / self.video_capture_height);
		}
		
		
	},

//==============================================================================================

	end:	function(){
		LED.LEDplugin.stopVideoCapture();

		LED.LEDplugin.removeEventListener("newcaptureimage", self.newframe);

		LED.LEDplugin.removeEventListener("newinterestpoints", self.newMotionFrame);


		df.removedom(self.grid_full);
		df.removedom(self.grid_scaled)
		df.removedom(self.pointcanvas)
		self.grid_full = undefined;
		self.grid_scaled = undefined;
		self.pointcanvas = undefined;
		self.current_video_device = undefined;
	},

//==============================================================================================

	set_interest_point_tracking	: false,
	setVideoCaptureFrameStride	: 2,
	
//==============================================================================================
//	getGridIndex : function(x,y){
	//	return ((Math.floor(y)*(self.video_capture_width))+Math.floor(x))
//	},

	newframe: function(videoimage) {	
		if (videoimage==undefined) {
			return
		}

		if (self.started!=true) {
			self.frames=[]
			return
		}

		for (var i=0;i<LED.queue.length;i++) {
			if (LED.queue[i].uses_camera == true) {
				self.frames = [];
				return
			}
		}
		
		var griddata = self.imagedata.data = []

		var grid = LED.base64decode(videoimage)

		var grid_full_data = self.grid_full_ctx.getImageData(0,0,self.video_capture_width, self.video_capture_height)
		var new_grid_data = [];
console.log("grid length:",grid.length)
		for (var i=0,ii=0;i<grid.length;i+=3,ii+=4){
			grid_full_data.data[ii]   = grid.charCodeAt(i)
			grid_full_data.data[ii+1] = grid.charCodeAt(i+1)
			grid_full_data.data[ii+2] = grid.charCodeAt(i+2)
			grid_full_data.data[ii+3] = 255
		}
		self.grid_full_ctx.putImageData(grid_full_data, 0, 0);
		
		self.grid_scaled_ctx.drawImage(self.grid_full,0,0);

		var grid_scaled_data = self.grid_scaled_ctx.getImageData(0,0,LED.grid_width, LED.grid_height);

		var griddata = grid_scaled_data.data

		self.frames[0] = griddata

		if (self.save_frames && self.set_interest_point_tracking !== true) {
			self.saveVideoFrame();
		}

	},
//==============================================================================================
	save_frame_timeout	: undefined,
	save_frame_frequency: .5,			//milliseconds
	video_capture_width	: 44,
	video_capture_height: 22,

	saveVideoFrame: function(){

		if (self.save_frame_timeout !== undefined){
			return
		}

		var this_frame = self.grid_full.toDataURL("image/png");

		df.ajax.send({
			url	: "saveVideoFrame",
			verb:"post",
			formdata:LED.base64encode(this_frame),
			callback:function(ajaxobj){
				self.save_frame_timeout = setTimeout(function(){
					self.save_frame_timeout = undefined
				}, self.save_frame_frequency * 1000)
			}
		})

	},

//==============================================================================================

	newMotionFrame:	function(videoimage,json){
				
//console.log(json)
		//if (json.length==0) {return}
//console.log(json)
//		var ctx = LED.registry["Video Input"].pointcanavs.getContext("2d")
//		ctx.clearRect(0, 0, self.camera_width/8,self.camera_height/8);

		var points = df.json.deserialize(json)
		
		LED.motion_data = points

		var getGridIndex = function(x,y){
			return ((Math.floor(y)*(self.camera_width/8))+Math.floor(x))
		}

		LED.motion_grid = LED.base64decode(videoimage)

		var griddata = []
		for (var y=0;y<(self.camera_height/8);y++) {
			for (var x=0;x<(self.camera_width/8);x++) {
				
				var gray = LED.motion_grid.charCodeAt(getGridIndex(x,y))
				
				var ix = LED.getCanvasIndex(x,y)
				
				griddata[ix]	= gray
				griddata[ix+1]	= gray
				griddata[ix+2]	= gray
				griddata[ix+3]	= 255
				
			}
		}
console.log("newMOTIONframe")

		if (self.set_interest_point_tracking){
			//self.frames.push(griddata)
				self.frames[0] = griddata			
		}


		
	},

//==============================================================================================

	lastframe:undefined,
	
	camera_width:360,
	camera_height:240,

	render:	function() {
	
		//make a copy of the data, because the data may get updated while this function is executing, before it puts data to the canvas		
		
		if (self.play_state == "paused"){return}
		
		//var imagedata = LED.registry["Video Input"].pointcanavs.getContext("2d").getImageData(0,0,self.camera_width/8,self.camera_height/8)
	//LED.composite_ctx.drawImage(LED.registry["Video Input"].pointcanavs, 0, 0);//{data:data,width:LED.grid_width,height:LED.grid_height}


		for (var i=0;i<LED.queue.length;i++) {
			if (LED.queue[i].uses_camera == true) {
				return
			}
		}
		
		
		if (self.frames.length>0) {
			var framedata = self.frames.shift()
			self.lastframe = framedata
		} else if (self.lastframe !== undefined) {
			var framedata = self.lastframe
		} else {
			return
		}

		
		var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)
		
		for (var i=0;i<framedata.length;i++){imagedata.data[i] = framedata[i]}
		var data = imagedata.data

		if (self.invert_red||self.invert_green||self.invert_blue) {
			for (var i=0;i<data.length;i+=4) {
				if (self.invert_red) {data[i]	= 255-data[i]}
				if (self.invert_green){data[i+1]= 255-data[i+1]}
				if (self.invert_blue){data[i+2] = 255-data[i+2]}
			}
		}		

		if (self.condense_field == true) {

			for (var x=0;x<LED.grid_width;x++) {
				var newval = 0
				var rmax = 0
				var gmax = 0
				var bmax = 0
				for (var y=0;y<LED.grid_height;y++){
					var idx = LED.getCanvasIndex(x,y)
					if (data[idx]>rmax)   {rmax = data[idx]}
					if (data[idx+1]>gmax) {gmax = data[idx+1]}
					if (data[idx+2]>bmax) {bmax = data[idx+2]}
				}
				for (var y=0;y<LED.grid_height;y++){
					var idx = LED.getCanvasIndex(x,y)
					data[idx]	= rmax
					data[idx+1] = gmax
					data[idx+2] = bmax
				}
			}		
		}
		
		if (self.delay_field == true) {

			for (var x=0;x<LED.grid_width;x++) {
				var newval = 0
				var rmax = 0
				var gmax = 0
				var bmax = 0
				for (var y=0;y<LED.grid_height;y++){
				
					var idx = LED.getCanvasIndex(x,y)
					if (data[idx]>rmax)   {rmax = data[idx]}
					if (data[idx+1]>gmax) {gmax = data[idx+1]}
					if (data[idx+2]>bmax) {bmax = data[idx+2]}
		
					if (y<LED.grid_height-1) {
						var idx2	= LED.getCanvasIndex(x,y+1)
						data[idx]	= (self.delay_frame[idx] = self.delay_frame[idx2] || 0) * (y/LED.grid_height)
						data[idx+1] = (self.delay_frame[idx+1] = self.delay_frame[idx2+1] || 0) * (y/LED.grid_height)
						data[idx+2] = (self.delay_frame[idx+2] = self.delay_frame[idx2+2] || 0) * (y/LED.grid_height)
					}
				}
				
				var idx		= LED.getCanvasIndex(x,LED.grid_height-1)
				data[idx]	= rmax
				data[idx+1] = gmax
				data[idx+2] = bmax
				self.delay_frame[idx]	= rmax
				self.delay_frame[idx+1] = gmax
				self.delay_frame[idx+2] = bmax
			}
		}

		//imagedata.data = data
		//imagedata.width = LED.grid_width
		//imagedata.height= LED.grid_height

		LED.composite_ctx.putImageData(imagedata, 0, 0);//{data:data,width:LED.grid_width,height:LED.grid_height}
		
		
		if (self.set_interest_point_tracking && LED.motion_data) {
			var ctx = LED.composite_ctx
			var pointlen = LED.motion_data.length//Math.min(1,LED.motion_data.length)//points.length
			for (var i=0;i<pointlen;i++) {
				var thispoint = LED.motion_data[i]
				// Begin Drawing Path
				ctx.beginPath();
				// Background color for the object that we'll draw
				ctx.fillStyle = "#00ff00";
				// Draw the arc
				// ctx.arc(x, y, radius, start_angle, end_angle, anticlockwise)
				// angle are in radians
				// 360 degrees = 2p radians or 1 radian = 360/2p = 180/p degrees
				ctx.arc(thispoint.x, thispoint.y, 1 , 0, Math.PI*2, false);	//thispoint.amplitude/5
				// Close Drawing Path
				ctx.closePath();
				// Fill the canvas with the arc
				ctx.fill();
			}
		}

	},

	delay_frame:[],

	//=============================================
	//Video and video plugins need these functions
	play:	function(idx) {

		LED.LEDplugin.startVideoCapture("RGB")
	},
	pause:	function() {
		LED.LEDplugin.stopVideoCapture()
	//	self.current_video_device = undefined
		
	},
	//=============================================

	condense_field:false,
	invert_red	: false,
	invert_green: false,
	invert_blue	: false,
	
	
	interface:	function(){

/*		
		df.gadgets.checkbox({
			width		: 50,
			height		: 50,
			checkcolor	: "#00ff00",
			checked		: true,
			style		: "",
			checkstyle	: "",
			checkhtml	: "",
			target		: "",
			onchange	: function(evtobj) {
			
			},
			events		: [
				{
					name	: "",
					hander	: function(evtobj) {
					
					}
				}
			]
		})
*/

		df.newdom({
			tagname		:"df-onoff",
			target	: LED.pluginwindow,
			attributes:{
				id			: "condenseswitch",
				name		: "Vertical Field",
				namestyle	: "margin-top:15px;width:180px;margin-left:15px;",
				style		: "position:relative;",
				class		: "onoff",
				value		: self.condense_field,
				width		: 35,
				onchange	: function(val){
					self.condense_field = val
				}
			}
		})
		
		df.newdom({tagname:"br",target:LED.pluginwindow})
		
		df.newdom({
			tagname		:"df-onoff",
			target	: LED.pluginwindow,
			attributes:{
				id			: "delayswitch",
				name		: "Vertical Buffer",
				namestyle	: "margin-top:5px;width:180px;margin-left:15px;",
				style		: "position:relative;",
				class		: "onoff",
				value		: self.delay_field,
				width		: 35,
				onchange	: function(val){
						self.delay_frame = []
						self.delay_field = val
				}
			}
		})
		
		df.newdom({tagname:"br",target:LED.pluginwindow})

		df.newdom({
			tagname		:"df-onoff",
			target	: LED.pluginwindow,
			attributes:{
				id			: "redswitch",
				name		: "Invert Red",
				namestyle	: "margin-top:15px;width:180px;margin-left:15px;",
				style		: "position:relative;",
				class		: "onoff",
				value		: self.invert_red,
				width		: 35,
				onchange	: function(val){
						self.invert_red = val
				}
			}
		})
		
		df.newdom({tagname:"br",target:LED.pluginwindow})

		df.newdom({
			tagname		:"df-onoff",
			target	: LED.pluginwindow,
			attributes:{
				id			: "greenswitch",
				name		: "Invert Green",
				namestyle	: "margin-top:5px;width:180px;margin-left:15px;",
				style		: "position:relative;",
				class		: "onoff",
				value		: self.invert_green,
				width		: 35,
				onchange	: function(val){
						self.invert_green = val
				}
			}
		})
		
		df.newdom({tagname:"br",target:LED.pluginwindow})

		df.newdom({
			tagname		:"df-onoff",
			target	: LED.pluginwindow,
			attributes:{
				id			: "blueswitch",
				name		: "Invert Blue",
				namestyle	: "margin-top:5px;width:180px;margin-left:15px;",
				style		: "position:relative;",
				class		: "onoff",
				value		: self.invert_blue,
				width		: 35,
				onchange	: function(val){
						self.invert_blue = val
				}
			}
		})
		
		df.newdom({tagname:"br",target:LED.pluginwindow})
		

		df.newdom({
			tagname		:"df-onoff",
			target	: LED.pluginwindow,
			attributes:{
				id			: "trackswitch",
				name		: "Motion",
				namestyle	: "margin-top:5px;width:180px;margin-left:15px;",
				style		: "position:relative;",
				class		: "onoff",
				value		: (LED.registry["Video Input"].set_interest_point_tracking==true),
				width		: 35,
				onchange	: function(val){
					
					self.set_interest_point_tracking = val
					
					LED.LEDplugin.stopVideoCapture()

					setTimeout(function(){

						if (self.set_interest_point_tracking == true) {
							LED.LEDplugin.setVideoCaptureConfig(0, self.camera_width, self.camera_height, 0, self.camera_width/8, self.camera_height/8)
						} else {
							LED.LEDplugin.setVideoCaptureConfig(0, self.camera_width, self.camera_height, 0, self.video_capture_width || LED.grid_width, self.video_capture_height || LED.grid_height)
						}
						
						
						if (self.set_interest_point_tracking) {
							
							updateMotionParameters()
							
							var transform = df.json.serialize({
								ax	: -0.012178,
								ay	: -0.000714,
								bx	: 46.677414,
								by	: 23.814672,
								cxx	: -1.115329,
								cxy	: -0.076554,
								cyx	: -0.168725,
								cyy	: -0.882535
							})
							
							LED.LEDplugin.setInterestPointTransform(transform, 45, 30);
						}

						LED.LEDplugin.setInterestPointTracking(self.set_interest_point_tracking); //false calls newcaptureimage, true calls newinterestpoints

						LED.LEDplugin.startVideoCapture("RGB")
						
					},40)
/*

    // defines the mapping from camera view to LED grid
    // it is important that capture size = 45x30, because this transform has
    // been computed using that information.
	var transform = "{\"ax\":-0.012178, \"ay\":-0.000714, \"bx\":46.677414, \"by\":23.814672, \"cxx\":-1.115329, \"cxy\":-0.076554, \"cyx\":-0.168725, \"cyy\":-0.882535}"

	// transform = plugin.fitInterestPointTransformFromFile(autogrid_cfg, 0.5, scale_factor, dst_width, dst_height);

	plugin.setInterestPointTransform(transform, dst_width, dst_height);

*/
					
				}
			}
		})
		
		df.newdom({tagname:"br",target:LED.pluginwindow})



		df.newdom({
			tagname:"input",
			target	: LED.pluginwindow,
			attributes:{
				type	: "button",
				style	: "font-size:22px;margin-left:15px;margin-top:10px;",
				value	: "Camera Settings"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt) {
						LED.LEDplugin.showVideoCaptureSettingsDialog()
					
					}
				}
			]
		})

//===================================================================================
//setVideoCaptureSettings(float brightness, float contrast, float hue, float saturation)

		var slidertypes = [
			{
				id:"brightnessslider",
				name:"Brightness",
				startvalue:self.brightness,
				min:0,
				max:1,
				handler:function(val){
					self.brightness = val
					LED.LEDplugin.setVideoCaptureSettings(self.brightness, self.contrast, self.hue, self.saturation)
				}
			},
			{
				id:"contrastslider",
				name:"Contrast",
				startvalue:self.contrast,
				min:0,
				max:1,
				handler:function(val){
					self.contrast = val
					LED.LEDplugin.setVideoCaptureSettings(self.brightness, self.contrast, self.hue, self.saturation)
				}
			},
			{
				id:"hueslider",
				name:"Hue",
				startvalue:self.hue,
				min:0,
				max:1,
				handler:function(val){
					self.hue = val
					LED.LEDplugin.setVideoCaptureSettings(self.brightness, self.contrast, self.hue, self.saturation)
				}
			},
			{
				id:"saturationslider",
				name:"Saturation",
				startvalue:self.saturation,
				min:0,
				max:1,
				handler:function(val){
					self.saturation = val
					LED.LEDplugin.setVideoCaptureSettings(self.brightness, self.contrast, self.hue, self.saturation)
				}
			}
		]
		
		var sliders = LED.sliderList(slidertypes)
		
		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:10px;left:230px;background-color:#cacaca;padding:5px;color:white;"
			},
			children	: sliders
		
		})

		//--------------------------------------------------------------------------------------
/*
	plugin.setInterestPointParameters(0.01, // filterConstant  range: 0 ... 0.1
                                      1.3, // sigma   range: 0 ... 3
                                      10, // factor  range: 0 ... 20
                                      0.01, // maxError  fixed 0.01
                                      1, //threshold   range: 0 ... 3
                                      100,  //maxPoints  fixed somevalue
                                      true) // laplace  on/off toggle
*/

		var updateMotionParameters = function() {
			LED.LEDplugin.setInterestPointParameters(
				self.motion_filterConstant,	// filterConstant (0.0 - 0.1 slider), default .01
				self.motion_sigma,			// sigma  (0.5 - 3 range), default = 1
				self.motion_factor,			// factor (0 - 20 range), default = 8
				0.01,						// maxError (no slider)
				self.motion_threshold,		// threshold  (0 - 10 range), default = 3
				self.motion_maxPoints,		// maxPoints (settings, max 100?), default = 50
				self.motion_laplace
			) 		
		}

		var slidertypes = [
			{
				id:"motion_filterConstant",
				name:"filterConstant",
				startvalue:(self.motion_filterConstant*10),
				min:0,
				max:1,
				handler:function(val){
					self.motion_filterConstant = val/10
					updateMotionParameters()
				}
			},
			{
				id:"motion_sigma",
				name:"Sigma",
				startvalue:((self.motion_sigma)/3),
				min:0,
				max:1,
				handler:function(val){
					self.motion_sigma = (val*3)
					updateMotionParameters()
				}
			},
			{
				id:"motion_factor",
				name:"Factor",
				startvalue:(self.motion_factor/20),
				min:0,
				max:1,
				handler:function(val){
					self.motion_factor = val*20
					updateMotionParameters()
				}
			},
			{
				id:"motion_threshold",
				name:"Threshold",
				startvalue:(self.motion_threshold/3),
				min:0,
				max:1,
				handler:function(val){
					self.motion_threshold = val*3
					updateMotionParameters()
				}
			},
			{
				id:"motion_maxPoints",
				name:"maxPoints",
				startvalue:(self.motion_maxPoints),
				min:0,
				max:100,
				handler:function(val){
					self.motion_maxPoints = Math.round(val)
					updateMotionParameters()
					return self.motion_maxPoints
				}
			}
		]
		
		//--------------------------------------------------------------------------------------

		updateMotionParameters()
		
		var sliders = LED.sliderList(slidertypes)
		
		var motionsettingsdiv = df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:10px;left:560px;background-color:#cacaca;padding:5px;color:white;"
			},
			children	: sliders
		
		})
		
		//--------------------------------------------------------------------------------------

		df.newdom({
			tagname		:"df-onoff",
			target	: motionsettingsdiv,
			attributes:{
				id			: "laplaceswitch",
				name		: "Laplace",
				namestyle	: "margin-top:5px;width:290px;",
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.motion_laplace==true),
				width		: 35,
				onchange	: function(val){
						self.motion_laplace = val
				}
			}
		})

		//--------------------------------------------------------------------------------------

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "show_input_video",
				name		: "Show Input",
				style	: "position:absolute;top:400px;left:10px;",
				class		: "onoff",
				value		: (self.show_input_video === true),
				width		: 50,
				onchange	: function(val){
						self.show_input_video = val

						self.grid_full.style.display = self.show_input_video ? "block" : "none"
				}
			}
		})		

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "saveframes",
				name		: "Save Frames",
				style	: "position:absolute;top:469px;left:10px;",
				class		: "onoff",
				value		: (self.save_frames==true),
				width		: 50,
				onchange	: function(val){
						self.save_frames = val
				}
			}
		})		

//==========================================================================================

		var sliders = LED.sliderList([
			{
				id:"video_capture_frequency",
				name:"Capture Frequency",
				startvalue:self.save_frame_frequency, //.03,//Math.max(self.save_frame_frequency, .003) * 10000,
				width:200,
				min:0,
				max:10,
				handler:function(val){
					self.save_frame_frequency = Math.max(val, .03) 

					return (self.save_frame_frequency).toFixed(2)

				}
			}
		])

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "captureVideoControls",
				style	: "position:absolute;top:434px;left:237px;z-index:5000;color:white;background-color:#efeadc;width:300px;overflow:hidden;overflow-y:auto;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})

		//--------------------------------------------------------------------------------------
		
	},
	
	save_frames:false,

	settings_to_save:[

		"motion_filterConstant",
		"motion_sigma",
		"motion_factor",
		"motion_threshold",
		"motion_maxPoints",
		"motion_laplace",

		"brightness",
		"contrast",
		"hue",
		"saturation"
	
	],

	motion_filterConstant	: .1,
	motion_sigma			: .31,
	motion_factor			: .12,
	motion_threshold		: 2,
	motion_maxPoints		: 50,
	motion_laplace			: true,

	brightness	: .5,
	contrast	: .8,
	hue			: .5,
	saturation	: 1
	

}

LED.plugins.push(self);


})();
