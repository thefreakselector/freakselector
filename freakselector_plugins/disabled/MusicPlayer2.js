(function(){

var self = {
	name					: "Music Player2",
	icon					: "Music Player.png",
	has_audio			: true,
	has_eqData		: true,


	volume_level	: 1,
	fade_time			: 6,

	randomize			:true,

	//---------------------------------------------------

	start:	function(startsong){


	},

	end:	function(){

		try{clearInterval(self.positionInterval)}catch(e){}
		//soundManager.stopAll();

		if (self.music) {
			self.music.stop()
			self.music.whileplaying = null;
			self.music.unload()
			soundManager.destroySound(self.music.sID)
			delete self.music
		}

		if (self.testCanvas){
			self.testCtx = undefined;
			df.removedom(self.testCanvas);
			self.testCanvas = undefined;
		}

	},

	//----------------------------------------------------------------------------------------------------
	//audio and video plugins need these functions
	play:	function(idx) {
		if (self.music.playState==0 || self.music.paused == true) {
			soundManager.play(self.music.sID)
			try{clearInterval(self.positionInterval)}catch(e){}
			self.positionInterval = setInterval(function(){
					self.updatePosition(self.music)
			},1000)
		}
	},

	//----------------------------------------------------------------------------------------------------

	pause:	function() {
		soundManager.pause(self.music.sID)
		try{clearInterval(self.positionInterval)}catch(e){}
	},

	//----------------------------------------------------------------------------------------------------

	stop:	function() {
		self.music.stop() //soundManager.stopAll();
		try{clearInterval(self.positionInterval)}catch(e){}
	},

	//----------------------------------------------------------------------------------------------------

	next:	function() {
		self.treeService.next();

	},

	//----------------------------------------------------------------------------------------------------

	last:	function() {

		self.treeService.last();

	},

	//----------------------------------------------------------------------------------------------------

	setMute:	function(mute){
		self.mute = mute
	},
	getMute:	function() {
		return self.mute
	},

	setVolume:	function(vol){
					// 0 - 1
	},
	getVolume:	function(){
		return 1	// 0 - 1
	},

	getPosition:	function(){
		return self.position || 0
	},
	setPosition:	function(milliseconds){
		self.position = milliseconds || 0
	},

	fileSelect: function(file){
		self.selectSong(file)
	},

	//=============================================

//	render: function(context){
//	},


	interface:	function(){

//		if (self.goback.length === 0) {return}

		self.listholder = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes	: {
				id		: "listholder",
				className	: "buttonlist",
				style	: "left:5px;width:570px;"
			},
			children	: [
				{
					tagname	: "ul",
					attributes:	{
						"id"	: "playlist"
					}
				}
			]
		})

		if(!self.treeService){
			self.treeService = new LED.treeService({
				media_type: self.name,
				listholder: self.listholder,
				fileSelect: self.fileSelect,
				filetypes : self.filetypes,
				randomize : self.randomize
			})
		} else {
			self.treeService.listholder = self.listholder
			self.treeService.renderTree()
		}


	//==========================================================================================

		var slidertypes = [
			{
				id:"progress_slider",
				name:"Position",
				startvalue:self.position,
				min:0,
				max:1,
				handler:function(val){
					var music = self.music

					var duration = music.duration
					if (self.music.bytesLoaded < self.music.bytesTotal) {
						duration = music.durationEstimate
					}

					var totalseconds = duration/1000

					var time = self.secondsToTime(totalseconds-(totalseconds*val))

					self.newposition = (totalseconds*val)

					return "-"+time

				},
				mousedown:function(){
					self.positionmoving = true
					self.newposition = undefined

				},
				mouseup:function(){
					self.positionmoving = false
					if (self.newposition!=undefined) {
						self.music.setPosition(self.newposition*1000)
					}
				}
			},
			{
				id:"music_volume_slider",
				name:"Volume",
				startvalue:self.volume_level,
				min:0,
				max:1,
				handler:function(val){
					self.volume_level = val
					//self.music.setVolume((LED.master_volume * self.volume_level)*100);
				}
			},
			{
				id:"fade_time_slider",
				name:"Fade Time",
				startvalue:self.fade_time,
				min:0,
				max:10,
				handler:function(val){
					self.fade_time = val
					//self.volume_level = val
					//self.music.setVolume((LED.master_volume * self.volume_level)*100);
				}
			}
		]

		self.sliders = LED.sliderList(slidertypes)

		self.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:587px;top:6px;color:white;"
				},
				children	: self.sliders
		})

		try{
			df.dg("slider_progress_slider").getElementsByClassName("df_slider_gadget")[0].style.zIndex = 100
		}catch(e){}
		self.bufferedDiv = df.newdom({
			tagname	: "div",
			target	: df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0],
			attributes:{
				style:"pointer-events:none;width:"+df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0].offsetWidth+"px;height:"+df.dg("slider_progress_slider").getElementsByClassName("df_slider_race")[0].offsetHeight+"px;background-color:red;z-index:50;position:absolute;overflow:hidden;opacity:.3"
			}
		})

	//==========================================================================================

		var randomize_button = df.newdom({
			tagname		:"df-onoff",
			target		: self.positionDiv,
			attributes:{
				id		: "musicplayer_randomize",
				name		: "Randomize",
				style		: "position:relative;top:20px;left:20px;",
				class		: "onoff",
				value		: self.randomize,
				width		: 40,
				height		: 40,
				onchange	: function(val){
					self.randomize = val;
					self.treeService.randomize = val;
				}
			}
		})


	},

	//----------------------------------------------------------------------------------------------------

	selectSong:	function(song) {
		var item = song

		if (self.music){self.music.stop()} //soundManager.stopAll();

		if (self.music) {
			self.music.whileplaying = null;
			self.music.unload()
			soundManager.destroySound(self.music.sID)
			delete self.music
		}

		var music = soundManager.createSound({
			id				: escape(item),
			url				: "getAudio/"+item,
			autoLoad		: true,
			stream			: true,
			autoPlay		: true,
			useEQData		: true,
			useWaveformData	: true,
			usePeakData		: true,
			whileplaying	: self.analyzeSound,
			onfinish:	function(){

				self.next()
			},
			onplay			: function(){//onload
try{
				try{self.bufferedDiv.style.left="0px"}catch(e){}
				this.juice = {
					frameCount		: 0,
					volume			: 0,
					freqBands		: [],
					freqBands16		: [],
					avgFreqBands	: [],
					relFreqBands	: [],
					longAvgFreqBands: [],
					avgRelFreqBands	: [],
					waveData 		: [],
					eqData 			: [],
					freqData 		: []
				}

				var numFreqBands = 3;
				for (var i=0;i<numFreqBands;i++) {
					this.juice.avgFreqBands[i] = 0;
					this.juice.longAvgFreqBands[i] = 0;
				}
				self.music = this
				this.setVolume(0);
				self.play_state = "playing"

				try{clearInterval(self.positionInterval)}catch(e){}
				self.positionInterval = setInterval(function(){
						self.updatePosition(self.music)
				},1000)

}catch(e){
console.warn("music start error")
console.error(e)
self.next()
}

			}
		});
	},

	//==========================================================================================

	analyzeSound:	function() {

		var juice = this.juice;

		var numFreqBands = 3;
		var freqBandInterval = 256 / numFreqBands;

		var eqData		= juice.eqData		= this.eqData;
		var peakData	= juice.peakData	= this.peakData;
		var waveData	= juice.waveData	= this.waveformData;

		if (waveData.length == 512) {
			juice.waveDataL = waveData.slice(0, 256);
			juice.waveDataR = waveData.slice(256);
			this.waveDataL = juice.waveDataL
			this.waveDataR = juice.waveDataR
		} else {
			juice.waveDataL = juice.waveDataR = juice.waveData;
			this.waveDataL = this.waveDataR = juice.waveData;
		}

		if (eqData.length == 512) {
			juice.eqDataL = eqData.slice(0, 256);
			juice.eqDataR = eqData.slice(256);
		} else {
			juice.eqDataL = juice.eqDataR = juice.eqData;
		}

		var freqBands			= juice.freqBands;
		var freqBands16			= juice.freqBands16;
		var avgFreqBands		= juice.avgFreqBands;
		var longAvgFreqBands	= juice.longAvgFreqBands;
		var relFreqBands		= juice.relFreqBands;
		var avgRelFreqBands		= juice.avgRelFreqBands;

		for (var i=0;i<numFreqBands;i++) {
			freqBands[i] = 0;
		}

		for (var i=0;i<128;i++) {
			freqBands[(i/freqBandInterval/2)>>0] += eqData[i];
		}

		for (var i=0;i<16;i++)
			freqBands16[i] = 0;
		for (var i=0;i<128;i++) {
			freqBands16[(i/8)>>0] += eqData[i];
		}

		juice.frameCount++;

		for (var i=0;i<numFreqBands;i++) {

			if (freqBands[i] > avgFreqBands[i])
				avgFreqBands[i] = avgFreqBands[i]*0.2 + freqBands[i]*0.8;
			else
				avgFreqBands[i] = avgFreqBands[i]*0.5 + freqBands[i]*0.5;

			if (juice.frameCount < 50)
				longAvgFreqBands[i] = longAvgFreqBands[i]*0.900 + freqBands[i]*0.100;
			else
				longAvgFreqBands[i] = longAvgFreqBands[i]*0.992 + freqBands[i]*0.008;

			if (abs(longAvgFreqBands[i]) < 0.001) {
				relFreqBands[i] = 1.0;
			} else {
				relFreqBands[i]  = freqBands[i] / longAvgFreqBands[i];
			}

			if (abs(longAvgFreqBands[i]) < 0.001) {
				avgRelFreqBands[i] = 1.0;
			} else {
				avgRelFreqBands[i]  = avgFreqBands[i] / longAvgFreqBands[i];
			}
		}

		juice.position = this.position;

		//self.showAnalyzedSound(juice);
//


		var pos_seconds = this.position / 1000
		var dur_seconds = this.duration / 1000

		if (pos_seconds < self.fade_time) {
			var vol_fade = pos_seconds / self.fade_time
			//var new_vol = vol_fade * (self.volume_level * 100)
			self.music.setVolume(((LED.car_volume * LED.master_volume * self.volume_level * vol_fade) * 100)||0);
//console.log(vol_fade,new_vol)
		} else if (pos_seconds > dur_seconds-self.fade_time) {
			var vol_fade = (dur_seconds - pos_seconds) / self.fade_time
			//var new_vol = vol_fade * (self.volume_level * 100)
			self.music.setVolume(((LED.car_volume * LED.master_volume * self.volume_level * vol_fade) * 100)||0);
//console.log(vol_fade,new_vol)
		} else {
			self.music.setVolume((LED.car_volume * LED.master_volume * self.volume_level)*100);
//console.log(LED.master_volume , self.volume_level)
		}
	},

	//==========================================================================================

	showAnalyzedSound: function(juice){
		if (self.has_interface !== true) {return}

		if (!self.testCanvas){
			self.testCanvas = df.newdom({
				tagname: "canvas",
				target: LED.pluginwindow,
				attributes:{
					width:256,
					height:100,
					style: "position:absolute;top:400px;left:610px;width:256px;height:100px;background-color:black;"
				}
			})

			self.testCtx = self.testCanvas.getContext("2d");
		}

		var eqDataL = juice.waveDataR
		var ctx = self.testCtx;

		ctx.clearRect(0, 0, 256, 100);

		//var imagedata = self.testCtx.getImageData(0,0,256,100)
		

//console.log(eqData.length)
		ctx.strokeStyle = "#00ff00"
		ctx.lineWidth = 1;
		ctx.beginPath();

		for (var i=0;i<256;i++){
			var yy = Math.round(eqDataL[i] * 100);
			var linetop = 100 - yy
			ctx.moveTo(i,99)
			ctx.lineTo(i, linetop);
		}
		ctx.stroke();			
/*
		for ( var xx=0;xx<LED.grid_width;xx++){
			var bgcol = self.effect1b_grad.shift()
			self.effect1b_grad.push(bgcol)

			var bgcol2 = self.effect1b_grad2.shift()
			self.effect1b_grad2.push(bgcol2)

			var bg_r = parseInt(bgcol.substr(0,2),16)
			var bg_g = parseInt(bgcol.substr(2,2),16)
			var bg_b = parseInt(bgcol.substr(4,2),16)

			var rs = parseInt(bgcol2.substr(0,2),16)
			var gs = parseInt(bgcol2.substr(2,2),16)
			var bs = parseInt(bgcol2.substr(4,2),16)

			self.gradientV({x:xx, y:0, r:rs,g:gs,b:bs},{x:xx, y:LED.grid_height/2, r:bg_r,g:bg_g,b:bg_b},imagedata.data)
		}
*/
//		self.testCtx.putImageData(imagedata, 0, 0);		
	},

	//==========================================================================================

	updatePosition:	function(music) {

		if (self.has_interface != true) {return}

		var duration = music.duration

		self.completed = (music.position/duration) 

		if (self.positionmoving!=true) {

			df.dg("slider_progress_slider").getElementsByTagName("input")[0].value = self.completed * 100
//console.log("self.completed * 100",self.completed * 100, self.positionmoving)		
		}

			var totalseconds = duration/1000

			var time = self.secondsToTime(totalseconds-(totalseconds*self.completed))

			df.dg("slidervalue_progress_slider").innerHTML = "-"+time //(self.completed).toFixed(2)
/*
return

//		self.progress_inner.style.left = Math.round(100*completed)+"px"

		var scroll_gadget_width = df.dg("slider_progress_slider").offsetWidth
		var race_width = scroll_gadget_width

		if (self.music.bytesLoaded < self.music.bytesTotal) {
			duration = music.durationEstimate

			self.bufferedDiv.style.display="block"
			self.bufferedDiv.style.left = Math.round((race_width+scroll_gadget_width) *  (self.music.bytesLoaded / self.music.bytesTotal ) )+"px"

		} else {
			self.bufferedDiv.style.display="none"
		}


		if (self.positionmoving!=true) {

			//self.positionDiv.getElementsByClassName("df_slider_gadget")[0].style.left = Math.round(race_width*self.completed)+"px"



			var totalseconds = duration/1000

			var time = self.secondsToTime(totalseconds-(totalseconds*self.completed))

			df.dg("slidervalue_progress_slider").innerHTML = "-"+time //(self.completed).toFixed(2)
		}

		//var queue = window.soundManager.sounds
*/

	},

	//==========================================================================================

	secondsToTime:	function (seconds) {

		// multiply by 1000 because Date() requires miliseconds
		var date = new Date(0,0,0,0,0,0,seconds * 1000);
		var hh = date.getHours();
		var mm = date.getMinutes();
		var ss = date.getSeconds();
		// This line gives you 12-hour (not 24) time
		if (hh > 12) {hh = hh - 12;}
		// These lines ensure you have two-digits
		if (hh < 10) {hh = "0"+hh;}
		if (mm < 10) {mm = "0"+mm;}
		if (ss < 10) {ss = "0"+ss;}
		// This formats your string to HH:MM:SS
		var t = ((hh!="00")?hh+":":"")+mm+":"+ss;
		return t
	}

	//==========================================================================================

}

LED.plugins.push(self)


})();
