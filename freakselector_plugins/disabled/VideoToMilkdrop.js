/*
	// Video capture. framerate may be ignored by the device.
	bool setVideoCaptureConfig(long device, long capwidth, long capheight, long framerate, long outputwidth, long outputheight);
	// Will start calling the event handler onnewcaptureimage and pass the image as a base64-encode string with the desired format
	bool startVideoCapture(const std::string& format);
	void stopVideoCapture();
	// Returns base64-encoded image in desired format. Returns empty string if no new image available. Do not call if event-based capture is
	// in progress!
	std::string getCaptureImage(const std::string& format);




iMicro Webcam:
160x120
176x144
320x240
352x288
640x480
800x600
1024x768
1280x1024


*/


LED.plugins.push({

	name		: "Video To Milkdrop",
	fullscreen	: true, 

	
	
	start:	function(){
	
		this.imagedata = {data:new Array((LED.grid_width*LED.grid_height)*4),width:LED.grid_width,height:LED.grid_height}
		
		var self = this
				
		this.openVideoPort()

	}
	,

	openVideoPort:	function(newport) {
	
		var self = this	

		if (this.current_video_device!=undefined) {
		
			LED.LEDplugin.stopVideoCapture()
			
		} else if (newport == undefined) {

			var newframe = function(videoimage) {	

				var grid = LED.base64decode(videoimage)
				var griddata = []
				for (var i=0;i<grid.length;i+=3) {
					griddata.push(grid.charCodeAt(i))
					griddata.push(grid.charCodeAt(i+1))
					griddata.push(grid.charCodeAt(i+2))
					griddata.push(255)
				}
				self.imagedata = {data:griddata,width:LED.grid_width,height:LED.grid_height}
			}

			LED.LEDplugin.addEventListener("newcaptureimage", newframe, false);  
  			newport = 0
		}
		LED.LEDplugin.setVideoCaptureConfig(newport, 320, 240, 0, LED.grid_width, LED.grid_height)
		LED.LEDplugin.startVideoCapture("RGB")
		this.current_video_device = newport
		
	},

	render:	function() {


		LED.composite_ctx.putImageData(this.imagedata, 0, 0);

	},
	
	end:	function(){

	},

	//=============================================
	//Video and video plugins need these functions
	controls:{
		play:	function(idx) {

		},
		pause:	function() {

		},
		next:	function() {

		},
		last:	function() {

		},
		mute:	function(){

		},
		unmute:	function() {

		},
		volume:	function(vol){

		}
	},
	//=============================================

	
	interface:	function(){


	}

})






















































