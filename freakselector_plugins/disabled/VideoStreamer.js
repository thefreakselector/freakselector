(function(){

var self = {

	name		: "Video Streamer",
	icon		: "Video Player.png",
	has_audio	: true,		//this can toggle???
	fullscreen	: true,

/*
to encode:

ffmpeg2theora -v 10 -V 1500 --optimize -x 96 -y 48 -a 8 -A 96 -o "01 Farbrausch - fr041 Debris (Breakpoint 2007).ogv" "01 Farbrausch - fr041 Debris (Breakpoint 2007).mp4"


*/
	looping	: true,
	
	stream_src:	"http://192.168.1.116:8080/video",
	
	start:	function(){
				
		if (self.videoplayer==undefined) {

			self.videoplayer = df.newdom({
				tagname:"video",
				target:LED.contextHolder,
				attributes:{
					style:"width:"+((LED.grid_width/LED.grid_height)*80)+"px;height:80px;z-index:60000;",
					className:"previewcontext"
				}

			})

			df.attachevent({
				name:"loadedmetadata",
				domobj:self.videoplayer,
				handler:function(evtobj){
				//	self.videoplayer.loop = self.looping
				//	self.videoplayer.currentTime = 0
					self.videoplayer.play()
					self.play_state = "playing"
				}
			})
			
			self.videoplayer.src = self.stream_src
					self.videoplayer.play()
		}
	},
	
	end:	function(){
		
		self.pause()
		self.goback = []
		self.tree = undefined
		self.currently_playing = undefined
	},

	//=============================================
	//audio and video plugins need these functions
	play:	function(idx) {
		try{
			if (self.videoplayer){
				self.videoplayer.play()
			}
		}catch(e){}
	},
	pause:	function() {
		try{
			self.videoplayer.pause()
		}catch(e){}
	},
	stop:	function() {
		try{
			self.videoplayer.pause()
		}catch(e){}
	},

	
	//=============================================

	render: function(context){

		LED.composite_ctx.drawImage(self.videoplayer,0,0,LED.grid_width,LED.grid_height);

	},

	interface:	function(){
		

		
		self.streamsrc = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "width:500px;height:30px;font-size:25px;position:absolute;top:10px;left:10px;padding:5px;",
				value	: self.stream_src
			},
			events:[
				{
					name	: "change",
					handler	: function(evt){
						self.stream_src = evt.srcElement.value
					}
				}
			]
		
		})
		
		
//==========================================================================================
		
		
	}		

}

LED.plugins.push(self)


})();
