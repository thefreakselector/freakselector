(function(){

var self = {

	name		: "Settings",
	icon		: "Settings.png",

	start:	function(){

	},

	settings_to_save	: [
		"hue_value",
		"sat_value",
		"procmode"
	],

	hue_value	: 0,
	sat_value	: .8,
	procmode	: 0,

	interface:	function(){

		var sliders=[]

		var slider_tmr
		var slidertypes = [

		//NIRCMD
			{
				id:"lcdbright",
				name:"LCD Brightness",
				startvalue:(LED.LCD_brightness||1),
				min:0,
				max:1,
				handler:function(val){

					var newval = Math.floor((val*10))/10

					if (LED.LCD_brightness != newval) {
						LED.LCD_brightness = newval
							try{clearTimeout(slider_tmr)}catch(e){}
							slider_tmr = setTimeout(function(){
								df.ajax.send({
								url	: "nircmd/"+LED.base64encode("setbrightness "+(LED.LCD_brightness*100)+" 3")
								})
							},20)
					}

					return LED.LCD_brightness
				}
			},

			{
				id:"sysvolume",
				name:"System Volume",
				startvalue:LED.system_volume||1,
				min:0,
				max:1,
				handler:function(val){

					if (LED.system_volume != val) {
						LED.setSystemVolume(val)
					}

					return Math.round(val*100)
				}
			},

			//Other stuff

			{
				id:"musicvolume",
				name:"Volume",
				min:0,
				max:1,
				startvalue:LED.master_volume,
				tracker:function() {return LED.master_volume},
				handler:function(val){
					LED.volume_follow_screen = true

					LED.master_volume = val
				}
			},
			{
				id:"brightness",
				name:"Brightness Attenuation",
				startvalue:LED.bright_dimmer,
				min:0,
				max:1,
				tracking:function(){return LED.LEDplugin.ledAttenuation},
				handler:function(val){
					LED.brightness_follow_screen = true

					LED.bright_dimmer = val
					LED.LEDplugin.ledAttenuation = val
				}
			},
			{
				id:"red",
				name:"Red Attenuation",
				startvalue:LED.rdimmer,
				min:0,
				max:1,
				handler:function(val){
					LED.rdimmer = val
					LED.LEDplugin.redAttenuation = val
				}
			},
			{
				id:"green",
				name:"Green Attenuation",
				startvalue:LED.gdimmer,
				min:0,
				max:1,
				handler:function(val){
					LED.gdimmer = val
					LED.LEDplugin.greenAttenuation = val

				}
			},
			{
				id:"blue",
				name:"Blue Attenuation",
				startvalue:LED.bdimmer,
				min:0,
				max:1,
				handler:function(val){
					LED.bdimmer = val
					LED.LEDplugin.blueAttenuation = val

				}
			},
			{
				id:"gamma",
				name:"Gamma",
				min:0,
				max:10,
				startvalue:LED.ledGamma,
				handler:function(val){
					LED.LEDplugin.ledGamma = val
					LED.ledGamma = val
				}
			},
			{
				id:"pixelsize",
				name:"Pixel Size",
				min:0,
				max:LED.grid_width/2,
				startvalue:LED.ledRenderSize,
				handler:function(val,evtsrc){
					LED.ledRenderSize = val
					LED.LEDplugin.ledRenderSize = LED.ledRenderSize
				}
			}/*,
			{
				id:"GaussianKernel_sigma",
				name:"GaussianKernel_sigma",
				startvalue:LED.GaussianKernel_sigma,
				min:0,
				max:1,
				handler:function(val,evtsrc){
					LED.GaussianKernel_sigma = val * 3
					LED.LEDplugin.setGaussianKernel(LED.GaussianKernel_sigma , LED.GaussianKernel_maxError)
					return Math.round(LED.GaussianKernel_sigma)
				}
			},
			{
				id:"GaussianKernel_maxError",
				name:"GaussianKernel_maxError",
				startvalue:LED.GaussianKernel_maxError,
				min:0,
				max:1,
				handler:function(val,evtsrc){
					LED.GaussianKernel_maxError = val * 3
					LED.LEDplugin.setGaussianKernel(LED.GaussianKernel_sigma , LED.GaussianKernel_maxError)
					return Math.round(LED.GaussianKernel_maxError)

				}
			}*/


		]

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		var sliders = LED.sliderList(slidertypes)


		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})

//==========================================================================================
/*
	bool get_flipInputImageHorizontal();
	void set_flipInputImageHorizontal(bool val);
	bool get_flipInputImageVertical();
	void set_flipInputImageVertical(bool val);
*/
//==========================================================================================

		var button_top_px = 10;
		var button_height = 40;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "verticalflip",
				name		: 'Flip Vertical',
				namestyle	: "width:185px;",
				style		: "position:absolute;left:690px;top:"+button_top_px+"px;",
				className	: "onoff",
				width		: 25,
				value		: LED.LEDplugin.flipInputImageVertical,
				onchange	: function(val){
					LED.LEDplugin.flipInputImageVertical = val
				}
			}
		})

		button_top_px = button_top_px + button_height;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "horizontalflip",
				name		: 'Flip Horzontal',
				namestyle	: "width:183px;",
				style		: "position:absolute;left:690px;top:"+button_top_px+"px;",
				className	: "onoff",
				width		: 25,
				value		: LED.LEDplugin.flipInputImageHorizontal,
				onchange	: function(val){
					LED.LEDplugin.flipInputImageHorizontal = val
				}
			}
		})

		button_top_px = button_top_px + button_height;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "fastrender",
				name		: 'Fast Render',
				namestyle	: "width:183px;",
				style		: "position:absolute;left:690px;top:"+button_top_px+"px;",
				className	: "onoff",
				width		: 25,
				value		: LED.render_fast,
				onchange	: function(val){
					LED.render_fast = val
				}
			}
		})

		button_top_px = button_top_px + button_height;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "showinputimage",
				name		: 'Show Input',
				namestyle	: "width:183px;",
				style		: "position:absolute;left:690px;top:"+button_top_px+"px;",
				className	: "onoff",
				value		: (LED.LEDplugin.showInputImage==true),
				width		: 25,
				onchange	: function(val){
					LED.LEDplugin.showInputImage = val
				}
			}
		})

		button_top_px = button_top_px + button_height;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "nodisplay",
				name		: 'Show Display',
				namestyle	: "width:183px;",
				style		: "position:absolute;left:690px;top:"+button_top_px+"px;",
				className	: "onoff",
				value		: (LED.LEDholder.style.top != "-500px"),
				width		: 25,
				onchange	: function(val){

					if (val) {
						LED.LEDholder.style.top = "0px"
						df.removedom(self.logo)
						LED.LEDplugin.setMonitorSize( LED.display_width,LED.display_height )
					} else {
						LED.LEDplugin.setMonitorSize( 0,0 )
						LED.LEDholder.style.top = "-500px"
						self.logo = df.newdom({
							tagname	: "img",
							target	: LED.LEDwindow,
							attributes:{
								src	: "images/freak_selector.png",
								style:"position:absolute;top:0px;left:100%;margin-left:-300px;margin-top:20px;"
							}
						})
					}


				}
			}
		})

		button_top_px = button_top_px + button_height;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "analogin",
				name		: 'Analog Inputs',
				namestyle	: "width:183px;",
				style		: "position:absolute;left:690px;top:"+button_top_px+"px;",
				className	: "onoff",
				value		: (LED.capture_analog==true),
				width		: 25,
				onchange	: function(val){
					LED.capture_analog = val
				}
			}
		})

		button_top_px = button_top_px + button_height;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "fullscreen",
				name		: 'FullScreen',
				namestyle	: "width:183px;",
				style		: "position:absolute;left:690px;top:"+button_top_px+"px;",
				className	: "onoff",
				value		: (document.fullscreenEnabled || document.mozFullscreenEnabled || document.webkitIsFullScreen ? true : false),
				width		: 25,
				onchange	: function(val){
					LED.full_screen = val
					if (val){
						document.body.webkitRequestFullScreen(document.body.ALLOW_KEYBOARD_INPUT)
					} else {
						document.webkitCancelFullScreen(document.body.ALLOW_KEYBOARD_INPUT)
					}

				}
			}
		})


		button_top_px = button_top_px + button_height;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "resizing",
				name		: 'Scale UI',
				namestyle	: "width:183px;",
				style		: "position:absolute;left:690px;top:"+button_top_px+"px;",
				className	: "onoff",
				value		: LED.allow_scaling,
				width		: 25,
				onchange	: function(val){
					LED.allow_scaling = val
					LED.windowResize()

				}
			}
		})




/*
    ///////////////////////////////////////////////////////////////////////////////
	// AutoGrid
	// delay_ms is the delay in milliseconds between setting a LED pattern and capturing
	// an image. Recommended: 250ms
	// Threshold determines which pixels, relative to the peak of the LED pattern are
	// used to compute the position/radius/weight of the pattern. Recommended: 0.25
	// Once auto grid is complete, the callback is called with true as argument.
	void startAutoGrid(const std::string& filename, long delay_ms, float threshold);
	// Cancels a auto grid in progress. The callback will be called with false as argument.
	void cancelAutoGrid();
	FB_JSAPI_EVENT(autogriddone, 1, (bool));
*/
/*
		var autogridstart = df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id		: "startautogrid",
				name		: 'Autogrid',
				namestyle	: "width:183px;",
				style		: "position:absolute;top:320px;left:690px;",
				className	: "onoff",
				value		: (self.autogrid_running==true),
				width		: 25,
				onchange	: function(val){

					if (val) {
if (self.autogrid_running == true) {return}

					  //plugin.setVideoCaptureConfig(0, cap_width, cap_height, 0, img_width, img_height)
					  //plugin.addEventListener("autogriddone", autoGridDone, false);
					  //setTimeout(runAutoGrid, 1000)

						var cap_width = 320
						var cap_height = 200
						var img_width = 320/2
						var img_height = 200/2

						var autoGridDone = function(isdone){
							alert("auto grid done")
						}

						LED.LEDplugin.setVideoCaptureConfig(0, cap_width, cap_height, 0, img_width, img_height)
						LED.LEDplugin.addEventListener("autogriddone", autoGridDone, false);
						self.autogrid_running = true
						self.autogrid_interval = setTimeout(function(){
							LED.LEDplugin.startAutoGrid("D:\\Greencloud\\autogrid.json", 250  , .25)
						},2000)


					} else {
						try{clearInterval(self.autogrid_interval)}catch(e){}
						if (self.autogrid_running == true) {
							LED.LEDplugin.cancelAutoGrid()
							self.autogrid_running = false
						}
					}


				}
			}
		})
*/

		df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				type	: "button",
				style	: "background-color:red;color:white;position:absolute;width:110px;top:380px;left:766px;font-family:arial;font-size:20px;border:1px solid black;border-radius:8px;padding:5px;padding-left:10px;padding-right:10px;",
				value	: "Restart"
			},
			events:[
				{
					name	: "click",
					handler	: function() {
						var okay = confirm("Restart the Freak Selector?",function(result){
							if (result == true) {
								location.reload()
							}
						},"#ff0000","#ffffff",20,LED.LEDwindow)
					}
				}
			]
		})


		df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				id		: "quitbutton",
				type	: "button",
				style	: "background-color:red;color:white;position:absolute;width:110px;top:440px;left:766px;font-family:arial;font-size:20px;border:1px solid black;border-radius:8px;padding:5px;padding-left:10px;padding-right:10px;",
				value	: "Quit"
			},
			events:[
				{
					name	: "click",
					handler	: function() {
						var okay = confirm("QUIT the Freak Selector?",function(result){
							if (result == true) {
								df.ajax.send({
									url	: "nircmd/"+LED.base64encode('killprocess "chrome.exe"')
								})
							}
						},"#ff0000","#ffffff",20,LED.LEDwindow)


					}
				}
			]
		})



		df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				id		: "shutdownbutton",
				type	: "button",
				style	: "background-color:red;color:black;position:absolute;width:110px;top:500px;left:766px;font-family:arial;font-size:20px;border:1px solid black;border-radius:8px;padding:5px;padding-left:10px;padding-right:10px;",
				value	: "Shutdown"
			},
			events:[
				{
					name	: "click",
					handler	: function() {
						var okay = confirm("Shut Down?",function(result){
							if (result == true) {
								df.ajax.send({
									url	: "nircmd/"+LED.base64encode('exitwin shutdown force')
								})
							}
						},"#ff0000","#ffffff",20,LED.LEDwindow)


					}
				}
			]
		})


//==========================================================================================


// 1 = AUTO_BRIGHTNESS            percentile(*)
// 2 = AUTO_CONTRAST              percentile(*)
// 3 = AUTO_LEVELS                percentile(*)
// 4 = HISTOGRAM_EQUALIZATION
// 5 = HUE_SATURATION             hue [-1,1], 0 = unchanged                 saturation >= 0, 1 = unchanged
//
// (*) percentile is a parameter used to determine the range of RGB values.
// e.g. percentile = 0.01 means that the minimum is defined as the value exceeded by 99% of pixels. Likewise for the maximum.
// This reduces noise somewhat as the range does not depend on a single (noisy) pixel, but introduces a slight reduction in range.
// Use percentile = 0 to define the range as simply min to max.
//	void addRGBProcess(long type, float arg1, float arg2);

// clears the colorspace processing queue
//	void clearRGBProcess();

//	self.LEDplugin.addRGBProcess(4,.2,.2)
//	self.LEDplugin.addRGBProcess(3,.2,.2)
//	self.LEDplugin.addRGBProcess(2,.2,.2)
//	self.LEDplugin.addRGBProcess(1,.2,.2)


//==========================================================================================
if (LED.LEDplugin.addRGBProcess) {
		var procwin = df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "processWindow",
				style	: "position:absolute;top:5px;left:358px;z-index:5000;color:white;background-color:#7a7a7a;padding:5px;overflow:hidden;border-radius:5px;"
			}/*,
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "processes",
						style	: "clear:both;position:relative;"
					},
					children	: processes

				}
			]*/
		})

		var namestyle = "width:303px;font-size:30px;margin-bottom:3px;"
		var radio_off = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREVDM0YyQzhEMjExRTE4NzRFRkI0QUYyQjJEQUJBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREVDM0YzQzhEMjExRTE4NzRFRkI0QUYyQjJEQUJBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBERUMzRjBDOEQyMTFFMTg3NEVGQjRBRjJCMkRBQkEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBERUMzRjFDOEQyMTFFMTg3NEVGQjRBRjJCMkRBQkEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5VRjJsAAAPFUlEQVR42tRba4xd11X+9j6P+563PTN+Tx3XTtomTZsUxwaq0hY1kdug/igIED9AiH+AkGiQqARSQZXoD2hBldqqPxAS/ACUJqGCug2pUoxLjUMedeO8HNcZP+J6nvd1nnuz1t7n3rn33DszSXFnhmNt33POPfec9e211rceZ48oju6D2bSGcH1UZt8N6RYQ1W/SqRRSekiTNtxCFa5fvVc43u9Lr3wSUDPSLRXoWArasDUbiZSoNG7FtLukVXoujYMvJeHq6bixQHL7dIGCkBJ+eRIkPOpXn0MSrEIIaW7gbnJ7A5rA/rZfm3nELY7MpUrIOEnpPE1QChrJ8J/Sv56DzZG8NcCCHiuVLro0wyXPcx4ulMYe9qKRW0KLryXB8h8ZwJDr3mB9wGam3OOliQN/5xZH74hihTBKsW92EsfmZnH44DR2T46gXCzQRAqwkknV5lOYY5hZleYz973gyZfZNYPfZdiGTkMQxri5sIKLl67hfy5cxg9fvYpUFabK08ceUWH9d1oLr31axcFX3yZgDbc0+rnS+P4/TJTjhFGCd92xFw996F6ceN87MT5SgetKI5IRVgoDQPbu06eTO8+mZs4J2XcdDwvw7W0s19PnXsTfP34Gp7/7PEIUx2p77vlKsDz/sIrbp/h5AyYy4MMzd8GvTH5W+tXPBHTDPbvH8CunTuDn7juGUsFFFCcQjgvX882QjrQAjNbE2gQIa1hC9hx3tZsdy34r4GM7GehqvPs7ufGUfO/ZV/DnX/o6vnvuJVRKBeho5YmoufCJxo0f9PlwH2A4Dkb23/dZrzb7GaUU7r/7Hfj1h38W02S6YZzCK5ZRGplEoVyB43qZoFYQmQkks2MDLPteZse9+32/HbK/2W86n70bc8vnv/oEvvwPTyJJNdL2j7+zfOk/PpQEdZowxwIujOzp0B8qu4/+WXX27j9O0hSfeug4PnryPUiSGA4x9MjsQfglAiqta/UB6xUuL/gQUJ3zQ4FuMGEbTmb2ydu/n72AX/uDvyG389C+9crZldfPnDBX0AWOPzJLJurAK4/fP7L//X9L5CTe965D+MRH7kOz2YJbnSStH4X0CkQOFIw6Q/cPnfsc9p3ZR+6cGvytXue+Zh926M5vs/uZ89lFhw9MG3f49pkXUJvYsz+NmyoJG087hMFxCyNEyAlG9t17Rni18YnRMn751AMQikx4dJrAHqObSaRkIgMAVA6gGgSqVU74/DFy9+tOiBj+jN57on8SOSCxKyqy0JPvP4rnL17Bi5euozK2+2Tr1mtfUUnUdJh0/MrEr1Zn3v1bKV344Afvwb7pMSivgvG5e8xNOmB1DpjeSEN5YAxg2LV0Lu2dIGxsNcOfJdaOSTlxkoCCCI5RZHns9PeRioJDIetQsPyjf3SKk4dRnb7zUe1VJ/YTUGbjKFEYP3QPZKFCWZYaaqZrgET3XEr/JQwghTF/M+j7tf2ez559lR133KXvXPYMnddur5nnJ4GUFLTaJme4dnMJ33/uVVRqE0fi9tIXncqud763NDlHGYoWP/PeI2CTlsVRVPceBZPXMAFStXac0lOihIFqOOQ3Hs2sS4TI5OYIO5jBO8c25NA+bOhxOgSWZ+KMiPg5PIlJzrQ1adWYtBpUhKYQlFD4VHGE3VOj+MZTz0BJ39FxK3Qppj4C4YmRqotp+rLebGNq716EqTDIOHz1CyO6QrEAbIN7xgSOTEmMlwV8QvCWUwix4aExb9ZomyZ0oamxGtrMy+U4zs7WI0sva5t8Q/hYbixhbv8ufODuw3jqv14mYp74TZeSjOPs6BOjFYMgUg4EJd6cxfDxwA2FPcczXisIfOCgi7lJ+dMrF8gUajSJ/KwGAZ5fVmhGGp7DE6T7479RDk24YhkdUOpAYTXF3ccO4smzL8Ivjky5Qnq7ONGvlosEMqQwNIXUKSGgvJnBWZBiLZ5KC3ZXReDBOz2MFLeqUAKqBPrILgev3Uqx2LKgpeqJ6ylnZNrI6ZCgsZZotUMcIF82qXDqeq5TqBUYsEOOFwQRSjUqDRV9qWJrMsacddecuUqqEciP3VnYUrBdhZNAdxDoH1xPsNom0G6PMgSZubIypw7zikAShqiUCyj6LuLIc1zpepLZVdPgSsSli8JYG7MwQGHzWHtTbZj4wbuKGC1tPdjOxrIcnnJw/kpsZJWZKVulWKt0SNaEK1fCxLPh0kzFwhFupjdi5ITMOIJHhh/GPFOqz5z5psyA/KBjM44ptLXeHsA81RVfYKYm8fpCav3ZWKHomrfDIZKwqChGzM7cXx4KROSzbNJuyMAtYJmFEqb7gGZysaXw0F1lzmcouGuI7VMyXHr47prAyzcVWd2adjuu55BJx4RJEaaQtKz7AAtbaYQE1qf4FSecnikDshEo1IkdW8SMRQ/YOypM6tYJ+tu1peRnnsPZGwFTa2Zty0/iJJoExpFGbPaJiXGiA5h3OMmg79Am0DdXE6w0IwOYQQkbklGtSFR98mM6YPPezs2YLYHmgiGILZlJsRZGHfZZ0nBqTDrpdk/c3tZJu9mEdFcxPkNZSpJmRCXMtQxQcqdKr+XV27mpjFQVpXtMXG6W8HQyOUkCx8RFKSkwyvsw2/xKvUVOHmB0Mu0mqUqvtde6ZSH9Z9K6bdewMI1CNlsm4lTpnho782ECGpOGuUujM1N28zfh21hQKt/Ts4k7l187ALDK+mBhwiat4KneNFNYHybACQNmXx3exLNVCfuoTvsRmQoo3XrAeh3fkVn8CQhUQISaOqKvk+LQcRSShomlgyhBR8VuPr4ZH00HNdxr0jsBMOcN5MImPLKGWT/9cVgZsjIajuP127TsF3oDk9Za37aQpP8PN+HQGFO+UG9zGM1IdQAwm7QlLZ1n6d63DSpLNQc1rG+7hjcDve73hIoCCc5faqJa8rB33KNiQa+ZNQmYRJkPx8k6GhZrxLRVJv2TAjb0qgVa5KcXr0WID5Swh0CrLG9wXGVSy40BZyZtMqkcaam000bRtzXL+kkBd2SwdYDCC2+0CHwB+yY927Klio/NOQljkzb3ZVr5B6yrYcPUlL2ktk+1rSbNvTKWgwlWcyos8NK1FlZbHt4xXYR0yMcp6UhDyqUjnculc32VjUx6p2nYuhjJy4RFx1cXIvPFgd0Vo1lLWmoDk2ZAqR1959kqUr2jfNi0ZzNZdU9ovbYYolT0USYfjtikU2zkw5mGdU7D3a7g1gLe4NW1jRpszmaIPlkvv9nGPieBQ4QVpXqweOjOj+KuRtZIzpNWunPicNekU2vSqrfnSbtc07/RaGKafDjV0sqbz6VFl6Uzh93BmZY16UyeNAfYNL+olm8GwMoKRmq1DeJwxtL/7wAPtF9SE7JWVltkrBKdZSjDw9IwDac7rFoyWV9P9peKgSUbneZFsx1usORBr712HCAttXMaALoTUUwIJZnkOk6+2RoPQ/BDNKxNFx+G0HZCA4Bbx1wp8UIbBpwPo91qZ1PA5rp0ADDPA3MAs1/F2wEdDxqtUFO1ZIENyqNsb30zwICtlpCrlviHi40Er9yIMX3EM28Mt1XDVOAvNVI0grTbc8vjkFw95SCvQ1pZ0M0h5mbZY+dbeOBwzZrRNgLmvvSZlwPUWwnKBTFovVoapclNTTqrhwccng4LDvD0iwHOXfJx/5yPZrj1zXgWy/eA+cUU/3yuBVfq4e7F2h1CWlIrfhdopTaxSqXd4iE/eBq58virf21gqcmvOGxWprZw8Aszbhf/9ekm5hcodRR6qKwml+A2UG+WTWYpk7ARr61Y4457mxgvMf1eE9Bzw3eo9rwS4U/+aQVtIrCS10k3f/qD3yHx+MK/NfAvz7RQdIfLaIZZe9HqvmVnZaokUq5Oo2U6nLElpoBM6tAxgXYKQ2mdNwb51IUAv9tS+PSpGu7a55p2i10Kcft8u7O0kd8q+K6gKijBF7/ZwDeeDUgGsQGP8C9jyGgp4/OsI6uSxPEruz5cHJ09YqsjAqwiJN44Eme0m60MG7yO48qtBN98ro2FhkKtKA3gzjKF9DYMqt+poFe4Sv76KPnr5x6rE39EKPmiL7cYHMTNSQPF1ktriajjIlq9sSBG9t370dFDJ053SgehqZzyZ1CvHaf9dPOXWjTL3Awv+RIVYktmzKJ3e5iMW1HNUBly5Jd6rGV+gbYpsQkXldYLKLZfNfs2jHlYuXz2yy45+Lfi1tKCX52a5IRDCwdevAgRr0I5lXXNutd4iq4t01aawHJDm9U1t8OuO28RhLBu1M0RNknBZNqAH84bLPY+ZH1hQ8Wt5c87iv1VxbXS5NzPIzNrocn+VYjAnbXVylsZZnWUzt7q3Z7RWXVgmPYtyqHgoBxeQiF+k9VqQ5HroXH9wn/Xrz37F8Kr7DJvIKaO/uKiV5mq8TJEMytk2vXCUTT8I2bfqkxgZ25WNjbfYnIDY8Gz3XOsXZXG+ubzj96fBCvnHa86xWagSNO3SmP7P97FRHZUSG8ZAgjExBobab3jhl2XyWCvYyx8HnbJmmVnSb5Lmn08XLn6l9Ir8jtlxywuTZoLzwi38EChtvuONf8jAlK34Og2KIGj4WfWq7Ys9m407LJHflMYoJpexmh80SxY64BlZg4Wf3Rt+fX/PKHTWDFHCY//+qOTihH40bkTT5QmDp7SPbm0AL+bcdGSu1CX+ynCVWGzVHF72OltR2dbDXlooarmUVY3yXPDHpnYaF2EqzfmF1759tE0bLY6C8TdPMMl7eWPx63RJ73y2C/Y3rRZxWUeUFHXzc0jAhwLHkUzEVsLV8HVATzdJHurk2SRkc/K2PFGB3F78UqwOv8eUmSr96+M3EFWp4tbCx9OguWzpfFDxw3gjL3tTTUKWEVRL3ffuW49QXVjAsPvUxiTVLBy9Y1o9foRwhLlqxvH8cp9YP3aNNk+/Whp/mtxe0m4xZHj5Oyu6GHDbJXjtg70mK8BSv6qk1A1blz4erB0+QThShh81LhJtUHc/fsAuZ7hWE0v/mn96nOHGm/+8DEqMkIIe97+iYnE9izUEl1NikwOlQSq9eOXzy9f/t7JYPmNT9p2x3DZ3I2TFvYN9WawdOWX0qA+RtnY70m/+huOX56mr30pKaO2ZLB1yKliMIxLJY5OoqUkWH08ai58Iapfvygdn2T2NhTnfwUYAD6rBaOq1Xm+AAAAAElFTkSuQmCC'
		var radio_on  = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkU1RUZENjk2QzhEMjExRTE4Njg1Q0RGNkI0RUQ0ODQ0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkU1RUZENjk3QzhEMjExRTE4Njg1Q0RGNkI0RUQ0ODQ0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RTVFRkQ2OTRDOEQyMTFFMTg2ODVDREY2QjRFRDQ4NDQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RTVFRkQ2OTVDOEQyMTFFMTg2ODVDREY2QjRFRDQ4NDQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6O65tlAAAT70lEQVR42sxba4wd5Xl+vrme+9nds+v1ZX3DNiwEAtghBUdWKYIUUxJFVRpSVSVV/1SRGv60qmgqpeo9Ulu1kJZK9EejVGrVKimhCuKSJgRiagdCuBhiwMYxxl476931Xs5lrt/0fb+ZOWfOnLNrm7puzurznJkzl+95L897mc+iUJ+C+kQRhGGhvOF6aIYNb2WWDoXQNBNh0IFhV2BYlY1CN+/RzNI+QE5rRnGC9ktCCBNX5iOjMGiHfnuBJnw8kuGB0HeeCNzld/zmPM3bojlLCE2DVWqAJo+V068hcJYhhKZuYKx5exICgyawt1nV9b9lFGq/FEox7gchHScBhaARDL+U/jI7F/xEF4mYHkuoC1sEcJNp6r9sF0f+3PRq3xOReDRwFh9TgKGtev3qgJWkjC3FsS1/ZBTq93u+1FwvxNSGBqa3b8COrZNY16ihVLBJkIIkSI8R8VaofSipamqb+12w8LXknMHf4o8YKgbH9TE7v4S3js/glTdP4MfHThdCad9dmpy+W7or32vPv/tF6TsHLxFwBKNY/1RxdPPfB1Lf6HoBPrRzE+75hZuxd/fVGK2VYRiampKarCYUAC37nbZ67jibmjomtL7zeMQAL+3D83r+pSP4l/98Ac98/3W4KNxe3Xjjc87iqT+Qfucv+Xn5jxjw4fXXwSo3PqFZlccdLxAb143gs/fuxb6PTKNoG/D8AEI3YJiWGpquxQCU1kRPACI2LKFl9rvaTfa1fivg/VgY6Gq8e522tkgOvXoUf/bIN/H9l95GuWgj8pZ+12vN/3Xz7Bt9PqyTX2bgA4XRLZ/QS41/lTKybr1pJ377138R1+7YCAKPgLjJqk2gUJ+AWRmFUahAt8vQrBL0Qom2Reg0NLMAQUOzeGsrEmRhEuuRsGirm7Q11ZZ+TIaOSOgsIdrSAA8B9sjYb2MDjzJzzQpgan0Dv7L/ViXQV358ApFW+LhACOf8e8/JwO0B1ol9FcUQOZXGd36SxjdDKe3P3HMrPsM3oN98sOa3wx6ZJIAFmoSuJhBKGjwZmfmeHFfb3H7fNVH/sey+HHJO9jcFnr/n2E4na9t3yzRunN6Kbzz1A5iF2u2abtru4vvfSc/TrdoGkrYOszQ6Vdu851tETpXdH9qGT975EbRabRiVBmqbryGt2fHEZO+h2RHltsN+S7XUd0wOXhutct8UpEzZWvbup44nJ+3YMqnc4b9eOIzq2MZ9od96MXCbxwg7mbRdI0IOUJu6+RFhVm8bq5dw3723QcgQZn2SwE7TzTSEJPIBADIHUA4CjWRu8vl95O7XFYgY/ozsPdEvRDZ/SRfIMMTH9lyD1986iSPHz6A8su6W9ty7/yQDz9eZdKzy2O7K+usfCsNQ2//zN2JqcgTSLGN0+43qJinYaJhZraahPDAGMOzcjPlG2clfhMZ7z8r6uQY/CEBBBNMUWR5/5kWEwm5QyDrjLL73ol5o7EBl8tovR2bl5s0ElNnYCyRGt90IjQgppO/RGr4kU00oX4wQMIAw8VUFRmS+Z7Y5f065QOaPJc+I8trNmnleCKQkp91ROcPM7Hm8+NoxlKtjO/3O+X/UyxNXbyg2tj9EmVPx527aBTZprVBHZdM1CGjmwyaQJaWQnuIFDDSCTn5jkmQNIlsyHNqPB4eZdD8OOfQdcehRx3iomI4kRCUj0TYLMciZdiRFbNJDCI9ZPqDwKX0P68breOLZH0FqViPy2wcNiqmfotgxWqsYmKQfV1odjG/aBDcUChmzef9kRHdSURInNo4I7BrXMFoSsAjBRacQYs3dOAwRgA4JdL4VYdmNqdbgOM7OlplLd4tYqBAWFpvnsX3zBD764R149gfvEDGP3W9QkvExdvSxelkh8KQOQYk3ZzG8P3BDER9jiVdtgY9uNbC9of3flQtkClUSIj+rSYBPLUq0vAgmh2z22CQhUfNTyiGBS56jDp9cK6C8/8MUpr5z8AisQm2XITTzGk70K6UCgXQpDI0j1IuUaIQKXAwykWQiPQY7URbYf62JWkHgSn0qBHrXhI5350IstGPQmuwpRAs5I4vUPHWaqB9paHdcbCFfVqlwaNQN3a5OMGCdHM9xPBSrVBpK+lH6sckoc4665sxVUpVA3n2tfVFgo2jtOkiISxMYc8FOAv3GTIBlh0AbGWUIMnMZzznUmVcEAtdFuWSjYBnwPbNkaIZZZnaNaHAlYtBJrh8ps1BAEeex8U0jxcT7ryugXhRrAsxuh4FOj2UBi27FJNYUBs9lB4F++aSv5qolphwrJbZKSlTJnLnC8JU0DJKUT3mtEaf39CPVtY7nwSTDd32WlOwzZ74pM+COcR3T63VVaOdxqKBPg+I5PSxQWx55jcZVkjYAiPd1yvrS39NzhgEvU0o+WdVwYj6M/VlZoeiat84hkrBIz4fvd+cgjJQfPfJZNmnDZeAxYC0JJUz3DklyoS1xz3UlldL7QdStXVlbDMz3fbqHg06nQ77TpljowG276rgkxldNgSgGZpgGbMtGsVhEoVCAbdvKrYQR/2aaJvmdoUYKvl84DFjg6CwJWPa0m7qeTibtEyZJmFzSctRXD9NJ3MVwCaxF8csPpJogg2w6EivEjm1ixgIVN5vqQqVuadDnD4P1yDra7TaWV5axvLiM9lwb55rnMOPOoON3YsAZ8+6aNM3QojKzUq1gcmwSu8YoFyiPwSt5xKoWLCseeRPncGUSKKrq4MueWcflJ3ESCYFxhB6bfRALOgXMXzjJoN/QIdCzywGWWp4CHEWxNDnZqJQ1VCzWplTmnU6cNchgFxcXsTC7gLNnz+Ip/Sl8d+S7OGGeIHuQq5IQ6Q5FragmxN+3Lm7FvYv34tPFT2NychKyEl/LoNncszE6tj7iHj8mM030wihXTqzhUJl00C2rjGzrpNNqUWm6jNH1lKUEYUJUQp3LADXEGpJDtNtsNrEwt4CTMyfxcP1hHGocQkmjQmTkPlT0Sn+PKwP2nH8OT55/kjK2UBX/b9LfG/INPLfwHB6cfZDy4Wk01jUU2Kw/p6WhpHSPictIEp40k9Nowj5xUUgK9Ho+nGiYbrK00iYnd1BvhN0kVUa9grNbFtI/Kq2TsXaZnFyi/uXlZSz9dAlfK30Nh8YPwQgMfGnTl/D5DZ/vgtH5T2QGYo09evZRfOH4F5RwC/THceVg4yAeOf0IHnjzAXihh6mpqS6pZRt6bLZMxKGMtdvnwwTUJw1zlyZKTNnop3uhQk8MSuZ7enHirpi4HzATVXuxjcOdw3h+4nloroZt9jZcZV+Fby98OyYp4scUZBa0LWzcULoBm/RNeN97H2bS8eXjB8YO4I6Td6A6W0W5TNXb6Kgit5TAGLAbsElLmDKbZorYhwlwwIDZV4c38eKqhH00CvsBqwoo7AcsldnEzNxebuMV8QraYRumZ1KW4+PA/AEFlLWbsrNFOa6lWeq4EgT9sbmHTgjh9fo2bO4traXuedPyTYr5mc1ZyMzgJjE6z8EhUA4Raqj3QpIKS7TvuaRhYmnHC5Cq2Mgn78pHw0ENZ006BZzGWQW646DpNyFckm6ooxW28PLcy+iEHSwFS0oQbNoMtGbUMGlPYtQcRckoqWey0DS/PzYzg6/4K3CdOLRl47qU3BTXVXhkDbN++uOwVGSlNOz7q7dpWdrRGiYdh5bUzaNuouEHFGs5fhNgjVJTDkWHncNUdbnqniLpuvE15+jvXfqzNRt1s46yUVag+Lo+BdDMQ2Jaz/C6YNPQFpIr+bS/0uEwmpDqAGA26Zi0ojxLZ5lAJqnmoIajAZNOJ6Imw8mIJ1Q+yw8g+SqTXfW9Cf3NO/NYoD/VnsUgYLoFaS++f5rJpWVhQGp9+XgLlaKJTaPsRlHPrOm8wEt82A9W0bDoEdPFmnQ3nZQxYLhIgry4iHJYdJl6tXpZ+rL/OfRdiNRagDb56VszHvwtRWwk0DLJG3RDqtRybcCJSatMKkda7DIy6jdpJZjExFTmRPdlkxbR5SkZFbAQfc/oPjPhm7gOkDj8fpvA25hqmPEbEXIPNufA9VXa3Jdp5auYVTUcpkQV96nSHLrrWz4n3ZfwZuxiOiL+4HPUVqSRQ6pCRtKc3p6h1LZt4qrJAjSdeIWSjtClXNqLcrl0rq+ylknnSWtAw46AwGXSMAY13B0Z5aj5MmHRwdPznprclnVlpdmYtOQaJs03C+PRd5wfHA6SVmpiKl/mSOFdvg6IAhwkkSMDLvVhpYBkrlHGKGYWXBSp8CiRD3ts0iHW8uHUN3Ma7nYF+wGnEk+1sUad8ME+EfoaCXnAks1ZDdE31xM/7WBKD6ATYXlhNFg89GiRuxpJIzlPWmG/Sfe1cHgO3FMKxSW3bVbVMIEQetKKzHdRMkmSUkLWjegr1/TvN1uYJB8OIy2ebz6XFl2WThz2AmFJJueoZJ1SPY1v7ETAZWpici6glbRupZR2Vbq6SfOCMAdYNb+olm8Rgy4toVatrhGHE7O5FMCczFu2hYbegOZoccJwGT6ceTVqDXVvfkZ3bsMAD1hVqELWEuX4oeylrMPD0jANh8Orpfg1pQ6zYOLq2tUozxA70p/2v1QzBSFUREXdk++darkLWuSyv1AMLNlImxetjpupwYeFJVULDxkybQD0woOqeAwDdtHGtsY27B3dq+pjFaJSn77Ewdd65Ht7R/Zi29g21erhZ2TJKx4yyfuHz3dYt3RopoUhGo5UFx+K0NIGQO9FtK5Kt3K9jP3b96Plt3Bo4RDFxkhVRxd8n5L5BFGgyGrf+D7cvf1ulOol1ejLalgJWoOqlHihDQPOh9FutXNBwOq8cAAwT4w5gNmvbIq+nlYKmBtxnAXdp92HnTM78cO5H2LWnVVhY9W1SZn1LNwQWGevw57xPdizcQ9GG6OoEuGkRX/Wh9k0225E1VIMTMrB0kT11i+8iic2XeSqJb5woRng6Fkfk7tM9cYwm/Nyk427Eor6yfxur9yO3Rt243zzPFzfHeha5pvv/J7aNm2MVcdQqVRQLBeVAEulkrp3VrvqOgpX55shmk7Y7bnlcWhpfrCmhqO4eFAslUPMzbLHX27jth3V2IyyjEoaUL1l1rZdgFt1URujQt+bpNo1VFpeEzD3vCi0mZapAPK9eJv2pfPXGnTNC+84WGkHKNli0HojTSlNG6LhaLB4yGQWGZO2qZJ7/oiDl45buGW7hZYbIRsNeGLpBLkNk7596Mu3V6mK0jcSLLB0ZBvwvaQDIJng1EKIb7zUhqFFQ7SbLMMaRlqRDByhmQqRilUyrj1FJFdZDAb87ZNN/N3n6iiTZLmJJnKazr42GfauadjLtHSbvT4LthsCk4b7V55p4dR8oOaQb1Ykjgk9DCC6vQ5V7Ada4DYXeivWOB/uEOMFqt+rAnpuWDrVnic9/OHXl9AhAiuag9VMVlus8XTLQzXgMiM9np6TfaeUvy+/Q+Lx0FNNfOtHbRSM4XNUQ629aHcJke8nA69tRKH3E9q9QUmFJxqsUF1LoHV7KK3zh0E++6aDB9oSv3dvFddNGVSEp0shhrXcxcVGpUECZ0shfViGoCoowMNPN/HEqw7NQQzwSL6Q1rzzmVSDzw9WdKs8sbVQ33BnXB1xA85DYI4i0OvdbGXY4HUcJ+cCPP1aB/NNiWpBi5sCSWgLVxlBboRrDKrfqaCXOE3++hj56188vkL84aFoia4/Dx/ECUEThfbbvUJTN+Atn31J1KZu3lXftvcwHbSVH1Pg96z1WKneSt/DC6eAJGX246KlKX9ixiyYlyeX5lZUy5WKHPmlHmvZ1C/iJTwlO+X2YRQ6x9T3OIyZWDpx8H4iLXnUb59/3qqM38UJB693NP0FCH8ZUi+vatZZ4ykYUH6z1AIWm9HAksAP3tPqtV6LZiZHWPMi4oCwCcs9Fa/dRLyANXSbc3578Qmjs8AujK9Y1dvv6qo/8lDuvIWl0u6Y3i9y9loiAR0faDXw6h2AIVFy1dYvgSy6J8k1nYx2DbRnjn61PX9sQdftGgJ35VihtuEu3a5sVholiRjhkqoxXX08edsfreEz/98jSpYxUhHjn0HVPRKbh3pfrBE7u4uLxw/cH4X+im5WxhlgJP3OkeLI5t/orp+nC+xwThGAI8Z6bPQziDhel2mgEJzBiPs64iVrMQyNfHdl5tUvukunn+FlzbrgdcoyQNCaPyUM27Or6+7MLkouyDnoUYcqthINK1mMJod3Eq/wiJc98ptCB5XwBOr+W2rBWgqWTdlZeO8/Fn/y379D2o2Yo3TdLEEkf35z9oBeqLXM0ujHu00gutiKllAOz1Ba1iTQNsLIogdxfqt1w8CVG1qypdIzaqEWHsdI8A6K8lx3vjF3GXCXz3594d1nf5VMOmTFQoh8i0dD0Fn8K79dL5qlkT+Oe9NqFZcqt8ryDEpyFh4q8AWPAv/vCFzJD5urETkwCayFFZqZp+YXZV7ZCH6r2Fn4N2f51OdIMn62qWgMsjqd3J7/k8BZLBVHtz0Yr5aPk5L4phHpeBmFaLH7zvXKfpKcPF54qIgqqzAmKWfp9L97y2d+TfDrByEuVA+rNazoLJz6fYrPB8oTV3+Z2Pv62G9T4PHDfmY+DJQURcQ715x9+0+95uzDZnEsGvYGRPlwVrtWdZ1y9tBrc7A+6rfm/znwmnOaUdiiGdaE8gVk1vdeeXSJJpP+tyoKnLPOwolHm2eP/KbfmnuG/Ve3Sup3Ak/FkA+s1rXMmzdxYdM5f/JvQmflHygbu0OzKp+lm03Tz6OaZpQV7+OKqZtfXAcEoE1flqLAOx44y495rfmnvZUz5zTdQlzqrq6J/xFgABYmFSieOlzIAAAAAElFTkSuQmCC'
		var radio_w = 48
		var radio_h = 48

		var addProc_autobright = function(){
			LED.LEDplugin.addRGBProcess(1,.01,0);
		}
		var addProc_autocontrast = function(){
			LED.LEDplugin.addRGBProcess(2,.01,0);
		}
		var addProc_autolevels = function(){
			LED.LEDplugin.addRGBProcess(3,.01,0);
		}
		var addProc_histogram = function() {
			LED.LEDplugin.addRGBProcess(4,.01,0);
		}
		var addProc_huesat = function(){
			LED.LEDplugin.addRGBProcess(5,self.hue_value,self.sat_value);
		}

		var proc0 = df.newdom({
			tagname		:"df-onoff",
			target		: procwin,
			attributes:{
				id		: "testonoff0",
				name		: "Process Off",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.procmode==0),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){

					if (val==false) {return true}

					proc0.setState(false)
					proc1.setState(false)
					proc2.setState(false)
					proc3.setState(false)
					proc4.setState(false)
					//proc6.setState(false)

					self.procmode = 0

					LED.LEDplugin.clearRGBProcess();
					addProc_huesat()

				}
			}
		})

		df.newdom({tagname:"br",target:procwin})

		var proc1 = df.newdom({
			tagname		:"df-onoff",
			target		: procwin,
			attributes:{
				id		: "testonoff1",
				name		: "Auto Brightness",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.procmode==1),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){

					if (val==false) {return true}

					proc0.setState(false)
					proc1.setState(false)
					proc2.setState(false)
					proc3.setState(false)
					proc4.setState(false)
					//proc6.setState(false)

					self.procmode = 1

					LED.LEDplugin.clearRGBProcess();
					addProc_autobright()
					addProc_huesat()

				}
			}
		})

		df.newdom({tagname:"br",target:procwin})



		var proc2 = df.newdom({
			tagname		:"df-onoff",
			target		: procwin,
			attributes:{
				id		: "testonoff2",
				name		:  "Auto Contrast",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.procmode==2),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){

					if (val==false) {return true}

					proc0.setState(false)
					proc1.setState(false)
					proc2.setState(false)
					proc3.setState(false)
					proc4.setState(false)
					//proc6.setState(false)

					self.procmode = 2

					LED.LEDplugin.clearRGBProcess();
					addProc_autocontrast()
					addProc_huesat()

				}
			}
		})

		df.newdom({tagname:"br",target:procwin})

		var proc3 = df.newdom({
			tagname		:"df-onoff",
			target		: procwin,
			attributes:{
				id		: "testonoff3",
				name		: "Auto Levels",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.procmode==3),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){

					if (val==false) {return true}

					proc0.setState(false)
					proc1.setState(false)
					proc2.setState(false)
					proc3.setState(false)
					proc4.setState(false)
					//proc6.setState(false)

					self.procmode = 3

					LED.LEDplugin.clearRGBProcess();
					addProc_autolevels()
					addProc_huesat()

				}
			}
		})

		df.newdom({tagname:"br",target:procwin})

		var proc4 = df.newdom({
			tagname		:"df-onoff",
			target		: procwin,
			attributes:{
				id		: "testonoff4",
				name		: "Histogram EQ",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.procmode==4),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){

					if (val==false) {return true}

					proc0.setState(false)
					proc1.setState(false)
					proc2.setState(false)
					proc3.setState(false)
					proc4.setState(false)
					//proc6.setState(false)

					self.procmode = 4

					LED.LEDplugin.clearRGBProcess();
					addProc_histogram()
					addProc_huesat()

				}
			}
		})

		df.newdom({tagname:"br",target:procwin})

/*
		var proc5 = df.newdom({
			tagname		:"df-onoff",
			target		: procwin,
			attributes:{
				id		: "testonoff5",
				name		: "Hue-Saturation",
				namestyle	: namestyle,
				style		: "position:relative;",
				class		: "onoff",
				value		: (self.procmode==5),
				width		: radio_w,
				height		: radio_h,
				image_on	: radio_on,
				image_off	: radio_off,
				onchange	: function(val){

					if (val==false) {return true}

					proc0.setState(false)
					proc1.setState(false)
					proc2.setState(false)
					proc3.setState(false)
					proc4.setState(false)
					//proc5.setState(false)

					self.procmode = 5

					LED.LEDplugin.clearRGBProcess();
					LED.LEDplugin.addRGBProcess(5,self.hue_value,self.sat_value);

				}
			}
		})
*/
		var slidertypes = [
			{
				id		: "hueslider",
				name		: "Hue",
				startvalue	: self.hue_value+1,
				startvaluetext	: (self.hue_value-1).toFixed(2),
				min		: 0,
				max		: 2,
				handler		: function(val){
					self.hue_value = val-1
console.log('self.hue_value = ',self.hue_value)
					LED.LEDplugin.clearRGBProcess();

					if (self.procmode == 1) {
						addProc_autobright()
					} else if (self.procmode == 2) {
						addProc_autocontrast()
					} else if (self.procmode == 3) {
						addProc_autolevels()
					} else if (self.procmode == 4) {
						addProc_histogram()
					}
					addProc_huesat()

					return (val-1).toFixed(2)
				}
			},
			{
				id		: "saturationslider",
				name		: "Saturation",
				startvalue	: self.sat_value,
				min		: 0,
				max		: 1,
				handler		: function(val){
					self.sat_value = val

					LED.LEDplugin.clearRGBProcess();
					if (self.procmode == 1) {
						addProc_autobright()
					} else if (self.procmode == 2) {
						addProc_autocontrast()
					} else if (self.procmode == 3) {
						addProc_autolevels()
					} else if (self.procmode == 4) {
						addProc_histogram()
					}

					addProc_huesat()

					return (val).toFixed(2)
				}
			}
		]

		var sliders = LED.sliderList(slidertypes)

		df.newdom({
			tagname	: "div",
			target	: procwin,
			attributes:{
				style	: "position:relative;top:3px;left:0px;"
			},
			children	: sliders
		})
}

/*
		df.newdom({tagname:"br",target:procwin})

		df.newdom({
			tagname	: "select",
			target	: procwin,
			attributes:{
				style	: "font-family:arial;font-size:36px;"
			},
			children:[
				df.newdom({
					tagname	: "option",
					attributes:{
						value	: "testgrid",
						html	: "testgrid"
					}
				}),
				df.newdom({
					tagname	: "option",
					attributes:{
						value	: "autogrid1",
						html	: "autogrid1"
					}
				})
			],
			events	: [
				{
					name	: "change",
					handler	: function(evtobj) {
						var thismap = evtobj.target.childNodes[evtobj.target.selectedIndex].value
						LED.LEDplugin.setConfiguration(df.json.serialize(window[thismap]))

					}
				}
			]
		})
*/
//==========================================================================================

/*		var processes = [div1,div2,div3,div4,div5,div6]

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "processWindow",
				style	: "position:absolute;top:5px;left:400px;z-index:5000;color:white;background-color:#efeadc;width:400px;overflow:hidden;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "processes",
						style	: "clear:both;position:relative;"
					},
					children	: processes

				}
			]
		})
*/

//==========================================================================================

	}

}

LED.plugins.push(self);


})();
