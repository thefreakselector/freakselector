(function(){

var self = {
	name		: "Side Lights",
	icon		: "Test Patterns.png",
	top_plugin: true,

	output_plugin: true,

	brightness:	.1,

	start:	function(){

		if (self.COMplugin === undefined) {

			self.COMplugin = df.newdom({
				tagname		: "object",
				target		: LED.LEDholder,
				attributes	: {
					id		: "PimpHat_COMplugin",
					type	: "application/x-comadapter",
					style	: "position:absolute;top:-205px;left:-205px;height:100px;width:100px;z-index:50000;"
				}
			})
console.log("self.COMplugin",self.COMplugin)
		}

	},

//************************************************************************************************

	end:	function(){
		if (self.opencom){
			self.closeCom()
		}

	},

	render: function(){

		self.most_recent_frame = Array.apply(null,LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height).data)

		var red = 0;
		var green = 0;
		var blue = 0;
		for (var i=0;i<self.most_recent_frame.length;i+=4){
			red   = red + self.most_recent_frame[i]
			green = green + self.most_recent_frame[i+1]
			blue  = blue + self.most_recent_frame[i+2]
		}

		red =     Math.round(red / (self.most_recent_frame.length/4));
		green =   Math.round(green / (self.most_recent_frame.length/4));
		blue =    Math.round(blue / (self.most_recent_frame.length/4));

		self.ambilight_color = df.gethex(red)+df.gethex(green)+df.gethex(blue)

		try{
			self.ambicolor.style.backgroundColor = "#"+self.ambilight_color;
		}catch(e){}

//console.log("self.ambilight_color",self.ambilight_color, red, green, blue)

		if (self.opencom && self.clear_to_send === true) {
			self.sendFrame()
		}
	
	},

	ambilight: false,

	ambilight_color: "000000",
	color: "000000",
	last_color: undefined,
	last_mode: undefined,

	sendFrame:	function(callback){

		self.clear_to_send = false

		var send_mode = 0
		self.send_mode = send_mode

		var color = self.ambilight ? self.ambilight_color : self.color
	
		var red   = df.getdec(color.substr(0,2))
		var green = df.getdec(color.substr(2,2))
		var blue  = df.getdec(color.substr(4,2))

		var payload = [];

		payload.push(String.fromCharCode(red))
		payload.push(String.fromCharCode(green))
		payload.push(String.fromCharCode(blue))

    var size_h = payload.length >> 8 & 0x00ff;
    var size_l = payload.length & 0x00ff;

    var dataframe = window.btoa("MCX" + String.fromCharCode(send_mode) + String.fromCharCode(size_h) + String.fromCharCode(size_l) + payload.join(""))

    if (self.opencom){
			self.opencom.writeAsync(dataframe,true)
		}

		if (callback){callback(send_mode)}
	},


	sendFrame_PWM:	function(callback){

		self.clear_to_send = false

		var send_mode = 2;
		self.send_mode = send_mode

		var color = self.ambilight ? self.ambilight_color : self.color
	
		var red   = df.getdec(color.substr(0,2))
		var green = df.getdec(color.substr(2,2))
		var blue  = df.getdec(color.substr(4,2))

		var payload = [];

		payload.push(String.fromCharCode(red))
		payload.push(String.fromCharCode(green))
		payload.push(String.fromCharCode(blue))
		payload.push(String.fromCharCode(red))
		payload.push(String.fromCharCode(green))
		payload.push(String.fromCharCode(blue))
		payload.push(0)
		payload.push(0)

    var size_h = payload.length >> 8 & 0x00ff;
    var size_l = payload.length & 0x00ff;

    var dataframe = window.btoa("MCX" + String.fromCharCode(send_mode) + String.fromCharCode(size_h) + String.fromCharCode(size_l) + payload.join(""))

    if (self.opencom){
			self.opencom.writeAsync(dataframe,true)
		}

		if (callback){callback(send_mode)}
	},

	openCom: function(){

			var thiscom = self.portselect.getElementsByTagName("option")[self.portselect.selectedIndex].value
			self.selected_port_index = self.portselect.selectedIndex;
console.log(thiscom)
			var bitrate		= self.bitrate_select.getElementsByTagName("option")[self.bitrate_select.selectedIndex].value
			var bits			= self.bits_select.getElementsByTagName("option")[self.bits_select.selectedIndex].value
			var parity		= self.parity_select.getElementsByTagName("option")[self.parity_select.selectedIndex].value
			var stopbits	= self.stopbits_select.getElementsByTagName("option")[self.stopbits_select.selectedIndex].value

			var com_string = thiscom+":"+bitrate+","+bits+","+parity+","+stopbits
			self.opencom = self.COMplugin.open(com_string) //"COM6:115200,8,NOPARITY,ONESTOPBIT"

			self.opencom.setReadOptions(1,1,"M",false)

			self.opencom.addEventListener("readAsync", self.newdata , false);
			self.opencom.addEventListener("writeDone", self.writeDone , false);

			self.clear_to_send = true;

			self.openport.value = "CLOSE";

	},

	closeCom: function(){
		if (self.newdata_timeout){clearTimeout(self.newdata_timeout)}
		try{
			self.opencom.close()
		}catch(e){
			console.log("PORT DID NOT CLOSE")
		}
		self.opencom = undefined
		self.openport.value = "OPEN"
	},

	clear_to_send:false,

	newdata_timeout: undefined,

	newdata:	function(data){
		
		data = String(data);

		if (data === "M") {
			self.clear_to_send = true
			if (self.newdata_timeout){clearTimeout(self.newdata_timeout)}
			df.dompops.closepop();
			self.newdata_timeout = setTimeout(self.restartCom,2000)
			if (self.opencom  && self.send_mode === 0){
				self.sendFrame_PWM(function(){
						self.clear_to_send = true
				})
			}
		}

	},

	restartCom: function(){
		alert("LOST SIDELIGHT COM")
		self.closeCom();
		self.openCom()

		if (self.newdata_timeout){clearTimeout(self.newdata_timeout)}
		self.newdata_timeout = setTimeout(self.restartCom,2000)
	},

	writeDone:	function(evt){
//console.log("writeDone")

	},


//==============================================================================================

	recording:[],

	interface:	function(){

//self.COMplugin.openDebugConsole()
		if (!self.port_list){
			self.port_list = self.COMplugin.getPortList();
		}

		self.ports = df.json.deserialize(self.port_list)
console.log("PORTS",self.ports)
		var portOptions = []
		for (var i=0;i<self.ports.length;i++) {
			portOptions.push(df.newdom({
				tagname	: "option",
				attributes	: {
					value	: self.ports[i],
					html	: self.ports[i]
				}
			}))
		}

/*
		portOptions.unshift(df.newdom({
			tagname	: "option",
			attributes	: {
				value	: "COM12",
				html	: "COM12"
			}
		}))
*/

		self.portselect = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:20px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			events	: [
				{
					name	: "change",
					handler	: function(evtobj) {
//						self[tests[evtobj.target.selectedIndex]]()
					}
				}
			],
			children:portOptions

		})

		self.portselect.selectedIndex = self.selected_port_index || 0;

		var bitrates = [];

		var bitrate_arr = ["230400", "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200", "460800", "921600"]  //;@ 7.37mhz    : 0x0f = 460800bps, 0x07 = 921600bps  // "1200","2400","4800","9600","19200","38400","57600","115200","460800",

		for (var i=0;i<bitrate_arr.length;i++){
			bitrates.push(
				df.newdom({
					tagname:"option",
					attributes:{
						value	: bitrate_arr[i],
						html	: bitrate_arr[i]
					}
				})
			)
		}

		self.bitrate_select = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:60px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:bitrates

		})

		self.bits_select = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:100px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:[
				{
					tagname:"option",
					attributes:{
						value	: "8",
						html	: "8"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "9",
						html	: "9"
					}
				}
			]

		})


		self.parity_select = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:140px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:[
				{
					tagname:"option",
					attributes:{
						value	: "NOPARITY",
						html	: "None"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "EVENPARITY",
						html	: "Even"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "ODDPARITY",
						html	: "Odd"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "SPACEPARITY",
						html	: "Space"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "MARKPARITY",
						html	: "Mark"
					}
				}
			]

		})

		self.stopbits_select = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:180px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:[
				{
					tagname:"option",
					attributes:{
						value	: "ONESTOPBIT",
						html	: "1"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "ONE5STOPBITS",
						html	: "1,5"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "TWOSTOPBITS",
						html	: "2"
					}
				}
			]
		})


		self.openport = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:20px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: self.opencom ? "CLOSE" : "OPEN"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){

						if (self.opencom != undefined) {
							self.closeCom()
						} else {
							self.openCom()
						}

					}
				}
			]

		})

		self.testport = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:120px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "TEST"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){

						if (self.opencom != undefined) {
							self.closeCom()
						}

					}
				}
			]
		})

		self.ambicolor = df.newdom({
			tagname: "div",
			target: LED.pluginwindow,
			attributes:{
				style: "position:absolute;top:300px;left:300px;width:100px;height:30px;background-color:#000000;"
			}
		})

		self.colorpicker1_div = df.newdom({
			tagname:"div",
			target:LED.pluginwindow,
			attributes:{
				style:"position:absolute;top:20px;left:550px;"
			}
		})

		self.colorpicker1 = new df.ColorPicker({
			target: self.colorpicker1_div,
			width : 256,
			height: 50,
			startvalue:self.color,
			callback:function(new_color){
				self.color = new_color
			}
		})

		self.toggle_ambilight = df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "toggle_ambilight",
				name		: "Ambilight",
				style		: "position:absolute;top:434px;left:331px;z-index:9999",
				class		: "onoff",
				value		: self.ambilight,
				width		: 65,
				onchange	: function(val){
					self.ambilight = val
				}
			}
		})
		
		//----------------------------------------------------------------
/*
		var slidertypes = [
			{
				id:"Pimphat_rotation_slider",
				name:"Rotation",
				startvalue:self.rotation_angle,
				min:0,
				max:360,
				handler:function(val){
					self.rotation_angle = val
				}
			},
			{
				id:"Pimphat_brightness_slider",
				name:"Brightness",
				startvalue:self.brightness,
				min:0,
				max:1,
				handler:function(val){
					self.brightness = val
				}
			}
		]

		var sliders = LED.sliderList(slidertypes)

		this.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:587px;top:6px;color:white;"
				},
				children	: sliders
		})


*/

	//------------------------------------------------------------------
	//end init
	}

	//------------------------------------------------------------------


};


LED.plugins.push(self);


})();
