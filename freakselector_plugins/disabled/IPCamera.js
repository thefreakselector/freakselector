/*
uses Android IP Webcamera
http://ip-webcam.appspot.com/cheats.html

To enter cheat, open IP Webcam settings, press hardware menu key and select "Cheats". Cheats are made for advanced users and entered values often are not checked for validity, so use them on your own risk!

Options are set with set command and be reset to default value with reset command.

Examples:
set(Video,320,240)
reset(Photo)
set(Awake,true)

Commands not exposed to UI:

set(Photo,X,Y)
	Changes photo resolution to X*Y

set(Video,X,Y)
	Changes video resolution to X*Y. Useful if you have Android < 2.2 (only default resolution is shown here) or want to use larger resolution (they are blacklisted because do not work as expected on most phones).

set(HtmlPath,/sdcard/html)
	Sets a path where web server searches for a web page before returning the builtin one. To override the main page name the file "index.html". Files should contain HTTP headers and an empty line after.
	Example:

	Content-Type: text/html

	<html>Hello, world!</html>


set(DisablePhoto,true)
	Disables taking photos without autofocus.

set(DisablePhotoAF,true)
	Disables taking photos with autofocus.

set(DisableImmediatePhoto,true)
	Disables taking imeediate photos (used in javascript renderer).

set(DisableTorchCtl,true)
	Disables controlling LED flash via the web interface.

set(DisableFocusCtl,true)
	Disables triggering focus via the web interface.


Commands exposed to UI (for use with API):

set(Awake,true)
	Stay awake
set(Audio,true)
set(FFC,false)
	Use front-facing camera
set(Notification,true)
set(Portrait,false)
set(Quality,50)
set(Port,8080)
set(Login,)
set(Password,)


*/


LED.plugins.push({

	name		: "IP Camera",
	icon		: "Video Input.png",
	fullscreen	: true, 

	ipaddress	: "192.168.1.127",
	port		: "8080",

	settings_to_save	: [
		"ipaddress",
		"port"
	],
	//jpeg stream = http://192.168.1.127:8080/shot.jpg?rnd=240590
	updateSettings:	function() {
		this.ipinput.value = this.ipaddress
		this.portinput.value = this.port
		this.stop()
		this.start()	
	},

	start:	function(){
	
		if (df.browser!="chrome") {
			alert("only the Chrome web browser supports this plugin")
			return false
		}
	
		this.imagedata = {data:new Array((LED.grid_width*LED.grid_height)*4),width:LED.grid_width,height:LED.grid_height}
		
		var self = this
		
		if (this.cameravideo==undefined) {

			this.iframe = df.newdom({
				tagname	: "iframe",
				target	: LED.pluginwindow,
				attributes: {
					style	: "position:absolute;top:150px;left:50px;width:100px;height:100px;z-index:9999;",
					src	: "http://"+self.ipaddress+":"+self.port //"http://192.168.2.180:8080"
				}
			})
			
			this.cameravideo = df.newdom({
				tagname	: "img",
				//target	: LED.pluginwindow,
				target:LED.contextHolder,
				attributes	: {
					style	: "position:absolute;top:200px;left:250px;z-index:9999999;width:128px;height:96px;",
					width	: LED.grid_width,
					height	: LED.grid_height,
					controls: true
				},
				events:[
					{
							name:"loadedmetadata",
							handler:function(evtobj){
								//this.currentTime = 0; // Seek to 122 seconds
								self.cameravideo.play()
							}
					}
				]
			})


/*
			df.attachevent({
				name	: "message",
				target	: window,
				handler	: function(data) {
					self.cameravideo.src = data.data
				}
			})
*/
		}

		self.cameravideo.src = "http://"+self.ipaddress+":"+self.port+"/shot.jpg"


	},
	end:	function(){	
		df.removedom(this.iframe)
		df.removedom(this.cameravideo)
		this.cameravideo = undefined
	},

	render:	function() {
	try{
		if (this.cameravideo==undefined) {return}
		LED.composite_ctx.drawImage(this.cameravideo,0,0,LED.grid_width,LED.grid_height)

		var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

	}catch(e){
		return
	}
	
		var data = imagedata.data
/*
		for (var x=0;x<LED.grid_width/2;x++) {
			for (var y=0;y<LED.grid_height;y++) {

					var idx = LED.getCanvasIndex(x,y)
					var fidx  = LED.getCanvasIndex(LED.grid_width-x,y)
					
					var tr = data[idx]
					var tg = data[idx+1]
					var tb = data[idx+2]

					data[idx] 	= data[fidx]
					data[idx+1] = data[fidx+1]
					data[idx+2] = data[fidx+2]

					data[fidx]		= tr
					data[fidx+1]	= tg
					data[fidx+2]	= tb
		
			}
		}

		for (var y=0;y<LED.grid_height;y++) {
			for (var x=0;x<LED.grid_width/2;x++) {

					var idx = LED.getCanvasIndex(x,y)
					var fidx  = LED.getCanvasIndex(x,LED.grid_height-y)
					
					var tr = data[idx]
					var tg = data[idx+1]
					var tb = data[idx+2]

					data[idx] 	= data[fidx]
					data[idx+1] = data[fidx+1]
					data[idx+2] = data[fidx+2]

					data[fidx]		= tr
					data[fidx+1]	= tg
					data[fidx+2]	= tb
		
			}
		}

*/

		LED.composite_ctx.putImageData(imagedata,0,0)

	},

	delay_frame:[],


	//=============================================
	//Video and video plugins need these functions
	play:	function(idx) {
		this.ipaddress = this.ipinput.value
		this.port = this.portinput.value
		this.stop()
		this.start()
	},
	stop:	function() {
		df.removedom(this.iframe)
		df.removedom(this.cameravideo)
		this.cameravideo = undefined
	},
	//=============================================

	
	
	interface:	function(){

		var self = this
		
		this.ipinput = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				type	: "text",
				style	: "position:absolute;top:10px;left:10px;width:315px;height:50px;font-size:40px;",
				value	: self.ipaddress
			},
			events:[
				{
					name	: ["change","keyup","keypress"],
					handler	: function(evtobj) {
						self.ipaddress = evtobj.target.value
					}
				}
			]
		})

		this.portinput = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				type	: "text",
				style	: "position:absolute;top:10px;left:336px;width:130px;height:50px;font-size:40px;",
				value	: self.port
			},
			events:[
				{
					name	: ["change","keyup","keypress"],
					handler	: function(evtobj) {
						self.port = evtobj.target.value
					}
				}
			]
		})
		
		
		this.message = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes:{
				style:"position:absolute;top:200px;left:30px;font-family:arial;font-size:20px;color:white;padding:20px;border:2px solid black;border-radius:10px;background-color:#2a3a4a;",
				html:'uses Android IP Webcamera<br><br><a href="http://ip-webcam.appspot.com/cheats.html" style="color:#8aaaff!important;">http://ip-webcam.appspot.com/</a>'
			}
		
		})

	}
	

})






















































