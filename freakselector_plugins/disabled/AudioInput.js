/*
    ///////////////////////////////////////////////////////////////////////////////
	// Audio input
	//
	// Returns number of available audio inputs
	long enumerateAudioInputs();
	// Returns name of audio input.
	std::string audioInputName(long device);
	// Initializes device for use.
	bool initializeAudioInput(long device);
	// Sets autoscale. A running average of the peak is kept, and the output is scaled
	// such that the average peak is mapped to targetMax.
	// The maximim amplication applied to the signal is maxAmplification. 1.0 for this param disables the feature.
	// alphaRise and alphaFall are the coefficients for the running average. Range is [0,1].
	// Smaller alpha values mean that the running average responds slower.
	// If alphaRise > alphaFall, the running average responds quicker if the volume increases than if it falls, and vice versa.
	// Reasonable initial settings may be (0.1, 0.005, 100, 0.5) or (0.01, 0.01, 100, 0.2)
	void setAudioInputAutoScale(float alphaRise, float alphaFall, float maxAmplification, float targetMax);
	// Starts audio capture, e.g. sampleRate 22050, buffersPerSecond 30
	bool startAudioInput(long sampleRate, long buffersPerSecond);
	// Stops audio capture.
	void stopAudioInput();
	// Returns latest audio data, or empty string if no new data is available.
	std::string getAudioData();
	// Fires onnewaudiodata event
	FB_JSAPI_EVENT(newaudiodata, 0, ());

*/
(function(){

var self = {
	name		: "Audio Input",
	has_audio	: true,	//it doesn't really have audio, but it should cause other audio sources to stop if this is solo'd
	has_eqData	: true,
	icon		: "Audio Input.png",

	start:	function(){


		self.music = {
			juice:{
				frameCount		: 0,
				volume			: 0,
				freqBands		: [],
				freqBands16		: [],
				avgFreqBands	: [],
				relFreqBands	: [],
				longAvgFreqBands: [],
				avgRelFreqBands	: [],
				waveData 		: [],
				eqData 			: [],
				freqData 		: []
			},
			eqData:[],
			peakData:{left:0.0,right:0.0},
			waveformData:{left:[],right:[]},
			waveDataL:[],
			waveDataR:[]
		}

		var numFreqBands = 3;
		for (var i=0;i<numFreqBands;i++) {
			self.music.juice.avgFreqBands[i] = 0;
			self.music.juice.longAvgFreqBands[i] = 0;
		}

		self.openAudioPort()

	},


	end:	function(){

 		LED.LEDplugin.stopAudioInput();

	},

	//=============================================
	//audio and video plugins need these functions
	play:	function(idx) {

	},
	pause:	function() {

	},
	next:	function() {

	},
	last:	function() {

	},
	mute:	function(){

	},
	unmute:	function() {

	},
	volume:	function(vol){

	},
	//=============================================


	openAudioPort:	function(newport) {
		var self = this


		if (self.eventHookup != true) {
	  		LED.LEDplugin.addEventListener("newaudiodata", self.newbuffer, false);
	  		self.eventHookup = true
	  	}

		if (self.current_audio_device != undefined && newport != undefined && newport != self.current_audio_device) {
			LED.LEDplugin.stopAudioInput()
			self.current_audio_device = newport
		}

		if (self.current_audio_device == undefined) {
			self.current_audio_device = 0
		}

		setTimeout(function(){
			LED.LEDplugin.initializeAudioInput(self.current_audio_device);
			LED.LEDplugin.startAudioInput(22050/2, 30);

		},20)


	},

	newbuffer:	function(data) {
		try{
			var data = LED.LEDplugin.getAudioData()
		}catch(e){
console.log("error getting audio data")
			return
		}

		if (data=="") {return}

		var newmusic = JSON.parse(data)

		self.music.eqData		= newmusic.eqData
		self.music.peakData		= newmusic.peakData
		self.music.waveformData	= newmusic.waveformData

		self.analyzeSound()

	},

	//--------------------------------------

	alpha_rise		: .17,
	alpha_fall		: .23,
	max_amplitude		: 100,
	target_max_amplitude	: 1,

	settings_to_save:	[
		"alpha_rise",
		"alpha_fall",
		"max_amplitude",
		"target_max_amplitude"
	],

	updateSettings:	function() {
		LED.LEDplugin.setAudioInputAutoScale(self.alpha_rise,self.alpha_fall,self.max_amplitude,self.target_max_amplitude)
	},


	carHorn:	function(){
			soundManager.play(LED.registry["Sound Effects"].music.sID)
	},


	//--------------------------------------

	interface:	function(){

		var self = this

  		var inputs = LED.LEDplugin.enumerateAudioInputs();

		var select = df.newdom({
			tagname:"select",
			target:LED.pluginwindow,
			attributes:{
				style:"font-size:30px;width:500px;height:45px;position:absolute;top:10px;left:10px;"
			},
			events:[
				{
					name	: "change",
					handler	: function(evtobj) {
 						LED.LEDplugin.stopAudioInput();
						var new_audio_device = evtobj.target.selectedIndex
						self.openAudioPort(new_audio_device)
					}

				}
			]
		})

  		for (var i=0;i<inputs;i++) {
			var thisoption = df.newdom({
				tagname	: "option",
				target	: select,
				attributes:{
					html	: LED.LEDplugin.audioInputName(i),
					value	: LED.LEDplugin.audioInputName(i)
				}
			})

  			if (i == self.current_audio_device) {thisoption.setAttribute("selected",true)}

		}

//==========================================================================================
		var slidertypes = [
			{
				id:"alpharise",
				name:"Alpha Rise",
				startvalue:self.alpha_rise,
				min:0,
				max:1,
				handler:function(val){
					//val = val / 10
					if (val<.001) {val = .001}
					var newval = val
					self.alpha_rise = newval
					self.updateSettings()
				}
			},
			{
				id:"alphafall",
				name:"Alpha Fall",
				startvalue:self.alpha_fall,
				min:0,
				max:1,
				handler:function(val){
					//val = val / 10
					if (val<.001) {val = .001}
					var newval = val
					self.alpha_fall = newval
					self.updateSettings()
				}
			},
			{
				id:"maxapli",
				name:"Max Amplitude",
				startvalue:self.max_amplitude,
				min:0,
				max:100,
				handler:function(val){
					if (val<1) {val=1}
					var newval = val
					self.max_amplitude = newval
					self.updateSettings()
				}
			},
			{
				id:"targetmax",
				name:"Target Max Amplitude",
				startvalue:self.target_max_amplitude,
				min:0,
				max:1,
				handler:function(val){
					if (val<1) {val=1}
					var newval = val
					self.target_max_amplitude = newval
					self.updateSettings()
				}
			}
		]

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		var sliders = LED.sliderList(slidertypes)


		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:530px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:460px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})


		self.updateSettings()

//==========================================================================================

		self.testInterface()

	},

	testInterface:	function () {
//----------------------------------------
//test interface
		var testwidth = 256

		var self = this


		var testholder = df.newdom({
			tagname	:"div",
			target	: LED.pluginwindow,
			attributes:{
				id		: "testdiv",
				style	: "overflow:hidden;position:absolute;top:100px;left:232px;width:"+testwidth+"px;height:100px;border:1px solid white;background-color:#caeaff;z-index:60000",
				html	: ""
			}
		})

		self.testcanvas_ctx = df.newdom({
			tagname	: "canvas",
			target	: testholder,
			attributes:{
				style	: "width:"+testwidth+"px;height:100px;",
				width	: 256,
				height	: 100
			}
		}).getContext("2d")

		df.newdom({
			tagname		:"df-onoff",
			target		: df.dg("pluginwindow"),
			attributes:{
				id			: "testonoff1",
				name		: "Test On/Off",
				style		: "position:absolute;top:100px;left:10px;",
				class		: "onoff",
				value		: self.testrender_enabled,
				width		: 55,
				onchange	: function(val){
					self.testrender_enabled = val
				}
			}
		})
	},

	testrender_enabled:false,
	testRender:	function() {
		var self = this

		var juice = self.music.juice
		var testreverse = false

		self.testcanvas_ctx.fillStyle = "#00ff00"
		self.testcanvas_ctx.fillRect(0,0,256,100)
		self.testcanvas_ctx.fillStyle = "#000000"

		for (var y = 0;y<256;y++) {
			var newval = Math.round(juice.eqData[y]*100)
			newval = Math.min(newval,100)
			self.testcanvas_ctx.fillRect(y,100-newval,1,newval-100)
		}
	},

//----------------------------------------

	analyzeSound:	function() {


		var juice = self.music.juice;

		var numFreqBands = 3;
		var freqBandInterval = 256 / numFreqBands;

		var eqData		= juice.eqData		= self.music.eqData;
		var peakData	= juice.peakData	= self.music.peakData;
		var waveData	= juice.waveData	= self.music.waveformData;

		if (waveData.length == 512) {
			juice.waveDataL = waveData.slice(0, 256);
			juice.waveDataR = waveData.slice(256);
			self.music.waveDataL = juice.waveDataL
			self.music.waveDataR = juice.waveDataR
		} else {
			juice.waveDataL = juice.waveDataR = juice.waveData;
		}

		if (eqData.length == 512) {
			juice.eqDataL = eqData.slice(0, 256);
			juice.eqDataR = eqData.slice(256);
		} else {
			juice.eqDataL = juice.eqDataR = juice.eqData;
		}

		var freqBands			= juice.freqBands;
		var freqBands16			= juice.freqBands16;
		var avgFreqBands		= juice.avgFreqBands;
		var longAvgFreqBands	= juice.longAvgFreqBands;
		var relFreqBands		= juice.relFreqBands;
		var avgRelFreqBands		= juice.avgRelFreqBands;

		for (var i=0;i<numFreqBands;i++) {
			freqBands[i] = 0;
		}

		for (var i=0;i<128;i++) {
			freqBands[(i/freqBandInterval/2)>>0] += eqData[i];
		}

		for (var i=0;i<16;i++)
			freqBands16[i] = 0;
		for (var i=0;i<128;i++) {
			freqBands16[(i/8)>>0] += eqData[i];
		}

		juice.frameCount++;

		for (var i=0;i<numFreqBands;i++) {

			if (freqBands[i] > avgFreqBands[i])
				avgFreqBands[i] = avgFreqBands[i]*0.2 + freqBands[i]*0.8;
			else
				avgFreqBands[i] = avgFreqBands[i]*0.5 + freqBands[i]*0.5;

			if (juice.frameCount < 50)
				longAvgFreqBands[i] = longAvgFreqBands[i]*0.900 + freqBands[i]*0.100;
			else
				longAvgFreqBands[i] = longAvgFreqBands[i]*0.992 + freqBands[i]*0.008;

			if (abs(longAvgFreqBands[i]) < 0.001) {
				relFreqBands[i] = 1.0;
			} else {
				relFreqBands[i]  = freqBands[i] / longAvgFreqBands[i];
			}

			if (abs(longAvgFreqBands[i]) < 0.001) {
				avgRelFreqBands[i] = 1.0;
			} else {
				avgRelFreqBands[i]  = avgFreqBands[i] / longAvgFreqBands[i];
			}
		}

		juice.position = juice.frameCount/100 //self.position;

		if (self.testrender_enabled == true && self.has_interface == true) {
			self.testRender()
		}


	}
}


LED.plugins.push(self)

})();

















