(function(){

var self = {
	name		: "Analog Input",
//	icon		: "Test Patterns.png",
//	autostart	: true,

	capture_analog:true,


	start:	function(){
		var self = LED.registry["Analog Input"]
		self.capture_analog = true
		if (!self.music) {
		//	self.initHorn()
		}
	},

	render:	function(){
		var self = LED.registry["Analog Input"]


		if (LED.viper_board_active == true && self.capture_analog == true) {
			self.Analog1 = 1 - LED.LEDplugin.readAD(0);
			self.Analog2 = 1 - LED.LEDplugin.readAD(1);
			self.Analog3 = 1 - LED.LEDplugin.readAD(2);
			self.Analog4 = 1 - LED.LEDplugin.readAD(3);
/*
			if (self.Analog2!=-2){
				if (Math.abs(LED.last_analog2_value - self.Analog2) > .05) {
					LED.brightness_follow_screen = false
				}
				LED.last_analog2_value = self.Analog2

				if (LED.brightness_follow_screen != true) {
					LED.bright_dimmer = self.Analog2
					LED.LEDplugin.ledAttenuation = self.Analog2
				}
			}
			if (self.Analog1!=-1){
				if (Math.abs(LED.last_analog1_value - self.Analog1) > .05) {
					LED.volume_follow_screen = false
				}
				LED.last_analog1_value = self.Analog1

				if (LED.volume_follow_screen != true) {
					LED.master_volume = self.Analog1
				}
			}
*/
		}

		if (self.music) {
			if (self.Analog4 > .5) {
				//self.music.play(self.music.sID)
				//self.next();
			} else {
				//self.music.stop(self.music.sID)
			}
		}

		if (self.has_interface == true) {
			self.div1.innerHTML = (self.Analog1||-1).toFixed(2)
			self.div2.innerHTML = (self.Analog2||-1).toFixed(2)
			self.div3.innerHTML = (self.Analog3||-1).toFixed(2)
			self.div4.innerHTML = (self.Analog4||-1).toFixed(2)

			self.val1.style.top = 255-(self.Analog1*255)
			self.val2.style.top = 255-(self.Analog2*255)
			self.val3.style.top = 255-(self.Analog3*255)
			self.val4.style.top = 255-(self.Analog4*255)
		}

	},

//************************************************************************************************
	hornUrl: "http://greencloud-pc/freakselector_plugins/media/carhorn.mp3",

	initHorn:	function(){

		var self = LED.registry["Analog Input"]

		var item = self.hornUrl

		if (self.music) {
			self.music.whileplaying = null;
			self.music.unload()
			soundManager.destroySound(self.music.sID)
			delete LED.registry["Analog Input"].music
		}

		self.music = soundManager.createSound({
			id				: escape(item),
			url				: item,
			autoLoad		: true,
			stream			: true,
			autoPlay		: false,
			useEQData		: true,
			useWaveformData	: true,
			usePeakData		: true,
			onfinish:	function(){

			},
			onplay			: function(){//onload
try{
				self.current_sound = escape(item)

				LED.registry["Analog Input"].music = this

				self.play_state = "playing"

}catch(e){
console.warn("music start error")
console.error(e)
self.next()
}

			}
		});

	},

	end:	function(){
		try{clearInterval(this.scanInterval)}catch(e){}
		self.capture_analog = false

	},

	interface:	function(){

//LED.COMplugin.openDebugConsole()

		var self = this


		self.div1 = df.newdom({
			target	: LED.pluginwindow,
			attributes:{
				style:	"position:absolute;top:10px;left:10px;height:25px;border:1px solid #cacaca;border-radius:5px;font-family:arial;font-size:20px;padding:4px;margin:4px;"
			}
		})
		self.div2 = df.newdom({
			target	: LED.pluginwindow,
			attributes:{
				style:	"position:absolute;top:10px;left:70px;height:25px;border:1px solid #cacaca;border-radius:5px;font-family:arial;font-size:20px;padding:4px;margin:4px;"
			}
		})
		self.div3 = df.newdom({
			target	: LED.pluginwindow,
			attributes:{
				style:	"position:absolute;top:10px;left:130px;height:25px;border:1px solid #cacaca;border-radius:5px;font-family:arial;font-size:20px;padding:4px;margin:4px;"
			}
		})
		self.div4 = df.newdom({
			target	: LED.pluginwindow,
			attributes:{
				style:	"position:absolute;top:10px;left:190px;height:25px;border:1px solid #cacaca;border-radius:5px;font-family:arial;font-size:20px;padding:4px;margin:4px;"
			}
		})

		//=======================================================

		self.val1 = df.newdom({
			tagname	: "div",
			attributes:{
				style	: "position:absolute;top:250px;left:0px;width:40px;height:255px;background-color:white;"
			}
		})

		self.guage1 = df.newdom({
			target	: LED.pluginwindow,
			tagname	: "div",
			attributes:{
				style:	"position:absolute;top:70px;left:20px;height:255px;width:40px;border:1px solid #2a2a3a;border-radius:5px;overflow:hidden;background-color:#000000;"
			},
			children:[
				self.val1
			]
		})


		self.val2 = df.newdom({
			tagname	: "div",
			attributes:{
				style	: "position:absolute;top:250px;left:0px;width:40px;height:255px;background-color:white;"
			}
		})

		self.guage2 = df.newdom({
			target	: LED.pluginwindow,
			tagname	: "div",
			attributes:{
				style:	"position:absolute;top:70px;left:80px;height:255px;width:40px;border:1px solid #2a2a3a;border-radius:5px;overflow:hidden;background-color:#000000;"
			},
			children:[
				self.val2
			]
		})


		self.val3 = df.newdom({
			tagname	: "div",
			attributes:{
				style	: "position:absolute;top:250px;left:0px;width:40px;height:255px;background-color:white;"
			}
		})

		self.guage3 = df.newdom({
			target	: LED.pluginwindow,
			tagname	: "div",
			attributes:{
				style:	"position:absolute;top:70px;left:140px;height:255px;width:40px;border:1px solid #2a2a3a;border-radius:5px;overflow:hidden;background-color:#000000;"
			},
			children:[
				self.val3
			]
		})


		self.val4 = df.newdom({
			tagname	: "div",
			attributes:{
				style	: "position:absolute;top:250px;left:0px;width:40px;height:255px;background-color:white;"
			}
		})

		self.guage4 = df.newdom({
			target	: LED.pluginwindow,
			tagname	: "div",
			attributes:{
				style:	"position:absolute;top:70px;left:200px;height:255px;width:40px;border:1px solid #2a2a3a;border-radius:5px;overflow:hidden;background-color:#000000;"
			},
			children:[
				self.val4
			]
		})

/*
		try{clearInterval(this.scanInterval)}catch(e){}
		self.scanInterval = setInterval(function(){

//			self.ad1 = LED.LEDplugin.readAD(0);
//			self.ad2 = LED.LEDplugin.readAD(1);
//			self.ad3 = LED.LEDplugin.readAD(2);
//			self.ad4 = LED.LEDplugin.readAD(3);

		},50)
*/



		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "analogonoff",
				name		: 'Analog Inputs',
				namestyle	: "width:183px;",
				style		: "position:absolute;top:160px;left:690px;",
				className	: "onoff",
				value		: (LED.viper_board_active == true && self.capture_analog==true),
				width		: 25,
				onchange	: function(val){
					self.capture_analog = val
				}
			}
		})


	}

}

LED.plugins.push(self)

})();
