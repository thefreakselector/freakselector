LED.plugins.push({

	name		: "Demo Plugin",
	//icon		: "Settings.png",

	start:	function(){


	}
	,
	end:	function(){

	},

	interface:	function(){

		var gadgets = []



		gadgets.push(df.newdom({
			tagname		:"df-onoff",
			target		: df.dg("pluginwindow"),
			attributes:{
				id			: "testonoff1",
				name		: "test on off",
				style		: "position:relative;top:10px;margin-left:10px;",
				class		: "onoff",
				value		: true,
				size		: 55,
				onchange	: function(val){
console.log("changed:",val)
					LED.showInputImage = val
					LED.LEDplugin.showInputImage = LED.showInputImage
					
							//this.LEDplugin.normalizeInput = this.normalizeInput

				}
			}
		}))

		gadgets.push(df.newdom({
			tagname		:"df-onoff",
			target		: df.dg("pluginwindow"),
			attributes:{
				id			: "testonoff2",
				name		: "test on off",
				style		: "position:relative;top:10px;margin-left:10px;",
				class		: "onoff",
				value		: true,
				size		: 55,
				onchange	: function(val){
console.log("changed:",val)
					LED.normalizeInput = val
					LED.LEDplugin.normalizeInput = LED.normalizeInput

							//this.LEDplugin.normalizeInput = this.normalizeInput

				}
			}
		}))

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:100%;margin-left:-710px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:760px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: gadgets

				}
			]
		})


	}

})