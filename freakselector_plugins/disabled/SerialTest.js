LED.plugins.push({
	name		: "Serial Test",
//	icon		: "Test Patterns.png",


	start:	function(){
		


	},

//************************************************************************************************

	end:	function(){

	},

	interface:	function(){
	
//LED.COMplugin.openDebugConsole()
		
		var self = this

		var ports = df.json.deserialize(LED.COMplugin.getPortList())
		var portOptions = []
		for (var i=0;i<ports.length;i++) {
			portOptions.push(df.newdom({
				tagname	: "option",
				attributes	: {
					value	: ports[i],
					html	: ports[i]
				}
			}))
		}
		
		var portselect = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:20px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			events	: [
				{
					name	: "change",
					handler	: function(evtobj) {
//						self[tests[evtobj.target.selectedIndex]]()
					}
				}
			],
			children:portOptions
		
		})

		var bitrates = []
		var bitrate_arr = ["1200","2400","4800","9600","19200","38400","57600","115200"]
		for (var i=0;i<bitrate_arr.length;i++){
			bitrates.push(
				df.newdom({
					tagname:"option",
					attributes:{
						value	: bitrate_arr[i],
						html	: bitrate_arr[i]
					}
				})
			)
		}
		var bitrate_select = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:60px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:bitrates
		
		})

		var bits_select = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:100px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:[
				{
					tagname:"option",
					attributes:{
						value	: "8",
						html	: "8"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "9",
						html	: "9"
					}
				}
			]
		
		})


		var parity_select = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:140px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:[
				{
					tagname:"option",
					attributes:{
						value	: "NOPARITY",
						html	: "None"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "EVENPARITY",
						html	: "Even"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "ODDPARITY",
						html	: "Odd"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "SPACEPARITY",
						html	: "Space"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "MARKPARITY",
						html	: "Mark"
					}
				}
			]
		
		})

		var stopbits_select = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:180px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:[
				{
					tagname:"option",
					attributes:{
						value	: "ONESTOPBIT",
						html	: "1"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "ONE5STOPBITS",
						html	: "1,5"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: "TWOSTOPBITS",
						html	: "2"
					}
				}
			]
		})

		
		var openport = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:20px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "OPEN"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){
						var thiscom = portselect.getElementsByTagName("option")[portselect.selectedIndex].value
						
						var bitrate	= bitrate_select.getElementsByTagName("option")[bitrate_select.selectedIndex].value
						var bits	= bits_select.getElementsByTagName("option")[bits_select.selectedIndex].value
						var parity	= parity_select.getElementsByTagName("option")[parity_select.selectedIndex].value
						var stopbits	= stopbits_select.getElementsByTagName("option")[stopbits_select.selectedIndex].value
console.log(thiscom)
						var com_string = thiscom+":"+bitrate+","+bits+","+parity+","+stopbits
console.log(com_string)
						var port = LED.COMplugin.open(com_string) //"COM6:115200,8,NOPARITY,ONESTOPBIT"
console.log("port",port)

//						port.setReadOptions(long timeoutMs,long maxSize,const std::string& delimiters,bool base64);
						port.setReadOptions(100,1024,"\r",false);

						//port.addEventListener("readDone", function(context,data){
						port.addEventListener("readAsync", function(data){
							console.log(data.replace(/[\r\n\f]/g,""))						
							//port.read("asyncContext", false, 500, 1024, "\r")
						}, false);
						/*
						port.addEventListener("writeDone", function(data){
						
						}, false);
						
						port.read("asyncContext", false, 500, 1024, "\r")
						*/


					}
				}
			]	
		
		})
		

		
	}

})