
LED.plugins.push({

	name		: "Fluid",
	//icon		: "Video Input.png",
	//fullscreen	: true,
	
	uses_camera	: true,
	
	uses_position:true,
	
	render:	function(){
		
		var self = this
		
		if (LED.motion_data && LED.motion_data.length>0) {
			
			self.motioninput(LED.motion_data)
			
//			for (var i=0;i<LED.motion_data.length;i++){
//				this.waterModel.touchWater(LED.motion_data[i].x+offx, LED.motion_data[i].y+offy, 1.5,  self.pixel);	//Math.min(LED.motion_data[i].amplitude,1)
//			}

		}


			self.update()

			LED.composite_ctx.drawImage(self.canvas, 0, 0,LED.grid_width,LED.grid_height)

	},

	
	start:	function(){
	},
	
	end:	function() {
		try{clearTimeout(LED.registry.Water.frame_tmr)}catch(e){}
	},
	
	interface:	function() {
	
		var self = this
/*
		if (df.dg("waterHolder")==null) {
			df.newdom({
				tagname:"div",
				target:LED.pluginwindow,
				attributes:{
					id	: "waterHolder",
					style	: "position:absolute;top:20px;left:20px;"
				}
			})
		}
*/

//==========================================================================================
		var self = this
		
		var sliders=[]

		var slidertypes = [
			{
				id:"particle_size",
				name:"Particle Size",
				startvalue:self.particle_size-.02,
				min:0,
				max:10,
				handler:function(val){
					self.particle_size = val+.02
					return self.particle_size.toFized(2)
				}
			},
			{
				id:"particle_velocity",
				name:"Particle Velocity",
				startvalue:self.particle_velocity,
				min:0,
				max:2,
				handler:function(val){
					self.particle_velocity = val
				}
			},
			{
				id:"max_particles",
				name:"Max Particles",
				startvalue:self.max_particles-1,
				min:0,
				max:499,
				handler:function(val){
					self.max_particles = Math.round(val+1)
					return self.max_particles
				}
			},
			{
				id:"RANGE",
				name:"Range",
				startvalue:self.RANGE-.1,
				min:0,
				max:32,
				handler:function(val){
					self.RANGE = val+.1
					return self.RANGE.toFixed(3)
				}
			},
			{
				id:"PRESSURE",
				name:"Pressure",
				startvalue:self.PRESSURE-.00001,
				min:0,
				max:1,
				handler:function(val){
					self.PRESSURE = (val+.00001)
					return self.PRESSURE.toFixed(3)
				}
			},
			{
				id:"PRESSURE_NEAR",
				name:"Pressure Near",
				startvalue:self.PRESSURE_NEAR-.001,
				min:0,
				max:1,
				handler:function(val){
					self.PRESSURE_NEAR = val
					return self.PRESSURE_NEAR.toFixed(3)
				}
			},
			{
				id:"DENSITY",
				name:"Density",
				startvalue:self.DENSITY-1,
				min:0,
				max:20,
				handler:function(val){
					self.DENSITY = val
					return self.DENSITY.toFixed(3)
				}
			},
			{
				id:"VISCOSITY",
				name:"Viscosity",
				startvalue:self.VISCOSITY,
				min:0,
				max:1,
				handler:function(val){
					self.VISCOSITY = val
					return self.VISCOSITY.toFixed(3)
				}
			},
			{
				id:"gravity",
				name:"Gravity",
				startvalue:self.gravity,
				min:0,
				max:1,
				handler:function(val){
					self.gravity = val
					return self.gravity.toFixed(3)
				}
			},
			{
				id:"maxage",
				name:"Max Particle Age",
				startvalue:self.maxage,
				min:0,
				max:2000,
				handler:function(val){
					self.maxage = val
					return self.maxage.toFixed(1)
				}
			},
			{
				id:"colorfade",
				name:"Color Fade",
				startvalue:self.colorfade,
				min:0,
				max:10,
				handler:function(val){
					self.colorfade = Math.round(val)
					return self.colorfade
				}
			}
		]

//		waterCanvas.setLightRefraction(self.lightrefract);
//		waterCanvas.setLightReflection(self.waterreflect);
//		waterModel.setDamping(self.waterdamping);
//		waterModel.resetSizeAndResolution(LED.grid_width, LED.grid_height, self.waterresolution);

		//------------------------------------------------------------------------------------------------------
		
		var thisvalue
		
		var sliders = LED.sliderList(slidertypes)


		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:355px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})
		
//==========================================================================================
		self.initialize()

	},
	
	
	initialize:	function() {

		var self = this

		self.canvas = df.newdom({
			tagname	: "canvas",
			target	: LED.pluginwindow,
			attributes:{
				id:"fluidcanvas",
				width	: 44,
				height	: 22,
				style	: "position:absolute;top:10px;left:10px;border:1px solid black;"
			}
		})
		
		//==================================================================================================
		// Flow
		// Ported from flash project - http://wonderfl.net/c/qszD
		// Click to add liquid particles!

		function Flow(){

			var canvas;
			var context;

			var width;
			var height;

			var mouseDown = false;
			var mouse = {x: 11, y: 2};
			var interval;

			var GRAVITY = 0.05;
			var RANGE = 11;
			var RANGE2 = RANGE * RANGE;
			var DENSITY = 2.5;
			var PRESSURE = .1;
			var PRESSURE_NEAR = .1;
			var VISCOSITY = 0.1;
			var NUM_GRIDS = 11; // Width/range
			var INV_GRID_SIZE = 1 / (22/ NUM_GRIDS);

			var particles; //Vector.<Particle>

			var numParticles;
			var neighbors; //Vector.<Neighbor>

			var numNeighbors;

			var count;
			var press;
			var grids; //Vector.<Vector.<Grid>>;

			//----------------------------------------------------------

			this.initialize = function(){
				canvas  = self.canvas
				context = canvas.getContext('2d');

				width = 44;//window.innerWidth
				height = 22;//window.innerHeight

				canvas.width = width;
				canvas.height = height;

				particles = new Array();
				numParticles = 0;

				neighbors = new Array();
				numNeighbors = 0;

				grids = new Array();

				var i, j;

				for (i = 0; i < NUM_GRIDS; i++){
					grids[i] = new Array(NUM_GRIDS);
					for (j = 0; j < NUM_GRIDS; j++){
						grids[i][j] = new Grid();
					}	
				}

				count = 0;

				canvas.addEventListener('mousemove', MouseMove, false);
				canvas.addEventListener('mousedown', MouseDown, false);
				canvas.addEventListener('mouseup', MouseUp, false);
				canvas.addEventListener('mouseout', MouseOut, false);

			//	var interval = setInterval(Update, 35);

			}

			//----------------------------------------------------------

			//allow the mouse to be stopped via function (for demos)
			this.stopMouse = function() {
				mouseDown = false; 
			}

			//----------------------------------------------------------

			var Update = self.update = function(){

				ClearFrame();

				if (mouseDown){
					pour();
				}

				move();

			}

			//----------------------------------------------------------

			var motioninput = self.motioninput = function(data){

			
				var i;
				for (i = -4; i <= 4; i++){


					if (data.length>0) {
						thisdata = data.shift()
					} else {
						break
					}
					
					var mousex = Math.round(Math.min(44,Math.max(thisdata.x,0)))
					var mousey = Math.round(Math.min(22,Math.max(thisdata.y,0)))

					var newx = mousex//(i * .1)+mousex
					particles[numParticles++] = new Particle(newx, mousey, Math.floor(count / 10 % 5)); 

					//Particles y velocity = 5.
					particles[numParticles - 1].vy = self.particle_velocity;

					//Limit these!
					if (numParticles > self.max_particles){
						particles.shift();
						numParticles--;
					}
				}
				
			}
			//----------------------------------------------------------

			var pour = function(){
				var mousex = Math.min(44,Math.max(mouse.x,0))
				var mousey = Math.min(22,Math.max(mouse.y,0))
				if (isNaN(mousex) || isNaN(mousey)) {return}

				var i;
				for (i = -4; i <= 4; i++){

					var newx = (i * .1)+mousex

					particles[numParticles++] = new Particle(newx, mousey, Math.floor(count / 10 % 5)); 

					//Particles y velocity = 5.
					particles[numParticles - 1].vy =  self.particle_velocity;

					//Limit these!
					if (numParticles > self.max_particles){
						particles.shift();
						numParticles--;

					}
				}

			}

			//----------------------------------------------------------

			var move = function(){
				
				if (particles.length>self.max_particles){
					particles = particles.splice(0,self.max_particles)
					numParticles = self.max_particles
				}
				
				if (self.maxage>0 || self.colorfade>0) {
					for (i = numParticles-1;i>-1; i--){
						particles[i].age += 1
						if ( ((self.maxage > 0) && (particles[i].age > self.maxage)) || ((self.colorfade>0) && (particles[i].color == "#000000")) ) {
							particles.splice(i,1)
						}
					}
				}
				numParticles = particles.length
				
				for (i = 0; i < numParticles; i++){
					p = particles[i];
					p.RANGE = self.RANGE
					p.PRESSURE_NEAR = self.PRESSURE_NEAR
					p.VISCOSITY = self.VISCOSITY
					p.gravity = self.gravity										
				}

				count++;

				updateGrids();
				findNeighbors();
				calcForce();

				var i, p;

				for (i = 0; i < numParticles; i++){
					p = particles[i];
					moveParticle(p);

					context.beginPath();
					context.fillStyle = p.color;  
					context.arc(p.x - 1,  p.y - 1, self.particle_size , 0, Math.PI*2, false);
					context.closePath();
					context.fill();
				}

				if (self.colorfade>0) {
					for (i = numParticles-1;i>-1; i--){
						particles[i].color = "#"+df.newcolor(particles[i].color,-self.colorfade)
					}
				}

				
			}

			//----------------------------------------------------------

			var updateGrids = function(){

				var p;
				var i, j;

				for (i = 0; i < NUM_GRIDS; i++){

					for (j = 0; j < NUM_GRIDS; j++){

						//Is this meant to clear the grid?
						grids[i][j].particles.length = 0;
						grids[i][j].numParticles = 0;
					}	
				}

				for (i = 0; i < numParticles; i++){

					p = particles[i];

					//Zero all of the things!
					p.fx = 0;
					p.fy = 0;
					p.density = 0;
					p.densityNear = 0;

					p.gx = Math.floor(p.x * INV_GRID_SIZE);
					p.gy = Math.floor(p.y * INV_GRID_SIZE);

					if(p.gx < 0){

						p.gx = 0;
					}

					if (p.gy < 0){

						p.gy = 0;
					}

					if (p.gx > NUM_GRIDS - 1){
						p.gx = NUM_GRIDS - 1;	
					}

					if (p.gy > NUM_GRIDS - 1){
						p.gy = NUM_GRIDS - 1;
					}
				}
			}

			//----------------------------------------------------------

			var findNeighbors = function(){

				var i;
				var p;
				numNeighbors = 0;

				for(i = 0; i < numParticles; i++){

					p = particles[i];

					var xMin = p.gx != 0;
					var xMax = p.gx != (NUM_GRIDS - 1);

					var yMin = p.gy != 0;
					var yMax = p.gy != (NUM_GRIDS - 1); 

					if (!p.gx || !p.gy){continue}

					findNeighborsInGrid(p, grids[p.gx][p.gy]);

					if(xMin){
						findNeighborsInGrid(p, grids[p.gx - 1][p.gy]);
					}

					if(xMax){
						findNeighborsInGrid(p, grids[p.gx + 1][p.gy]);
					}

					if(yMin){
						findNeighborsInGrid(p, grids[p.gx][p.gy - 1]);
					}

					if(yMax){
						findNeighborsInGrid(p, grids[p.gx][p.gy + 1]);
					}

					if(xMin && yMin){
						findNeighborsInGrid(p, grids[p.gx - 1][p.gy - 1]);
					}

					if(xMin && yMax){
						findNeighborsInGrid(p, grids[p.gx - 1][p.gy + 1]);
					}

					if(xMax && yMin){
						findNeighborsInGrid(p, grids[p.gx + 1][p.gy - 1]);
					}

					if(xMax && yMax){
						findNeighborsInGrid(p, grids[p.gx + 1][p.gy + 1]);
					}

					grids[p.gx][p.gy].add(p);
				}
			}

			//----------------------------------------------------------

			var findNeighborsInGrid = function(pi, g){

				var j;

				var pj, distance;

				for (j = 0; j < g.numParticles; j++){

					pj = g.particles[j];

					distance = (pi.x - pj.x) * (pi.x - pj.x) +
							   (pi.y - pj.y) * (pi.y - pj.y);

					if (distance < (self.RANGE*self.RANGE)){

						if(neighbors.length == numNeighbors){

							neighbors[numNeighbors] = new Neighbor();
						} 

						neighbors[numNeighbors++].setParticle(pi, pj);

					}	
				}
			}

			//----------------------------------------------------------

			var calcForce = function(){

				var i;

				for (i = 0; i < numNeighbors; i++){

					neighbors[i].calcForce();
				}	
			}

			//----------------------------------------------------------

			var moveParticle = function(p){

				p.vy += self.gravity;

				if (p.density > 0){

					p.vx += p.fx / (p.density * 0.9 + 0.1);
					p.vy += p.fy / (p.density * 0.9 + 0.1);	
				}

				p.x += p.vx;
				p.y += p.vy;

				if (p.x < 1){
					p.vx += (1 - p.x) * 0.5 - p.vx * 0.005;
				}

				if (p.x > 43){		
					p.vx += (43 - p.x) * 0.5 - p.vx * 0.005;
				}

				if(p.y < 1){
					p.vy += (1 - p.y) * 0.5 - p.vy * 0.005;
				}

				if(p.y > 21){
					p.vy += (21 - p.y) * 0.5 - p.vy * 0.005;
				}

			}

			//----------------------------------------------------------

			var MouseMove = function(e) {
				mouse.x = e.layerX || e.offsetX;
			mouse.y = e.layerY || e.offsetY;
			}

			//Clear the screen, 
			var MouseDown = function(e) {
				e.preventDefault();
				mouseDown = true;
			}

				//Clear the screen, 
			var MouseUp = function(e) {
				e.preventDefault();
				mouseDown = false;
			}

			var MouseOut = function(e) {
				e.preventDefault();
				mouseDown = false;
				ClearFrame();
			}

			var ClearFrame = function(){
				canvas.width = canvas.width
			}
		}

		//========================================================================================

		function Neighbor(){

			this.p1;
			this.p2;

			this.distance;

			this.nx;
			this.ny;

			this.weight;

			//Constants - should be taken from flow class;
			this.RANGE = self.RANGE;
			this.PRESSURE = self.PRESSURE;
			this.PRESSURE_NEAR = self.PRESSURE_NEAR;
			this.DENSITY = self.DENSITY;
			this.VISCOSITY = self.VISCOSITY;

			//----------------------------------------------------------

			this.setParticle = function(p1, p2){

				this.p1 = p1;
				this.p2 = p2;

				this.nx = p1.x - p2.x;
				this.ny = p1.y - p2.y;

				this.distance = Math.sqrt(this.nx * this.nx + this.ny * this.ny);

				this.weight = 1 - this.distance / this.RANGE;

				var density = this.weight * this.weight;

				p1.density += density;
				p2.density += density;

				density *= this.weight * this.PRESSURE_NEAR;

				p1.densityNear += density;
				p2.densityNear += density;

				//Interted distance
				var invDistance = 1 / this.distance;

				this.nx *= invDistance;
				this.ny *= invDistance;

			}

			//----------------------------------------------------------

			this.calcForce = function(){

				var p;

				var p1 = this.p1;
				var p2 = this.p2;

				if(this.p1.type != this.p2.type){

					p = (p1.density + p2.density - this.DENSITY * 1.5) * this.PRESSURE;

				} else {

					 p = (p1.density + p2.density - this.DENSITY * 2) * this.PRESSURE;

				}

				var pn = (p1.densityNear + p2.densityNear) * this.PRESSURE_NEAR;

				var pressureWeight = this.weight * (p + this.weight * pn);
				var viscocityWeight = this.weight * this.VISCOSITY;

				var fx = this.nx * pressureWeight;
				var fy = this.ny * pressureWeight;

				fx += (p2.vx - p1.vx) * viscocityWeight;
				fy += (p2.vy - p1.vy) * viscocityWeight;

				p1.fx += fx;
				p1.fy += fy;

				p2.fx -= fx;
				p2.fy -= fy;
			}
		}

		//========================================================================================

		function Particle(x, y, type){

			this.x = x;
			this.y = y;

			this.gx;
			this.gy;
			this.vx = 0;
			this.vy = 0;
			this.fx = 0;
			this.fy = 0;
			this.age = 0;

			this.density;
			this.densityNear;

			this.color;
			this.type = type;
			this.gravity = self.gravity; //Should get the main class gravity.

			switch(type){

				case 0:
					this.color = this.startcolor = "#6060ff";
					break;
				case 1:
					this.color = this.startcolor = "#ff6000";
					break;
				case 2:
					this.color = this.startcolor = "#ff0060";
					break;
				case 3:
					this.color = this.startcolor = "#00d060";
					break;
				case 4:
					this.color = this.startcolor = "#d0d000";
					break;
			}

		}

		//========================================================================================

		//Grids for quicker grouping
		function Grid(){

			this.particles = new Array();
			this.numParticles = 0;

			this.add = function(particle){

				this.particles[this.numParticles++] = particle;
			}
		}

		//========================================================================================

		var app = new Flow();
		app.initialize();

	},
	
//========================================================================================
//========================================================================================


	settings_to_save:[
		"particle_size",
		"particle_velocity",
		"max_particles",
		"RANGE",
		"PRESSURE",
		"PRESSURE_NEAR",
		"DENSITY",
		"VISCOSITY",
		"gravity",
		"maxage",
		"colorfade"
	],
	
	"particle_size"		: 1,
	"particle_velocity"	: 0.15,
	"max_particles"		: 500,
	"RANGE"				: 16,
	"PRESSURE"			: 0.0001,
	"PRESSURE_NEAR"		: 0.01,
	"DENSITY"			: 6,
	"VISCOSITY"			: 0.05,
	"gravity"			: 0.05,
	"maxage"			: 0,
	"colorfade"			: 2


})
