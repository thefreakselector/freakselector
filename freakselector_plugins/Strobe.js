(function(){

var self ={
	name		: "Strobe",
	icon		: "Strobe.png",
	fullscreen	: true,

	strobe_color1:	"ff0000",
	strobe_color2:	"0000ff",
	strobe_color3:	"00ff00",
	strobe_state: false,

	start:	function(){
		
		self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

	},
	
	fill:	function(r,g,b,a) {
	
		self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)
		var data = self.imagedata.data

		for (var x=0;x<LED.grid_width;x++) {
			for (var y=0;y<LED.grid_height;y++) {
				var px = LED.getCanvasIndex(x,y)
				data[px]   = r || 0
				data[px+1] = g || 0
				data[px+2] = b || 0
				data[px+3] = a || 0
			}
		}			
	
	},

	end:	function(){
		LED.frame_time_override = undefined
	},
	
	strobe_idx:0,

	frame_count:0,
	strobe_speed:.3,
	enable_color3:false,

	next_frame:1,
	strobe_idx:0,
	frame_loop3:[0,1,2],

	render: function(){
		
		self.frame_count = self.frame_count + self.strobe_speed;

		var show_next_frame = false;
		if (self.frame_count >= self.next_frame){
			self.next_frame = Math.floor(self.frame_count)+1;
			show_next_frame = true;
		}

		if (self.enable_color3){
			if (show_next_frame){
				self.frame_loop3 = self.frame_loop3.concat(self.frame_loop3.shift());
			}
			if ( self.frame_loop3[0] === 0 ) {
				self.fill(df.getdec(df.hexcolor(self.strobe_color1).substr(0,2)),df.getdec(df.hexcolor(self.strobe_color1).substr(2,2)),df.getdec(df.hexcolor(self.strobe_color1).substr(4,2)),255)
				self.strobe_idx++
			} else if ( self.frame_loop3[0] === 1 ) {
				self.fill(df.getdec(df.hexcolor(self.strobe_color2).substr(0,2)),df.getdec(df.hexcolor(self.strobe_color2).substr(2,2)),df.getdec(df.hexcolor(self.strobe_color2).substr(4,2)),255)
				self.strobe_idx=0
			} else if ( self.frame_loop3[0] === 2 ) {
				self.fill(df.getdec(df.hexcolor(self.strobe_color3).substr(0,2)),df.getdec(df.hexcolor(self.strobe_color3).substr(2,2)),df.getdec(df.hexcolor(self.strobe_color3).substr(4,2)),255)
				self.strobe_idx=0
			}

		} else {
			var strobe_frame = self.frame_count & 1;
			if (strobe_frame) {
				self.fill(df.getdec(df.hexcolor(self.strobe_color1).substr(0,2)),df.getdec(df.hexcolor(self.strobe_color1).substr(2,2)),df.getdec(df.hexcolor(self.strobe_color1).substr(4,2)),255)
				self.strobe_idx++
			} else {
				self.fill(df.getdec(df.hexcolor(self.strobe_color2).substr(0,2)),df.getdec(df.hexcolor(self.strobe_color2).substr(2,2)),df.getdec(df.hexcolor(self.strobe_color2).substr(4,2)),255)
				self.strobe_idx=0
			}
		}
		
//console.log(self.strobe_color2)		
//LED.pluginwindow.style.backgroundColor = "#"+self.strobe_color2

//console.log(imagedata.data)

		LED.composite_ctx.putImageData(self.imagedata, 0, 0);

//		LED.composite_ctx.drawImage(self.canvas,0,0)

	},

	interface:	function(){
				
		var colorpicker1_div = df.newdom({
			tagname:"div",
			target:LED.pluginwindow,
			attributes:{
				style:"position:absolute;top:20px;left:20px;"
			}
		})

		self.colorpicker1 = new df.ColorPicker({
			target: colorpicker1_div,
			width : 256,
			height: 50,
			startvalue:self.strobe_color1,
			callback:function(new_color){
				self.setcolor1({newcolor:new_color})
			}
		})


		var colorpicker2_div = df.newdom({
			tagname:"div",
			target:LED.pluginwindow,
			attributes:{
				style:"position:absolute;top:20px;left:318px;"
			}
		})

		self.colorpicker2 = new df.ColorPicker({
			target: colorpicker2_div,
			width : 256,
			height: 50,
			startvalue:self.strobe_color2,
			callback:function(new_color){
				self.setcolor2({newcolor:new_color})
			}
		})


		var colorpicker3_div = df.newdom({
			tagname:"div",
			target:LED.pluginwindow,
			attributes:{
				style:"position:absolute;top:20px;left:614px;"
			}
		})

		self.colorpicker3 = new df.ColorPicker({
			target: colorpicker3_div,
			startvalue:self.strobe_color3,
			width : 256,
			height: 50,
			callback:function(new_color){
				self.setcolor3({newcolor:new_color})
			}
		})


		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "toggle_third_color",
				name		: "3rd Color",
				style		: "position:absolute;top:190px;left:672px;z-index:9999",
				class		: "onoff",
				value		: self.enable_color3,
				width		: 65,
				onchange	: function(val){

					self.enable_color3 = val
				}
			}
		})
		


		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "toggle_fast_renderer",
				name		: "Fast Render",
				style		: "position:absolute;top:434px;left:331px;z-index:9999",
				class		: "onoff",
				value		: LED.render_fast,
				width		: 65,
				onchange	: function(val){

					LED.render_fast = val
				}
			}
		})
		



		var slidertypes = [
			{
				id:"speed_slider",
				name:"Speed",
				startvalue:self.strobe_speed,
				min:0,
				max:1,
				handler:function(val){
					self.strobe_speed = val
				}
			}
		]

		var sliders = LED.sliderList(slidertypes)

		self.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:275px;top:300px;color:white;"
				},
				children	: sliders
		})




	},
	

	setcolor1:function(data){
		self.strobe_color1 = data.newcolor
	},
	setcolor2:function(data){
		self.strobe_color2 = data.newcolor
	},
	setcolor3:function(data){
		self.strobe_color3 = data.newcolor
	}

}


LED.plugins.push(self)


})();
