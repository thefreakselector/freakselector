(function(){


var self = {
	name		: "Center Fade",
//	icon		: "Center Fade.png",

	fullscreen	: true,

	haspreview:true,

	rasters:[],

	start:	function(){

		self.effect1b_grad = [].concat(self.makeGradient("ff0000","000000",1100), self.makeGradient("ff0000","2389df",1100), self.makeGradient("2389df","00ff00",450), self.makeGradient("00ff00","ff89df",1700), self.makeGradient("ff89df","5588cc",1400), self.makeGradient("5588cc","999999",740), self.makeGradient("999999","f3d922",1400), self.makeGradient("f3d922","127643",1040), self.makeGradient("127643","ff0000",1930) )

		self.effect1b_grad2 = [].concat(self.makeGradient("ff0000","00ff00",1600), self.makeGradient("00ff00","0000ff",1470), self.makeGradient("0000ff","ff00ff",1800), self.makeGradient("ff00ff","ff0000",1300) )
	}
	,
	end:	function(){

	},

	play:	function(){
		self.paused = false
	},
	pause:	function(){
		self.paused = true
	},

//--------------------


	gradientV:	function(start,end,imgdata){

		var ri = (end.r -start.r) / (LED.grid_height/2)
		var r = start.r
		var gi = (end.g -start.g ) / (LED.grid_height/2)
		var g = start.g
		var bi = (end.b -start.b) / (LED.grid_height/2)
		var b = start.b

		for (var y=Math.floor(LED.grid_height/2);y<LED.grid_height;y++){
			
      self.setPixel({
        x:start.x,
        y:y,
        r:Math.round(r),
        g:Math.round(g),
        b:Math.round(b)
      },imgdata);

			self.setPixel({
        x:start.x,
        y:Math.round( (LED.grid_height/2) - (y-(LED.grid_height/2)) )-1,
        r:Math.round(r),
        g:Math.round(g),
        b:Math.round(b)
      },imgdata);

			r+=ri;
			g+=gi;
			b+=bi;
		}
	},

	setPixel:	function(obj,imgdata){

		var i = (obj.y*LED.grid_width*4) + (obj.x*4)
		imgdata[i] = obj.r || 0
		imgdata[i+1] = obj.g || 0
		imgdata[i+2] = obj.b || 0
		imgdata[i+3] = obj.a || 255
	},

	makeGradient:	function(startcolor,endcolor,length){
		var start_r = parseInt(startcolor.substr(0,2),16)
		var start_g = parseInt(startcolor.substr(2,2),16)
		var start_b = parseInt(startcolor.substr(4,2),16)


		var end_r = parseInt(endcolor.substr(0,2),16)
		var end_g = parseInt(endcolor.substr(2,2),16)
		var end_b = parseInt(endcolor.substr(4,2),16)

		var this_r = start_r
		var this_g = start_g
		var this_b = start_b

		var grad = []
		for ( var i = 0; i < length; i++){
			grad.push(("0"+(Math.round(this_r).toString(16))).substr(-2) +  ("0"+(Math.round(this_g).toString(16))).substr(-2) +  ("0"+(Math.round(this_b).toString(16))).substr(-2) )

			this_r = this_r += ((end_r-start_r)/length)
			this_g = this_g += ((end_g-start_g)/length)
			this_b = this_b += ((end_b-start_b)/length)
		}
		return grad

	},

//---------------------

	render: function(){

		var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

		for ( var xx=0;xx<LED.grid_width;xx++){
			var bgcol = self.effect1b_grad.shift()
			self.effect1b_grad.push(bgcol)

			var bgcol2 = self.effect1b_grad2.shift()
			self.effect1b_grad2.push(bgcol2)

			var bg_r = parseInt(bgcol.substr(0,2),16)
			var bg_g = parseInt(bgcol.substr(2,2),16)
			var bg_b = parseInt(bgcol.substr(4,2),16)

			var rs = parseInt(bgcol2.substr(0,2),16)
			var gs = parseInt(bgcol2.substr(2,2),16)
			var bs = parseInt(bgcol2.substr(4,2),16)

			self.gradientV({x:xx, y:0, r:rs,g:gs,b:bs},{x:xx, y:LED.grid_height/2, r:bg_r,g:bg_g,b:bg_b},imagedata.data)
		}

		LED.composite_ctx.putImageData(imagedata, 0, 0);

	},

	settings_to_save:[
	],

	interface:	function(){

/*
		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id		: "vertical",
				name		: "Vertical/Horizontal",
				style		: "position:absolute;top:10px;left:10px;",
				class		: "onoff",
				value		: self.vertical,
				width		: 40,
				height		: 40,
				onchange	: function(val){
					self.vertical = val
				}
			}
		})
*/
	}

}

	LED.plugins.push(self)

})();
