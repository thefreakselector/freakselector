(function(){

var self = {


	name		: "Waves",
	//icon		: "Video Input.png",

	settings_to_save:	[
	],

	start:	function(){


		if (self.canvas == undefined) {
			self.canvas = df.newdom({
				tagname:"canvas",
				target	: LED.pluginwindow,//LED.contextHolder,
				attributes:{
					id	: "sparkles_canvas",
					style	: "position:relative;top:20px;left:20px;",
					width	: LED.grid_width,
					height	: LED.grid_height
				}
			})
			self.ctx = self.canvas.getContext('2d')
		}

		//self.update();
	},

	end:	function() {

	},

	wave_freq: .1,
	wave_speed: .001,
	wave_bright: 1,
	wave_move: 0,
	color1: '#ffffff',
	color2: '#00ff00',
	render:	function(){

		var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height);
		var this_frame = imagedata.data;

		var c1 = tinycolor(self.color1).toRgb();
		var c2 = tinycolor(self.color2).toRgb();

	     var bg_r = c1.r;
	     var bg_g = c1.g;
	     var bg_b = c1.b;

	     var fg_r = c2.r;
	     var fg_g = c2.g;
	     var fg_b = c2.b;

	     for (var x=0;x<LED.grid_width;x++){
	        var ys = ((LED.grid_width/360) * x) * self.wave_freq;
	        self.wave_move = self.wave_move + self.wave_speed;
	        ys = ys + self.wave_move;
	        var y = Math.round( ((Math.sin(ys) * LED.grid_height) / 2) + Math.round(LED.grid_height/2)  );

	        for (var i=0;i<y;i++){
	          var px = LED.getCanvasIndex(x,i);
	          this_frame[px]   = fg_r;
	          this_frame[px+1] = fg_g;
	          this_frame[px+2] = fg_b;
	          this_frame[px+3] = 255;
	        }
	        for (var i=y;i<LED.grid_height;i++){
	          var px = LED.getCanvasIndex(x,i);
	          this_frame[px]   = bg_r;
	          this_frame[px+1] = bg_g;
	          this_frame[px+2] = bg_b;
	          this_frame[px+3] = 255;
	        }
	      }

		LED.composite_ctx.putImageData(imagedata, 0, 0);

//		LED.composite_ctx.drawImage(self.canvas, 0, 0,LED.grid_width,LED.grid_height)

	},

	interface:	function() {

		var sliders=[]

		var slidertypes = [

			{
				id:"wave_speed",
				name:"Speed",
				startvalue: self.wave_speed,
				min:0,
				max:.1,
				handler:function(val){
					self.wave_speed = val/10;
					return (val * 10).toFixed(2)
				}
			},

			{
				id:"wave_freq",
				name:"Frequency",
				startvalue: self.wave_freq,
				min:0,
				max:30,
				handler:function(val){
					self.wave_freq = val;
					return (val/30).toFixed(2)
				}
			},

			{
				id:"wave_bright",
				name:"Bright",
				startvalue: self.wave_bright,
				min:0,
				max:1,
				handler:function(val){
					self.wave_bright = val;
				}
			},

		]

		//------------------------------------------------------------------------------------------------------

		var color1 = new df.ColorPicker({
			//target: raster_colorpicker_div,
			width : 300,
			height: 42,
			startvalue:self.color1,
			callback:function(new_color, picker_config){
				self.color1 = '#'+new_color
			}
		})

		var color2 = new df.ColorPicker({
			//target: raster_colorpicker_div,
			width : 300,
			height: 42,
			startvalue:self.color2,
			callback:function(new_color, picker_config){
				self.color2 = '#'+new_color
			}
		})

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		var sliders = LED.sliderList(slidertypes)

		sliders.push(color1.containerDiv)
		sliders.push(color2.containerDiv)

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})


//==========================================================================================


}



}


	LED.plugins.push(self)
})();

