(function(){

var self = {
	name		: "Image Processor",
	//icon		: "Video Input.png",

  render_last: true,
  sort_top: true,
  auto_start: true,

	settings_to_save:	[
		'contrast',
		'saturation',
		'brightness',
		'gamma'
	],

	start:	function(){

	},

	end:	function() {

	},

	render:	function(){

		var imageData = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

		//------------------------------------------------------------------------
		var filter_contrast = new fabric.Image.filters.Contrast({
			contrast: self.contrast,
		});

		filter_contrast.applyTo2d({
			imageData: imageData
		})

		//------------------------------------------------------------------------
		var filter_saturation = new fabric.Image.filters.Saturation({
			saturation: self.saturation,
		});

		filter_saturation.applyTo2d({
			imageData: imageData
		})

		//------------------------------------------------------------------------
		var filter_brightness = new fabric.Image.filters.Brightness({
			brightness: self.brightness,
		});

		filter_brightness.applyTo2d({
			imageData: imageData
		})

		//------------------------------------------------------------------------
		var filter_gamma = new fabric.Image.filters.Gamma({
			brightness: [self.gamma,self.gamma,self.gamma],
		});

		filter_gamma.applyTo2d({
			imageData: imageData
		})

		//------------------------------------------------------------------------

		LED.composite_ctx.putImageData(imageData, 0, 0);

//		LED.composite_ctx.drawImage(self.canvas, 0, 0,LED.grid_width,LED.grid_height)

	},

	update:	function(){

	},

	contrast: .8,
	saturation: 1,
	brightness: 0,
	gamma: 0,

	interface:	function() {

    df.newdom({
      tagname   :"input",
      target    : LED.pluginwindow,
      attributes:{
        id    : "ip_reset",
        className: "bigbutton2",
        value   : "Default Values",
        type    : "button",
        style   : "position: absolute;top: 0px;left: 613px;width: 267px;height: 58px;background-color: #000000;color: white;",
        onclick : function(val){

        	self.contrast = 0;
        	self.saturation = 0;
        	self.brightness = 0;
        	self.gamma = 0;

        	LED.pluginwindow.innerHTML = ''
        	self.interface()

        }
      }
    })


//==============================================

		var sliders=[]

		var slidertypes = [

			{
				id:"ip_contrast",
				name:"Contrast",
				startvalue:self.contrast+1,
				min:0,
				max:2,
				handler:function(val){
					self.contrast = val - 1
					return (val - 1).toFixed(2)
				}
			},

			{
				id:"ip_brightness",
				name:"Brightness",
				startvalue:self.brightness+1,
				min:0,
				max:2,
				handler:function(val){
					self.brightness = val - 1
					return (val - 1).toFixed(2)
				}
			},

			{
				id:"ip_saturation",
				name:"Saturation",
				startvalue:self.saturation+1,
				min:0,
				max:2,
				handler:function(val){
					self.saturation = val - 1
					return (val - 1).toFixed(2)
				}
			},

			{
				id:"ip_gamma",
				name:"Gamma",
				startvalue:self.gamma+1,
				min:0,
				max:2,
				handler:function(val){
					self.gamma = val - 1
					return (val - 1).toFixed(2)
				}
			}

		]

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		self.sliders = LED.sliderList(slidertypes)

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: self.sliders

				}
			]
		})


//==========================================================================================


}



}


	LED.plugins.push(self)
})();

