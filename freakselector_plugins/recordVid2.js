(function(){

  var self = {
    name		: "RecordVid2",
  
  	render_last	: true,
    sort_top: true,
  
    brightness:	1,
  
    pixel_map: [],
  
    pixelRadius: 3,
    pixelDisplayRadius: 30,
    furDisplay: true,
    isrecording:false,

    start:	function(){

      self.pixel_map = [];
      for (var screen in LED.pixelmap){
        self.pixel_map = self.pixel_map.concat(LED.pixelmap[screen]);
      }
  
      var max_x = 0;
      var max_y = 0;
      for (var i=0;i<self.pixel_map.length;i++){
        max_x = Math.max(self.pixel_map[i].x, max_x);
        max_y = Math.max(self.pixel_map[i].y, max_y);
      }
  
      for (var i=0;i<self.pixel_map.length;i++){
        self.pixel_map[i].x = self.pixel_map[i].x / max_x
        self.pixel_map[i].y = self.pixel_map[i].y / max_y
      }
  
      console.log(self.pixel_map)
  
    },

  //************************************************************************************************
  
    end:	function(){
      df.removedom(self.pixel_canvas);
      LED.composite.style.display = 'block';
    },
  
  
    render: function(){
      self.most_recent_frame = Array.apply(null,LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height).data)
      if (self.isrecording === true || self.furDisplay === true){
        self.mapPixels()
      }
    },
  
    //rotation_angle: 90,
  
  //==============================================================================================
  
      mapPixels_simple:  function(){
          var thismap = self.pixel_map
  
          var data = self.most_recent_frame
  
          function getPixel(x,y){
              var idx = ((x*4)+(y*LED.grid_width*4))
              return {r:data[idx],g:data[idx+1],b:data[idx+2],a:data[idx+3]}
          }
  
          var pixels = []
  
          for (var i=0;i<thismap.length;i++){
              var this_x = thismap[i].x
              var this_y = thismap[i].y
  
              var pixel_x = Math.round(this_x * (LED.grid_width-1));
              var pixel_y = Math.round(this_y * (LED.grid_height-1));
              var pixel_color = getPixel(pixel_x,pixel_y);
              var rx = Math.round(pixel_color.r * self.brightness);
              var gx = Math.round(pixel_color.g * self.brightness);
              var bx = Math.round(pixel_color.b * self.brightness);
              pixels.push(String.fromCharCode(gx));
              pixels.push(String.fromCharCode(rx));
              pixels.push(String.fromCharCode(bx));

              if (self.isrecording==true){
                self.recordingC.push(rx);
                self.recordingC.push(gx);
                self.recordingC.push(bx);
              }
          }
          self.frames.innerHTML = self.recordingC.length / self.pixel_map.length;
  
          return pixels
      },
  
  //==============================================================================================

    mapPixels:  function(){

        var thismap = self.pixel_map

        var data = self.most_recent_frame

//          self.pixel_canvas_ctx.clearRect(0,0,LED.grid_width,LED.grid_height);
        self.pixel_canvas_ctx.clearRect(0,0,self.canvasw, self.canvash);

        function getPixel(x,y){
            var idx = ((x * 4) + (y * LED.grid_width * 4))
            return {r:data[idx], g:data[idx+1], b:data[idx+2], a:data[idx+3]}
        }

        function getMatrix(x, y, rad){

          if (rad === 0) {

            var pixel_color = getPixel(x, y);
            var rr = pixel_color.r * self.brightness;
            var gg = pixel_color.g * self.brightness;
            var bb = pixel_color.b * self.brightness;
            var aa = pixel_color.a;

          } else {

            var rr = 0;
            var gg = 0;
            var bb = 0;
            var aa = 0;
            var count = 0;
            var pixel = {};

            for (var yy=y-rad;yy<y+Math.max(rad,1);yy++){
                for (var xx=x-rad;xx<x+Math.max(rad,1);xx++){
                    if (xx < 0){continue}
                    if (yy < 0){continue}
                    if (xx > LED.grid_width - 1) {continue}
                    if (yy > LED.grid_height - 1){continue}
                    pixel = getPixel(Math.round(xx), Math.round(yy) );
                    rr += pixel.r//||0
                    gg += pixel.g//||0
                    bb += pixel.b//||0
                    aa += pixel.a//||0
                    count++
                }
            }

            rr = Math.round((rr / count) * self.brightness);
            gg = Math.round((gg / count) * self.brightness);
            bb = Math.round((bb / count) * self.brightness);
            aa = Math.round(aa / count);
          }

          return {r:rr, g:gg, b:bb, a:aa}
        }

        var pixels = [];
/*
        var theta = self.rotation_angle * (Math.PI/180);
        var s = Math.sin(theta);
        var c = Math.cos(theta);
*/
        var pixel_color;

        for (var i=0;i<thismap.length;i++){

            var this_x = Math.round(thismap[i].x * (LED.grid_width - 1));
            var this_y = Math.round(thismap[i].y * (LED.grid_height - 1));

            /*
            var rot_x = this_x -= .5 // for centering
            var rot_y = this_y -= .5 // for centering

            var new_x = rot_x * c - rot_y * s
            var new_y = rot_y * s - rot_x * c

            this_x = new_x += .5
            this_y = new_y += .5

            var pixX = this_x * LED.grid_width;
            var pixY = this_y * LED.grid_height;

            this_x = Math.round(pixX);
            this_y = Math.round(pixY);
*/

            pixel_color = getMatrix(this_x, this_y, self.pixelRadius);

            self.renderFur(this_x,this_y,pixel_color);
              
            var rx = Math.round(pixel_color.r * pixel_color.a);
            var gx = Math.round(pixel_color.g * pixel_color.a);
            var bx = Math.round(pixel_color.b * pixel_color.a);

            pixels.push(String.fromCharCode(gx));
            pixels.push(String.fromCharCode(rx));
            pixels.push(String.fromCharCode(bx));

            if (self.isrecording==true){
              self.recordingC.push(rx)
              self.recordingC.push(gx)
              self.recordingC.push(bx)
            }
        }

        self.updateCounts();

        return pixels
    },

    //==============================================================================================

    updateCounts: function(){
      self.frames.innerHTML = (self.recordingC.length / self.pixel_map.length / 3) + 'frames';
      self.fileSize.innerHTML = (self.recordingC.length / 1024) < 1024 ? (self.recordingC.length / 1024).toFixed(1)+'KB' : (self.recordingC.length / 1024 / 1024).toFixed(3)+'MB';
      self.fileTime.innerHTML = ((self.recordingC.length / 3 / self.pixel_map.length) / 30).toFixed(1) + ' sec'
    },

    //==============================================================================================
    
    renderFur: function(x, y, pixel_color){
      var rr = pixel_color.r;
      var gg = pixel_color.g;
      var bb = pixel_color.b;
      var aa = pixel_color.a;

      var xx = Math.max(((x / LED.grid_width) * self.canvasw) ,0);
      var yy = Math.max(((y / LED.grid_height) * self.canvash),0);
      var pw = Math.max(Math.round(self.pixelDisplayRadius * (LED.grid_width / self.canvasw)),1)*2;
      var ph = Math.max(Math.round(self.pixelDisplayRadius * (LED.grid_height / self.canvash)),1)*2;

      var cc = 'rgba('+(rr)+','+(gg)+','+(bb)+','+(aa/255)+')';
      var cc2 = 'rgba('+(rr)+','+(gg)+','+(bb)+',0)';
      var pmax = Math.max(pw,ph);
      var grd = self.pixel_canvas_ctx.createRadialGradient(xx, yy, 1, xx, yy, pmax);
      grd.addColorStop(0, cc);
      grd.addColorStop(1, cc2);
      self.pixel_canvas_ctx.fillStyle = grd;
      self.pixel_canvas_ctx.fillRect(xx-pmax, yy-pmax, pmax*2, pmax*2);
    },

    //==============================================================================================
  
    recording: 	[],
    recordingC: [],
  
    interface:	function(){
      if (self.pixel_canvas){
        self.pixel_canvas_ctx = undefined;
        df.removedom(self.pixel_canvas)
      }
      self.canvasw = parseInt(LED.LEDholder.style.width,10);
      self.canvash = parseInt(LED.LEDholder.style.height,10);
      self.pixel_canvas = df.newdom({
        tagname: "canvas",
        target: LED.LEDholder,
        attributes:{
          width: self.canvasw,//LED.grid_width,
          height: self.canvash,//LED.grid_height,
          style: "width:"+self.canvasw+'px;height:'+self.canvash+'px;z-index:9999;position:absolute;top:3px;left:3px;border:2px solid red;'
        }
      });

      self.pixel_canvas_ctx = self.pixel_canvas.getContext('2d');
      LED.composite.style.display = 'none';
  
      //------------------------------------------------------------------------------------------------------------------

      var isrecording = df.newdom({
        tagname	: "input",
        target	: LED.pluginwindow,
        attributes:{
          style	: "position:absolute;top:100px;left:300px;font-size:45px;font-family:'BanksiaBold'",
          type	: "button",
          value	: "Record"
        },
        events	: [
          {
            name	: "click",
            handler	: function(evt){
              self.isrecording = true
            }
          }
        ]
      });
  
      //------------------------------------------------------------------------------------------------------------------

      var stoprecording = df.newdom({
        tagname	: "input",
        target	: LED.pluginwindow,
        attributes:{
          style	: "position:absolute;top:175px;left:300px;font-size:45px;font-family:'BanksiaBold'",
          type	: "button",
          value	: "Stop Record"
        },
        events	: [
          {
            name	: "click",
            handler	: function(evt){
              self.isrecording = false
            }
          }
        ]
      });
  
      //------------------------------------------------------------------------------------------------------------------
      
      var clearButton = df.newdom({
        tagname	: "input",
        target	: LED.pluginwindow,
        attributes:{
          style	: "position:absolute;top:305px;left:300px;font-size:45px;font-family:'BanksiaBold'",
          type	: "button",
          value	: "Clear"
        },
        events	: [
          {
            name	: "click",
            handler	: function(evt){
              self.recordingC = [];
              self.updateCounts();
            }
          }
        ]
      });
  
      //------------------------------------------------------------------------------------------------------------------

      self.frames = df.newdom({
        tagname	: "div",
        target	: LED.pluginwindow,
        attributes:{
          style	: "position:absolute;top:240px;left:300px;font-size:45px;font-family:'BanksiaBold';height:50px;border:1px solid black;background-color:white;padding:3px;"
        }
      })
  
      self.fileSize = df.newdom({
        tagname	: "div",
        target	: LED.pluginwindow,
        attributes:{
          style	: "position:absolute;top:240px;left:450px;font-size:45px;font-family:'BanksiaBold';height:50px;border:1px solid black;background-color:white;padding:3px;"
        }
      })
  
      self.fileTime = df.newdom({
        tagname	: "div",
        target	: LED.pluginwindow,
        attributes:{
          style	: "position:absolute;top:240px;left:10px;font-size:45px;font-family:'BanksiaBold';height:50px;border:1px solid black;background-color:white;padding:3px;"
        }
      })
  
  /*
      var exportbutton = df.newdom({
        tagname	: "input",
        target	: LED.pluginwindow,
        attributes:{
          style	: "position:absolute;top:305px;left:300px;font-size:45px;font-family:'BanksiaBold'",
          type	: "button",
          value	: "Export CSV"
        },
        events	: [
          {
            name	: "click",
            handler	: function(evt){
  console.clear()
  console.log(self.recording.join("\n"))
            }
          }
        ]
      })
  */
  
      //---------------------------------------------------------------------------------
  
      var exportName = df.newdom({
        tagname	: "input",
        target	: LED.pluginwindow,
        attributes:{
          style	: "position:absolute;top:400px;left:300px;font-size:45px;font-family:'BanksiaBold'",
          type	: "text",
          value	: ""
        },
  //			events	: []
      })
  
      var exportbutton2 = df.newdom({
        tagname	: "input",
        target	: LED.pluginwindow,
        attributes:{
          style	: "position:absolute;top:470px;left:300px;font-size:45px;font-family:'BanksiaBold'",
          type	: "button",
          value	: "Export .vid"
        },
        events	: [
          {
            name	: "click",
            handler	: function(evt){
              var binarr = new Uint8Array(self.recordingC)
              var bin = new Blob([binarr], {type: 'application/octet-binary'});
              console.log(bin.size)
  
  
              saveAs(bin, (exportName.value || "testfile") + ".vid");
            }
          }
        ]
      })
  
      //---------------------------------------------------------------------------------
  
      var slidertypes = [
        /*
        {
          id:"Pimphat_rotation_slider",
          name:"Rotation",
          startvalue:self.rotation_angle,
          min:0,
          max:360,
          handler:function(val){
            self.rotation_angle = val
          }
        },
        */
        {
          id:"Pimphat_brightness_slider",
          name:"Brightness",
          startvalue:self.brightness,
          min:0,
          max:1,
          handler:function(val){
            self.brightness = val
          }
        },
        {
          id:"recordvid_pixel_radius",
          name:"Pixel Average Radius",
          startvalue:self.pixelRadius,
          min:0,
          max:10,
          handler:function(val){
            self.pixelRadius = Math.round(val)
            return Math.round(val + 1)
          }
        },
        {
          id:"recordvid_display_radius",
          name:"Pixel Display Radius",
          startvalue:self.pixelDisplayRadius,
          min:0,
          max:39,
          handler:function(val){
            val = val + 1;
            self.pixelDisplayRadius = Math.round(val)
            return Math.round(val+1)
          }
        }
      ]
  
      var sliders = LED.sliderList(slidertypes)
  
      this.positionDiv = df.newdom({
          tagname		: "div",
          target		: LED.pluginwindow,
          attributes	: {
            id		: "sliders",
            style	: "position:absolute;left:587px;top:6px;color:white;"
          },
          children	: sliders
      })
  /*
      self.rotation = df.newdom({
        tagname	: "select",
        target	: LED.pluginwindow,
        attributes	: {
          style	: "position:absolute;top:180px;left:20px;font-size:30px;font-family:'BanksiaBold'"
        },
        children:[
          {
            tagname:"option",
            attributes:{
              value	: 0,
              html	: "0 Deg"
            }
          },
          {
            tagname:"option",
            attributes:{
              value	: 90,
              html	: "90 Deg"
            }
          },
          {
            tagname:"option",
            attributes:{
              value	: 180,
              html	: "180 Deg"
            }
          },
          {
            tagname:"option",
            attributes:{
              value	: 270,
              html	: "270 Deg"
            }
          }
        ],
        events:[
          {
            name	: "change",
            handler	: function(evt){
  
              self.rotation_angle = evt.srcElement.children[evt.srcElement.selectedIndex].value
  
            }
          }
  
        ]
      })
  */

    },
  
  
  };
  
  LED.plugins.push(self);
  
  
  })();
  