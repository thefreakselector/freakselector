(function(){

var self = {
	name		: "Blinky Output",
//	icon		: "Test Patterns.png",

  auto_start: false,

  sort_top: true,

  output_plugin: true,
  render_last: true,

	brightness:	.5,

	blinkyoutput_domain: "192.168.1.126",
	blinkyoutput_port: 80,
	use_websocket: false,

  rotation_angle: 90,
  pixelRadius:0,

  active: false,

	start:	function(){
    self.active = true;
    setTimeout(self.connectLocalblinkyoutput, 1000);

    self.getBaseIPAddress(function(data){
console.log(data)
    })
	},

	end:	function(){
		if (self.websocket){
			self.websocket.close();
			self.websocket = undefined;
		}
    self.active = false;
    self.mode = undefined;
	},

//************************************************************************************************

render: function(){
	if (!self.websocket){
		return
	}
	self.most_recent_frame = Array.apply(null,LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height).data)

  self.sendFrame_Websocket()
},

//************************************************************************************************

sendFrame_Websocket:	function(frame){
  if (!self.active){return}

  var adjusted_frame = self.mapPixels()

  console.log("readystate:",self.websocket.readyState , self.websocket.OPEN)
  if (self.use_websocket === true && self.websocket && self.websocket.readyState === self.websocket.OPEN) {

  //console.log(">>>>>adjusted_frame",adjusted_frame)
  /*
    var fixed_frame = [];
    while(adjusted_frame.length > 0){
      var seg = adjusted_frame.splice(0,64*3);
      //seg = seg.concat([0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0]);
      fixed_frame = fixed_frame.concat(seg);
    }
    packet = new Uint8Array([ 0,0,0,0].concat(fixed_frame));
  */
  console.log(">>>>>>>>>>",adjusted_frame)
    packet = new Uint8Array([].concat(adjusted_frame));
  //	packet = new Uint8Array(fixed_frame);
    self.websocket.send(packet.buffer);
  //console.log(">>>>>packet",packet.buffer)
  }
/*

if (self.use_websocket !== true) {
	  df.ajax.send({
    url   : 'http://'+self.blinkyoutput_domain+'/videoframe' ,
    verb: "get",
//		verb:"post",
		//formdata:JSON.stringify( savedData )
  });
}
*/

/*
	var dataframe = window.btoa("MCX" + String.fromCharCode(self.pixel_map.length	) + adjusted_frame.join(""))
	self.tmr_before_send = new Date();
	self.opencom.writeAsync(dataframe,true)
	self.tmr_after_send = new Date();
*/

},


//==============================================================================================

  mapPixels:  function(){

	//var thismap = self.pixel_map
	var thismap = [];
	for (var side in LED.pixelmap){
	//	var side = 'front'
		for (var i=0;i<LED.pixelmap[side].length;i++){
	//		LED.pixelmap[side][i].not_mapped = true;
			thismap.push(LED.pixelmap[side][i])
		}
	}

/*
	var side = 'left'
	for (var i=0;i<LED.pixelmap[side].length;i++){
//	for (var i=LED.pixelmap[side].length-1;i>0;i--){
		thismap.push(LED.pixelmap[side][i])
	}

	var side = 'back'
	for (var i=0;i<LED.pixelmap[side].length;i++){
		thismap.push(LED.pixelmap[side][i])
	}
	var side = 'right'
	for (var i=0;i<LED.pixelmap[side].length;i++){
		thismap.push(LED.pixelmap[side][i])
	}
*/

	var data = self.most_recent_frame
	if (!data){return}

	//--------------------------------------------------------------

	function getPixel(x,y){
	  var idx = ((x*4)+(y*LED.grid_width*4))
	  return {r:data[idx],g:data[idx+1],b:data[idx+2],a:data[idx+3]}
	}

	//--------------------------------------------------------------

      function getMatrix(x,y,r){
        var matrix = {}

        var rr = 0
        var gg = 0
        var bb = 0
        var aa = 0
        var count = 0
        var pixel = {}
        for (var yy=y-r;yy<y+r;yy++){
          for (var xx=x-r;xx<x+r;xx++){
            if (xx<0){continue}
            if (yy<0){continue}
            if (xx>LED.grid_width) {continue}
            if (yy>LED.grid_height){continue}
            pixel = getPixel(Math.round(xx),Math.round(yy))
            rr += pixel.r||0
            gg += pixel.g||0
            bb += pixel.b||0
            aa += pixel.a||0
            count++
          }
        }
        rr = rr / count
        gg = gg / count
        bb = bb / count
        aa = aa / count

        return {r:rr,g:gg,b:bb,a:aa}
      }

      //--------------------------------------------------------------

      var pixels = []
/*
      var theta = self.rotation_angle * (Math.PI/180)
      var s = Math.sin(theta)
      var c = Math.cos(theta)
*/
      var pixel_color

      for (var i=0;i<thismap.length;i++){

      	if (thismap[i].not_mapped){
			pixels.push(0);
			pixels.push(0);
			pixels.push(0);
			continue;
      	}


        var this_x = thismap[i].x
        var this_y = thismap[i].y

        var pixX = this_x// * LED.grid_width;
        var pixY = this_y// * LED.grid_height;

        var pixel_x = Math.round(pixX)
        var pixel_y = Math.round(pixY)

//        if (self.pixelRadius == 0) {
            pixel_color = getPixel(pixel_x,pixel_y)
//       } else {
//            pixel_color = getMatrix(pixel_x,pixel_y,self.pixelRadius)
//        }

        var rx = Math.round(pixel_color.r * self.brightness)
        var gx = Math.round(pixel_color.g * self.brightness)
        var bx = Math.round(pixel_color.b * self.brightness)

        pixels.push(rx)
        pixels.push(gx)
        pixels.push(bx)

      }

      return pixels

  },

//==============================================================================================

  connectWebsocket: function() {
  	if (self.local_blinkyoutput_tmr){
  		clearTimeout(self.local_blinkyoutput_tmr);
  		self.local_blinkyoutput_tmr = undefined;
  	}
    if (self.websocket == undefined) {
      self.websocket = new WebSocket("ws://"+self.blinkyoutput_domain+":"+ self.blinkyoutput_port);
      self.proc1.parentNode.style.background = "yellow";
      self.websocket.onerror=function(event){
        console.log("Error", event, self.websocket);
        self.proc1.parentNode.style.background = "red";
        self.websocket = undefined;
        self.proc1.setState(false)
        self.mode = undefined
      }
      self.websocket.onopen = function(event){
        self.proc1.parentNode.style.background = "";
        self.mode = 1;
	      self.proc1.setState(true)
      }
    }
  },


  //-----------------------------------------------------------------------
  startLocalblinkyoutput: function(val){
    if (val==false) {return true}
    self.proc1.setState(false)
    self.mode = 0
    self.connectLocalblinkyoutput()
  },

	interface:	function(){

    var procwin = df.newdom({
      tagname   : "div",
      target    : LED.pluginwindow,
      attributes  : {
        id    : "bpmSelectWin",
        style : "width:242px;position:absolute;top:6px;left:6px;z-index:5000;color:white;background-color:#7a7a7a;padding:5px;overflow:hidden;border-radius:5px;"
      }
    })

    var namestyle = "width:230px;font-size:20px;margin-bottom:3px;";
    var radio_off = LED.radio_off_image;
    var radio_on  = LED.radio_on_image;
    var radio_w = 40;
    var radio_h = 40;


    self.proc1 = df.newdom({
      tagname   :"df-onoff",
      target    : procwin,
      attributes:{
        id    : "modeSelect1",
        name    : "Use Websocket",
        namestyle : namestyle,
        style   : "position:relative;",
        class   : "onoff",
        value   : (self.mode === 1),
        width   : radio_w,
        height    : radio_h,
        image_on  : radio_on,
        image_off : radio_off,
        onchange  : function(val){

          if (val==false) {return true}

          self.proc1.setState(false)
    //      self.proc2.setState(false)

          self.mode = 1
          self.use_websocket = true;

          self.proc1.parentNode.style.background = "yellow";

          if (self.websocket !== undefined) {
            self.websocket.close();
            self.websocket = undefined;
          }

          self.connectWebsocket();

        }
      }
    });

/*
    self.proc2 = df.newdom({
      tagname   :"df-onoff",
      target    : procwin,
      attributes:{
        id    : "modeSelect1",
        name    : "Use HTTP",
        namestyle : namestyle,
        style   : "position:relative;",
        class   : "onoff",
        value   : (self.mode === 2),
        width   : radio_w,
        height    : radio_h,
        image_on  : radio_on,
        image_off : radio_off,
        onchange  : function(val){

          if (val==false) {return true}

          self.proc1.setState(false)

          self.mode = 2

          self.proc2.parentNode.style.background = "green";
          self.use_websocket = false;
        }
      }
    });
*/

    //----------------------------------------------------------------------
    df.newdom({tagname:"br",target:procwin})

		self.fc_domain = df.newdom({
			tagname	: "input",
      target    : procwin,
			attributes	: {
				type: "text",
				style	: "width:100%;font-size:30px;font-family:'BanksiaBold'",
				value: self.blinkyoutput_domain,
        placeholder: 'Server Address'
			},events:[
        {
          name:'keyup',
          handler: function(evt){
            self.blinkyoutput_domain = self.fc_domain.value;
          }
        }
      ]
		})

		self.fc_port = df.newdom({
			tagname	: "input",
      target    : procwin,
			attributes	: {
				type: "text",
				style	: "width:100%;font-size:30px;font-family:'BanksiaBold'",
				value: self.blinkyoutput_port,
        placeholder: 'Server Port'
			}
		})

    //--------------------------------------
    df.newdom({tagname:"br",target:procwin})
    df.newdom({tagname:"br",target:procwin})

    self.startOSK = df.newdom({
      tagname : "input",
      target    : procwin,
      attributes:{
        style : "width:100%;font-size:45px;font-family:'BanksiaBold'",
        type  : "button",
        value : "Keyboard"
      },
      events  : [
        {
          name  : "click",
          handler : function(evt){
            LED.openOnScreenKeyboard()
          }
        }
      ]
    })

    //--------------------------------------

		var slidertypes = [
			{
				id:"Pimphat_brightness_slider",
				name:"Brightness",
				startvalue:self.brightness,
				width: 820,
				height: 120,
				gadget_size: 120,
				min:0,
				max:1,
				handler:function(val){
					self.brightness = val
				}
			}
		]

		var sliders = LED.sliderList(slidertypes)

		self.positionDiv = df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "sliders",
				style	: "position:absolute;left:5px;top:375px;color:white;"
			},
			children	: sliders
		})

		//---------------------------------------

	},

	//------------------------------------------------------------------

}

self.servers = [];

//----------------------------------------------------------------------------------------------------------

self.addServer = function(config){

  config.websocket = new WebSocket("wss://"+config.domain+":"+ config.port)

  config.websocket.onerror = function(err){
    wssFail(config)
  }

  self.servers.push(config);

  return config
}

//----------------------------------------------------------------------------------------------------------
var error_iframe
function wssFail(config){
  var https_address = "https://"+config.domain+":"+config.port

  $.ajax({
    url   : https_address,
    method: "get",
    type  : "text"
  }).success(function(livereload_response){
    error_iframe.remove()
    error_iframe = undefined;
  }).fail(function(err){
    if (!error_iframe){
      error_iframe = $('<div style="position:fixed;top:20%;left:calc(10% - 2vw);text-align:center;font-size:4vw;border:1vw solid orange;border-radius:1vw;width:80%;max-height:60%;overflow-y:auto;z-index:999999999;background-color:black;color:white;padding:20px;">Can\'t connect to FcWSS blinkyoutput secury server. Click the link below to fix this.<br><a href="'+https_address+'" style="color:#5588ff" target="_accept_security">Click And Accept Security Token.</a></div>')
      $('body').append(error_iframe);
    }
    console.log("livereload err",err)
  });



}

//----------------------------------------------------------------------------------------------------------

self.removeServer = function(config){
  var config = self.getConfig(config);
  for (var i=self.servers.length-1;i>-1;i--){
    if (self.servers[i].serial === serial){
      var config = self.servers.splice(i,1);
      config.websocket.close()
      config = undefined;
      return
    }
  }
}

//----------------------------------------------------------------------------------------------------------

self.getConfig = function(serial){
  if (typeof serial === "object"){
    if (serial.serial){
      return serial
    }
  }
  for (var i=0;i<self.servers.length;i++){
    if (self.servers[i].serial === serial){
      return serial
    }
  }
}

//----------------------------------------------------------------------------------------------------------

self.padPixels = function(count){
  return JSON.parse('['+((count>0) ? '0,0,0' : '') + ((count > 1) ? ',0,0,0'.repeat(count - 1) : '') + ']')
}

//----------------------------------------------------------------------------------------------------------

self.sendFrame_XXX = function(frame_config, frame_data_source){
  var config = self.getConfig(frame_config);

  var frame_data = [].concat(frame_data_source);

  var pixel_frame = [];
  for (var idx=0;idx<8;idx++){
    pixel_frame = pixel_frame.concat( frame_data.splice(0, (config.outputs[idx].pixels*3) ) ).concat( self.padPixels(64-config.outputs[idx].pixels) )
  }

  //var packet = new Uint8Array([ 0,0,0,0].concat(frame_data));
  var packet = JSON.stringify([0,0,0,0].concat(pixel_frame));

  config.websocket.send(packet);

}

//----------------------------------------------------------------------------------------------------------

self.getBaseIPAddress = function(callback){
  //from http://net.ipcalf.com/, also see http://stackoverflow.com/questions/20194722/can-you-get-a-users-local-lan-ip-address-via-javascript
  //
  // NOTE: window.RTCPeerConnection is "not a constructor" in FF22/23
  var RTCPeerConnection = /*window.RTCPeerConnection ||*/ window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

  if (RTCPeerConnection) {

    var rtc = new RTCPeerConnection({iceServers:[]});
    if (1 || window.mozRTCPeerConnection) {      // FF [and now Chrome!] needs a channel/stream to proceed
        rtc.createDataChannel('', {reliable:false});
    };

    rtc.onicecandidate = function (evt) {
        // convert the candidate to SDP so we can run it through our general parser
        // see https://twitter.com/lancestout/status/525796175425720320 for details
        if (evt.candidate) grepSDP("a="+evt.candidate.candidate);
    };
    rtc.createOffer(function (offerDesc) {
        grepSDP(offerDesc.sdp);
        rtc.setLocalDescription(offerDesc);
    }, function (e) { console.warn("offer failed", e); });


    var addrs = Object.create(null);
    addrs["0.0.0.0"] = false;

    function updateDisplay(newAddr) {
      var test_ip = newAddr.match(/([0-9]{1,3}\.){1,3}/igm)
      var is_valid_ip = ((test_ip||[]).length>0)
      if (is_valid_ip && test_ip[0] !== "0.0.0." ) {
        callback(newAddr)
      }
    }

    function grepSDP(sdp) {
        var hosts = [];
        sdp.split('\r\n').forEach(function (line) { // c.f. http://tools.ietf.org/html/rfc4566#page-39
            if (~line.indexOf("a=candidate")) {     // http://tools.ietf.org/html/rfc4566#section-5.13
                var parts = line.split(' '),        // http://tools.ietf.org/html/rfc5245#section-15.1
                    addr = parts[4],
                    type = parts[7];
                if (type === 'host') updateDisplay(addr);
            } else if (~line.indexOf("c=")) {       // http://tools.ietf.org/html/rfc4566#section-5.7
                var parts = line.split(' '),
                    addr = parts[2];
                updateDisplay(addr);
            }
        });
    }

  }else {
    callback()
      //document.getElementById('list').innerHTML = "<code>ifconfig | grep inet | grep -v inet6 | cut -d\" \" -f2 | tail -n1</code>";
      //document.getElementById('list').nextSibling.textContent = "In Chrome and Firefox your IP should display automatically, by the power of WebRTCskull.";
  }

}

//----------------------------------------------------------------------------------------------------------

self.scanForFC = function(baseIP, port, wss, progress, foundblinkyoutput){

  var ip_start = baseIP;

  var ip_base = ip_start.split(".").slice(0,-1).join(".")+"."

  var ip_postfix = parseInt(ip_start.split(".").pop(),10);

  function testIP(ip_addr, callbackSuccess, callback){

    var starttime = Date.now();

    if (progress){progress('start-ws',ip_addr)}

    var ws = new WebSocket( (wss ? 'wss' : 'ws') + "://" + ip_addr + ":" + (port||7890) )

    var ws_tmr = setTimeout(function(){
      if (ws){ws.onerror()};
    },8000)

    ws.onopen = function(){
      clearTimeout(ws_tmr)
      callbackSuccess(ip_addr)
      ws.onerror = ws.onclose = ws.onopen = function(){};
      ws=undefined;
      if (progress){progress('found-blinkyoutput',ip_addr)}
    }

    ws.onclose = function(){
      if (ws){ws.onerror()};
    }

    ws.onerror=function(){
      ws.onerror = ws.onclose = ws.onopen = function(){};
      ws.close();
      clearTimeout(ws_tmr)
      ws=undefined;
      if (progress){progress('no-blinkyoutput',ip_addr)}
    }

    callback()
  }


  function scanIPs(){

    testIP(ip_base+String(ip_postfix), function(found_ip){

      foundblinkyoutput(found_ip)

    }, function(){

      if (ip_postfix<254){
        ip_postfix++
        setTimeout(scanIPs,100)
      }

    });
  }

  scanIPs()



}


	LED.plugins.push(self)

})();
