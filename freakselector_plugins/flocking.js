(function(){

var self = {

	name		: "Flock",
	//icon		: "Video Input.png",
	
	render:	function(){

		self.update()
		LED.composite_ctx.drawImage(self.canvas, 0, 0,LED.grid_width,LED.grid_height)

	},

	
	start:	function(){
	
	},
	
	end:	function() {
		try{clearInterval(self.interval_timer)}catch(e){}

	},
	
	settings_to_save:	[
		"bird_count",
		"bird_speed",
		"bird_size",
		"bird_colorvariation",
		"color_variation",
		"color_red",
		"color_green",
		"color_blue",
		"opacity"
	],
	
	bird_count:	90,
	bird_speed: 3,
	bird_size : 3,
	
	color_variation	: 1,
	color_red		: 255,
	color_green		: 255,
	color_blue		: 0,
	opacity			: 1,
	
	
	interface:	function() {

//================================================================================================================
//================================================================================================================

// Get the canvas DOM element along with it's 2D context
// which is basically what we do our work on


var canvas = self.canvas = df.newdom({
		tagname:"canvas",
		target	: LED.contextHolder,
		attributes:{
			id	: "flock_canvas",
			style	: "position:absolute;top:20px;left:20px;"
		}
	})

var ctx = canvas.getContext('2d')
var window_width = LED.grid_width
var window_height = LED.grid_height

// Set canvas's width and height to that
// of window (the view port)
canvas.width = LED.grid_width;
canvas.height = LED.grid_height;

//==============================================

var width = LED.grid_width;
var height = LED.grid_height;
var timerID = 0;

var c = self.canvas
var ctx = LED.composite_ctx //c.getContext('2d');
c.width = width;
c.height = height;


	var boids = [];

	var speed		= self.bird_speed
	var size		= self.bird_size
	var totalBoids	= self.bird_count
	
	var wait_tmr
	
	var init = function(nowait){
		try{clearTimeout(wait_tmr)}catch(e){}
		if (nowait!=true) {
			wait_tmr = setTimeout(function(){init(true)},20)
			return
		}
		
		speed		= self.bird_speed
		size		= self.bird_size
		totalBoids	= self.bird_count
		
		boids = [];
		
		for (var i = 0; i < totalBoids; i++) {

			var red_color	= Math.floor( (self.color_variation * (Math.floor(Math.random() * 255))) + ((1-self.color_variation)*self.color_red) )
			var green_color	= Math.floor( (self.color_variation * (Math.floor(Math.random() * 255))) + ((1-self.color_variation)*self.color_green) )
			var blue_color	= Math.floor( (self.color_variation * (Math.floor(Math.random() * 255))) + ((1-self.color_variation)*self.color_blue) )
		
			boids.push({
				x: Math.random() * width,
				y: Math.random() * height,
				v: {
					x: Math.random() * 2 - 1,
					y: Math.random() * 2 - 1
				},
				c: 'rgba(' + red_color + ',' + green_color + ',' + blue_color + ', '+self.opacity+')'
			});
		}
		//setInterval(update, 40);	
	}
	
	var calculateDistance = function(v1, v2){
		x = Math.abs(v1.x - v2.x);
		y = Math.abs(v1.y - v2.y);
		
		return Math.sqrt((x * x) + (y * y));
	}
	
	var checkWallCollisions = function(index){
		if (boids[index].x > width) {
			boids[index].x = 0;
		}
		else 
			if (boids[index].x < 0) {
				boids[index].x = width;
			}
		
		if (boids[index].y > height) {
			boids[index].y = 0;
		}
		else 
			if (boids[index].y < 0) {
				boids[index].y = height;
			}
	}
	
	var addForce = function(index, force){
	
		boids[index].v.x += force.x;
		boids[index].v.y += force.y;
		
		magnitude = calculateDistance({
			x: 0,
			y: 0
		}, {
			x: boids[index].v.x,
			y: boids[index].v.y
		});
		
		boids[index].v.x = boids[index].v.x / magnitude;
		boids[index].v.y = boids[index].v.y / magnitude;
	}
	
	//This should be in multiple functions, but this will
	//save tons of looping - Gross!
	var applyForces = function(index){
		percievedCenter = {
			x: 0,
			y: 0
		};
		flockCenter = {
			x: 0,
			y: 0
		};
		percievedVelocity = {
			x: 0,
			y: 0
		};
		count = 0;
		for (var i = 0; i < boids.length; i++) {
			if (i != index) {
			
				//Allignment
				dist = calculateDistance(boids[index], boids[i]);
				
				//console.log(dist);
				if (dist > 0 && dist < 50) {
					count++;
					
					//Alignment
					percievedCenter.x += boids[i].x;
					percievedCenter.y += boids[i].y;
					
					//Cohesion
					percievedVelocity.x += boids[i].v.x;
					percievedVelocity.y += boids[i].v.y;
					//Seperation
					if (calculateDistance(boids[i], boids[index]) < 12) {
						flockCenter.x -= (boids[i].x - boids[index].x);
						flockCenter.y -= (boids[i].y - boids[index].y);
					}
				}
			}
		}
		if (count > 0) {
			percievedCenter.x = percievedCenter.x / count;
			percievedCenter.y = percievedCenter.y / count;
			
			percievedCenter.x = (percievedCenter.x - boids[index].x) / 400;
			percievedCenter.y = (percievedCenter.y - boids[index].y) / 400;
			
			percievedVelocity.x = percievedVelocity.x / count;
			percievedVelocity.y = percievedVelocity.y / count;
			
			flockCenter.x /= count;
			flockCenter.y /= count;
		}
		
		addForce(index, percievedCenter);
		
		addForce(index, percievedVelocity);
		
		addForce(index, flockCenter);
	}
	
	var update = self.update = function(){
	
		
		//clearCanvas();
		for (var i = 0; i < boids.length; i++) {
		
			//Draw boid
			
			ctx.beginPath();
			ctx.strokeStyle = boids[i].c;

			ctx.lineWidth = size;
			ctx.moveTo(boids[i].x, boids[i].y);
			boids[i].x += boids[i].v.x * speed;
			boids[i].y += boids[i].v.y * speed;
			applyForces(i);
			ctx.lineTo(boids[i].x, boids[i].y);
			ctx.stroke();
			ctx.fill();
			
			checkWallCollisions(i);	
			
		}
	}
	
	//Gui uses this to clear the canvas
	 var clearCanvas = function(){
		ctx.fillStyle = 'rgba(0, 0, 0, 1.0)';
		ctx.beginPath();
		ctx.rect(0, 0, width, height);
		ctx.closePath();
		ctx.fill();
	}
    
    init();
//================================================================================================================
//================================================================================================================
//	bird_count:	90,
//	bird_speed: 3,
//	bird_size : 3,


		var sliders=[]

		var slidertypes = [
			{
				id:"flockcount",
				name:"Count",
				startvalue:self.bird_count-1,
				min:0,
				max:99,
				handler:function(val){
					self.bird_count = Math.round(val)+1
					init()
				}
			},
			{
				id:"flocksize",
				name:"Size",
				startvalue:self.bird_speed,
				min:0,
				max:20,
				handler:function(val){
					self.bird_speed = val		
					init()

				}
			},
			{
				id:"flockspeed",
				name:"Speed",
				startvalue:self.bird_size,
				min:0,
				max:30,
				handler:function(val){
					self.bird_size = val					
					init()

				}
			},
			
			{
				id:"flockcolorvar",
				name:"Color Variability",
				startvalue:self.color_variation,
				min:0,
				max:1,
				handler:function(val){
					self.color_variation = val
					init()

				}
			},
			{
				id:"flock_color_red",
				name:"Red",
				startvalue:self.water_color_red,
				min:0,
				max:255,
				handler:function(val){
					self.color_red = val					
					init()

				}
			},
			{
				id:"flock_color_green",
				name:"Green",
				startvalue:self.color_green,
				min:0,
				max:255,
				handler:function(val){
					self.color_green = val					
					init()

				}
			},
			{
				id:"flock_color_blue",
				name:"Blue",
				startvalue:self.color_blue,
				min:0,
				max:255,
				handler:function(val){
					self.color_blue = val					
					init()

				}
			},
			{
				id:"flock_opacity",
				name:"Opacity",
				startvalue:self.opacity,
				min:0,
				max:1,
				handler:function(val){
					self.opacity = val					
					init()

				}
			}
		]
		
		//------------------------------------------------------------------------------------------------------
		
		var thisvalue
		
		var sliders = LED.sliderList(slidertypes)


		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})
		
//==========================================================================================


}



}


	LED.plugins.push(self)

})();

