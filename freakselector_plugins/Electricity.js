(function(){

// forked from akm2's "Noise Abstraction" http://jsdo.it/akm2/ncsw
/**
 * SimplexNoise class,
 * Point class,
 * extend, random
 * @see http://jsdo.it/akm2/2e21
 */


/**
 * Perticle
 */
function Perticle() {
    Akm2.Point.call(this);
    this.latest = new Akm2.Point();
    this.reborn();
}

Perticle.prototype = Akm2.extend(new Akm2.Point(), {
    reborn: function(x, y) {
        this.set(x, y);
        this.latest.set(this);
        this.age = 0;
    }
});


var self = {
	name		: "Electricity",
//	icon		: "Center Fade.png",

  sort_top: true,

	fullscreen	: true,

	haspreview:true,

	PERTICLE_NUM	: 150,
	STEP					: 7,
	Z_INC					: 0.0135,
	Hval					: 20,
	zoff					: 0,
	shadowBlur		: 20,
	angle					: 8,
	arc						: 2,
	lineWidth			: 1,

	perticles			: [],
	simplexNoise	: new Akm2.SimplexNoise(),

	start:	function(){

		if (self.canvas === undefined){
			self.canvas = df.newdom({
				tagname:"canvas",
				target	: LED.contextHolder,
				attributes:{
					id	: "electricity_canvas",
					style	: "position:absolute;top:20px;left:20px;"
				}
			})

			self.canvasWidth = self.canvas.width = LED.grid_width;
			self.canvasHeight = self.canvas.height = LED.grid_height;
			self.context = self.canvas.getContext('2d');
			self.context.globalCompositeOperation = 'lighter';
			/*
				lighter
				screen
				color-dodge
				difference
			*/
			self.context.lineWidth = self.lineWidth;
			self.context.lineCap = self.context.lineJoin = 'round';
		}

		self.perticles = [];
		for (var i = 0; i < self.PERTICLE_NUM; i++) {
		    self.perticles[i] = new Perticle();
		}
	}
	,
	end:	function(){

	},

	play:	function(){
		self.paused = false
	},
	pause:	function(){
		self.paused = true
	},

//--------------------

	render: function(){

    self.context.lineWidth = self.lineWidth;

    self.context.save();
    self.context.globalCompositeOperation = 'source-over';
    self.context.fillStyle = 'hsla(' + self.Hval + ', 100%, 5%, 0.2)';
    self.context.fillRect(0, 0, self.canvasWidth, self.canvasHeight);
    self.context.restore();

    var p, latest, angle;

    self.context.beginPath();
    self.context.strokeStyle = 'hsla(' + self.Hval + ', 100%, 70%, 0.3)';
    for (var i = 0, len = self.perticles.length; i < len; i++) {
    	p = self.perticles[i];
    	latest = p.latest;

    	self.context.moveTo(latest.x, latest.y);
    	self.context.lineTo(p.x, p.y);
    }
    self.context.stroke();

    self.context.save();
    self.context.beginPath();
    self.context.shadowBlur = self.shadowBlur;
    self.context.shadowColor = 'hsla(' + self.Hval + ', 100%, 50%, 0.075)';
    for (i = 0; i < len; i++) {
			p = self.perticles[i];

			self.context.moveTo(p.x + self.STEP, p.y);
      self.context.arc(p.x, p.y, self.STEP, 0, Math.PI * self.arc, false);

      p.latest.set(p);

      angle = Math.PI * self.angle * self.simplexNoise.noise(p.x / self.canvasWidth * 1.75, p.y / self.canvasHeight * 1.75, self.zoff);
      p.offset(Math.cos(angle) * self.STEP, Math.sin(angle) * self.STEP);

    	if (p.x < 0 || p.x > self.canvasWidth || p.y < 0 || p.y > self.canvasHeight) {
    	    p.reborn(Akm2.randUniform(self.canvasWidth), Akm2.randUniform(self.canvasHeight));
    	}
    }
    self.context.fill();
    self.context.restore();

    self.zoff += self.Z_INC;


		var imagedata = self.context.getImageData(0,0,LED.grid_width,LED.grid_height)

		LED.composite_ctx.putImageData(imagedata, 0, 0);

	},

	settings_to_save:[
		'PERTICLE_NUM',
		'STEP',
		'Z_INC',
		'Hval',
		'zoff',
		'shadowBlur',
		'angle',
		'arc',
		'lineWidth'
	],

	interface:	function(){

		var slidertypes = [
			{
				id:"electricity_hue_slider",
				name:"Hue",
				startvalue:self.Hval,
				min:0,
				max:256,
				handler:function(val){
					self.Hval = val
				}
			}
			,
			{
				id:"electricity_Z_INC_slider",
				name:"Z_INC",
				startvalue:self.Z_INC,
				min:0,
				max:1,
				handler:function(val){
					self.Z_INC = val / 10
					return val
				}
			}
			,
			{
				id:"electricity_step_slider",
				name:"Step",
				startvalue:self.STEP,
				min:0,
				max:10,
				handler:function(val){
					self.STEP = val
				}
			}
			,
			{
				id:"electricity_shadowblur_slider",
				name:"Shadowblur",
				startvalue:self.shadowBlur,
				min:0,
				max:50,
				handler:function(val){
					self.shadowBlur = val
				}
			}
			,
			{
				id:"electricity_angle_slider",
				name:"Angle",
				startvalue:self.angle,
				min:0,
				max:20,
				handler:function(val){
					self.angle = val - 10
					return (val - 10).toFixed(2)
				}
			}
			,
			{
				id:"electricity_arc_slider",
				name:"Arc",
				startvalue:self.arc,
				min:0,
				max:10,
				handler:function(val){
					self.angle = val - 5
					return (val - 5).toFixed(2)
				}
			}
			,
			{
				id:"electricity_lineWidth_slider",
				name:"lineWidth",
				startvalue:self.lineWidth,
				min:0,
				max:5,
				handler:function(val){
					self.lineWidth = val
				}
			}
			,
			{
				id:"electricity_PERTICLE_NUM_slider",
				name:"Perticles",
				startvalue:self.PERTICLE_NUM,
				min:0,
				max:500,
				handler:function(val){
					self.PERTICLE_NUM = val
					self.perticles = [];
					for (var i = 0; i < self.PERTICLE_NUM; i++) {
					    self.perticles[i] = new Perticle();
					}
				}
			}

		]



		var sliders = LED.sliderList(slidertypes)

		self.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:10px;top:6px;color:white;width:400px;height:540px;overflow-y:auto;"
				},
				children	: sliders
		})


		df.newdom({
			tagname: 'input',
				target		: LED.pluginwindow,
			attributes:{
				type: 'button',
				style: 'position: absolute;top:10px;left:500px;width:200px;height:40px;cursor: pointer;',
				value: 'Fire'
			},
			events:[
				{
					name: 'click',
					handler: function(evt){
						self.Hval = 33.28;
						self.Z_INC = 0.024;
						self.STEP = 0.5;
						self.shadowBlur = 29;
						self.angle = -1;
						self.arc = 2;
						self.lineWidth = .5;
						self.PERTICLE_NUM = 500;
						self.start();


					}
				}
			]
		})


	}

}

	LED.plugins.push(self)

})();
