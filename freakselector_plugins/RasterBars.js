(function(){

var self = {
	name		: "Raster Bars",
	icon		: "Raster Bars.png",

	//fullscreen	: true,

	haspreview:true,

	settings_to_save	: [
		"rasters",
		"speed",
		"vertical"
	],

	rasters:[],

	speed:.3,

	start:	function(){

		self.rasters = [];

	//	self.interface()
		if (self.rasters.length==0) {

			self.rasters.push({
				y		: 0,
				speed	: 0,
				visible:true,
				resize_height:0,
				gradient: ["ffff002f","00ffffcf","ff800090","ffff002f"],
				height	: LED.grid_height*4

			})

			self.rasters.push({
				y		: 0,
				speed	: 1,
				visible:true,
				resize_height:0,
				gradient: ["00000000","00ff006f","ffffffcf","00800090","00000000"],
				height	: LED.grid_height

			})

			self.rasters.push({
				y		: 0,
				speed	: -.1,
				visible:true,
				resize_height:0,
				gradient: ["0000ff00","0055ff6f","0066ffff","0055ff6f","0000ff00"],
				height	: LED.grid_height

			})

			self.rasters.push({
				y		: 0,
				speed	: -.4,
				visible:true,
				resize_height:0,
				gradient: ["ff000000","ff0000ff","ff000000"],
				height	: 22,
				resize_height:0
			})


console.log(self.rasters)

			df.anim({
				styles:{
					rastery:{
						min:0,
						max:-LED.grid_height,
						steps:55,
						slope:df.slideout,
						loop:true,
						report:function(animobj){
							LED.registry["Raster Bars"].rasters[0].y = Math.round(animobj.styles.rastery.currentval)
						}
					}
				}

			})
		}

	}
	,
	end:	function(){

	},

	play:	function(){
		self.paused = false
	},
	pause:	function(){
		self.paused = true
	},

	vertical: true,

	render: function(){

		var hv = self.vertical||false //horizontal or vertical

		var hvside = (hv)?LED.grid_width:LED.grid_height

		var imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

		for (var i=0;i<self.rasters.length;i++) {

			var thisraster = self.rasters[i]

			if (thisraster.visible === false) {
				continue
			}

			var thisgrads = df.gradient(self.rasters[i].gradient,Math.max(Math.floor(thisraster.height||0),0))
			if (thisgrads.length==0) {
				continue
			}

			var barheight = Math.min(hvside,thisgrads.length)

			if (self.paused!=true) {
				thisraster.height+=(thisraster.resize_height!=undefined)?thisraster.resize_height:0
				if ((thisraster.height<1)||(thisraster.height > (thisraster.maxheight||(hvside*10))) ){thisraster.resize_height=-thisraster.resizeHeight}

				thisraster.y += ((thisraster.speed!=undefined) ? thisraster.speed : 0) * self.speed
				if (thisraster.y<=-(thisraster.height) ){thisraster.y = hvside}
				if (thisraster.y>hvside ){thisraster.y=-thisraster.height}
			}

			var offx=.5
			var x=0;
			var y=0;

			for ((hv)?x=0:y=0;(hv)?(x<LED.grid_width):(y<LED.grid_height);(hv)?x++:y++) {
				var baridx = Math.round(-thisraster.y + ((hv)?x:y) )
				if (thisgrads[baridx]==undefined){continue}

				var alpha = parseInt(thisgrads[baridx].substr(6,2),16)/255
				var alpha_= 1-alpha
				var red = (parseInt(thisgrads[baridx].substr(0,2),16)*alpha)
				var grn = (parseInt(thisgrads[baridx].substr(2,2),16)*alpha)
				var blu = (parseInt(thisgrads[baridx].substr(4,2),16)*alpha)

				for ((hv)?y=0:x=0;(hv)?(y<LED.grid_height):(x<LED.grid_width);(hv)?y++:x++ ) {
					var px = LED.getCanvasIndex(x,y)
					var bar_red = Math.max(Math.min(255,Math.round((imagedata.data[px]*  alpha_) + red)),0)
					var bar_grn = Math.max(Math.min(255,Math.round((imagedata.data[px+1]*alpha_) + grn)),0)
					var bar_blu = Math.max(Math.min(255,Math.round((imagedata.data[px+2]*alpha_) + blu)),0)
					imagedata.data[px]	 = bar_red
					imagedata.data[px+1] = bar_grn
					imagedata.data[px+2] = bar_blu
					imagedata.data[px+3] = 255
				}
			}
		}

		LED.composite_ctx.putImageData(imagedata, 0, 0);

//		LED.composite_ctx.drawImage(self.canvas,0,0)

	},


	interface:	function(){

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id		: "vertical",
				name		: "Vertical/Horizontal",
				style		: "position:absolute;top:50px;left:610px;",
				class		: "onoff",
				value		: self.vertical,
				width		: 40,
				height		: 40,
				onchange	: function(val){
					self.vertical = val
				}
			}
		})


//==========================================================================================




		var sliders=[]

		var slidertypes = [
			{
				id:"rasters_speed",
				name:"Speed",
				startvalue:self.speed,
				min:0,
				max:1,
				handler:function(val){
					self.speed = val
				}
			}
		]

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		var sliders = LED.sliderList(slidertypes)


		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;overflow:hidden;overflow-y:auto;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})

//==========================================================================================
		var rasters_options = [df.newdom({
			tagname:"option",
			attributes:{
				value:"",
				innerHTML:"Select..."
			}
		})];

		for (var i=0;i<self.rasters.length;i++){
			rasters_options.push(df.newdom({
				tagname: "option",
				attributes:{
					value:i,
					innerHTML: "Raster "+i
				}
			}))
		}

		self.rasterSelector = df.newdom({
			tagname: "select",
			target: LED.pluginwindow,
			attributes:{
				style: "position:absolute;top:50px;left:410px;font-size:30px;padding:5px;border-radius:5px;"
			},
			children: rasters_options,
			events:[
				{
					name: "change",
					handler:function(evt){
						editRaster(self.rasterSelector.value)
					}
				}
			]
		})

//==========================================================================================
		self.rasterEditor = df.newdom({
			tagname:"div",
			target:LED.pluginwindow,
			attributes:{
				style:"position:absolute;top:105px;left:6px;width:877px;height:440px;border:1px solid rgb(42, 42, 42);border-radius:6px;background-color:#2a2a2a;"
			}
		})

	 function editRaster(r_idx){

	 		self.rasterEditor.innerHTML = "";

			var raster = self.rasters[r_idx];

			var rasterColors = df.newdom({
				tagname		: "div",
				target		: self.rasterEditor,
				attributes	: {
					id		: "rasterColors",
					style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#000000;width:447px;height:430px;overflow:hidden;overflow-y:auto;border-radius:5px;"
				},
				children	: [

					{
						tagname		: "div",
						attributes	: {
							id		: "sliders",
							style	: "clear:both;position:relative;"
						}
					}
				]
			})

			for (var i=0;i<raster.gradient.length;i++){

				var this_div = df.newdom({
					tagname:"div",
					target:rasterColors,
					attributes:{
						style:"position:relative;top:0px;left:0px;width:100%;margin-bottom:15px;"
					}
				})

				var this_colorpicker = new df.ColorPicker({
					//target: raster_colorpicker_div,
					width : 400,
					height: 50,
					startvalue:raster.gradient[i],
					callback:function(new_color, picker_config){
						raster.gradient[picker_config.idx] = new_color + raster.gradient[picker_config.idx].slice(-2)
					},
					idx: i
				})

				var raster_colorpicker_div = df.newdom({
					tagname:"div",
					target:this_div,
					attributes:{
						style:"position:relative;top:0px;left:0px;width:400px;height:"+this_colorpicker.containerDiv.offsetHeight+"px"
					},
					children:[
						this_colorpicker.containerDiv
					]
				})


				var opacity_slider = df.newdom({
					tagname:"input",
					target : this_div,
					attributes:{
						type:"range",
						step:".01",
						style:"width:402px;height:50px;position:",
						className:"df_slider_race slider-horizontal"
					},
					events:[
						{
							name:"input",
							handler:function(evt){
								evt.cancelBubble=true;
								evt.stopPropagation();
								var opacity = Math.round((evt.srcElement.value / 100) * 255)
								raster.gradient[evt.srcdata.idx] = raster.gradient[evt.srcdata.idx].slice(0,6) + df.gethex(opacity)
							},
							idx:i
						},
						{
							name:"change",
							handler:function(evt){
								evt.cancelBubble=true;
								evt.stopPropagation();
								var opacity = Math.round((evt.srcElement.value / 100) * 255)
								raster.gradient[evt.srcdata.idx] = raster.gradient[evt.srcdata.idx].slice(0,6) + df.gethex(opacity)
							},
							idx:i
						},
						{
							name:"click",
							handler:function(evt){
								evt.cancelBubble=true;
								evt.stopPropagation();
								//evt.preventDefault();
							}
						},
						{
							name:"mousedown",
							handler:function(evt){
								evt.cancelBubble=true;
								evt.stopPropagation();
								//evt.preventDefault();
							}
						},
						{
							name:"mouseup",
							handler:function(evt){
								evt.cancelBubble=true;
								evt.stopPropagation();
								//evt.preventDefault();
							}
						}
					]
				})

				opacity_slider.value = (df.getdec(raster.gradient[i].slice(-2))/256)*100


			}

			//------------------------------------------------------------------------------------------------------

				var sliders=[];

				var slidertypes = [
					{
						id:"raster_width",
						name:"Width",
						startvalue:raster.height,
						min:0,
						max:256,
						width:256,
						handler:function(val){
							raster.height = val;
						}
					},
					{
						id:"raster_speed",
						name:"Speed",
						startvalue:raster.speed,
						min:0,
						max:1,
						width:256,
						handler:function(val){
							return (raster.speed = (val * 10) - 5).toFixed(1)
						}
					}
				]

				var sliders = LED.sliderList(slidertypes)

				df.newdom({
					tagname		: "div",
					target		: self.rasterEditor,
					attributes	: {
						id		: "raster_sliders_"+i,
						style	: "position:absolute;top:0px;left:476px;z-index:5000;color:white;"
					},
					children	: sliders
				})

				df.newdom({
					tagname		:"df-onoff",
					target		: self.rasterEditor,
					attributes:{
						id		: "raster_on_off",
						name		: "Visible",
						style	: "position:absolute;top:50px;left:770px;z-index:5000;",
						class		: "onoff",
						value		: raster.visible,
						width		: 40,
						height		: 40,
						onchange	: function(val){
							raster.visible = val
						}
					}
				})



		}



	}

}

//TODO:  Add opacity slider to each raster color slider area.
//TODO:  Add and Remove colors to a gradient
//TODO:  Need to have Add and Remove buttons to create or remove raster bars.




LED.plugins.push(self);



})();
