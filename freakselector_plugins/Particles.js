(function(){

var self = {

	name		: "Particles",
	//icon		: "Video Input.png",

	render:	function(){

		self.repaint();
		LED.composite_ctx.drawImage( LED.registry["Particles"].canvas, 0, 0, LED.grid_width, LED.grid_height )
	},

	start:	function(){
				self.createParticle();

	},

	end:	function() {
	},


	particles:	[],

	drawParticle: function(particle) {
		var ctx = self.ctx
		// Begin Drawing Path
		ctx.beginPath();
		// Background color for the object that we'll draw
		ctx.fillStyle = particle.bg_color;
		// Draw the arc
		// ctx.arc(x, y, radius, start_angle, end_angle, anticlockwise)
		// angle are in radians
		// 360 degrees = 2p radians or 1 radian = 360/2p = 180/p degrees
		ctx.arc(particle.x, particle.y, particle.radius, 0, Math.PI*2, false);
		// Close Drawing Path
		ctx.closePath();
		// Fill the canvas with the arc
		ctx.fill();
	},

	createParticle:	function() {

		var rgb = tinycolor(self.color).toRgb()

		if (self.color_cycle){

			var hsl = tinycolor(self.color).toHsl()
			hsl.h += self.color_cycle_speed;
			if (hsl.h > 360){hsl.h = 0}
			rgb = tinycolor(hsl).toRgb();
			self.color = rgb
		}

		var	rgba = "rgba("+Math.round(rgb.r)+","+Math.round(rgb.g)+","+Math.round(rgb.b)+","+self.opacity.toFixed(3)+")";

		var particle = {
			x				: LED.grid_width/2,
			y				: LED.grid_height/2,
			x_speed	: self.randomInt(self.max_speed) || .01,
			y_speed	: self.randomInt(self.max_speed) || .01,
			radius	: self.particle_radius,
			bg_color: rgba//hsla

		}
		self.particles.push(particle);
	},

	randomInt: function(max) {
		return  (Math.random() * (max*2) ) - max;
	},

	repaint:	function() {
		// Clear the entire Canvas
		self.ctx.clearRect(0, 0, self.window_width, self.window_height);

		// Re-draw all particles we have in our bag!
		for (var i = self.particles.length - 1 ;i>-1; i--) {

			var particle = self.particles[i];

			self.drawParticle(particle);

			particle.x = particle.x + particle.x_speed;
			particle.y = particle.y + particle.y_speed;

			if( (particle.x >= LED.grid_width) || (particle.x < 0) || (particle.y >= LED.grid_height) || (particle.y < 0) ){

				self.particles.splice(i,1)
				if (self.particles.length < self.max_particles){
					self.createParticle();
				}

			}

		}

		if (self.particles.length < self.max_particles){
			self.createParticle();
		}

	},

	opacity:.5,
	color:"00ff00",
	color_cycle_speed:1,

	max_particles:100,
	max_speed: 3,
	particle_radius: .2,

	interface:	function() {

//================================================================================================================
//================================================================================================================
// Get the canvas DOM element along with it's 2D context
// which is basically what we do our work on

		self.canvas = df.newdom({
			tagname	: "canvas",
			target	: LED.contextHolder,
			attributes:{
				id	: "particles_canvas",
				style	: "position:absolute;top:20px;left:20px;"
			}
		})

		self.ctx = self.canvas.getContext('2d')
		self.window_width  = LED.grid_width
		self.window_height = LED.grid_height

		// Set canvas's width and height to that
		// of window (the view port)
		self.canvas.width = LED.grid_width;
		self.canvas.height = LED.grid_height;


//================================================================================================================
//================================================================================================================
		var sliders=[]

		var slidertypes = [
			{
				id:"max_particles",
				name:"Max Particles",
				startvalue:self.max_particles,
				min:0,
				max:100,
				handler:function(val){
					self.max_particles = val
				}
			},
			{
				id:"max_speed",
				name:"Max Speed",
				startvalue:self.max_speed,
				min:0,
				max:3,
				handler:function(val){
					self.max_speed = val
				}
			},
			{
				id:"particle_radius",
				name:"Particle Radius",
				startvalue:self.particle_radius,
				min:0,
				max:3,
				handler:function(val){
					self.particle_radius = val
				}
			},
			{
				id:"particle_color_cyclespeed",
				name:"Color Cycle Speed",
				startvalue:self.color_cycle_speed,
				min:0,
				max:3,
				handler:function(val){
					self.color_cycle_speed = val
				}
			},

			{
				id:"particle_opacity",
				name:"Opacity",
				startvalue:self.opacity,
				min:0,
				max:1,
				handler:function(val){
					self.opacity = val
				}
			}
		]

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		var sliders = LED.sliderList(slidertypes)


		var this_colorpicker = new df.ColorPicker({
			//target: raster_colorpicker_div,
			width : 300,
			height: 50,
			startvalue: self.color,
			callback:function(new_color, picker_config){
				self.color = new_color;
			}
		})
		sliders.push(this_colorpicker.containerDiv)

				

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})


		var color_cycle = df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id		: "vertical",
				name		: "Color Cycle",
				style		: "position:absolute;top:10px;left:400px;",
				class		: "onoff",
				value		: self.color_cycle,
				width		: 40,
				height		: 40,
				onchange	: function(val){
					self.color_cycle = val
				}
			}
		})		
		


	}



}

	LED.plugins.push(self)

})();





