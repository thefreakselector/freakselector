(function(){

var self = {

	name		: "Settings",
	icon		: "Settings.png",

  sort_top: true,

	start:	function(){

	},

	settings_to_save	: [
	],

  fps_count: 0,
  fps_last_time: -1,
  render: function(){
    var fps_this_time = new Date().getSeconds();
    if (fps_this_time !== self.fps_last_time) {
      if (self.fps_last_time === -1) {
        self.fpsCounter.innerHTML = "FPS: ??"
      } else {
        self.fpsCounter.innerHTML = "FPS: "+self.fps_count;
      }
      self.fps_last_time = fps_this_time;
      self.fps_count = 0;
    }
    self.fps_count++
  },

	interface:	function(){

    self.fps_last_time = -1;
    self.fps_count = 0;

    self.fpsCounter = df.newdom({
      tagname: 'div',
      target: LED.pluginwindow,
      attributes:{
        html: 'FPS: ??',
        style: 'position:absolute;top:-40px;left:428px;background-color:#cacaca;border:#808080;color:#303030;border-radius:4px;font-family:arial;font-size:20px;padding:5px;'
      }
    })

		var sliders=[]

		var slider_tmr
		var slidertypes = [

		//NIRCMD
			{
				id:"lcdbright",
				name:"LCD Brightness",
				startvalue: LED.LCD_brightness,
				min:0,
				max:1,
				handler:function(val){

					var newval = Math.floor((val*10))/10

					if (LED.LCD_brightness != newval) {
						LED.LCD_brightness = newval
							try{clearTimeout(slider_tmr)}catch(e){}
							slider_tmr = setTimeout(function(){
								df.ajax.send({
								url	: "nircmd/"+LED.base64encode("setbrightness "+(LED.LCD_brightness*100)+" 3")
								})
							},20)
					}

					return LED.LCD_brightness
				}
			},

			{
				id:"sysvolume",
				name:"System Volume",
				startvalue:LED.system_volume||1,
				min:0,
				max:1,
				handler:function(val){

					if (LED.system_volume != val) {
						LED.setSystemVolume(val)
					}

					return Math.round(val*100)
				}
			},

			//Other stuff

			{
				id:"musicvolume",
				name:"Volume",
				min:0,
				max:1,
				startvalue:LED.master_volume,
				tracker:function() {return LED.master_volume},
				handler:function(val){
					LED.volume_follow_screen = true

					LED.master_volume = val
				}
			}

		]

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		var sliders = LED.sliderList(slidertypes)


		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})

    var button_top_px = 10;
    var button_left_px = 365;
		var button_height = 60;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "fastrender",
				name		: 'Fast Render',
				namestyle	: "width:183px;",
				style		: "position:absolute;left:"+button_left_px +"px;top:"+button_top_px+"px;",
				className	: "onoff",
				width		: 45,
				value		: LED.render_fast,
				onchange	: function(val){
					LED.render_fast = val
				}
			}
		})

		button_top_px = button_top_px + button_height;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "fullscreen",
				name		: 'FullScreen',
				namestyle	: "width:183px;",
        style   : "position:absolute;left:"+button_left_px +"px;top:"+button_top_px+"px;",
				className	: "onoff",
				value		: (document.fullscreenEnabled || document.mozFullscreenEnabled || document.webkitIsFullScreen ? true : false),
				width		: 45,
				onchange	: function(val){
					LED.full_screen = val
					if (val){
						document.body.webkitRequestFullScreen(document.body.ALLOW_KEYBOARD_INPUT)
					} else {
						document.webkitCancelFullScreen(document.body.ALLOW_KEYBOARD_INPUT)
					}

				}
			}
		})


		button_top_px = button_top_px + button_height;

		df.newdom({
			tagname		:"df-onoff",
			target		: LED.pluginwindow,
			attributes:{
				id			: "resizing",
				name		: 'Scale UI',
				namestyle	: "width:183px;",
        style   : "position:absolute;left:"+button_left_px +"px;top:"+button_top_px+"px;",
				className	: "onoff",
				value		: LED.allow_scaling,
				width		: 45,
				onchange	: function(val){
					LED.allow_scaling = val
					LED.windowResize()

				}
			}
		})


		df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				type	: "button",
				style	: "background-color:red;color:white;position:absolute;width:210px;top:300px;left:660px;font-family:arial;font-size:40px;border:1px solid black;border-radius:8px;padding:5px;padding-left:10px;padding-right:10px;",
				value	: "Restart"
			},
			events:[
				{
					name	: "click",
					handler	: function() {
						var okay = confirm("Restart the Freak Selector?",function(result){
							if (result == true) {
								location.reload()
							}
						},"#ff0000","#ffffff",20,LED.LEDwindow)
					}
				}
			]
		})


		df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				id		: "quitbutton",
				type	: "button",
				style	: "background-color:red;color:white;position:absolute;width:210px;top:390px;left:660px;font-family:arial;font-size:40px;border:1px solid black;border-radius:8px;padding:5px;padding-left:10px;padding-right:10px;",
				value	: "Quit"
			},
			events:[
				{
					name	: "click",
					handler	: function() {
						var okay = confirm("QUIT the Freak Selector?",function(result){
							if (result == true) {
								df.ajax.send({
									url	: "nircmd/"+LED.base64encode('killprocess "chrome.exe"')
								})
							}
						},"#ff0000","#ffffff",20,LED.LEDwindow)


					}
				}
			]
		})



		df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				id		: "shutdownbutton",
				type	: "button",
				style	: "background-color:red;color:black;position:absolute;width:210px;top:480px;left:660px;font-family:arial;font-size:40px;border:1px solid black;border-radius:8px;padding:5px;padding-left:10px;padding-right:10px;",
				value	: "Shutdown"
			},
			events:[
				{
					name	: "click",
					handler	: function() {
						var okay = confirm("Shut Down?",function(result){
							if (result == true) {
								df.ajax.send({
									url	: "nircmd/"+LED.base64encode('exitwin shutdown force')
								})
							}
						},"#ff0000","#ffffff",20,LED.LEDwindow)


					}
				}
			]
		})



		df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				id		: "turndisplayoff",
				type	: "button",
				style	: "height:174px;background-color: rgb(202, 202, 202);color:black;position:absolute;width:320px;top:10px;left:565px;font-family:arial;font-size:40px;border:1px solid black;border-radius:8px;padding:5px 10px;",
				value	: "Turn Display Off"
			},
			events:[
				{
					name	: "click",
					handler	: function() {

							df.ajax.send({
								url	: "nircmd/"+LED.base64encode('screensaver')
							})

					}
				}
			]
		})



	}

}

LED.plugins.push(self);


})();
