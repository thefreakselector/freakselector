(function(){

var self = {
	name		: "TestString175x1",
//	icon		: "Test Patterns.png",

//	render_last	: true,
  sort_top: true,

	brightness:	1,

	//175 x 1 strip
	pixel_map	: [{"x":0,"y":0},{"x":1,"y":0},{"x":2,"y":0},{"x":3,"y":0},{"x":4,"y":0},{"x":5,"y":0},{"x":6,"y":0},{"x":7,"y":0},{"x":8,"y":0},{"x":9,"y":0},{"x":10,"y":0},{"x":11,"y":0},{"x":12,"y":0},{"x":13,"y":0},{"x":14,"y":0},{"x":15,"y":0},{"x":16,"y":0},{"x":17,"y":0},{"x":18,"y":0},{"x":19,"y":0},{"x":20,"y":0},{"x":21,"y":0},{"x":22,"y":0},{"x":23,"y":0},{"x":24,"y":0},{"x":25,"y":0},{"x":26,"y":0},{"x":27,"y":0},{"x":28,"y":0},{"x":29,"y":0},{"x":30,"y":0},{"x":31,"y":0},{"x":32,"y":0},{"x":33,"y":0},{"x":34,"y":0},{"x":35,"y":0},{"x":36,"y":0},{"x":37,"y":0},{"x":38,"y":0},{"x":39,"y":0},{"x":40,"y":0},{"x":41,"y":0},{"x":42,"y":0},{"x":43,"y":0},{"x":44,"y":0},{"x":45,"y":0},{"x":46,"y":0},{"x":47,"y":0},{"x":48,"y":0},{"x":49,"y":0},{"x":50,"y":0},{"x":51,"y":0},{"x":52,"y":0},{"x":53,"y":0},{"x":54,"y":0},{"x":55,"y":0},{"x":56,"y":0},{"x":57,"y":0},{"x":58,"y":0},{"x":59,"y":0},{"x":60,"y":0},{"x":61,"y":0},{"x":62,"y":0},{"x":63,"y":0},{"x":64,"y":0},{"x":65,"y":0},{"x":66,"y":0},{"x":67,"y":0},{"x":68,"y":0},{"x":69,"y":0},{"x":70,"y":0},{"x":71,"y":0},{"x":72,"y":0},{"x":73,"y":0},{"x":74,"y":0},{"x":75,"y":0},{"x":76,"y":0},{"x":77,"y":0},{"x":78,"y":0},{"x":79,"y":0},{"x":80,"y":0},{"x":81,"y":0},{"x":82,"y":0},{"x":83,"y":0},{"x":84,"y":0},{"x":85,"y":0},{"x":86,"y":0},{"x":87,"y":0},{"x":88,"y":0},{"x":89,"y":0},{"x":90,"y":0},{"x":91,"y":0},{"x":92,"y":0},{"x":93,"y":0},{"x":94,"y":0},{"x":95,"y":0},{"x":96,"y":0},{"x":97,"y":0},{"x":98,"y":0},{"x":99,"y":0},{"x":100,"y":0},{"x":101,"y":0},{"x":102,"y":0},{"x":103,"y":0},{"x":104,"y":0},{"x":105,"y":0},{"x":106,"y":0},{"x":107,"y":0},{"x":108,"y":0},{"x":109,"y":0},{"x":110,"y":0},{"x":111,"y":0},{"x":112,"y":0},{"x":113,"y":0},{"x":114,"y":0},{"x":115,"y":0},{"x":116,"y":0},{"x":117,"y":0},{"x":118,"y":0},{"x":119,"y":0},{"x":120,"y":0},{"x":121,"y":0},{"x":122,"y":0},{"x":123,"y":0},{"x":124,"y":0},{"x":125,"y":0},{"x":126,"y":0},{"x":127,"y":0},{"x":128,"y":0},{"x":129,"y":0},{"x":130,"y":0},{"x":131,"y":0},{"x":132,"y":0},{"x":133,"y":0},{"x":134,"y":0},{"x":135,"y":0},{"x":136,"y":0},{"x":137,"y":0},{"x":138,"y":0},{"x":139,"y":0},{"x":140,"y":0},{"x":141,"y":0},{"x":142,"y":0},{"x":143,"y":0},{"x":144,"y":0},{"x":145,"y":0},{"x":146,"y":0},{"x":147,"y":0},{"x":148,"y":0},{"x":149,"y":0},{"x":150,"y":0},{"x":151,"y":0},{"x":152,"y":0},{"x":153,"y":0},{"x":154,"y":0},{"x":155,"y":0},{"x":156,"y":0},{"x":157,"y":0},{"x":158,"y":0},{"x":159,"y":0},{"x":160,"y":0},{"x":161,"y":0},{"x":162,"y":0},{"x":163,"y":0},{"x":164,"y":0},{"x":165,"y":0},{"x":166,"y":0},{"x":167,"y":0},{"x":168,"y":0},{"x":169,"y":0},{"x":170,"y":0},{"x":171,"y":0},{"x":172,"y":0},{"x":173,"y":0},{"x":174,"y":0}],

	start:	function(){

		if (self.socket === undefined){
			df.ajax.send({
				url: '/bluetoothServer',
				callback: function(ajaxobj){
					var serverInfo = JSON.parse(ajaxobj.text);

				//	self.socketConnect(serverInfo.bluetoothWSPort)


				}
			})
		}


		df.ajax.send({
			url: '/serialPortList',
			callback: function(ajaxobj){
				var serialPorts = JSON.parse(ajaxobj.text);

				console.log('>>>>>serialports:',serialPorts)

			}
		})

		self.getBluetoothSerialports();

/*
		if (self.COMplugin == undefined) {

			self.COMplugin = df.newdom({
				tagname		: "object",
				target		: LED.LEDholder,
				attributes	: {
					id		: "PimpHat_COMplugin",
					type	: "application/x-comadapter",
					style	: "position:absolute;top:-205px;left:-205px;height:100px;width:100px;z-index:50000;"
				}
			})
		}
*/

	},

	getBluetoothSerialports: function(){
		df.ajax.send({
			url: '/bluetoothDeviceList',
			callback: function(ajaxobj){
				self.bluetooth_device_list = JSON.parse(ajaxobj.text);
				self.drawBtDeviceList();
			}
		})
	},

	drawBtDeviceList: function(){
		if (self.has_interface && self.bluetooth_device_list){
			if (self.bt_device_list_holder) {
				self.bt_device_list_holder.innerHTML = ''
			} else {
				self.bt_device_list_holder = df.newdom({
					tagname: 'div',
					target: LED.pluginwindow,
					attributes:{
						style: "position:absolute;top:235px;left:10px;background-color:#cacaca;width:280px;height:300px;"
					}
				})
			}

			for (var i=0;i<self.bluetooth_device_list.length;i++){
				var dev = self.bluetooth_device_list[i];
				df.newdom({
					tagname: 'input',
					target: self.bt_device_list_holder,
					attributes:{
						value: dev.name,
						type: 'button',
						style: "width: 100%;height: 40px;text-align:center;line-height:30px;font-size:26px;background-color:white;color:black;padding:5px;margin-bottom:5px;cursor:pointer;"
					},
					events:[
						{
							name: 'click',
							handler: function(evt){
								var address = evt.srcdata.data.address;
								df.ajax.send({
									url: '/bluetoothDeviceConnect?address='+address,
									callback: function(ajaxobj){
										console.log('>>>>>>>>>>>>>>>>bt connected:',ajaxobj.text)
										self.btAddress = address;
										var btPort = JSON.parse(ajaxobj.text);
								    self.socketConnect(btPort.bluetoothWSServerPort);
									}
								})
							},
							data: {
								address: dev.address
							}
						}
					]
				})
				console.log('>>>dev:',dev)
			}

		}
	},

	socket_connected: false,
	clear_to_send: true,

  socketConnect: function(port){
      self.socket = new WebSocket( ((location.protocol==="https:")?'wss:':'ws:')+'//localhost:'+(port || 3300))

      self.socket.onopen = function(){
        console.log('bluetooth connected')
        self.socket_connected = true
//        if (tab_needs_reload == true){
  //        doReload()
    //    }

    	      	self.clear_to_send === true
//self.sendFrame()

	      self.socket.onmessage = function(message){
	      	console.log('>>>message froms bluetoothWS:',message.data)
	      	self.clear_to_send = true
	      }

	    }
      self.socket.onclose = function(){
        self.socket = undefined;
 	      	self.clear_to_send = false
//       setTimeout(self.socketReconnect,500)
        console.log('bluetoothWS closed')
      }

  },

  socketReconnect: function(){
    console.log("socket reconnect")
//    tab_needs_reload = true;
    self.socketConnect()
  },

  socketDisconnect: function(){
      self.socket.close()
      self.socket = undefined;
      self.socket_connected = false;
 	    self.clear_to_send = false;
 },

//************************************************************************************************

	end:	function(){
		self.socketDisconnect();
	},

	isrecording:false,

	render: function(){
		self.most_recent_frame = Array.apply(null,LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height).data)

//		if (self.clear_to_send === true) {
//			self.sendFrame()
	//	}
		if (self.isrecording === true){
			self.mapPixels()
		}
	},

	sendFrame: function(){
		if (self.clear_to_send === false){
			return
		}
		if (self.socket_connected){
			self.clear_to_send = false;
			var adjusted_frame = self.mapPixels();
			var send_frame = "MCX" + String.fromCharCode(self.pixel_map.length) + adjusted_frame.join("");
			console.log("Sending Frame to WS")
			self.socket.send(JSON.stringify({
				address: self.btAddress,
				data:  btoa(send_frame)
			}));
		}
	},

	sendFrameX:	function(){

		self.clear_to_send = false

		var adjusted_frame = self.mapPixels()

		var dataframe = window.btoa("MCX" + String.fromCharCode(self.pixel_map.length) + adjusted_frame.join(""))
		self.tmr_before_send = new Date();
		self.opencom.writeAsync(dataframe,true)
		self.tmr_after_send = new Date();
	},

	rotation_angle: 90,

//==============================================================================================

    mapPixels_working:  function(){
        var thismap = self.pixel_map

        var data = self.most_recent_frame

        function getPixel(x,y){
            var idx = ((x*4)+(y*LED.grid_width*4))
            return {r:data[idx],g:data[idx+1],b:data[idx+2],a:data[idx+3]}
        }

        var pixels = []

        var theta = self.rotation_angle * (Math.PI/180)
        var s = Math.sin(theta)
        var c = Math.cos(theta)

        for (var i=0;i<thismap.length;i++){
            var this_x = thismap[i].x
            var this_y = thismap[i].y
/*
            var rot_x = this_x -= .5 // for centering
            var rot_y = this_y -= .5 // for centering

            var new_x = rot_x * c - rot_y * s
            var new_y = rot_y * s - rot_x * c

            this_x = new_x += .5
            this_y = new_y += .5
*/
            var pixel_x = Math.round(this_x * LED.grid_width)
            var pixel_y = Math.round(this_y * LED.grid_height)
            var pixel_color = getPixel(pixel_x,pixel_y)
            var rx = Math.round(pixel_color.r * self.brightness)
            var gx = Math.round(pixel_color.g * self.brightness)
            var bx = Math.round(pixel_color.b * self.brightness)
            pixels.push(String.fromCharCode(gx))
            pixels.push(String.fromCharCode(rx))
            pixels.push(String.fromCharCode(bx))
        }

if (self.isrecording==true){
    self.frames.innerHTML = pixels.length / (thismap.length * 3);
}
        return pixels

    },

//==============================================================================================
    pixelRadius:0,

    mapPixels:  function(){

        var thismap = self.pixel_map

        var data = self.most_recent_frame

        function getPixel(x,y){
            var idx = ((x*4)+(y*LED.grid_width*4))
            return {r:data[idx],g:data[idx+1],b:data[idx+2],a:data[idx+3]}
        }
        function getMatrix(x,y,r){
            var matrix = {}

            var rr = 0
            var gg = 0
            var bb = 0
            var aa = 0
            var count = 0
            var pixel = {}
            for (var yy=y-r;yy<y+r;yy++){
                for (var xx=x-r;xx<x+r;xx++){
                    if (xx<0){continue}
                    if (yy<0){continue}
                    if (xx>LED.grid_width) {continue}
                    if (yy>LED.grid_height){continue}
                    pixel = getPixel(Math.round(xx),Math.round(yy))
                    rr += pixel.r||0
                    gg += pixel.g||0
                    bb += pixel.b||0
                    aa += pixel.a||0
                    count++
                }
            }
            rr = rr / count
            gg = gg / count
            bb = bb / count
            aa = aa / count

            return {r:rr,g:gg,b:bb,a:aa}
        }

        var pixels = []

        var theta = self.rotation_angle * (Math.PI/180)
        var s = Math.sin(theta)
        var c = Math.cos(theta)

        var pixel_color

        for (var i=0;i<thismap.length;i++){
            var this_x = thismap[i].x
            var this_y = thismap[i].y
/*
            var rot_x = this_x -= .5 // for centering
            var rot_y = this_y -= .5 // for centering

            var new_x = rot_x * c - rot_y * s
            var new_y = rot_y * s - rot_x * c

            this_x = new_x += .5
            this_y = new_y += .5
*/
            var pixX = this_x * LED.grid_width;
            var pixY = this_y * LED.grid_height;

            var pixel_x = Math.round(pixX)
            var pixel_y = Math.round(pixY)

            if (self.pixelRadius == 0) {
                pixel_color = getPixel(pixel_x,pixel_y)
            } else {
                pixel_color = getMatrix(pixel_x,pixel_y,self.pixelRadius)
            }

            var rx = Math.round(pixel_color.r * self.brightness)
            var gx = Math.round(pixel_color.g * self.brightness)
            var bx = Math.round(pixel_color.b * self.brightness)

            pixels.push(String.fromCharCode(gx))
            pixels.push(String.fromCharCode(rx))
            pixels.push(String.fromCharCode(bx))


						if (self.isrecording==true){
							self.recordingC.push(rx)
							self.recordingC.push(gx)
							self.recordingC.push(bx)
						}
        }

if (self.isrecording==true){
    //self.frames.innerHTML = pixels.length / (thismap.length * 3);
}
console.log(JSON.stringify(pixels))
        return pixels

    },

//==============================================================================================

	recording: 	[],
	recordingC: [],

	interface:	function(){


		var isrecording = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:100px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "Record"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){
						self.isrecording = true
					}
				}
			]
		})

		var stoprecording = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:175px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "Stop Record"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){
						self.isrecording = false
					}
				}
			]
		})

		self.frames = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:240px;left:300px;font-size:45px;font-family:'BanksiaBold';height:50px;border:1px solid black;background-color:white;padding:3px;"
			}
		})

		var exportbutton2 = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:380px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "Export .bin"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){
						var binarr = new Uint8Array(self.recordingC)
						var bin = new Blob([binarr], {type: 'application/octet-binary'});
						console.log(bin.size)
						saveAs(bin, "testfile.bin");
					}
				}
			]
		})


		var slidertypes = [
			{
				id:"Pimphat_rotation_slider",
				name:"Rotation",
				startvalue:self.rotation_angle,
				min:0,
				max:360,
				handler:function(val){
					self.rotation_angle = val
				}
			},
			{
				id:"Pimphat_brightness_slider",
				name:"Brightness",
				startvalue:self.brightness,
				min:0,
				max:1,
				handler:function(val){
					self.brightness = val
				}
			}
		]

		var sliders = LED.sliderList(slidertypes)

		this.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:587px;top:6px;color:white;"
				},
				children	: sliders
		})

		self.rotation = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:180px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:[
				{
					tagname:"option",
					attributes:{
						value	: 0,
						html	: "0 Deg"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: 90,
						html	: "90 Deg"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: 180,
						html	: "180 Deg"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: 270,
						html	: "270 Deg"
					}
				}
			],
			events:[
				{
					name	: "change",
					handler	: function(evt){

						self.rotation_angle = evt.srcElement.children[evt.srcElement.selectedIndex].value

					}
				}

			]


		})

		setTimeout(self.drawBtDeviceList, 100)


	},

	//------------------------------------------------------------------

	newdata:	function(data){

		if (data == "M") {
//console.log("newdata",data)

			self.sendFrame()
		}

	},

	writeDone:	function(evT){
//console.log("writeDone")

	}

};

LED.plugins.push(self);


})();
