(function(){

var self = {
	name		: "RecordVid",
//	icon		: "Test Patterns.png",

//	render_last	: true,
  sort_top: true,

	brightness:	1,


	pixel_map: [],

	start:	function(){
/*
		var max_x = 0;
		var max_y = 0;
		for (var first in LED.pixelmap){break}
		var pixelmap = LED.pixelmap[first];
		for (var i=0;i<pixelmap.length;i++){
			if (pixelmap[i].x > max_x){max_x = pixelmap[i].x}
			if (pixelmap[i].y > max_y){max_y = pixelmap[i].y}
		}
		self.pixel_map = [];
		for (var i=0;i<pixelmap.length;i++){
			var xx = pixelmap[i].x / max_x;
			var yy = pixelmap[i].y / max_y;
			self.pixel_map.push({x:xx,y:yy});
		}

console.log("self.pixel_map:",self.pixel_map)
*/
		//self.pixel_map

		if (self.socket === undefined){
			df.ajax.send({
				url: '/bluetoothServer',
				callback: function(ajaxobj){
					var serverInfo = JSON.parse(ajaxobj.text);

				//	self.socketConnect(serverInfo.bluetoothWSPort)


				}
			})
		}


		df.ajax.send({
			url: '/serialPortList',
			callback: function(ajaxobj){
				var serialPorts = JSON.parse(ajaxobj.text);

				console.log('>>>>>serialports:',serialPorts)

			}
		})

		self.getBluetoothSerialports();

		self.pixel_map = [];
		for (var screen in LED.pixelmap){
			self.pixel_map = self.pixel_map.concat(LED.pixelmap[screen]);
		}

		var max_x = 0;
		var max_y = 0;
		for (var i=0;i<self.pixel_map.length;i++){
			max_x = Math.max(self.pixel_map[i].x, max_x);
			max_y = Math.max(self.pixel_map[i].y, max_y);
		}

		for (var i=0;i<self.pixel_map.length;i++){
			self.pixel_map[i].x = self.pixel_map[i].x / max_x
			self.pixel_map[i].y = self.pixel_map[i].y / max_y
		}

		console.log(self.pixel_map)

/*
		if (self.COMplugin == undefined) {

			self.COMplugin = df.newdom({
				tagname		: "object",
				target		: LED.LEDholder,
				attributes	: {
					id		: "PimpHat_COMplugin",
					type	: "application/x-comadapter",
					style	: "position:absolute;top:-205px;left:-205px;height:100px;width:100px;z-index:50000;"
				}
			})
		}
*/

	},

	getBluetoothSerialports: function(){
		df.ajax.send({
			url: '/bluetoothDeviceList',
			callback: function(ajaxobj){
				self.bluetooth_device_list = JSON.parse(ajaxobj.text);
				self.drawBtDeviceList();
			}
		})
	},

	drawBtDeviceList: function(){
		if (self.has_interface && self.bluetooth_device_list){
			if (self.bt_device_list_holder) {
				self.bt_device_list_holder.innerHTML = ''
			} else {
				self.bt_device_list_holder = df.newdom({
					tagname: 'div',
					target: LED.pluginwindow,
					attributes:{
						style: "position:absolute;top:235px;left:10px;background-color:#cacaca;width:280px;height:300px;"
					}
				})
			}

			for (var i=0;i<self.bluetooth_device_list.length;i++){
				var dev = self.bluetooth_device_list[i];
				df.newdom({
					tagname: 'input',
					target: self.bt_device_list_holder,
					attributes:{
						value: dev.name,
						type: 'button',
						style: "width: 100%;height: 40px;text-align:center;line-height:30px;font-size:26px;background-color:white;color:black;padding:5px;margin-bottom:5px;cursor:pointer;"
					},
					events:[
						{
							name: 'click',
							handler: function(evt){
								var address = evt.srcdata.data.address;
								df.ajax.send({
									url: '/bluetoothDeviceConnect?address='+address,
									callback: function(ajaxobj){
										console.log('>>>>>>>>>>>>>>>>bt connected:',ajaxobj.text)
										self.btAddress = address;
										var btPort = JSON.parse(ajaxobj.text);
								    self.socketConnect(btPort.bluetoothWSServerPort);
									}
								})
							},
							data: {
								address: dev.address
							}
						}
					]
				})
				console.log('>>>dev:',dev)
			}

		}
	},

	socket_connected: false,
	clear_to_send: true,

  socketConnect: function(port){
      self.socket = new WebSocket( ((location.protocol==="https:")?'wss:':'ws:')+'//localhost:'+(port || 3300))

      self.socket.onopen = function(){
        console.log('bluetooth connected')
        self.socket_connected = true
//        if (tab_needs_reload == true){
  //        doReload()
    //    }

    	      	self.clear_to_send === true
//self.sendFrame()

	      self.socket.onmessage = function(message){
	      	console.log('>>>message froms bluetoothWS:',message.data)
	      	self.clear_to_send = true
	      }

	    }
      self.socket.onclose = function(){
        self.socket = undefined;
 	      	self.clear_to_send = false
//       setTimeout(self.socketReconnect,500)
        console.log('bluetoothWS closed')
      }

  },

  socketReconnect: function(){
    console.log("socket reconnect")
//    tab_needs_reload = true;
    self.socketConnect()
  },

  socketDisconnect: function(){
      self.socket.close()
      self.socket = undefined;
      self.socket_connected = false;
 	    self.clear_to_send = false;
 },

//************************************************************************************************

	end:	function(){
		self.socketDisconnect();
	},

	isrecording:false,

	render: function(){
		self.most_recent_frame = Array.apply(null,LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height).data)

//		if (self.clear_to_send === true) {
//			self.sendFrame()
	//	}
		if (self.isrecording === true){
//			self.mapPixels_working()
			self.mapPixels()
		}
	},

	sendFrame: function(){
		if (self.clear_to_send === false){
			return
		}
		if (self.socket_connected){
			self.clear_to_send = false;
			var adjusted_frame = self.mapPixels_working();
			var send_frame = "MCX" + String.fromCharCode(self.pixel_map.length) + adjusted_frame.join("");
			console.log("Sending Frame to WS")
			self.socket.send(JSON.stringify({
				address: self.btAddress,
				data:  btoa(send_frame)
			}));
		}
	},

	sendFrameX:	function(){

		self.clear_to_send = false

		var adjusted_frame = self.mapPixels_working()

		var dataframe = window.btoa("MCX" + String.fromCharCode(self.pixel_map.length) + adjusted_frame.join(""))
		self.tmr_before_send = new Date();
		self.opencom.writeAsync(dataframe,true)
		self.tmr_after_send = new Date();
	},

	rotation_angle: 90,

//==============================================================================================

    mapPixels_working:  function(){
        var thismap = self.pixel_map

        var data = self.most_recent_frame

        function getPixel(x,y){
            var idx = ((x*4)+(y*LED.grid_width*4))
            return {r:data[idx],g:data[idx+1],b:data[idx+2],a:data[idx+3]}
        }

				var csv = []
        var pixels = []

        for (var i=0;i<thismap.length;i++){
            var this_x = thismap[i].x
            var this_y = thismap[i].y

            var pixel_x = Math.round(this_x * (LED.grid_width-1));
            var pixel_y = Math.round(this_y * (LED.grid_height-1));
            var pixel_color = getPixel(pixel_x,pixel_y);
            var rx = Math.round(pixel_color.r * self.brightness);
            var gx = Math.round(pixel_color.g * self.brightness);
            var bx = Math.round(pixel_color.b * self.brightness);
            pixels.push(String.fromCharCode(gx));
            pixels.push(String.fromCharCode(rx));
            pixels.push(String.fromCharCode(bx));
    //        csv.push(gx)
      //      csv.push(rx)
        //    csv.push(bx)
						if (self.isrecording==true){
							self.recordingC.push(rx);
							self.recordingC.push(gx);
							self.recordingC.push(bx);
						}
        }

		    self.frames.innerHTML = self.recordingC.length / self.pixel_map.length;


//				if (self.isrecording==true){
	//			    self.recording.push(  String("M").charCodeAt(0) + "," + String("C").charCodeAt(0) + "," + String("X").charCodeAt(0)+ "," + String(self.pixel_map.length) + "," + csv.join(",") )
		//		    self.frames.innerHTML = self.recording.length
			//	}

        return pixels

    },

//==============================================================================================
    pixelRadius:3,

    mapPixels:  function(){

        var thismap = self.pixel_map

        var data = self.most_recent_frame

        function getPixel(x,y){
            var idx = ((x*4)+(y*LED.grid_width*4))
            return {r:data[idx],g:data[idx+1],b:data[idx+2],a:data[idx+3]}
        }

        function getMatrix(x,y,r){
            var matrix = {}

            var rr = 0
            var gg = 0
            var bb = 0
            var aa = 0
            var count = 0
            var pixel = {}
            for (var yy=y-r;yy<y+r;yy++){
                for (var xx=x-r;xx<x+r;xx++){
                    if (xx<0){continue}
                    if (yy<0){continue}
                    if (xx>LED.grid_width) {continue}
                    if (yy>LED.grid_height){continue}
                    pixel = getPixel(Math.round(xx),Math.round(yy))
                    rr += pixel.r||0
                    gg += pixel.g||0
                    bb += pixel.b||0
                    aa += pixel.a||0
                    count++
                }
            }
            rr = rr / count
            gg = gg / count
            bb = bb / count
            aa = aa / count

            return {r:rr,g:gg,b:bb,a:aa}
        }

        var pixels = []

        var theta = self.rotation_angle * (Math.PI/180)
        var s = Math.sin(theta)
        var c = Math.cos(theta)

        var pixel_color

        for (var i=0;i<thismap.length;i++){
            var this_x = thismap[i].x
            var this_y = thismap[i].y
/*
            var rot_x = this_x -= .5 // for centering
            var rot_y = this_y -= .5 // for centering

            var new_x = rot_x * c - rot_y * s
            var new_y = rot_y * s - rot_x * c

            this_x = new_x += .5
            this_y = new_y += .5
*/
            var pixX = this_x * LED.grid_width;
            var pixY = this_y * LED.grid_height;

            var pixel_x = Math.round(pixX)
            var pixel_y = Math.round(pixY)

            if (self.pixelRadius == 0) {
                pixel_color = getPixel(pixel_x,pixel_y)
            } else {
                pixel_color = getMatrix(pixel_x,pixel_y,self.pixelRadius)
            }

            var rx = Math.round(pixel_color.r * self.brightness)
            var gx = Math.round(pixel_color.g * self.brightness)
            var bx = Math.round(pixel_color.b * self.brightness)

            pixels.push(String.fromCharCode(gx))
            pixels.push(String.fromCharCode(rx))
            pixels.push(String.fromCharCode(bx))

						if (self.isrecording==true){
							self.recordingC.push(rx)
							self.recordingC.push(gx)
							self.recordingC.push(bx)
						}
        }

		    self.frames.innerHTML = self.recordingC.length / self.pixel_map.length;

        return pixels

    },

//==============================================================================================

	recording: 	[],
	recordingC: [],

	interface:	function(){


		var isrecording = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:100px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "Record"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){
						self.isrecording = true
					}
				}
			]
		})

		var stoprecording = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:175px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "Stop Record"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){
						self.isrecording = false
					}
				}
			]
		})

		self.frames = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:240px;left:300px;font-size:45px;font-family:'BanksiaBold';height:50px;border:1px solid black;background-color:white;padding:3px;"
			}
		})

/*
		var exportbutton = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:305px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "Export CSV"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){
console.clear()
console.log(self.recording.join("\n"))
					}
				}
			]
		})
*/

		var clearButton = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:305px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "Clear"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){
						self.recordingC = [];
						self.recording = [];
						self.frames.innerHTML = ""
					}
				}
			]
		})

		//---------------------------------------------------------------------------------

		var exportName = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:400px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "text",
				value	: ""
			},
//			events	: []
		})

		var exportbutton2 = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;top:470px;left:300px;font-size:45px;font-family:'BanksiaBold'",
				type	: "button",
				value	: "Export .vid"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt){
						var binarr = new Uint8Array(self.recordingC)
						var bin = new Blob([binarr], {type: 'application/octet-binary'});
						console.log(bin.size)


						saveAs(bin, (exportName.value || "testfile") + ".vid");
					}
				}
			]
		})

		//---------------------------------------------------------------------------------

		var slidertypes = [
			{
				id:"Pimphat_rotation_slider",
				name:"Rotation",
				startvalue:self.rotation_angle,
				min:0,
				max:360,
				handler:function(val){
					self.rotation_angle = val
				}
			},
			{
				id:"Pimphat_brightness_slider",
				name:"Brightness",
				startvalue:self.brightness,
				min:0,
				max:1,
				handler:function(val){
					self.brightness = val
				}
			}
		]

		var sliders = LED.sliderList(slidertypes)

		this.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:587px;top:6px;color:white;"
				},
				children	: sliders
		})

		self.rotation = df.newdom({
			tagname	: "select",
			target	: LED.pluginwindow,
			attributes	: {
				style	: "position:absolute;top:180px;left:20px;font-size:30px;font-family:'BanksiaBold'"
			},
			children:[
				{
					tagname:"option",
					attributes:{
						value	: 0,
						html	: "0 Deg"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: 90,
						html	: "90 Deg"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: 180,
						html	: "180 Deg"
					}
				},
				{
					tagname:"option",
					attributes:{
						value	: 270,
						html	: "270 Deg"
					}
				}
			],
			events:[
				{
					name	: "change",
					handler	: function(evt){

						self.rotation_angle = evt.srcElement.children[evt.srcElement.selectedIndex].value

					}
				}

			]


		})

		setTimeout(self.drawBtDeviceList, 100)


	},

	//------------------------------------------------------------------

	newdata:	function(data){

		if (data == "M") {
//console.log("newdata",data)

			self.sendFrame()
		}

	},

	writeDone:	function(evT){
//console.log("writeDone")

	}

};

LED.plugins.push(self);


})();
