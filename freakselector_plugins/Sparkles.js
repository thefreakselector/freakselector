(function(){

var self = {


	name		: "Sparkles",
	//icon		: "Video Input.png",

	count: 5,
	speed: 6,
	size : 3,

	color_red		: 0,
	color_green		: 44,
	color_blue		: 255,

	settings_to_save:	[
		"count",
		"speed",
		"size",
		"color_red",
		"color_green",
		"color_blue"
	],

	sparkles: [],

	start:	function(){


		if (self.canvas == undefined) {
			self.canvas = df.newdom({
				tagname:"canvas",
				target	: LED.pluginwindow,//LED.contextHolder,
				attributes:{
					id	: "sparkles_canvas",
					style	: "position:relative;top:20px;left:20px;",
					width	: LED.grid_width,
					height	: LED.grid_height
				}
			})
			self.ctx = self.canvas.getContext('2d')
		}

		self.update();
	},

	end:	function() {

	},

	render:	function(){

		self.ctx.clearRect(0, 0, LED.grid_width, LED.grid_height);

		for (var i=0; i < self.count; i++){
			var s = self.sparkles[i];
			if (s.delay > 0){
				s.delay--
				if (s.delay > 0){
					continue
				}
				s.delay = 0;
			}

			self.ctx.beginPath();
			var circle = new Path2D();
    	circle.moveTo(s.x, s.y);
    	circle.arc(s.x, s.y, s.size * s.fade, 0, 2 * Math.PI);
    	//ctx.arc(x, y, radius, startAngle, endAngle, anticlockwise);
    	self.ctx.fillStyle = 'white';
    	self.ctx.fill(circle);

			s.fade = s.fade - (s.speed / 10);
			if (s.fade <= 0){
				self.sparkles[i] = self.newSparkle();
			}
		}

		LED.composite_ctx.drawImage(self.canvas, 0, 0,LED.grid_width,LED.grid_height)

	},

	newSparkle: function(){
		var random = (Math.random() * (self.speed / 8)) 
		var speed = self.speed + random

		return {
				r: Math.round(Math.random() * self.color_red),
				g: Math.round(Math.random() * self.color_green),
				b: Math.round(Math.random() * self.color_blue),
				speed: speed,
				size: Math.random() * self.size,
				delay: Math.random() * (500 / self.speed),
				fade: 1,
				x: Math.round(Math.random() * LED.grid_width),
				y: Math.round(Math.random() * LED.grid_height)
			}
	},

	update:	function(){

		self.sparkles = [];

		for (var i=0;i<self.count;i++){
			self.sparkles.push(self.newSparkle())
		}

	},


	interface:	function() {

		var sliders=[]

		var slidertypes = [

			{
				id:"sparkle_count",
				name:"Count",
				startvalue:self.count,
				min:1,
				max:100,
				handler:function(val){
					self.count = val;
					self.update();
				}
			},

			{
				id:"sparkle_speed",
				name:"Speed",
				startvalue:self.speed,
				min:0,
				max:100,
				handler:function(val){
					self.speed = val;
					self.update();
				}
			},
			{
				id:"sparkle_size",
				name:"Size",
				startvalue:self.size,
				min:0,
				max:20,
				handler:function(val){
					self.size = val;
					self.update();
				}
			},
			{
				id:"sparkle_red",
				name:"Red",
				startvalue:self.color_red,
				min:0,
				max:100,
				handler:function(val){
					self.color_red = val;
					self.update();
				}
			},

			{
				id:"radar_green",
				name:"Green",
				startvalue:self.color_green,
				min:0,
				max:100,
				handler:function(val){
					self.color_green = val;
					self.update();
				}
			},
			{
				id:"sparkle_blue",
				name:"Blue",
				startvalue:self.color_blue,
				min:0,
				max:100,
				handler:function(val){
					self.color_blue = val;
					self.update();
				}
			}
		]

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		var sliders = LED.sliderList(slidertypes)

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})


//==========================================================================================


}



}


	LED.plugins.push(self)
})();

