(function(){

var self = {
	name		: "Test Patterns",
	icon		: "Test Patterns.png",
	fullscreen	: true,


	start:	function(){

		self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

		self.patterns["All Black"]();
		self.selected_pattern = "All Black";

	},

//************************************************************************************************

	patterns:{

		"All Black":function(){
			self.fill(0,0,0,255)
		},

		"All Red":function(){
			self.fill(255,0,0,255)
		},

		"All Green":function(){
			self.fill(0,255,0,255)
		},

		"All Blue":function(){
			self.fill(0,0,255,255)
		},

		"All White":function(){
			self.fill(255,255,255,255)
		},

		"Pattern 1":	function() {

			self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)

			var data = self.imagedata.data

			var gradient = ["ff0000","ff0000","ffff00","ffff00","00ff00","00ff00","00ffff","00ffff","0000ff","0000ff","ff00ff","ff00ff","ffffff","ffffff","ff0000","ff0000","ffff00","ffff00","00ff00","00ff00","00ffff","00ffff","0000ff","0000ff","ff00ff","ff00ff","ffffff","ffffff"]


			for (var i=0;i<(LED.grid_width*LED.grid_height*4);i++){data[i]=0}

			for (var c=0;c<gradient.length;c++) {
				for (var x=c;x<LED.grid_width-c;x++) {
					for (var y=c;y<LED.grid_height-c;y++) {

						var px = LED.getCanvasIndex(x,y)
						data[px]   = parseInt(gradient[c].substr(0,2),16)
						data[px+1] = parseInt(gradient[c].substr(2,2),16)
						data[px+2] = parseInt(gradient[c].substr(4,2),16)
						data[px+3] = 255
					}
				}
			}


		},

		"Checker":	function() {

			self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)
			var data = self.imagedata.data


			for (var i=0;i<(LED.grid_width*LED.grid_height*4);i++){data[i]=0}

			var c = 0

			for (var y=0;y<LED.grid_height;y++) {
				for (var x=0;x<LED.grid_width;x++) {

					var px = LED.getCanvasIndex(x,y)

					if (c==0){c=255}else{c=0}

					data[px]   = c
					data[px+1] = c
					data[px+2] = c
					data[px+3] = 255
				}
			}



		},

		"Checker 2":	function() {

			self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)
			var data = self.imagedata.data


			for (var i=0;i<(LED.grid_width*LED.grid_height*4);i++){data[i]=0}

			var r = 0
			var g = 0
			var b = 255

			for (var y=0;y<LED.grid_height;y++) {
				for (var x=0;x<LED.grid_width;x++) {

					var px = LED.getCanvasIndex(x,y)

					if (r==0){r=255}else{r=0}
					if (b==0){b=255}else{b=0}

					data[px]   = r
					data[px+1] = g
					data[px+2] = b
					data[px+3] = 255
				}
			}



		},

		"Calibrate": function(){
			self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)
			var data = self.imagedata.data


			for (var i=0;i<(LED.grid_width*LED.grid_height*4);i++){data[i]=0}


			var quarter = LED.grid_width / 4;
			var x0 = 0
			var x1 = Math.round(quarter);
			var x2 = Math.round(quarter * 2);
			var x3 = Math.round(quarter * 3);
			for (var y=0;y<LED.grid_height;y++) {

					var px0 = LED.getCanvasIndex(x0,y)

					data[px0]   = 255
					data[px0+1] = 0
					data[px0+2] = 0
					data[px0+3] = 255

					var px1 = LED.getCanvasIndex(x1,y)

					data[px1]   = 0
					data[px1+1] = 255
					data[px1+2] = 0
					data[px1+3] = 255

					var px2 = LED.getCanvasIndex(x2,y)

					data[px2]   = 255
					data[px2+1] = 255
					data[px2+2] = 255
					data[px2+3] = 255

					var px3 = LED.getCanvasIndex(x3,y)

					data[px3]   = 0
					data[px3+1] = 0
					data[px3+2] = 255
					data[px3+3] = 255

			}

		},

		"Step Through": function(){
			if (self.patterns_stepthrough_idx === undefined) {
				self.patterns_stepthrough_idx = 0
				setTimeout(self.patterns["Step Through"],5000)
				return;
			}

			self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)
			var data = self.imagedata.data

			for (var i=0;i<(LED.grid_width*LED.grid_height*4);i++){data[i]=0}

			var allPixels = [];
			for (var screen in LED.pixelmap){
				allPixels = allPixels.concat(LED.pixelmap[screen])
			}

			var max_x=0,max_y=0;
			for (var i=0;i< allPixels.length;i++){
				max_x = Math.max(max_x,allPixels[i].x);
				max_y = Math.max(max_y,allPixels[i].y);
			}

//			for (var i=0;i< allPixels.length;i++){
				var i = self.patterns_stepthrough_idx;
				var xp = allPixels[i].x / max_x;
				var yp = allPixels[i].y / max_y;

				var xx = Math.floor(LED.grid_width * xp);
				var yy = Math.floor(LED.grid_height * yp);

				var px0 = LED.getCanvasIndex(xx,yy);

				data[px0]   = 255
				data[px0+1] = 0
				data[px0+2] = 0
				data[px0+3] = 255
			//}

			console.log(i)

			setTimeout(function(){
				self.patterns_stepthrough_idx++
				if (self.patterns_stepthrough_idx > allPixels.length-1){
					self.patterns_stepthrough_idx = 0;
				}
				self.patterns["Step Through"]()
			},1000)



		}

	},


//************************************************************************************************

	fill:	function(r,g,b,a) {

		self.imagedata = LED.composite_ctx.getImageData(0,0,LED.grid_width,LED.grid_height)
		var data = self.imagedata.data

		for (var x=0;x<LED.grid_width;x++) {
			for (var y=0;y<LED.grid_height;y++) {
				var px = LED.getCanvasIndex(x,y)
				data[px]   = r || 0
				data[px+1] = g || 0
				data[px+2] = b || 0
				data[px+3] = a || 0
			}
		}

	},

	end:	function(){

	},
	render: function(){

		if (self.strobe_color!=undefined) {
			if (self.strobe_state==true) {

				self.fill(df.getdec(df.gethex(self.strobe_color).substr(0,2)),df.getdec(df.gethex(self.strobe_color).substr(2,2)),df.getdec(df.gethex(self.strobe_color).substr(4,2)),255)
				self.strobe_state=false
			} else {
				self.fill(0,0,0,255)
				self.strobe_state=true
			}


		}

//console.log(imagedata.data)

		LED.composite_ctx.putImageData(self.imagedata, 0, 0);

//		LED.composite_ctx.drawImage(self.canvas,0,0)

	},

	selected_pattern: "All Black",

	interface:	function(){

		self.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "pattern_Scroll",
					style	: "position:absolute;left:10px;top:10px;width:300px;height:500px;overflow-x:hidden;overflow-y:auto;"
				}
		})



		var namestyle = "width:260px;font-size:30px;margin-bottom:3px;margin-left:10px;"
    var radio_off = LED.radio_off_image;
    var radio_on  = LED.radio_on_image;

		var tests = [];

		for (var selector in self.patterns) {


				tests.push(df.newdom({
					tagname		:"df-onoff",
					target		: self.positionDiv,
					attributes:{
						id			: "onoff_"+selector.replace(/[^a-zA-Z0-9]/g,""),
						name		: selector,
						namestyle	: namestyle,
						style		: "position:relative;",
						class		: "onoff",
						value		: self.selected_pattern === selector ? true : false,
						width		: 40,
						height		: 40,
						image_on	: radio_on,
						image_off	: radio_off,
						srcdata		: JSON.stringify({
							selector: selector
						}),
						onchange	: function(val, evt, onoff){

							for (var i=0;i<tests.length;i++){
								tests[i].value = false;
								tests[i].setState(false)
							}

							self.selected_pattern = onoff.srcdata.selector;

							self.patterns[onoff.srcdata.selector]()

						}
					}
				}))


			df.newdom({tagname:"br",target:self.positionDiv})

		}
	}
}

	LED.plugins.push(self)
})();

