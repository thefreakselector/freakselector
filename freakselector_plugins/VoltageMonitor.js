(function(){

var self = {

  name    : "Voltage Monitor",
  icon    : "Settings.png",

  sort_top: true,

  //always_render: true,
//  auto_start: true,
  //render_last : true,

  timer:0,


//Calibrated for 20V max voltage
//16.8 Volts = 4.2v * 4 (max charged battery voltage)
//16.8volts = data D214
//12.8 Volts = 3.2v * 4 (minimum allowed for lithium)
//12.8volts = data D162

//Calibrated for 5V max voltage
//4.2 Volts (max lithium battery charged voltage)
//4.2v = D214
//3.2 Volts (min lithium battery voltage)
//3.2v = D162

  inputs: [

    {
      name: "Computer Voltage",
      max_input_volts: 20,
      max_voltage: 16.8,
      min_voltage: 12.8
    },

    {
      name: "LED Voltage",
      max_input_volts: 5,
      max_voltage: 4.2,
      min_voltage: 3.2
    }/*,

    {
      name: "n/a",
      max_input_volts: 0,
      max_voltage: 0,
      min_voltage: 0
    },

    {
      name: "n/a",
      max_input_volts: 0,
      max_voltage: 0,
      min_voltage: 0
    },*/

  ],

  start:  function(){
   // if (self.get_tmr) {clearTimeout(self.get_tmr)}
    //self.getVoltages()
    self.timer = Date.parse(new Date());
    if (self.data){
      self.proccessData(self.data);
    }
  },

  end: function(){
//    alert('ended')
  },

  getVoltages: function(){
    df.ajax.send({
      url: '/getVoltages',
      callback: function(ajaxobj){
        self.data = JSON.parse(ajaxobj.text);
/*
        self.data=[
          JSON.parse('['+'180,'.repeat(50).slice(0,-1)+']'),
          JSON.parse('['+'199,'.repeat(50).slice(0,-1)+']'),
          JSON.parse('['+'0,'.repeat(50).slice(0,-1)+']'),
          JSON.parse('['+'0,'.repeat(50).slice(0,-1)+']'),
        ]
*/
        self.proccessData(self.data);
      }
    })
  },

  gradient: df.gradient(['ff0000','ffff00','00ff00'],256),

  proccessData: function(voltages){

    for (var i=0;i<self.inputs.length;i++){

      var input = self.inputs[i];
      var meter = self.UI[i];
      var data = voltages[i];

      if (!data) {continue}

      meter.ctx.clearRect(0, 0, self.canvas_width, 256);

      for (var x=0;x<data.length;x++){

        var datapoint = data[x];

        var min_adj = (input.min_voltage / input.max_input_volts) * 255;
        var max_adj = (input.max_voltage / input.max_input_volts) * 255;
        var v_range = max_adj - min_adj;
        var v_step  = 255 / v_range;
        var adjusted = Math.min(Math.max(Math.round((datapoint - min_adj) * v_step),0),255);

        meter.ctx.beginPath();
        meter.ctx.strokeStyle = '#'+self.gradient[adjusted]
        meter.ctx.moveTo(x, 255);
        meter.ctx.lineTo(x, 255-adjusted);
        meter.ctx.stroke();
      }

      var current_voltage = (((input.max_voltage - input.min_voltage) * (adjusted/255)) + input.min_voltage).toFixed(2);

      input.current_voltage = current_voltage;

      if (!isNaN(current_voltage)) {
        meter.current_voltage.innerHTML = current_voltage+'v';
        meter.current_voltage_large.innerHTML = current_voltage+'v';
        var current_top = 255-adjusted;
        meter.current_volts.style.top = current_top+'px';
        meter.current_volts.style.display = 'block'
      } else {
        meter.current_volts.style.display = 'none'
      }

    }
  },

  shutting_down: false,

  render: function(){
    var current = Date.parse(new Date());
    if (current - self.timer > 1500) {
      self.timer = current
      self.getVoltages()

      if (self.inputs[0].current_voltage < 12.9 && self.shutting_down === false){
        self.shutting_down = true
        df.ajax.send({
          url : "nircmd/"+LED.base64encode('exitwin shutdown force')
        })
      }
    }

  },

  interface:  function(){

    var meter_width = Math.round(880 / self.inputs.length); //220
    self.canvas_width = meter_width - 70;//150;

    self.UI = [];

    for (var i=0;i<self.inputs.length;i++){

      var this_input = self.inputs[i];

      var this_meter = {};

      this_meter.container = df.newdom({
        tagname: 'div',
        target: LED.pluginwindow,
        attributes:{
          style: 'position:absolute;top:10px;left:'+(10 + ( i * meter_width ) )+'px;display:block;width:'+(self.canvas_width+40)+'px;padding:10px;border:1px solid black;border-radius:5px;background-color:#404040;'
        }
      })

      this_meter.canvas = df.newdom({
        tagname: "canvas",
        target    : this_meter.container,
        attributes:{
          width: self.canvas_width,
          height: 256,
          style: 'position:relative;width:'+self.canvas_width+'px;height:256px;background-color:black;border:1px solid #cacaca;'
        }
      })
      this_meter.ctx = this_meter.canvas.getContext('2d');

      this_meter.max_volts = df.newdom({
        tagname: 'div',
        target: this_meter.container,
        attributes: {
          style: 'position:absolute;top:3px;left:calc(100% - 39px);z-index:99;font-family:arial;font-size:12px;color:black;background-color:white;padding:5px;border-radius:3px;'
        },
        children:[
          df.newdom({
            tagname: 'div',
            attributes: {
              style: 'position:relative;z-index:999;',
              html: this_input.max_voltage+'v'
            }
          }),
          df.newdom({
            tagname: 'div',
            attributes:{
              style: 'width:14px;height:14px;position:absolute;left:-6px;top:4px;background-color:white;z-index:99;transform:rotate(45deg);'
            }
          })
        ]
      })

      this_meter.min_volts = df.newdom({
        tagname: 'div',
        target: this_meter.container,
        attributes: {
          style: 'position:absolute;top:calc(100% - 25px);z-index:99;left:calc(100% - 39px);font-size:12px;font-family:arial;display:inline-block;color:black;background-color:white;padding:5px;border-radius:3px;',
        },
        children: [
          df.newdom({
            tagname: 'div',
            attributes: {
              style: 'position:relative;z-index:999;',
              html: this_input.min_voltage+'v'
            }
          }),
          df.newdom({
            tagname: 'div',
            attributes:{
              style: 'width:14px;height:14px;position:absolute;left:-6px;top:4px;background-color:white;z-index:99;transform:rotate(45deg);'
            }
          })
        ]
      })

     this_meter.current_volts = df.newdom({
        tagname: 'div',
        target: this_meter.container,
        attributes: {
          style: 'display:none;position:absolute;top:50%;z-index:999;left:calc(100% - 39px);font-size:12px;font-family:arial;color:black;background-color:#ffe088;padding:5px;border-radius:3px;',
        },
        children:[
          this_meter.current_voltage = df.newdom({
            tagname: 'div',
            attributes: {
              style: 'position:relative;z-index:999;',
              html: '?v'
            }
          }),
          df.newdom({
            tagname: 'div',
            attributes:{
              style: 'width:14px;height:14px;position:absolute;left:-6px;top:4px;background-color:white;z-index:99;transform:rotate(45deg);background-color:#ffe088;'
            }
          })
        ]
      })


     this_meter.namediv = df.newdom({
      tagname: 'div',
      target: this_meter.container,
      attributes: {
        style: 'position:absolute;top:calc(100% + 3px);background-color:#505050;width:calc(100% - 32px);color:white;font-family:arial;font-size:42px;line-height:50px;font-weight:700;padding:5px;border:1px solid black;border-radius:5px;text-align:center;',
        html: this_input.name
      }
     })

     this_meter.current_voltage_large = df.newdom({
      tagname: 'div',
      target: this_meter.container,
      attributes: {
        style: 'position:absolute;top:calc(100% + 66px);background-color:#505050;width:calc(100% - 32px);color:white;font-family:arial;font-size:56px;padding:5px;border:1px solid black;border-radius:5px;text-align:center;',
        html: '?'
      }
     })


      self.UI.push(this_meter);

    }

    //---------------------------------------------

    if (self.data){
      self.proccessData(self.data);
    }

  }

}

LED.plugins.push(self);


})();
