(function(){

var self = {
	name		: "Scroller",
	icon		: "Scroller.png",

  render_last : true,
  render_after_split: true,

	fontSize	:  20,
	fontFace	: "Arial",
	scrolltext	: "",
	fontWeight	: "normal",
	fontColor	: "0000ff",
	scroll_left : Number.NEGATIVE_INFINITY,
	scroll_width: 155,
	scroll_speed: 1.5,
	scroll_y	: 0,
	mode		: "scroll",

	settings_to_save	: [
		"fontSize",
		"fontFace",
		"scrolltext",
		"fontColor",
		"scroll_speed",
		"scroll_y"
	],

	password	: "burn",

	start:	function(){

		if (self.password !== "burn") {
			prompt({
				message	: "Enter Password",
				callback: function(value){
					self.password = value
					LED.startPlugin("Scroller")
					self.logout_tmr = setTimeout(function(){
						self.password = undefined
					},1000*60*10)
				},
				bgcolor		: "#ff0000",
				fontcolor	: "#ffffff",
				fontsize	: 20,
				target		: LED.LEDwindow,
				ispassword	: true
			})
			var removeidx
			for (var i=0;i<LED.queue.length;i++){
				if (LED.queue[i].name == self.name) {
					removeidx = i
					break
				}
			}
			if (removeidx!=undefined) {
				LED.queue.splice(i,1)
			}
			self.started = false

			return false
		}

		if (self.started!=true) {

			self.updateScrollSize()

			self.scroll_left =  Number.NEGATIVE_INFINITY

			if (self.mode=="pingpong") {
			//	var slopearray = []
			//	self.pingpong = df.anim({styles:{swing:{min:-self.scroll_width+LED.grid_width,max:0,slope:df.smoothmove,loop:true,steps:50}}})

			}
		}


	//	self.interface()
	}
	,

	updateScrollSize:	function() {

		var fontstyle = "font-size:"+self.fontSize+"px;font-weight:"+self.fontWeight+";font-family:"+self.fontFace
		self.scroll_width = df.fontsize.calcsize({text:self.scrolltext,style:fontstyle})
	},

	end:	function(){
		self.password	= "burn";
		self.scroll_left= Number.NEGATIVE_INFINITY;
	},

	render: function(){

		if (self.scroll_speed > 0 && self.scroll_left < -self.scroll_width) {
			self.scroll_left = (LED.grid_width)
		}

		if (self.scroll_speed < 0 && self.scroll_left > LED.grid_width) {
			self.scroll_left = -(self.scroll_width )
		}
		self.scroll_left -= self.scroll_speed

		var x = Math.round(self.scroll_left)
		var y = LED.grid_height + self.scroll_y

		if (self.scroll_width==undefined) {
			self.scroll_width = df.fontsize.calcsize({
				text	: self.scrolltext,
				style	: "font-size:"+self.fontSize+"px;font-weight:"+self.fontWeight+";font-family:"+self.fontFace+";"
			})
		}


		var font_style = self.fontWeight+" "+self.fontSize+"px "+self.fontFace;

		LED.composite_ctx.font = font_style //'bold 43px Arial'

		LED.composite_ctx.fillStyle = "#"+self.fontColor
		LED.composite_ctx.fillText(self.scrolltext,x,y)

		LED.composite_ctx.lineWidth = self.lineWidth;
		LED.composite_ctx.strokeStyle = self.strokeStyle;
		LED.composite_ctx.strokeText(self.scrolltext,x,y);

	},

	strokeStyle: "000000",
	lineWidth:.25,

	interface:	function(){

		var scrollwin = df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "sliderdiv",
				style	: "position:absolute;top:5px;left:10px;z-index:5000;color:white;"
			}
		})

		df.newdom({
			tagname		:"df-slider",
			target		: scrollwin,
			attributes:{
				id			: "slider_scrollspeed",
				name		: "Scroll Speed",
				style		: "position:absolute;top:-14px;left:670px;",
				className	: "slider",
				min			: 0,
				max			: 1,
				startvalue	: .5,
				height		: 45,
				width		: 200,
				onchange	: function(val){
					self.scroll_speed = (val * 10)-5
				}
			}

		})

		df.newdom({
			tagname		:"df-slider",
			target		: LED.pluginwindow,
			attributes:{
				id			: "slider_fontsize",
				name		: "Font Size",
				style		: "position:absolute;top:70px;left:826px;",
				className	: "slider",
				min			: 0,
				max			: 50,
				startvalue	: (self.fontSize-3),
				height		: 125,
				width		: 45,
				onchange	: function(val){
					self.fontSize = (3 + val)
console.log(self.fontSize)					
					self.updateScrollSize()
				}
			}

		})

		df.newdom({
			tagname		:"df-slider",
			target		: LED.pluginwindow,
			attributes:{
				id			: "slider_fonty",
				name		: "Font Y",
				style		: "position:absolute;top:200px;left:826px;",
				className	: "slider",
				min			: 0,
				max			: 80,
				startvalue	: (self.scroll_y+40),
				height		: 125,
				width		: 45,
				onchange	: function(val){
					self.scroll_y = 40-val
					self.updateScrollSize()
				}
			}

		})


		var fontdiv = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes:{
				id	: "font_selector_div",
				className : "buttonlist",
				style	: "position:absolute;top:332px;left:10px;width:600px;height:200px;background-color:#2a4a5a;border:2px solid black;border-radius:5px;overflow:hidden;overflow-y:auto;"
			}
		})

		var fontul = df.newdom({
			tagname	: "ul",
			target	: fontdiv
		})

		var fontlist = []
		var selectedfontdom
		for (var i=0;i<LED.fontlist.length;i++) {
			var thisfontdom = df.newdom({
					tagname:"li",
					target:fontul,
					attributes:{
						html:LED.fontlist[i],
						style	: "font-family:'"+LED.fontlist[i]+"';font-size:35px;",
						className : (self.fontFace==LED.fontlist[i])?"playing":""
					},
					events:[
						{
							name:"click",
							handler:function(evtobj){
								var thisfont = LED.fontlist[evtobj.srcdata.idx]
								self.fontFace = thisfont
								var doms = fontul.getElementsByTagName("*")
								for (var i=0;i<doms.length;i++) {
									doms[i].className = ""
								}
								evtobj.target.className = "playing"
								self.updateScrollSize()
							},
							idx:i
						}
					]
			})
			if (self.fontFace==LED.fontlist[i]) {
				selectedfontdom = thisfontdom
			}

		}


		df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes:{
				style	: "position:absolute;border-radius:10px;top:7px;left:517px;font-family:arial;font-size:29px;padding:5px;padding-left:15px;padding-right:15px;",
				type	: "button",
				value	: "UPDATE"

			},
			children:fontlist,
			events:[
				{
					name:"click",
					handler:function(evtobj){
						self.scrolltext = scrollinput.value
						self.scroll_left = Number.NEGATIVE_INFINITY
						self.updateScrollSize()
					}
				}
			]

		})

//---------------------------------------------------
		var scrollinput = df.newdom({
			tagname:"input",
			target	: LED.pluginwindow,
			attributes:{
				type:"text",
				style:"position:absolute;border-radius:10px;top:10px;left:10px;font-family:arial;font-size:30px;width:500px;height:45px;",
				value	: self.scrolltext
			}
		})

		df.osk.init(LED.pluginwindow,10,60)
		df.osk.getinput(scrollinput)

//---------------------------------------------------

		var colorpicker1_div = df.newdom({
			tagname:"div",
			target:LED.pluginwindow,
			attributes:{
				style:"position:absolute;top:332px;left:628px;"
			}
		})

		self.colorpicker1 = new df.ColorPicker({
			target: colorpicker1_div,
			width : 256,
			height: 50,
			startvalue:self.fontColor,
			callback:function(new_color){
				self.setcolor1({newcolor:new_color})
			}
		})


/*
		var color1 = df.newdom({
			tagname	: "input",
			target	: LED.pluginwindow,
			attributes	: {
				type	: "text",
				style	: "position:absolute;top:524px;left:700px;text-align:center;",
				value	: self.fontColor
			}

		})

		var colorobj1 = df.colortool.init({
			target	: color1,
			done	: self.setcolor1,
			callback: self.setcolor1,
			dither	: 1,
			width	: 160,
			height	: 160,
			top		: 328,
			left	: 675,
			offsettop : 0,
			offsetleft: 0
		})

		LED.pluginwindow.appendChild(colorobj1.div,true)
		colorobj1.div.removeAttribute("mousefx")
	*/
	},


	setcolor1:function(data){
		self.fontColor = data.newcolor
	},

	slide_scrollspeed:	function(val) {

		self.scroll_speed = (val * 10)-5
	},

	changeFont:	function(fontidx){
		self.fontFace = self.fontlist[fontidx]
		self.start()
	},


	changeMode:	function(mode) {

		if (mode=="scroll") {

		}

		if (mode=="pingpong") {


		}

	}

}

LED.plugins.push(self);


})();
