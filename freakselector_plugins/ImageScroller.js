(function(){


var self = {
	name		: "Image Scroller",
	icon		: "Image Viewer.png",
	fullscreen	: true,


	start:	function(){

		if (self.tempcanvas==undefined) {
			self.tempcanvas = df.newdom({
				tagname:"canvas",
				target:LED.contextHolder,
				attributes:{
					className:"previewcontext"
				//	style:"display:none;"
				}
			})
		}

		self.tempcanvas_ctx = self.tempcanvas.getContext("2d")


		self.image = new Image()
		self.image.onload = function() {

			self.aspect = self.image.width / self.image.height;

			self.tempcanvas.style.width = (self.aspect*LED.grid_height)+"px";
			self.tempcanvas.style.height= LED.grid_height+"px";
			self.thiswidth  = self.image.width;
			self.thisheight = self.image.height;
			self.thisx = 0;
			self.thisy = 0;

		}

		self.image.src = "Graphics/burning_man_1990.jpg"

	}
	,
	end:	function(){

	},

	xd:-3,
	yd:-2,
	wd:-2,
	hd:0,
	render: function(){


		self.tempcanvas_ctx.clearRect(0, 0, self.thiswidth*2,self.thisheight*2);

		if (self.thisx!=undefined) {
			self.thisx += self.xd
			self.thisy += self.yd

//console.log(self.thisx,self.thisy,LED.grid_width,self.thiswidth)


			if (self.thisx>0) {self.thisx=0;self.xd=-self.xd}
			if (self.thisy>0) {self.thisy=0;self.yd=-self.yd}
			if (self.thisx+self.thiswidth-(LED.grid_width*4)<0) {self.xd=-self.xd}
			if (self.thisy+self.thisheight-(LED.grid_height*4)<0) {self.yd=-self.yd}


			/*
			self.thiswidth += self.wd
			self.thisheight += self.wd*self.aspect

			if (
				self.thiswidth<LED.grid_width/2		||
				self.thiswidth>self.image.width*4	||
				self.thisheight<LED.grid_height/2	||
				self.thisheight>self.image.height*4
			) {
				self.wd=-self.wd
			}
			if (
				self.thisx < -(self.thiswidth-LED.grid_width)/2	||
				self.thisx > 0
			) {
				self.xd = (self.xd>0)?Math.random()*-3:Math.random()*3;
				self.thisx += self.xd
			}

			if (
				self.thisy < -(self.thisheight-LED.grid_height)/2	||
				self.thisy > 0
			) {

 				self.yd =  (self.yd>0)?Math.random()*-3:Math.random()*3;
				self.thisy += self.yd
			}
			*/

			self.tempcanvas_ctx.drawImage(self.image,self.thisx,self.thisy,self.thiswidth,self.thisheight)

			LED.composite_ctx.drawImage(self.tempcanvas, 0, 0,LED.grid_width,LED.grid_height)
		}


	},

	fileSelect: function(file){
		self.selectImage(file)
	},

	//----------------------------------------------------------------------------------------------------

	selectImage:	function(item) {

		self.image.src = '/getImage/'+item
console.log('>>>>>>>>item:',item)



	},


	filetypes: {
		'png' : 1,
		'jpg' : 1,
		'jpeg': 1,
		'bmp' : 1,
		'gif' : 1
	},

	interface:	function(){

		self.listholder = df.newdom({
			tagname	: "div",
			target	: LED.pluginwindow,
			attributes	: {
				id		: "listholder",
				className	: "buttonlist",
				style	: "left:5px;width:570px;"
			},
			children	: [
				{
					tagname	: "ul",
					attributes:	{
						"id"	: "playlist"
					}
				}
			]
		})

		if(!self.treeService){
			self.treeService = new LED.treeService({
				media_type: self.name,
				listholder: self.listholder,
				fileSelect: self.fileSelect,
				filetypes : self.filetypes,
				randomize : self.randomize
			})
		} else {
			self.treeService.listholder = self.listholder
			self.treeService.renderTree()
		}

	//==========================================================================================

		self.positionDiv = df.newdom({
				tagname		: "div",
				target		: LED.pluginwindow,
				attributes	: {
					id		: "sliders",
					style	: "position:absolute;left:587px;top:6px;color:white;"
				},
				children	: self.sliders
		})


		var select_folders = df.newdom({
			tagname		:"input",
			target		: self.positionDiv,
			attributes:{
				id		: "musicplayer_select_folders",
				className: "bigbutton2",
				value		: "Select Folders",
				type		: "button",
				style		: "position: absolute;top: 470px;left: 13px;width: 267px;height: 58px;background-color: #000000;color: white;",
				onclick	: function(val){
					LED.selectFolders(self.name, function(folders){
						self.treeService.refresh(folders);
					})
				}
			}
		})




	}

}

	LED.plugins.push(self)
})();

