(function(){

var self = {
	count: 1,
	speed: 6,
	size : 3,
	clear:false,

	color_red		: 0,
	color_green		: 44,
	color_blue		: 255,
	opacity			: 255,

	red_speed:1,
	green_speed:3,
	blue_speed:2.4,

	settings_to_save:	[
		"speed",
		"size",
		"clear",
		"color_red",
		"color_green",
		"color_blue",
		"opacity",
		"red_speed",
		"green_speed",
		"blue_speed"
	],

	name		: "Radar",
	//icon		: "Video Input.png",


	start:	function(){

	},

	end:	function() {

	},

	render:	function(){

		self.update()
		LED.composite_ctx.drawImage(self.canvas, 0, 0,LED.grid_width,LED.grid_height)

	},

	rotation_angle:0,


	update:	function(){

		//clearCanvas();

		var theta = self.rotation_angle * (Math.PI/180)

		var x = (LED.grid_width/2) + (Math.cos(theta) * (LED.grid_width))
		var y = (LED.grid_height/2) - (Math.sin(theta) * (LED.grid_width))

		//Draw boid
		if (self.clear != false) {
			self.ctx.clearRect(0, 0, LED.grid_width, LED.grid_height);
		}
		self.ctx.beginPath();
		self.ctx.moveTo(LED.grid_width/2,LED.grid_height/2)
		self.ctx.lineTo(x , y)

		self.ctx.strokeStyle = 'rgba(' + Math.round(self.color_red) + ',' + Math.round(self.color_green) + ',' + Math.round(self.color_blue) + ', '+self.opacity+')'
		self.ctx.lineWidth = self.size;

		self.ctx.stroke();
		self.ctx.fill();

		self.color_red+=self.red_speed
		if (self.color_red > 255 || self.color_red < 0){self.red_speed=-self.red_speed;self.color_red+=self.red_speed}
		self.color_green+=self.green_speed
		if (self.color_green > 255 || self.color_green < 0){self.green_speed=-self.green_speed;self.color_green+=self.green_speed}
		self.color_blue+=self.blue_speed
		if (self.color_blue > 255 || self.color_blue < 0){self.blue_speed=-self.blue_speed;self.color_blue+=self.blue_speed}

		self.rotation_angle += self.speed
		if (self.rotation_angle > 360) {self.rotation_angle = self.rotation_angle - 360}
	},


	interface:	function() {

		if (self.canvas == undefined) {
			self.canvas = df.newdom({
				tagname:"canvas",
				target	: LED.pluginwindow,//LED.contextHolder,
				attributes:{
					id	: "radar_canvas",
					style	: "position:relative;top:20px;left:20px;",
					width	: LED.grid_width,
					height	: LED.grid_height
				}
			})
			self.ctx = self.canvas.getContext('2d')
console.log(self.canvas,self.ctx)
		}

//================================================================================================================
//================================================================================================================

// Get the canvas DOM element along with it's 2D context
// which is basically what we do our work on



//==============================================

/*
		"speed",
		"size",
		"color_red",
		"color_green",
		"color_blue",
		"opacity",
		"red_speed",
		"green_speed",
		"blue_speed"

		"clear",
*/
		var sliders=[]

		var slidertypes = [
			{
				id:"radar_speed",
				name:"Rotation Speed",
				startvalue:self.speed,
				min:0,
				max:100,
				handler:function(val){
					self.speed = val
				}
			},
			{
				id:"radar_size",
				name:"Size",
				startvalue:self.size,
				min:0,
				max:80,
				handler:function(val){
					self.size = val
				}
			},
			{
				id:"radar_red_speed",
				name:"Red Speed",
				startvalue:self.red_speed,
				min:0,
				max:100,
				handler:function(val){
					self.red_speed = val
				}
			},

			{
				id:"radar_green_speed",
				name:"Green Speed",
				startvalue:self.green_speed,
				min:0,
				max:100,
				handler:function(val){
					self.green_speed = val
				}
			},
			{
				id:"radar_blue_speed",
				name:"Blue Speed",
				startvalue:self.blue_speed,
				min:0,
				max:100,
				handler:function(val){
					self.blue_speed = val
				}
			},
			{
				id:"radar_",
				name:"Opacity",
				startvalue:self.opacity,
				min:0,
				max:1,
				handler:function(val){
					self.opacity = val
				}
			}
		]

		//------------------------------------------------------------------------------------------------------

		var thisvalue

		var sliders = LED.sliderList(slidertypes)

		df.newdom({
			tagname		: "div",
			target		: LED.pluginwindow,
			attributes	: {
				id		: "settingsWindow",
				style	: "position:absolute;top:5px;left:5px;z-index:5000;color:white;background-color:#efeadc;width:347px;height:540px;overflow:hidden;overflow-y:scroll;border-radius:5px;"
			},
			children	: [
				{
					tagname		: "div",
					attributes	: {
						id		: "sliders",
						style	: "clear:both;position:relative;"
					},
					children	: sliders

				}
			]
		})


//==========================================================================================


}



}


	LED.plugins.push(self)
})();

