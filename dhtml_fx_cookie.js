/*
Code by Damien Otis 2004-2010
License found at http://www.dhtmldf.net/license.html
API Docs at http://www.dhtmldf.net/
*/
dhtmlfx.cookie = {
	autosavearr	:	[],
	autosaveinterval:	4000,
	autosavetmr	:	0,
	
	set	:	function(cookiename,cookievalue,expdate) {
		document.cookie = cookiename + "=" + escape(cookievalue) + "; path=/; expires="+ ((expdate==undefined)?"Fri, 31 Dec 2999 23:59:59 GMT":expdate)+";";
	},

	get	:	function(cookiename) {//cookiename as String
		var adpp = document.cookie;var index = adpp.indexOf(cookiename + "=");
		if (index == -1) return null;index = adpp.indexOf("=", index) + 1;
		var endstr = adpp.indexOf(";", index);
		if (endstr == -1) endstr = adpp.length;
		return unescape(adpp.substring(index, endstr));
	},
	
	getobject	:	function(objectname) {//objectname as String
		var df = dhtmlfx
		var thisobjval = df.cookie.get(objectname);
		if (thisobjval==null) {return}
		eval(objectname+" = "+thisobjval);
	},
	
	saveobject	:	function(objectname) {//objectname as String
		var df = dhtmlfx
		df.cookie.set(objectname,eval(objectname));
	},
	
	autosave	:	function(objectname) {//objectname as String
		var df = dhtmlfx
		df.cookie.set(objectname,eval(objectname));
		if (String(","+df.cookie.autosavearr.join(",")+",").indexOf(","+objectname+",")==-1) {
			df.cookie.autosavearr.push(objectname+":"+eval(objectname));
		}
		if (df.cookie.autosavetmr==0) {df.cookie.autosavetmr=setTimeout("df.cookie.doautosave()",df.cookie.autosaveinterval)}
	},
	
	doautosave	:	function() {// This function does not get called directly.
		var df = dhtmlfx
		for (var i=0;i<df.cookie.autosavearr.length;i++) {
			if (eval(df.cookie.autosavearr[i].split(":")[0])!=df.cookie.autosavearr[i].split(":")[1]) {
				df.cookie.saveobject(df.cookie.autosavearr[i].split(":")[0]);
			}
		}
		df.cookie.autosavetmr=setTimeout("df.cookie.doautosave()",df.cookie.autosaveinterval);
	}
};

