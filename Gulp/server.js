//----------------------------------------------------------------------------------
var CWD = process.cwd();
var qs = require('querystring');
var url = require('url');

var ws = require('ws');

var freakselectorIsRunning = false;
var start_browser_tmr = undefined;

var SerialPort = require('serialport');

//----------------------------------------------------------------------------------

gulp.task("start_static_server", function(resolve){

  startLiveReload()

  var serve = gulpServe({
    root:['/'],
    port:8080,      //default port is 8881
    hostname: "0.0.0.0",
    middleware: function(req,res){

      var this_url = req.url.split("?")[0];
      var url_split = this_url.split("/").slice(1);

      if (url_split[0] && Handlers[url_split[0]]){
        //Not a static file, these are method handlers.
        console.log(cc.set("fg_green",url_split[0]+" : "+this_url))
        var this_handler = url_split[0];
        Handlers[this_handler](req,res)

      } else {
        //This is for static files

        var this_url = decodeURIComponent(req.url.split("?")[0]);

        if (this_url === "/"){
          this_url = "/index.html"
        }

        if (this_url.indexOf('/quitFreakSelector')!==-1){
          process.abort()
        }

        var url_split = this_url.split("/").slice(1);

        var static_file = path.resolve(root_folder+this_url)

        fs.stat(static_file, function(err,stat){
          if (err){
            console.log(cc.set("fg_yellow","File not found : "+this_url))
console.log(cc.set('fg_yellow',JSON.stringify(req.headers,null,4)));
            res.setHeader("Content-Type", "plain/text");
            res.statusCode = 404;
            res.end("File Not Found.")
            return
          }

          console.log(cc.set("fg_cyan","Static file : "+this_url))

          var obj = {}
          obj.stat = stat
          obj.file_path = static_file;
          obj.file_name = url_split.slice(-1)[0];
          getBinaryFile(req,res,obj)

        })

      }

    }

  })

  if (args.serve){
    runSequence("openFreakSelectorWebInterface", "watch", resolve)
  }

  return serve()

})

//----------------------------------------------------------------------------------

gulp.task("watch",function(){
  var base_folder = path.resolve(root_folder+'/**/*');
console.log("base_folder",base_folder)
  gulpWatch(base_folder, function(evt){
console.log(cc.set('fg_red','>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'))
    //triggerLiveReload()
  })
})
//----------------------------------------------------------------------------------

function getPostData(req, callback){
  console.log("req.method",req.method)
  if (req.method.toLowerCase() === 'post' ) {
    var body = '';
    req.on('data', function(chunk) {
      body += chunk;
    });
    req.on('end', function() {
      var data = qs.parse(body);
      // now you can access `data.email` and `data.password`
      //req.body = JSON.stringify(data);
      callback(data)
    });
  } else {
    console.log(cc.set("fg_red","getPostData is using method "+req.method+" but needs to be POST"))
    callback()
  }
}

//----------------------------------------------------------------------------------

global.open = require("gulp-open");

gulp.task("openFreakSelectorWebInterface",function(){

  setTimeout(function(){

    console.log("global.livereload_browser_connected",global.livereload_browser_connected)

    if (global.livereload_browser_connected){ return }

    /*gulp.src(__filename)
      .pipe(open({uri: 'http://localhost:8080/', app:'chrome'}));
*/
    if (current_os === 'win32'){
      runexe();
    } else if (current_os === 'darwin'){
      runosx();
    }
    console.log("openFreakSelectorWebInterface")


  },50)



})

//----------------------------------------------------------------------------------

var Handlers = {
  "getDrives"               : getDrives,
  "getFreakselectorPlugins" : getFreakselectorPlugins,
  "getMilkdropPresets"      : getMilkdropPresets,
  "getFolders"              : getFolders,
  "setFolders"              : setFolders,
  "getAudio"                : getAudio,
  "getStream"               : getStream,
  "saveFolderPaths"         : saveFolderPaths,
  "getFolderPaths"          : getFolderPaths,
  "saveRecentlyPlayed"      : saveRecentlyPlayed,
  "getRecentlyPlayed"       : getRecentlyPlayed,
  "getJSONFile"             : getJSONFile,
  "nircmd"                  : runNircmd,
  "saveVideoFrame"          : saveVideoFrame,
  "getPath"                 : getPath,
  "FreakSelectorRunning"    : FreakSelectorRunning,
  "startFadecandyServer"    : startFadecandyServer,
  "killFadecandyServer"			: killFadecandyServer,
  "openOnScreenKeyboard"    : openOnScreenKeyboard,
  "getVoltages"             : getVoltages,
  "saveSetting"         		: saveSetting,
  "getSetting"           		: getSetting,
  "getImage"								: getImage,

  "mobileSensorServer"			: mobileSensorServer,

  "serialPortList"					: serialPortList
}

//----------------------------------------------------------------------------------

var mobileSensorWS;
var mobileSensorWS_connected;
var mobileSensorWSPort = 3300;

function mobileSensorServer(req, res){

	if (mobileSensorWS === undefined){
		portFinder.basePort = mobileSensorWSPort;

		portFinder.getPort(function(err,free_port){

			if (err){
				console.log(cc.set("fg_red",JSON.stringify(err,null,4)))
				return
			}

			mobileSensorWSPort = free_port;

			console.log("mobileSensorWSPort",free_port)

			//-------------------------------------------------

		  var WebSocketServer = ws.Server;

	  	mobileSensorWS = new WebSocketServer({port: mobileSensorWSPort});

		  mobileSensorWS.on('connection', function connection(ws) {
		    console.log("mobileSensorWS Connected")

		    mobileSensorWS_connected = true;

		    mobileSensorWS.on('message', function incoming(message) {
		      var message_parsed = JSON.parse(message);

console.log('>>>>>>>mobileSensorMessage:',message, message_parsed)
		    });
		  });

		  res.setHeader("Content-Type", "application/json");
		  res.statusCode = 200;
			res.end(JSON.stringify({
				"mobileSensorWSPort": mobileSensorWSPort
			}) );
		})
	} else {
		  res.setHeader("Content-Type", "application/json");
		  res.statusCode = 200;
			res.end(JSON.stringify({
				"mobileSensorWSPort": mobileSensorWSPort
			}) );
	}

}

//----------------------------------------------------------------------------------
/*
Bluetooth WSS bridge disabled because npm module bluetooth-serial-port does not install without an error

Add back to package.json:
   "bluetooth-serial-port": "2.1.6",


*/
/*
Handlers.bluetoothDeviceList = bluetoothDeviceList;
Handlers.bluetoothDeviceConnect = bluetoothDeviceConnect;


var BTSerialPort = require('bluetooth-serial-port')

function bluetoothDeviceList(req, res){
	var btSerial = new BTSerialPort.BluetoothSerialPort();
	btSerial.listPairedDevices(function(list){
	  res.setHeader("Content-Type", "application/json");
	  res.statusCode = 200;
		res.end(JSON.stringify(list))
	})
}

//----------------------------------------------------------------------------------

var connected_devices = {};
function bluetoothDeviceConnect(req, res){

	var queryData = url.parse(req.url, true).query;

	var address = ((queryData.address||'').match(/([a-fA-F0-9]{2}:){5}([a-fA-F0-9]{2})$/)||[])[0];

	if ( address ) {

		var btSerial = new BTSerialPort.BluetoothSerialPort();

		btSerial.findSerialPortChannel(address, function(channel) {
			btSerial.connect(address, channel, function() {
				console.log('connected');

				connected_devices[address] = connected_devices[address] || {}
				connected_devices[address].btSerial = btSerial;


//				function writeData(data){
	//				btSerial.write(new Buffer(data, 'utf-8'), function(err, bytesWritten) {
		//				if (err) console.log(err);
			//		});
				//}


				btSerial.on('data', function(buffer) {
					if (connected_devices[address].btWS) {
						connected_devices[address].btWS.send(buffer.toString('utf-8'))
					}
					console.log('>>>>>>>>BTSERIAL DATA RECEIVED:',buffer,buffer.toString('utf-8'));
				});


			  res.setHeader("Content-Type", "application/json");
			  res.statusCode = 200;
				res.end(JSON.stringify({
					"bluetoothWSServerPort": bluetoothWSServerPort
				}) );

			}, function () {
				console.log('cannot connect');
			  res.setHeader("Content-Type", "application/json");
			  res.statusCode = 201;
				res.end('BT Serial not connected' );
			});

			// close the connection when you're ready
			btSerial.close();
		}, function() {
			console.log('found nothing');
		  res.setHeader("Content-Type", "application/json");
		  res.statusCode = 201;
			res.end('No BT device found');
		});

	}

//[ { name: 'VideoFurBlinky4',
  //  address: '20:13:11:13:15:47',
    //services: [ [Object] ] } ]


}

//----------------------------------------------------------------------------------

Handlers.bluetoothServer = bluetoothServer;

var bluetoothWSServer;
var bluetoothWSServer_connected;
var bluetoothWSServerPort = 3400;

function bluetoothServer(req, res){

	if (bluetoothWSServer === undefined){
		portFinder.basePort = bluetoothWSServerPort;

		portFinder.getPort(function(err,free_port){

			if (err){
				console.log(cc.set("fg_red",JSON.stringify(err,null,4)))
				return
			}

			bluetoothWSServerPort = free_port;

			console.log("bluetoothWSServerPort",free_port)

			//-------------------------------------------------
			if (bluetoothWSServer_connected === undefined){
				bluetoothWSServer_connected = false;

		  	bluetoothWSServer = new ws.Server({port: bluetoothWSServerPort});

				bluetoothWSServer.on('connection', function( btWS ) {

				  console.log("bluetoothWSServer Connected")
				  bluetoothWSServer_connected = true;


					btWS.on('message', function(message) {
					   var message_parsed = JSON.parse(message);

						connected_devices[message_parsed.address] = connected_devices[message_parsed.address] || {};
						if (!connected_devices[message_parsed.address].btWS){
							connected_devices[message_parsed.address].btWS = btWS;
						}

						var btDevice = connected_devices[message_parsed.address].btSerial;
						if (btDevice){
							var serial_data = new Uint8Array(new Buffer(atob(message_parsed.data)));
console.log('Length:',serial_data.length,serial_data[serial_data.length-1])
							btDevice.write(serial_data, function(err, bytesWritten) {
								if (err) {
									return	console.log(err)
								};
								console.log("bytesWritten:",bytesWritten)
							});
						}
					});

				});

		  	bluetoothWSServer.on('close',function(){
				    bluetoothWSServer_connected = false;
				    console.log("bluetooth ws closed")
		  	});

		  } else {
		  	console.log("Bluetooth WS already connected")
		  }

		  res.setHeader("Content-Type", "application/json");
		  res.statusCode = 200;
			res.end(JSON.stringify({
				"bluetoothWSServerPort": bluetoothWSServerPort
			}) );
		})
	} else {
		  res.setHeader("Content-Type", "application/json");
		  res.statusCode = 200;
			res.end(JSON.stringify({
				"bluetoothWSServerPort": bluetoothWSServerPort
			}) );
	}

}

*/


//----------------------------------------------------------------------------------
function serialPortList(req, res) {

	SerialPort.list(function (err, ports) {
		  res.setHeader("Content-Type", "application/json");
		  res.statusCode = 200;
		  res.end(JSON.stringify({ports: ports}));
	});

}


//----------------------------------------------------------------------------------

var voltages;

function getVoltages(req, res){
  if (voltages === undefined) {
    startVoltageMonitor()
  }

  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.end(JSON.stringify( voltages || [] ))

}

gulp.task('startVoltageMonitor', startVoltageMonitor);

var max_data_length = 370;
var padding = JSON.parse( ['[', '0'.repeat(max_data_length).split('').join(','), ']'].join('') );

function startVoltageMonitor(){
  voltages = []; //padding.concat(200).slice(-50) ,[0],[0],[0]


  var ports = [];

  SerialPort.list(function (err, portlist) {
    ports = portlist;
    console.log("Searching for voltage monitor...")
    nextPort()
  });

  nextPort();

  function nextPort(){
    if (ports.length === 0){
      console.log("no voltage monitor found")
      return
    }

    var this_port = ports.shift();

    setTimeout(function(){
      if (voltages[0]){
        console.log("Voltage Monitor Found on port",this_port.comName);
        return
      } else {
        port.close(function(){
          console.log('closed',this_port.comName);
          nextPort();
        })
      }
    },5000);

    var port = new SerialPort(this_port.comName, {
      baudRate: 57600,
      autoOpen: true,
      dataBits: 8,
      stopBits: 1,
      parity: 'none',
      bufferSize: 1024,
      parser: SerialPort.parsers.byteLength(20)
    });

    var v;

    port.on('open', function() {

      console.log("Port opened:", this_port.comName);

      port.on('data',function(data){

        var values = data.toString('utf8');

        if (values.substr(0,4)==="MCX0"){
          v = data[4];
          voltages[0] = voltages[0] || [];
          if (v !== voltages[0][voltages[0].length-1]){
            voltages[0] = padding.concat(voltages[0]||[],v).slice(-max_data_length);
          }
        }

        if (values.substr(5,4)==="MCX1"){
          v = data[9];
          voltages[1] = voltages[1] || [];
          if (v !== voltages[1][voltages[1].length-1]){
            voltages[1] = padding.concat(voltages[1]||[],v).slice(-max_data_length);
          }
        }

        if (values.substr(10,4)==="MCX2"){
          v = data[14];
          voltages[2] = voltages[2] || [];
          if (v !== voltages[2][voltages[2].length-1]){
            voltages[2] = padding.concat(voltages[2]||[],v).slice(-max_data_length);
          }
        }

        if (values.substr(15,4)==="MCX3"){
          v = data[19];
          voltages[3] = voltages[3] || [];
          if (v !== voltages[3][voltages[3].length-1]){
            voltages[3] = padding.concat(voltages[3]||[],v).slice(-max_data_length);
          }
        }

        port.flush();

      })
    });

    // open errors will be emitted as an error event
    port.on('error', function(err) {
      console.log('Error: ', err.message);
      port.close(function(){
        nextPort()
      })
    })

    port.on('disconnect', function(msg) {
      console.log('Disconnected: ', msg);
    })

    port.on('close', function(msg) {
      console.log('Closed: ', msg);
    })

  }

}


//----------------------------------------------------------------------------------


function openOnScreenKeyboard(req, res){
  exec("osk.exe", function(err,stdout,stderr){

  })
  res.statusCode = 200;
  res.end('okay');
}

//----------------------------------------------------------------------------------

function startFadecandyServer(req, res){

  runSequence("start-fadecandy",function(){
    res.statusCode = 200;
    res.end('okay');
  });

}


//----------------------------------------------------------------------------------

function killFadecandyServer(req, res){

  runSequence("kill-fadecandy",function(){
    res.statusCode = 200;
    res.end('okay');
  });

}

//----------------------------------------------------------------------------------

function FreakSelectorRunning(req,res){
  console.log("<<<FreakSelectorRunning>>>")
  freakselectorIsRunning = true;
  if (start_browser_tmr){clearTimeout(start_browser_tmr);start_browser_tmr = undefined}
  res.setHeader("Content-Type", "plain/text");
  res.statusCode = 200;
  res.end("okay")
}


function runNircmd(req,res){
  var this_url = req.url.split("?")[0];
  var url_split = this_url.split("/").slice(1);

  var this_cmd = atob(url_split[1]);

  exec(CWD + "\\nircmd\\nircmdc.exe "+this_cmd, function(err,stdout,stderr){
console.log(err,'\n------------------------------\n',stdout,'\n------------------------------\n',stderr)
    res.end()
  })

}

//----------------------------------------------------------------------------------

function saveRecentlyPlayed(req,res){

  var this_url = req.url.split("?")[0];
  var url_split = this_url.split("/").slice(1);

  var body = [];

  req.on('data', function(chunk) {

    body.push(chunk);

  }).on('end', function() {

    body = Buffer.concat(body).toString();


    var recently_played = JSON.parse(body);

    var recently_played_file;
    try{
      recently_played_file = require(path.resolve(root_folder + path.sep + "recently_played.json")) || {};
    }catch(e){
      recently_played_file = {};
    }

    var media_type = decodeURIComponent(url_split[1]);

    recently_played_file[media_type] = recently_played;

    fs.writeFile(root_folder + path.sep + 'recently_played.json', JSON.stringify(recently_played_file), function (err,data) {
      if (err) {
        res.setHeader("Content-Type", "text/plain");
        res.statusCode = 500;
        res.end("Error writing folder_paths.json file to disk")
        return
      }
      res.setHeader("Content-Type", "text/plain");
      res.statusCode = 200;
      res.end("saved")
    })

  });

}

//----------------------------------------------------------------------------------

var saveVideo_Folder = "D:\\SavedVideo\\";

var savedFolders = {};

function saveVideoFrame(req,res){

  var this_url = req.url.split("?")[0];

  var url_split = this_url.split("/").slice(1);

  var body = [];

  req.on('data', function(chunk) {

    body.push(chunk);

  }).on('end', function() {

    body = Buffer.concat(body).toString();
//console.log("saveVideoFrame: ",body)

    var data = atob(body).replace(/^data:image\/\w+;base64,/, "");

//console.log("datA",data)

    var datenow = new Date();
    var date_folder = ("0"+(datenow.getMonth()+1)).substr(-2) + (datenow.getDate()) + datenow.getYear().toString().substr(-2);
    if (!savedFolders[date_folder]){
      var folder_exists
      try{folder_exists = fs.statSync(saveVideo_Folder + date_folder)}catch(e){}
      if (!folder_exists){
        fs.mkdirSync(saveVideo_Folder + date_folder)
        savedFolders[date_folder] = true
      } else {
        savedFolders[date_folder] = true
      }
    }

    var buf = new Buffer(data, 'base64');

    var frame_filename = Date.now()+".png"

    fs.writeFile(saveVideo_Folder + date_folder + path.sep + frame_filename, buf, function (err,data) {     if (err) {
        res.setHeader("Content-Type", "text/plain");
        res.statusCode = 500;
        res.end("Error writing video capture file to disk")
        return
      }
      res.setHeader("Content-Type", "text/plain");
      res.statusCode = 200;
      res.end("saved")
    })

  });

}

//----------------------------------------------------------------------------------

function getRecentlyPlayed(req,res){

  var this_url = req.url.split("?")[0];
  var url_split = this_url.split("/").slice(1);

  var media_type = decodeURIComponent(url_split[1]);

  var recently_played_file;
  try{
    recently_played_file = require(path.resolve(root_folder + path.sep + "recently_played.json")) || {};
  }catch(e){
    recently_played_file = {};
  }

  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.end(JSON.stringify(recently_played_file[media_type] || {} ))

}

//----------------------------------------------------------------------------------

function getJSONFile(req,res){

  var this_url = req.url.split("?")[0];
  var url_split = this_url.split("/").slice(2);

  var this_json_file = {};
  var filepath = path.resolve(root_folder + path.sep + url_split.join(path.sep))

console.log("getJSONFile>>>>>>",url_split, filepath)

  try{
    this_json_file = fs.readFileSync(filepath, 'utf8') || '';
  }catch(e){
    res.setHeader("Content-Type", "text/plain");
    res.statusCode = 404;
    res.end('could not find json file')
    return
  }

  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
//  res.end(JSON.stringify(this_json_file || {} ))
  res.end(this_json_file || '');

}

//----------------------------------------------------------------------------------

function saveFolderPaths(req,res){

  var new_folder_paths = JSON.parse(req.body.folder_paths);

  resolveFolderPaths(new_folder_paths)

  global.folder_paths = new_folder_paths;
console.log("folder_paths",global.folder_paths )
/*
  fs.writeFile(root_folder + path.sep + 'folder_paths.json', JSON.stringify(folder_paths), function (err,data) {
    if (err) {
      res.setHeader("Content-Type", "text/plain");
      res.statusCode = 500;
      res.end("Error writing folder_paths.json file to disk")
      return
    }
*/
  if (res){
      res.setHeader("Content-Type", "application/json");
      res.statusCode = 200;
      res.end(JSON.stringify(global.folder_paths ))
  }
/*
  });

*/
}

//----------------------------------------------------------------------------------

function setFolders(req,res){

  var this_url = req.url.split("?")[0];
  var url_split = this_url.split("/").slice(1);
  var media_type = decodeURIComponent(url_split[1]);

  getPostData(req, function(data){

    global.folder_paths[media_type] = JSON.parse(data.mapped_folders)
console.log("FOLDER PATHS",JSON.stringify(global.folder_paths,null,4))

    resolveFolderPaths(global.folder_paths)


    fs.writeFile(root_folder + path.sep + 'folder_paths.json', JSON.stringify(global.folder_paths), function (err,data) {
      if (err) {
        res.setHeader("Content-Type", "text/plain");
        res.statusCode = 500;
        res.end("Error writing folder_paths.json file to disk")
        return
      }

      res.setHeader("Content-Type", "application/json");
      res.statusCode = 200;
      res.end(JSON.stringify( data ))

    });
  })
}

//----------------------------------------------------------------------------------

try{
  global.folder_paths = require(path.resolve(root_folder + path.sep + "folder_paths.json"));
}catch(e){
  global.folder_paths = {};

  var folder_paths_exists
  try{folder_paths_exists = fs.statSync(path.resolve(root_folder + path.sep + "folder_paths.json"))}catch(e){}
  if (!folder_paths_exists){
    fs.writeFileSync(root_folder + path.sep + 'folder_paths.json', JSON.stringify(folder_paths), "UTF8");
  }
}

function resolveFolderPaths(this_folder_paths){
  for (var plugin in this_folder_paths){
    for (var plugin_folder in global.folder_paths [plugin]){
      this_folder_paths[plugin][plugin_folder] = path.resolve(this_folder_paths[plugin][plugin_folder]);
    }
  }
}

resolveFolderPaths(global.folder_paths );
saveFolderPaths({body:{folder_paths:JSON.stringify(global.folder_paths )}})

//----------------------------------------------------------------------------------

function getFolderPaths(req,res){

  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.end(JSON.stringify(global.folder_paths ))

}

//----------------------------------------------------------------------------------

function getFolderList(req,res){

  var folders = [];

  for (var folder in global.folder_paths ){
    folders.push(folder)
  }

  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.end(JSON.stringify(folders))

}

//----------------------------------------------------------------------------------

function getFolders(req,res){

  var this_url = req.url.split("?")[0];
  var url_split = this_url.split("/").slice(1);

  var this_plugin = unescape(url_split[1]);
  var this_folder = unescape(url_split[2]);

  if (url_split[2] === undefined) {
    var base_folders = [];
    for (var folder in global.folder_paths[this_plugin]){
      base_folders.push(folder)
    }

    res.setHeader("Content-Type", "application/json");
    res.statusCode = 200;
    res.end(JSON.stringify(base_folders))
    return
  }

  var folder_base = global.folder_paths[this_plugin][this_folder];


  if (folder_base){
    getFoldersRecursiveAsync(folder_base,function(err,data){
      if (err){
        console.log(err)
        res.setHeader("Content-Type", "application/text");
        res.statusCode = 500;
        res.end("ERROR")
        return
      }

      for (var i=0;i<data.length;i++){
        data[i] = this_folder + data[i].substr(folder_base.length).replace(/\\/g,"/")
      }

      res.setHeader("Content-Type", "application/json");
      res.statusCode = 200;
      res.end(JSON.stringify(data))

    })

  } else {
    console.log("No folder defined for:",url_split[1])
    res.setHeader("Content-Type", "text/plain");
    res.statusCode = 200;
    res.end("No Folder Found")
  }
}

//----------------------------------------------------------------------------------

function getPath(req,res){

  var this_url = req.url.split("?")[0];

  var url_split = this_url.split("/").slice(1);

  var this_folder = decodeURIComponent(url_split[1]);

console.log("getpath>>>>>",this_folder)

  getFoldersRecursiveAsync(this_folder, function(err,data){
if (err){
  console.log("error:",err)
}

      if (!data){ data = {folders:[],files:[]}}

    res.setHeader("Content-Type", "application/json");
    res.statusCode = 200;
    res.end(JSON.stringify(data))

  },undefined,true)
}

//----------------------------------------------------------------------------------

function getAudio(req,res){

  var obj = {};

  var this_url = decodeURIComponent(req.url).split("?")[0];
  var url_split = this_url.split("/").slice(1);

  var this_map    = url_split[1];
  var this_folder = url_split[2];
  var folder_path = global.folder_paths[this_map][this_folder];

  obj.file_path = folder_path + path.sep + (url_split.slice(3).join(path.sep) );

  obj.file_name = url_split.slice(-1)[0]

  getBinaryFile(req, res, obj)

}

//----------------------------------------------------------------------------------

function getStream(req,res){

  var obj = {};

  var this_url = decodeURIComponent(req.url).split("?")[0];
  var url_split = this_url.split("/").slice(1);

  var this_map    = url_split[1];
  var this_folder = url_split[2];
  var folder_path = global.folder_paths[this_map][this_folder];

  obj.file_path = folder_path + path.sep + (url_split.slice(3).join(path.sep) );

  obj.file_name = url_split.slice(-1)[0]

  //---------------------------------------------
    fs.stat(obj.file_path, function(err, stats) {
      if (err) {
        if (err.code === 'ENOENT') {
          // 404 Error if file not found
          return res.sendStatus(404);
        }
        res.end(err);
      }

      var range = req.headers.range;
      if (!range) {
       // 416 Wrong range
       return res.sendStatus(416);
      }
      var positions = range.replace(/bytes=/, "").split("-");
      var start = parseInt(positions[0], 10);
      var total = stats.size;
      var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
      var chunksize = (end - start) + 1;

      res.writeHead(206, {
        "Content-Range": "bytes " + start + "-" + end + "/" + total,
        "Accept-Ranges": "bytes",
        "Content-Length": chunksize,
        "Content-Type": "video/mp4"
      });

      var stream = fs.createReadStream(obj.file_path, { start: start, end: end })
      .on("open", function() {
        stream.pipe(res);
      }).on("error", function(err) {
        res.end(err);
      });
    });

}

//----------------------------------------------------------------------------------

function getBinaryFile(req, res, obj){

  if (obj.file_path){

    if (!obj.stat){
      fs.stat(obj.file_path, sendBinaryFile);
    } else {
      sendBinaryFile(null, obj.stat)
    }

    function sendBinaryFile(err, stat){

      var readStream
      try{
        readStream = fs.createReadStream(obj.file_path);
      }catch(e){
        res.setHeader("Content-Type", "application/text");
        res.statusCode = 500;
        res.end("ERROR: File read error: "+e.message)
        return
      }

      // We replaced all the event handlers with a simple call to readStream.pipe()
      readStream.on('open', function () {
      // This just pipes the read stream to the response object (which goes to the client)

        res.writeHead(200, {
            'Content-Type': getMimeType(obj.file_path),
            'Content-Length': stat.size,
            //'Content-Disposition': 'attachment; filename='+encodeURIComponent(obj.file_name)
        });
        readStream.pipe(res);
      });

      readStream.on('error', function(err) {
          res.statusCode = 500;
          res.end(err);
      });
    }

  } else {
    console.log("No folder defined for:",url_split[1])
    res.setHeader("Content-Type", "application/text");
    res.statusCode = 500;
    res.end("ERROR")
  }

}

//----------------------------------------------------------------------------------
function getImage(req,res){


  var obj = {};

  var this_url = decodeURIComponent(req.url).split("?")[0];
  var url_split = this_url.split("/").slice(1);

  var this_map    = url_split[1];
  var this_folder = url_split[2];
  var folder_path = global.folder_paths[this_map][this_folder];

  obj.file_path = folder_path + path.sep + (url_split.slice(3).join(path.sep) );

  obj.file_name = url_split.slice(-1)[0]

  if (obj.file_path){

  //---------------------------------------------

  if (!obj.stat){
    fs.stat(obj.file_path, sendBinaryFile);
  } else {
    sendBinaryFile(null, obj.stat)
  }


  function sendBinaryFile(err, stat){

    var readStream
    try{
      readStream = fs.createReadStream(obj.file_path);
    }catch(e){
      res.setHeader("Content-Type", "application/text");
      res.statusCode = 500;
      res.end("ERROR: File read error: "+e.message)
      return
    }

    // We replaced all the event handlers with a simple call to readStream.pipe()
    readStream.on('open', function () {
    // This just pipes the read stream to the response object (which goes to the client)

      res.writeHead(200, {
          'Content-Type': getMimeType(obj.file_path),
          'Content-Length': stat.size,
          //'Content-Disposition': 'attachment; filename='+encodeURIComponent(obj.file_name)
      });
      readStream.pipe(res);
    });

    readStream.on('error', function(err) {
        res.statusCode = 500;
        res.end(err);
    });
  }

} else {
  console.log("No folder defined for:",url_split[1])
  res.setHeader("Content-Type", "application/text");
  res.statusCode = 500;
  res.end("ERROR")
}



}
//----------------------------------------------------------------------------------


function getDrives(req,res){
/*
  var drivelist = require("drivelist");

  drivelist.list(function(err,disks){
    if (err){
      res.statusCode = 200;
      res.end(err)
      return
    }
    console.log(JSON.stringify(disks))
    res.setHeader("Content-Type", "application/json");
    res.statusCode = 200;
    res.end(JSON.stringify(disks))

  })
*/

//diskinfo does not work on osx :(

var diskInfo = require('diskinfo');

  diskInfo.getDrives(function(err,data){
    if (err){
      console.log("error getting drives:",err)
      res.setHeader("Content-Type", "application/text");
      res.statusCode = 500;
      res.end(JSON.stringify({"error":true,err:err}))
      return
    }

    var drives = {};
    for (var i=0;i<data.length;i++){
      drives[data[i].mounted] = drives[data[i].mounted] || {
        description:"",
        mountpoint: data[i].mounted,
        name: data[i].name
      }
    }

    var drive_arr = []
    for (var drive in drives){
      drive_arr.push(drives[drive]);
    }

    res.setHeader("Content-Type", "application/json");
    res.statusCode = 200;
    res.end(JSON.stringify(drive_arr))
  })

}


gulp.task("diskinfo",function(){
  var drivelist = require("drivelist");

  drivelist.list(function(err,disks){
    console.log(disks)
  })


})

//----------------------------------------------------------------------------------

function getFreakselectorPlugins(req,res){

  var this_url = decodeURIComponent(req.url).split("?")[0];
  var url_split = this_url.split("/").slice(1);

  var folder_base = path.resolve(root_folder+'/freakselector_plugins')

  getFilesRecursiveAsync(folder_base, function(err,data){

    for (var i=0;i<data.length;i++){
      data[i] = '/freakselector_plugins' + data[i].substr(folder_base.length).replace(/\\/g,"/")
    }

    var obj = {
      folders:[],
      files:data
    }

    res.setHeader("Content-Type", "application/json");
    res.statusCode = 200;
    res.end(JSON.stringify(obj))

  }, ".js$", undefined, true)

}

//--------------------------------------------------------------

function getMilkdropPresets(req,res){

}

//--------------------------------------------------------------
//S3 Mime Types

global.mime_types = {
  'a'      : 'application/octet-stream',
  'ai'     : 'application/postscript',
  'aif'    : 'audio/x-aiff',
  'aifc'   : 'audio/x-aiff',
  'aiff'   : 'audio/x-aiff',
  'au'     : 'audio/basic',
  'avi'    : 'video/x-msvideo',
  'bat'    : 'text/plain',
  'bin'    : 'application/octet-stream',
  'bmp'    : 'image/x-ms-bmp',
  'c'      : 'text/plain',
  'cdf'    : 'application/x-cdf',
  'csh'    : 'application/x-csh',
  'css'    : 'text/css',
  'dll'    : 'application/octet-stream',
  'doc'    : 'application/msword',
  'dot'    : 'application/msword',
  'dvi'    : 'application/x-dvi',
  'eml'    : 'message/rfc822',
  'eps'    : 'application/postscript',
  'etx'    : 'text/x-setext',
  'exe'    : 'application/octet-stream',
  'gif'    : 'image/gif',
  'gtar'   : 'application/x-gtar',
  'h'      : 'text/plain',
  'hdf'    : 'application/x-hdf',
  'htm'    : 'text/html',
  'html'   : 'text/html',
  "ico"    : "image/ico",
  'jpe'    : 'image/jpeg',
  'jpeg'   : 'image/jpeg',
  'jpg'    : 'image/jpeg',
  'js'     : 'application/x-javascript',
  'json'   : 'text/json',
  'ksh'    : 'text/plain',
  'latex'  : 'application/x-latex',
  'm1v'    : 'video/mpeg',
  'man'    : 'application/x-troff-man',
  'me'     : 'application/x-troff-me',
  'mht'    : 'message/rfc822',
  'mhtml'  : 'message/rfc822',
  'mif'    : 'application/x-mif',
  'mov'    : 'video/quicktime',
  'movie'  : 'video/x-sgi-movie',
  'mp2'    : 'audio/mpeg',
  'mp3'    : 'audio/mpeg',
  'mp4'    : 'video/mp4',
  'mpa'    : 'video/mpeg',
  'mpe'    : 'video/mpeg',
  'mpeg'   : 'video/mpeg',
  'mpg'    : 'video/mpeg',
  'ms'     : 'application/x-troff-ms',
  'nc'     : 'application/x-netcdf',
  'nws'    : 'message/rfc822',
  'o'      : 'application/octet-stream',
  'obj'    : 'application/octet-stream',
  'oda'    : 'application/oda',
  'ogv'    : 'video/ogg',
  'pbm'    : 'image/x-portable-bitmap',
  'pdf'    : 'application/pdf',
  'pfx'    : 'application/x-pkcs12',
  'pgm'    : 'image/x-portable-graymap',
  'png'    : 'image/png',
  'pnm'    : 'image/x-portable-anymap',
  'pot'    : 'application/vnd.ms-powerpoint',
  'ppa'    : 'application/vnd.ms-powerpoint',
  'ppm'    : 'image/x-portable-pixmap',
  'pps'    : 'application/vnd.ms-powerpoint',
  'ppt'    : 'application/vnd.ms-powerpoint',
  'pptx'    : 'application/vnd.ms-powerpoint',
  'ps'     : 'application/postscript',
  'pwz'    : 'application/vnd.ms-powerpoint',
  'py'     : 'text/x-python',
  'pyc'    : 'application/x-python-code',
  'pyo'    : 'application/x-python-code',
  'qt'     : 'video/quicktime',
  'ra'     : 'audio/x-pn-realaudio',
  'ram'    : 'application/x-pn-realaudio',
  'ras'    : 'image/x-cmu-raster',
  'rdf'    : 'application/xml',
  'rgb'    : 'image/x-rgb',
  'roff'   : 'application/x-troff',
  'rtx'    : 'text/richtext',
  'sgm'    : 'text/x-sgml',
  'sgml'   : 'text/x-sgml',
  'sh'     : 'application/x-sh',
  'shar'   : 'application/x-shar',
  'snd'    : 'audio/basic',
  'so'     : 'application/octet-stream',
  'src'    : 'application/x-wais-source',
  'swf'    : 'application/x-shockwave-flash',
  't'      : 'application/x-troff',
  'tar'    : 'application/x-tar',
  'tcl'    : 'application/x-tcl',
  'tex'    : 'application/x-tex',
  'texi'   : 'application/x-texinfo',
  'texinfo': 'application/x-texinfo',
  'tif'    : 'image/tiff',
  'tiff'   : 'image/tiff',
  'tr'     : 'application/x-troff',
  'tsv'    : 'text/tab-separated-values',
  'txt'    : 'text/plain',
  'ustar'  : 'application/x-ustar',
  'vcf'    : 'text/x-vcard',
  'wav'    : 'audio/x-wav',
  "woff"  : "application/font-woff",
  "woff2" : "application/font-woff",
  'wiz'    : 'application/msword',
  'wsdl'   : 'application/xml',
  'xbm'    : 'image/x-xbitmap',
  'xlb'    : 'application/vnd.ms-excel',
  'xls'    : 'application/vnd.ms-excel',
  'xlsx'    : 'application/vnd.ms-excel',
  'xml'    : 'text/xml',
  'xpdl'   : 'application/xml',
  'xpm'    : 'image/x-xpixmap',
  'xsl'    : 'application/xml',
  'xwd'    : 'image/x-xwindowdump',
  'zip'    : 'application/zip',

  "ttf"   : "application/font-ttf",
  "eot"   : "application/vnd.ms-fontobject",
  "otf"   : "application/font-otf",
  "svg"   : "image/svg+xml",
  "webm"  : "video/webm"

}


global.getMimeType = function(file){
  var filetype = file.toLowerCase().match(/\.(.*?)$/)

  if (filetype !== null){
    var mime_lookup = mime_types[ filetype[1] ];
    if (!mime_lookup){
      console.warn('No MIME Type for: ',file, '  using text/plain')
    }
    return mime_lookup || 'text/plain';
  }
  console.warn('No mine type for: ',file, '  using text/plain')
  return "text/plain"

}

//----------------------------------------------------------------------------------

function runosx(){
  if (freakselectorIsRunning){return}
  var chrome_location = 'open -a "Google Chrome" http://localhost:8080';
  exec(chrome_location, /*chrome_args,*/ function(err, data) {
        if (err){
          console.log(err)
        } else {
          console.log(data.toString());
        }
    }
  );
}

//----------------------------------------------------------------------------------


function runexe(){

  if (freakselectorIsRunning){return}


  console.log("Trying to start web browser, please wait..")

    var possible_chrome_locations = [
      'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe',
      'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe',
      '%USERPROFILE%\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe'
    ]
    var chrome_location;
    for (var i=0;i<possible_chrome_locations.length;i++){
      var get_stat = fs.existsSync(possible_chrome_locations[i]);
      if (get_stat){
        chrome_location = possible_chrome_locations[i]
        console.log(">>>>>>"+chrome_location);
        break
      }
    }

    var chrome_args = [
      '--disable-infobars',
      '--restore-last-session','--false',
      '--app-auto-launched','--true',
      'http://localhost:8080'
    ];

    if ( args['--kiosk'] ){
      chrome_args = [
        '--disable-infobars',
        '--allow-outdated-plugins',
        '--disable-background-timer-throttling',
        '--start-fullscreen',
        '--kiosk',
        '--incognito',
        '--restore-last-session','--false',
        '--app-auto-launched','--true',
        'http://localhost:8080'
      ]
    }

    execFile(chrome_location, chrome_args, function(err, data) {
          if (err){
            console.log(err)
          } else {
            console.log(data.toString());
          }
      }
    );

  //if (start_browser_tmr){clearTimeout(start_browser_tmr);start_browser_tmr = undefined}
  //start_browser_tmr = setTimeout(runexe, 5000)

}

//----------------------------------------------------------------------------------

function saveSetting(req, res){

  var settings_file = req.url.split("/")[2];

  var body = [];

  req.on('data', function(chunk) {

    body.push(chunk);

  }).on('end', function() {

    body = Buffer.concat(body).toString();

    var setting_filename = path.resolve(root_folder + path.sep + "settings/"+ settings_file +".json");

    var setting_data
    try{
      setting_data = JSON.parse(body);
    }catch(e){
      setting_data = {};
    }

    fs.writeFile(setting_filename, JSON.stringify(setting_data), function (err,data) {
      if (err) {
        res.setHeader("Content-Type", "text/plain");
        res.statusCode = 500;
        res.end("Error writing folder_paths.json file to disk")
        return
      }
      res.setHeader("Content-Type", "text/plain");
      res.statusCode = 200;
      res.end("saved")
    })

  });

}

//----------------------------------------------------------------------------------

function getSetting(req,res){

  var settings_file = req.url.split("/")[2];

  var setting_filename = path.resolve(root_folder + path.sep + "settings" + path.sep + settings_file +".json");

  var setting_data_file;
  try{
    fs.readFile(setting_filename, 'utf8', function (err,data) {
      if (err){
        setting_data_file = data || {};
      } else {
        setting_data_file = data;
      }
      res.setHeader("Content-Type", "application/json");
      res.statusCode = 200;
      res.end(setting_data_file);
   })
  }catch(e){
    setting_data_file = {};
    res.setHeader("Content-Type", "application/json");
    res.statusCode = 200;
    res.end(JSON.stringify(setting_data_file));
  }

}

//----------------------------------------------------------------------------------

