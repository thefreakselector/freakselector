
var ws = require('ws');
//-----------------------------------------------------------
global.portFinder			= global.portFinder		|| require('portfinder');

global.live_reload_port = 3100;

var live_reload_tmr

var first_run = true
global.triggerLiveReload = function(resolve){
    if (first_run){
      setTimeout(startLiveReload,500)
      first_run = false;
    } else {
      if (live_reload_tmr){clearTimeout(live_reload_tmr)}
      live_reload_tmr = setTimeout(liveReload,30);
    }
    if (resolve){resolve()}
}

var wss

global.startLiveReload = function(){

	var httpsServer;

	portFinder.basePort = live_reload_port;

	portFinder.getPort(function(err,free_port){

		if (err){
			console.log(cc.set("fg_red",JSON.stringify(err,null,4)))
			return
		}

		global.live_reload_port = free_port;

		console.log("live_reload_port",free_port)

		//-------------------------------------------------

	  var WebSocketServer = ws.Server;

  	wss = new WebSocketServer({port: live_reload_port});

	  wss.on('connection', function connection(ws) {
	    console.log("Livereload Connected")

	    global.livereload_browser_connected = true;

	    ws.on('message', function incoming(message) {
	      var message_parsed = JSON.parse(message);
	      for (var i=0;i<websocketCallbacks.length;i++){
	        websocketCallbacks[i](message_parsed)
	      }
	    });
	    //ws.send('something');
	  });
	})

	//-----------------------------------------------------------

}

var websocketCallbacks = [];
global.onWebsocketMessage = function(callback){
  websocketCallbacks.push(callback)
}

global.liveReload = function(){
  for (var ws_client in wss.clients) {
    wss.clients[ws_client].send('reload');
  }
}

//--------------------------------------------------------------------------------------

gulp.task('trigger_live_reload',triggerLiveReload);

//-------------------------------------------------------------------------------------------------------------------------

gulp.task('stop-servers',function(resolve){
  live_reload_server.close(function () {
    console.log("Live Reload Server Closed")
  });
})
