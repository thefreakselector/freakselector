(function(){

function newTree(config) {

	config = config || {};
	/*
	config.media_type = media type for folder_paths.json.
	config.listholder = DOM element that the tree renders inside. Exposed as treeService.listholder. Must be refreshed by plugin.interface();
	config.getDrives  = callback when getDrives has finished
	config.randomize  = flag to randomize. Exposed as treeService.randomize.
	*/

	var self = {
		//---------------------------------------------------

		init: function(){

			if (config.media_type === undefined) {
				alert("cannot get media type 'undefined'")
				return
			}

			self.listholder = config.listholder;
			self.media_type = config.media_type;
			self.randomize  = config.randomize;

			self.getDrives(function(drives){
console.log("DRIVES",drives)
				self.goback.push({folders:drives, files:[]});
				if (config.getDrives){config.getDrives(drives)}

				self.getRecentlyPlayed();

				self.renderTree();

			})

		},

		//---------------------------------------------------

		refresh: function(){
			self.goback = [];
			self.init();
		},

		//---------------------------------------------------

		getDrives: function(callback){
			df.ajax.send({
				url : "getFolders/" + self.media_type,
				callback:function(data) {
					var drives = df.json.deserialize(data.text);
					if (callback){callback(drives)};
				}
			})
		},

		//---------------------------------------------------

		getDrive: function(drivename, callback){

			df.ajax.send({
				url : "getFolders/" + self.media_type + "/" + drivename,
				callback:function(data) {
console.log(data)					
					var this_tree = df.json.deserialize(data.text);
					if (callback){callback(this_tree)};
				}
			})

		},

		//---------------------------------------------------

		selectFolder:	function(folder) {
			self.getTree(folder,function(tree){
				self.goback.push(self.tree)
				self.tree = tree
				self.renderTree()
			})
		},

		//---------------------------------------------------

		filetypes	: config.filetypes || {
			"mp3":1,
			"wav":1,
			"ogg":1,
			"ovg":1
		},

		//---------------------------------------------------

		goback:[],

		//---------------------------------------------------

		getTree:	function(folder, thiscallback) {

			df.ajax.send({
				url : "getFolders/" + self.media_type + ((folder)?'/'+folder : ''),
				callback:function(data) {
console.log("DATA",data)
					if (thiscallback!=undefined) {
						thiscallback(df.json.deserialize(data.text))
					}

				}
			})
		},

		//---------------------------------------------------

		renderTree:	function() {

			if (self.listholder !== undefined) {
				self.listholder.innerHTML = "";
			} else {
				return
			}

			var password
			var foldername

			var toplevel = (self.goback.length === 1);

			var this_tree = self.goback[self.goback.length-1];
			for (var this_tree_name in this_tree){break};

			var parent_folder_name = "";
			if (!toplevel){
				for (parent_folder_name in self.goback[self.goback.length-2]){break}
			}

			self.tree = this_tree[this_tree_name];

			//-----------------------------------------------------------------------------------

			if (toplevel){

				for (var i=0;i < this_tree.folders.length; i++){
					
					var drivename = this_tree.folders[i];

					df.newdom({
						tagname	: "li",
						target	: self.listholder,
						attributes:{
							className: "folderlist",
							style	: "width:480px;",
							html	: drivename.replace(/\//g,"\\")
						},
						events:	[
							{
								name	: "mouseup",
								handler	: function(evtobj) {
					
									var thispassword = evtobj.srcdata.password
									var thisdrive = evtobj.srcdata.drivename

									if (thispassword!=undefined) {
										prompt({
											message	:"Password protected",
											callback: function(val){

												if (val == thispassword) {
													self.selectFolder(thisfolder)

												}
											},
											bgcolor	: "#ff0000",
											color	: "#ffffff",
											ispassword:true
										})
										return
									}

									self.getDrive(thisdrive, function(new_tree){
										var obj = {};
										obj[thisdrive] = new_tree
										self.goback.push(obj);
										self.renderTree()
									});
								},
								drivename		: drivename,
								password		: password
							}
						]
					})				
				}
			}

			//-----------------------------------------------------------------------------------

			if (!toplevel){

			//-----------------------------------------------------------------------------------

				df.newdom({
					tagname	: "li",
					target	: self.listholder,
					attributes:{
						className: "folderlist",
						style	: "width:480px;",
						html	: ' <span style="color:#ffffff;">[<span style="font-size:25px;font-weight:700;">&#x21EA;</span> Back] <span style="float:right;">'+this_tree_name.replace(/\//g,"\\")+'</span></span>'
					},
					events:[
						{
							name: "mouseup",
							handler: function(evtobj){
								self.goback.pop();
								self.renderTree();
							}
						}
					]
				})


				for (var i=0;i < this_tree[this_tree_name].folders.length; i++){
					
					var this_folder = this_tree[this_tree_name].folders[i];
					for (var folder_name in this_folder){break};

					df.newdom({
						tagname	: "li",
						target	: self.listholder,
						attributes:{
							className: "folderlist",
							style	: "width:480px;",
							html	: folder_name.replace(/\//g,"\\")
						},
						events:	[
							{
								name	: "mouseup",
								handler	: function(evtobj) {
					
									var thispassword = evtobj.srcdata.password
									var folder_tree = evtobj.srcdata.folder_tree

									if (thispassword!=undefined) {
										prompt({
											message	:"Password protected",
											callback: function(val){

												if (val == thispassword) {
													self.selectFolder(thisfolder)

												}
											},
											bgcolor	: "#ff0000",
											color	: "#ffffff",
											ispassword:true
										})
										return
									}

										self.goback.push(folder_tree);
										self.renderTree()
									
								},
								folder_tree		: this_folder,
								password		: password
							}
						]
					})				
				}


			//-----------------------------------------------------------------------------------

				var current_file

				for (var i=this_tree[this_tree_name].files.length-1;i>-1;i--){

					var thisfile = (unescape(this_tree[this_tree_name].files[i])).replace(/\:\\/g,"/").replace(/\:/g,"").replace(/\\/g,"/")

					var file_name = thisfile.split(".").slice(0,-1).join(".");
					var file_ext = thisfile.split(".").pop().toLowerCase();

					if (!self.filetypes[file_ext]){
						this_tree[this_tree_name].files.splice(i,1)
					}

				}

				for (var i=0;i<this_tree[this_tree_name].files.length;i++) {

					var thisfile = (unescape(this_tree[this_tree_name].files[i])).replace(/\:\\/g,"/").replace(/\:/g,"").replace(/\\/g,"/")

					var file_name = thisfile.split(".").slice(0,-1).join(".");

					var thisfilebutton = df.newdom({
						tagname	: "li",
						target	: self.listholder,
						attributes:{
							className: "folderbutton"+((self.currently_playing==thisfile)?" playing":""),
							style	: "width:480px;",
							html	: file_name.split("/").pop().replace(/\//g,"\\") //unescape(thisfile.replace(/_/g," ")).substr(unescape(thisfile.replace(/_/g," ")).lastIndexOf("\\")+1)
						},
						events:	[
							{
								name	: "mouseup",
								handler	: function(evtobj) {

									var this_file = evtobj.srcdata.thisfile //self.tree.files[evtobj.srcdata.idx]

									var selected_file = self.media_type + "/" + self.getPath() + "/" + this_file;

									if (self.randomize){
//										delete self.recently_played[selected_file]
										self.recently_played[selected_file] = Date.now();
										self.saveRecentlyPlayed()
									}

									if (config.fileSelect) {
										config.fileSelect(selected_file)
									};

									self.currently_playing = evtobj.srcdata.thisfile//self.tree.files[evtobj.srcdata.idx]
									self.currently_playing_idx = evtobj.srcdata.idx
									self.renderTree()
								},
								idx		: i,
								thisfile: thisfile
							}
						]
					})
					if (self.currently_playing==thisfile) {
						current_file = thisfilebutton
					}
				}

				if (current_file!=undefined){
					current_file.scrollIntoView()
				}

			//-----------------------------------------------------------------------------------

			}

			//-----------------------------------------------------------------------------------
		}
		,
		//==========================================================================================

		recently_played:{},

		hasRecentlyPlayed: function(this_file){
			var has_played = false;

			if (self.recently_played[this_file]){
				has_played = true
			}

			if (has_played){

				var current_path = self.media_type + "/" + self.getPath() + "/";
				
				var current_folder_played = [];

				var has_all_played = true;

				for (var i=0;i<self.tree.files.length;i++){
					if (self.recently_played[ current_path + self.tree.files[i] ] ){
						var obj = {
							name: current_path + self.tree.files[i], 
							time: self.recently_played[ current_path + self.tree.files[i] ]
						}
						current_folder_played.push(obj)
					} else {
						has_all_played = false;
						break
					}
				}
				
				if (has_all_played){

					current_folder_played.sort(function(a,b){
						if (a.time > b.time) {return -1};
						if (a.time < b.time) {return 1}
						return 0
					})

					delete self.recently_played[current_folder_played.pop().name];

				}

			}

			return has_played
		},

		//----------------------------------------------------------------------------------------------------

		saveRecentlyPlayed: function(){
			df.ajax.send({
				url: "saveRecentlyPlayed/"+self.media_type,
				verb:"post",
				formdata:JSON.stringify(self.recently_played),
				callback:function(ajaxobj){
					
				}
			})
		},

		//----------------------------------------------------------------------------------------------------

		getRecentlyPlayed: function(callback){

			df.ajax.send({
				url: "getRecentlyPlayed/"+self.media_type,
				callback:function(ajaxobj){
					self.recently_played = JSON.parse(ajaxobj.text)
					if (callback){callback(self.recently_played)}
				}
			})

		},

		//----------------------------------------------------------------------------------------------------

		next:	function() {

			if (self.randomize) {
				var new_idx = Math.round(Math.random() * (self.tree.files.length-1))
				if (new_idx === self.currently_playing_idx){
					self.next()
					return
				}
				self.currently_playing_idx = new_idx
			} else {
				self.currently_playing_idx++
			}

			if (self.currently_playing_idx > self.tree.files.length-1) {self.currently_playing_idx = 0}

			var thisfile = self.tree.files[self.currently_playing_idx]
			var selected_file = self.media_type + "/" + self.getPath() + "/" + thisfile;

			if (self.randomize && self.hasRecentlyPlayed(selected_file)){
				self.next();
				return
			}

			self.currently_playing = thisfile

			if (config.fileSelect) {
				config.fileSelect(selected_file)
			};

			self.recently_played[selected_file] = Date.now();
			self.saveRecentlyPlayed()

			self.renderTree()
		},

		//----------------------------------------------------------------------------------------------------

		last:	function() {


			if (self.randomize) {
				var new_idx = Math.round(Math.random() * (self.tree.files.length-1))
				if (new_idx === self.currently_playing_idx){
					self.last()
					return
				}
			} else {
				self.currently_playing_idx--
			}

			if (self.currently_playing_idx < 0) {self.currently_playing_idx = (self.tree.files.length-1) }

			var thisfile = self.tree.files[self.currently_playing_idx]
			var selected_file = self.media_type + "/" + self.getPath() + "/" + thisfile;

			if (self.randomize && self.hasRecentlyPlayed(selected_file)){
				self.next();
				return
			}

			self.currently_playing = thisfile

			if (config.fileSelect) {
				config.fileSelect(selected_file)
			};

			self.recently_played[selected_file] = Date.now();
			self.saveRecentlyPlayed()

			self.renderTree()	
		},


		//----------------------------------------------------------------------------------------------------

		getPath: function(){
			var path_list = [];
			for (var i=1;i<self.goback.length;i++){
				for (var this_path in self.goback[i]){break};
				path_list.push(this_path);
			}
			return path_list.join("/");
		}

		//----------------------------------------------------------------------------------------------------

	}

	self.init();

	return self

}

LED.treeService = newTree; 

})();
