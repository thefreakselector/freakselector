/*
custom slider

two ways to instantiate:

inline:
	<df-slider id="slider_green" name="Green" style="float:left;" class="slider" min="0" max="1" startvalue="1" height="35" width="285" onchange="slide_green"></df-slider>



javascript:

	df.newdom({
		tagname		:"df-slider",
		target		: scrollwin,
		attributes:{
			id			: "slider_scrollspeed",
			name		: "Scroll Speed",
			style		: "float:left;",
			class		: "slider",
			min			: 0,
			max			: 1,
			startvalue	: .5,
			height		: 35,
			width		: 285,
			onchange	: function(val){
				self.scroll_speed = (val * 10)-5
			}
		}

	})


*/
df.gadgets=df.gadgets||{}
df.gadgets.slider = {

	init:	function() {

		var sliders = document.getElementsByTagName("df-slider")
		for (var i=0;i<sliders.length;i++) {
			df.gadgets.slider.initSlider(sliders[i])
		}
	},

	initSlider:	function(thisslider) {

		var thisw = parseInt(thisslider.getAttribute("width"),10)||200
		var thish = parseInt(thisslider.getAttribute("height"),10)||44

		var onchange
		var inline = false
		if (df.objtype(thisslider.getAttribute("onchange")) == "string"){
			onchange = thisslider.getAttribute("onchange")
			inline = true
		} else {
			onchange = thisslider.onchange
		}

		var mousedown
		var mdinline = false
		if (df.objtype(thisslider.getAttribute("onmousedown")) == "string"){
			mousedown = thisslider.getAttribute("onmousedown")
			mdinline = true
		} else {
			mousedown = thisslider.onmousedown
		}

		var mouseup
		var muinline = false
		if (df.objtype(thisslider.getAttribute("onmouseup")) == "string"){
			mouseup = thisslider.getAttribute("onmouseup")
			muinline = true
		} else {
			mouseup = thisslider.onmouseup
		}

		var slider = {
			id			: thisslider.id,
			width		: thisw,
			height		: thish,
			gadget_size	: Math.min(thisw,thish),
			target		: thisslider,
			onchange	: onchange,
			mousedown	: mousedown,
			mdinline	: mdinline,
			mouseup		: mouseup,
			muinline	: muinline,
			inline		: inline,
			thisslider	: thisslider,
			min			: (parseFloat(thisslider.getAttribute("min")))?parseFloat(thisslider.getAttribute("min")):0,
			max			: (parseFloat(thisslider.getAttribute("max")))?parseFloat(thisslider.getAttribute("max")):1,
			startval	: (parseFloat(thisslider.getAttribute("startvalue")))?parseFloat(thisslider.getAttribute("startvalue")):0
		}

		df.gadgets.slider.draw(slider)

	},

	draw:	function(slider) {

			var self = this

			var xmax = 0
			var ymax = 0
			var slider_width = slider.width
			var slider_height = slider.height
			var gadget_top = 0
			var gadget_left = 0

				var newslider = slider.sliderdiv = df.newdom({
					tagname:"input",
					target:slider.target,
					attributes:{
						type:"range",
						style:'width:'+slider_width+'px;height:'+slider_height+'px;position:relative;top:0px;left:0px;-webkit-appearance: '+ ((slider_height > slider_width) ? "slider-vertical" : "none")+';',
						className:"df_slider_race" + ((slider_height > slider_width)?" slider-vertical":" slider-horizontal")
					},
					events:[
						{
							name:"input",
							handler:function(evt){
								self.report(newslider.value, slider)
								evt.cancelBubble=true;
								evt.stopPropagation();
							}
						},
						{
							name:"change",
							handler:function(evt){
								self.report(newslider.value, slider)
								evt.cancelBubble=true;
								evt.stopPropagation();
							}
						},
						{
							name:"click",
							handler:function(evt){
								evt.cancelBubble=true;
								evt.stopPropagation();
								//evt.preventDefault();
							}
						},
						{
							name:"mousedown",
							handler:function(evt){
								evt.cancelBubble=true;
								evt.stopPropagation();
								if(slider.mousedown){slider.mousedown()}
								//evt.preventDefault();
							}
						},
						{
							name:"mouseup",
							handler:function(evt){
								evt.cancelBubble=true;
								evt.stopPropagation();
								if(slider.mouseup){slider.mouseup()}
								//evt.preventDefault();
							}
						}
					]
				})

				slider.this_slider = newslider;

				newslider.value = (slider.startval/slider.max) * 100


	},

	report: function reportHandler(val, slider){
		var adjval = (((1/100) * val) * slider.max) - slider.min ;

		var displayval = adjval.toFixed(2);

		var changeval;
		if (slider.onchange){
			changeval = slider.onchange(adjval)
		}

		var show_value_id = slider.this_slider.parentNode.getAttribute("showvalue");
		if (show_value_id) {
			df.dg(show_value_id).innerHTML = changeval || displayval
		}

	},

	track:function(slider){
		clearTimeout(slider.track_tmr);
//console.log("slider.gadget",slider)

		if (slider.gadget.offsetHeight == 0) {return}

		var thisval = slider.tracker()

		if (slider.last_val !== thisval) {
		}
		slider.last_val = thisval

		slider.track_tmr = setTimeout(function(){df.gadgets.slider.track(slider)},60)
	},

	mousedown:	function(evtobj) {

	},

	mouseup:	function(evtobj) {

	}
}

df.customdoms["df-slider"]={dominit:df.gadgets.slider.initSlider}

df.attachevent({target:window, name:"load", handler:df.gadgets.slider.init })

//df.attachevent({target:window, name:"mouseup", handler:df.gadgets.slider.mouseup })

