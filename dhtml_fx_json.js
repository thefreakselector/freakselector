/*
Code by Damien Otis 2004-2010
License found at http://www.dhtmldf.net/license.html
API Docs at http://www.dhtmldf.net/
*/
dhtmlfx.json = {

	set_json_prop	:	function (thisobj,jsonproperty,selector,newvalue) {
		var df = dhtmlfx
		thisobj = df.dg(df.testid(thisobj));
		var thisjson = df.json.deserialize(thisobj.getAttribute(jsonproperty));
		thisjson[selector] = newvalue;
		thisobj.setAttribute(jsonproperty,df.json.serialize(thisjson));
	},
	get_json_prop	:	function (thisobj,thisjson,selector) {
		var df = dhtmlfx
		try{
			thisobj = df.dg(df.testid(thisobj));
			try{var thisjson = df.json.deserialize("("+thisobj.getAttribute(thisjson)+")")}catch(e){var thisjson={}}
			return thisjson[selector]
		}catch(e){return undefined}
	},

	deserialize : function(thisjson) {
	//	try{
	//		return eval(["(",thisjson,")"].join(""))
	//	}catch(e){return {}}

//not sure which might be faster, need to do a study on this
		if ( (window.JSON!=undefined) ) {
		try{
			return window.JSON.parse(thisjson)
		}catch(e){
			try{
				return eval("("+thisjson+")")
			}catch(e){return {}}		
		}
		}

		try{
			return eval("("+thisjson+")")
		}catch(e){return {}}
	},
	
	serialize : function(thisobj,nofunc,doescape,altquote) {
		var df = dhtmlfx
	
		if ( (window.JSON!=undefined&&doescape!=true&&altquote!=true&&nofunc!=false) ) {
			try{return window.JSON.stringify(thisobj)}catch(e){}
		}
		
		if (thisobj==undefined||thisobj==null) {return ""}
		var qt='"';
		if(altquote==true){qt="'"}else{altquote=false;qt='"'}
		
		if (nofunc==undefined) {nofunc=false}
		if (doescape==undefined) {doescape=false}
		if (thisobj.constructor == Array) {return df.json.getarr(thisobj,nofunc,doescape,altquote)}
		var objtxt=[];
		for (var selector in thisobj) {
			var thisobjitem = thisobj[selector];

			if (nofunc==true&&String(selector).indexOf("HTML")!=-1){
				continue
			}

			if (thisobjitem==undefined) {continue}
			try{
				if (qt=='"') {
					var thisselector = String(selector).replace(/\"/g,'\\"')
				} else {
					var thisselector = String(selector).replace(/\'/g,"\\'")
				}

				if (thisobjitem.constructor==Boolean) {
					objtxt.push(qt+thisselector+qt+':'+thisobjitem);
					continue
				}
				if (thisobjitem.constructor==Object) {
					objtxt.push(qt+thisselector+qt+':'+df.json.getobj(thisobjitem,nofunc,doescape,altquote));
					continue
				}
				if (thisobjitem.constructor==Array) {
//				if (Object.prototype.toString.apply(thisobjitem)==="[object Array]") {
					objtxt.push(qt+thisselector+qt+':'+df.json.getarr(thisobjitem,nofunc,doescape,altquote));
					continue
				}
				if (thisobjitem.constructor==Number) {
					objtxt.push(qt+thisselector+qt+':'+thisobjitem);
					continue
				}
				if (thisobjitem.constructor==Function) {
					if (nofunc!=true) {objtxt.push(qt+selector+qt+":"+thisobjitem)} //String(thisobjitem).match(/function\s.*?\(/,"function (")[0].replace(/function\s/g,"").replace(/\(/g,""))}
					continue
				}
				if (thisobjitem.constructor==Date) {
					objtxt.push(qt+thisselector+qt+':'+qt+thisobjitem.toString()+qt);
					continue
				}
				if (thisobjitem.constructor==String) {
					objtxt.push(df.json.getstring(thisselector,thisobjitem,qt,doescape))
					continue
				}

				continue

				//alert("df.json.serialize: no constructor, "+selector+", "+thisobjitem);
			}catch(e){
				//alert("djhtml_fx_json.serialize error:"+e.message+"\n\nselector: "+selector)}
			}
		};

		return "{"+objtxt.join(",")+"}"
	},


	getarr : function(thisarr,nofunc,doescape,altquote) {
		var df = dhtmlfx
		var qt='"';
		if(altquote==true){qt="'"}else{altquote=false;qt='"'}

		var resultarr=[];
		for (var i=0;i<thisarr.length;i++) {
			var thisarritem = thisarr[i];
			if (thisarritem==undefined) {continue}
			try{

				if (thisarritem.constructor==Object) {
					resultarr.push(df.json.getobj(thisarritem,nofunc,doescape,altquote));
					continue
				}
				if (thisarritem.constructor==Array) {
					resultarr.push(df.json.getarr(thisarritem,nofunc,doescape,altquote));
					continue
				}
				if (thisarritem.constructor==Number) {
					resultarr.push(thisarritem);
					continue
				}
				if (thisarritem.constructor==Boolean) {
					resultarr.push(thisarritem);
					continue
				}
				if (thisarritem.constructor==Date) {
					resultarr.push(thisarritem.toString());
					continue
				}
				if (thisarritem.constructor==String) {
					if (doescape) {
						if (qt=='"') {
							resultarr.push(qt+escape(thisarritem.replace(/\"/g,'\\"'))+qt);
						} else {
							resultarr.push(qt+escape(thisarritem.replace(/\'/g,"\\'"))+qt);
						}
					} else {
						if (qt=='"') {
							resultarr.push(qt+thisarritem.replace(/\"/g,'\\"')+qt);
						} else {
							resultarr.push(qt+thisarritem.replace(/\'/g,"\\'")+qt);
						}
					}
					continue
				}
			}catch(e){
				//alert("djhtml_fx_json.getarr error:"+e.message+"\n\nselector: "+selector)
			}
		};
		return "["+resultarr.join(",")+"]"
	},

	getobj : function(thisobj,nofunc,doescape,altquote) {
		var df = dhtmlfx
		var qt='"';
		if(altquote==true){qt="'"}else{altquote=false;qt='"'}

		var resultarr=[];
		
		for (var selector in thisobj) {
			var thisobjitem = thisobj[selector];
			
			if (thisobjitem==undefined) {continue}
			try{
				if (qt=='"') {
					var thisselector = String(selector).replace(/\"/g,'\\"')
				} else {
					var thisselector = String(selector).replace(/\'/g,"\\'")
				}

				if (thisobjitem.constructor==Object) {
					resultarr.push(qt+thisselector+qt+':'+df.json.getobj(thisobjitem,nofunc,doescape,altquote)+"");
					continue
				}
				if (thisobjitem.constructor==Array) {
					resultarr.push(qt+thisselector+qt+':'+df.json.getarr(thisobjitem,nofunc,doescape,altquote)+"");
					continue
				}
				if (thisobjitem.constructor==Number) {
					resultarr.push(qt+thisselector+qt+':'+thisobjitem);
					continue
				}
				if (thisobjitem.constructor==Boolean) {
					resultarr.push(qt+thisselector+qt+':'+thisobjitem);
					continue
				}
				if (thisobjitem.constructor==Function) {
					if (nofunc!=true) {resultarr.push(qt+selector+qt+":"+thisobjitem)} //String(thisobjitem).match(/function\s.*?\(/,"function (")[0].replace(/function\s/g,"").replace(/\(/g,""))}
					continue
				}
				if (thisobjitem.constructor==Date) {
					resultarr.push(qt+thisselector+qt+':'+qt+thisobjitem.toString()+qt);
					continue
				}
				if (thisobjitem.constructor==String) {
					resultarr.push(df.json.getstring(thisselector,thisobjitem,qt,doescape))
					continue
				}
			}catch(e){
				//alert("djhtml_fx_json.getobj error:"+e.message+"\n\nselector: "+selector)
			}
		};
		return "{"+resultarr.join(",")+"}"
	},
	
	getstring:	function(thisselector,thisobjitem,qt,doescape) {
		if (doescape) {
			if (qt=='"') {
				return qt+thisselector+qt+':'+qt+encodeURIComponent(thisobjitem.replace(/[\n\r]/g,"").replace(/\"/g,'\\"'))+qt
			} else {
				return qt+thisselector+qt+':'+qt+encodeURIComponent(thisobjitem.replace(/[\n\r]/g,"").replace(/\'/g,"\\'"))+qt
			}
		} else {
			if (qt=='"') {
				return qt+thisselector+qt+':'+qt+thisobjitem.replace(/\n/g,"\\n").replace(/\r/g,"\\r").replace(/\"/g,'\\"')+qt
			} else {
				return qt+thisselector+qt+':'+qt+thisobjitem.replace(/\n/g,"\\n").replace(/\r/g,"\\r").replace(/\'/g,"\\'")+qt
			}
		}
	},
/*
	simpleJSONtoOBJ: function(testobj) {
		var df = dhtmlfx
		var test = testobj.substr(1,testobj.length-2)

//		if (test.substr(1,1)=="'") {
//			var tst1 = test.split(",'")		
//		} else {
//			var tst1 = test.split(',"')
//		}

var tst1 = test.split(',')

		var newobj={}

		var inobject = ""
		var inarray = ""
		var dataval = ""
		var selector = ""

		for (var i=0;i<tst1.length;i++) {

			var thistst = tst1[i]
			if (i>0) {thistst='"'+thistst}

			var objsplit = thistst.split(":")

			var startarray = false
			var startobject= false
			var endarray = false
			var endobject= false
			var arrayitem = false
			var isnumber = false
			var isstring = false
			var isboolean= false

			if (objsplit.length==1) {
				arrayitem=true
				if (objsplit[0].substr(objsplit[0].length-1,1)=="]") {endarray=true}
				dataval = objsplit[0]
			} else {
				if (objsplit[0].substr(objsplit[0].length-1,1)=="]") {endarray=true}
				if (objsplit[1].substr(0,1)=="[") {startarray=true}
				if (objsplit[1].substr(0,1)=="{") {startobject=true}
				if (objsplit[1].substr(objsplit[1].length-1,1)=="]") {endarray=true}
				if (objsplit[1].substr(objsplit[1].length-1,1)=="}") {endobject=true}
				dataval = objsplit[1]
				selector = objsplit[0].substr(1,objsplit[0].length-2)
			}

			if (startarray!=false||startobject!=false) {dataval=dataval.substr(1)}
			if (endarray!=false||endobject!=false) {dataval=dataval.substr(0,dataval.length-1)}

			if (isNaN(dataval)) {isstring=true;isnumber=false} else {isnumber=true;isstring=false}
			
			if (dataval=="true") {isboolean=true;isnumber=false;isstring=false;dataval=true}
			if (dataval=="false") {isboolean=true;isnumber=false;isstring=false;dataval=false}

			if (isstring==true) {
				dataval=dataval.substr(1,dataval.length-2)
			}else {
				if (dataval!=""&&isboolean!=true) {
					dataval=parseFloat(dataval)
				}
			}
			
			if (inarray==true&&endarray==false) {
				newobj[selector].push(dataval)
			}

			if (endarray==true&&inarray==true) {
				newobj[selector].push(dataval)
				inarray=""
			}

			if (startarray==true) {
				newobj[selector]=[]
				if (dataval!="") {newobj[selector].push(dataval)}
				if (dataval!="") {
					inarray = true
				}
			}

			if (inobject!="") {
				newobj[inobject][selector] = dataval

			}

			if (startobject==true&&inobject=="") {
				newobj[selector]={}
				var objdataval = objsplit[2]
				if (isNaN(objdataval)) {
					objdataval=objdataval.substr(1,objdataval.length-2)
				} else {
					objdataval = parseFloat(objdataval)
				}
				newobj[selector][dataval]=objdataval
				inobject = selector
			}

			if (endobject==true) {
				newobj[inobject][selector] = dataval
				inobject=""
			}

			if ((isstring==true||isnumber==true)&&startarray==false&&endarray==false&&startobject==false&&endobject==false&&arrayitem==false&&inobject==false) {
				newobj[selector] = dataval
			}

alert(df.json.serialize(newobj))
		//alert("objsplit[0]="+objsplit[0]+"\nselector="+selector+"\nobjsplit[1]="+objsplit[1]+"\nstartarray="+startarray+"\ndataval="+dataval+"\nstartobject="+startobject+"\nendarray="+endarray+"\nendobject="+endobject+"\narrayitem="+arrayitem+"\nisnumber="+isnumber+"\nisstring="+isstring+"\n\n\n"+df.json.serialize(newobj))


		}

		return newobj
	},	
*/


	copyobject:	function(thisobject) {
		var df = dhtmlfx
		return df.json.deserialize(df.json.serialize(thisobject))
	},

	sortobject:	function(thisarray,objectpath,newdir) {
	//moved to dhtmlfx root
		return df.sortobject(thisarray,objectpath,newdir)
	}
	
};

