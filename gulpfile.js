

global.fs             = require("fs");
global.path           = require("path");


global.gulp           = require('gulp');

global.runSequence    = require('run-sequence');

global.gulpServe			= require('gulp-serve');

global.requireDir     = require('require-dir');

global.getFilesRecursiveAsync = require('get-files-recursive-async');
global.getFoldersRecursiveAsync = require('get-folders-recursive-async');
global.cc             = require("node-console-colors");

global.root_folder    = __dirname;

global.atob           = require("atob");

global.proc           = require("child-proc");

global.cp             = require("child_process");
global.exec           = cp.exec;
global.execFile       = cp.execFile;
global.spawn          = cp.spawn;

global.os             = require('os');
global.current_os	  = os.platform().toLowerCase();

//--------------------------------------------------------------------------------------
//Require all files in gulp folder

requireDir(path.resolve(root_folder + '/Gulp'), {recurse:true});

global.args = (function(){var args={};process.argv.slice(2).forEach(function(val,idx,arr){args[val.split("=")[0]]=val.split("=")[1]||true});return args})();
console.log("ARGS:\n\n",JSON.stringify(args,null,4))
//--------------------------------------------------------------------------------------


gulp.task('watch',["watchGulp"],function(){
	gulp.watch([path.resolve(root_folder+"/*.js"), path.resolve(root_folder+"/freakselector_plugins/*.js")],function(evt){
		console.log(evt)
	})

	gulp.watch([path.resolve(root_folder+"/folder_paths.json")], function(evt){
		console.log(evt)
		global.folder_paths = require(path.resolve(root_folder+"/folder_paths.json"));
	})
})

gulp.task('serve', [/*'start-fadecandy',*/'start_static_server'], function(){

})

//--------------------------------------------------------------------------------------------
//Restart gulp if any gulp file is updated.
;(function(){

	var options = {
		'gulpfile'     : './gulpfile.js',
		'task'         : 'start_static_server',
		'pathToGulpjs' : './node_modules/gulp/bin/gulp.js'
	};


	var child_process = require('child_process');
	var gulpProcess   = null;
	var platform      = process.platform;

	gulp.task('watchGulp', ['spawn-child_process'], function() {

		getFilesRecursiveAsync(path.resolve(root_folder+"/Gulp"), function(err,gulpfiles){

			gulpfiles.push(path.resolve(root_folder+'/gulpfile.js'))

			gulp.watch(gulpfiles, ['kill-child_process', 'spawn-child_process']);

		})
	});

	//---------------------------------------------------------

	gulp.task('kill-child_process', function() {
		if(gulpProcess === null) {
			return;
		}
		console.log('kill gulp ( PID : ' + gulpProcess.pid + ' )');
		if(platform === 'win32') {
			child_process.exec('taskkill /PID ' + gulpProcess.pid + ' /T /F', function() {});
			return;
		}
		gulpProcess.kill();
	});

	//---------------------------------------------------------

	gulp.task('spawn-child_process', function() {
		var restart = '';
		//if(gulpProcess !== null) {
			restart = '--restart';
		//}
		gulpProcess = child_process.spawn('node', [options.pathToGulpjs, options.task, restart, '--gulpfile='+options.gulpfile], {stdio: 'inherit'});
		console.log('spawn gulp' + options.task + ' ( PID : ' + gulpProcess.pid + ' )');
	});

	//---------------------------------------------------------

	gulp.task("testfolders",function(){
		getFoldersRecursiveAsync("D:\\Greencloud\\Media\\Music\\", function(err,data){
console.log("getFolders:",err, JSON.stringify(data,null,2))
		}, [".mp3$"])
	})

//----------------------------------------------------------------------------------

  gulp.task("start-fadecandy",function(){

  	if (current_os === 'win32'){
	    var fc_server = path.resolve(global.root_folder+'\\FadeCandy\\bin\\fcserver.exe');
	    var config_file = path.resolve(global.root_folder+'\\Pixelmaps\\jensbikebasket.fadecandy.config.json');
console.log(">>>>>",config_file)
	    exec(fc_server + ' ' + config_file,{
	      detached:false
	    }, function(err, data){
	console.log("err, data",err, data);
	    });

  	} else if (current_os === 'darwin') {

		  var fadecandy_location = './Fadecandy/bin/fcserver-osx';
		  execFile(fadecandy_location, function(err, data) {
	        if (err){
	          console.log(err)
	        } else {
	          console.log(data.toString());
	        }
		    }
		  );
  	}
console.log(fc_server);
  })

	//---------------------------------------------------------
  gulp.task("kill-fadecandy",function(done){
		exec('taskkill /F /IM "fcserver.exe"', function(err, data) {
console.log(err,data)
			done();
		})
  })

  gulp.task('restart-fadecandy',function(resolve){
  	runSequence([ 'kill-fadecandy', 'start-fadecandy', resolve] )
  })
	//---------------------------------------------------------

process.on('uncaughtException', function (exception) {
   // handle or ignore error
   console.log(exception)
});

	//---------------------------------------------------------


})();
