/*
Code by Damien Otis 2004-2010
License found at http://www.dhtmldf.net/license.html
API Docs at http://www.dhtmldf.net/
*/

dhtmlfx.debug = {

	debugmode	: false,
	debugwindow	: "",
	debugwindowready: 0,
	debugarray	: [],
	debugline	: 0,
	debugtmr	: 0,

	remotereport	: false,
	remote_debug_userkey : "",
	clientid	: "",
	write_to_console: true,

	opendebugtmr:0,

	debugwin:	function(datatowrite) {
		var df = dhtmlfx
//usage:
//df.debug.debugwin({title:"whatever",message:"any message",color:"red"})

//alert(df.debug.getcaller(df.debug.debugwin.caller)+"\n\n"+df.debug.debugwin.caller)
//		if (df.debug.debugarray.length>200) {return}
		try{
			datatowrite.caller = df.debug.getcaller(df.debug.debugwin.caller)
		}catch(e){
			datatowrite.caller = "unknown"
		}
		if (datatowrite.force_remote==true) {
			df.debug.remote_debug_userkey = "dhtmlfx"
			df.debug.clientid = "unknown"
		}

		if (df.debug.remotereport!=false||datatowrite.force_remote==true&&datatowrite!="") {df.debug.remote_report(datatowrite)}

		try{if (df.debug.debugmode == false) {return}}catch(e){df.debug.debugmode=false;return}
		if (window.console!=undefined&&df.debug.write_to_console==true) {
			console.error(datatowrite,datatowrite.title,datatowrite.message.replace(/\<br\>/g,"\n").replace(/\<br\/\>/g,"\n"));
			return
		}

		if (df.debug.debugwindowready==1) {
			if (datatowrite!=""&&datatowrite!=undefined) {df.debug.debugarray.push(datatowrite)}
			df.debug.debugtmr = setTimeout(function(){df.debug.debugwin('')},20);
			return;
		}
		
		if (df.debug.debugwindowready==0) {
			try{clearTimeout(df.debug.opendebugtmr)}catch(e){}
			if (datatowrite!=""&&datatowrite!=undefined) {df.debug.debugarray.push(datatowrite)}
			df.debug.opendebugtmr = setTimeout(function() {
				
				df.debug.debugwindowready = 1

				try{

				var dhtmlfxcookie={};
				try{var dhtml_fx_cookie = df.cookie.get("dhtml_fx_debug")}catch(e){var dhtml_fx_cookie=null}
				if (dhtml_fx_cookie!=null) {
					dhtmlfxcookie=df.json.deserialize(dhtml_fx_cookie);
				}

				try{df.debug.debugwindow = window.open("about:blank","dhtml_fx_debug"+df.now(),"width="+(dhtmlfxcookie.debugwinwidth||800)+",height="+(dhtmlfxcookie.debugwinheight||600)+",top="+(dhtmlfxcookie.debugwintop)+",left="+(dhtmlfxcookie.debugwinleft)+",menubar=false,location=no,resizable=yes,status=no,titlebar=no,scrollbars=yes")}catch(e) {alert(e.description)}
				//try{
				//	window.addEventListener("beforeunload",	df.debug.closewindow,false);
				//}catch(e){
				//	alert(e.description);
					//document.body.attachEvent("onbeforeunload",	df.debug.closewindow);
				//}

				try{df.debug.debugwindow.document.open()}catch(e) {}
				debughtml=[];
				debughtml.push('<body onbeforeunload="try{window.opener.df.debug.closewindow()}catch(e){};" id="body" style="font-family:arial;font-size:12px;">');
				debughtml.push('<div style="font-size:14px;font-weight:700;padding:5px;">DEBUG WINDOW</div>Filter:<input onkeyup="window.opener.df.debug.filterupdate()" type="text" id="filter"/><input type="button" value="Clear" style="font-size:10px;font-family:verdana;" onclick="document.getElementById(\'filter\').value=\'\';window.opener.df.debug.filterupdate()"/><br/><br/><b>Start of Debug Session: '+new Date()+'</b><br/>Persist<input type="checkbox" id="sticky"/><hr/>\n');
				debughtml.push('<scr'+'ipt>window.df=window.opener.df;window.opener.df.debug.debugwindowready = 2;window.opener.focus()</scr'+'ipt>')
				try{df.debug.debugwindow.document.write(debughtml.join(""))}catch(e){}
				df.debug.debugline++;

				//df.debug.debugwindowready = 2
				df.debug.debugtmr = setTimeout(function(){df.debug.debugwin('')},300);
				}catch(e){alert("df.debug.debugwin - error opening debug window:\n"+e.message+"\n"+e.description)}
				
				//
			},100)
			return

		} else {

		try{
				try{clearTimeout(df.debug.debugtmr)}catch(e){}

				if (df.debug.debugwindowready!=2) {
					if (datatowrite!="") {
						df.debug.debugarray.push(datatowrite);
					}
					df.debug.debugtmr = setTimeout(function(){df.debug.debugwin('')},20);
					return;
				}

				if (datatowrite!=""&&datatowrite!=undefined) {
					df.debug.debugarray.push(datatowrite);
				}

				do {
					var thisdatatowrite = df.debug.debugarray.shift()
					try{
						var thisdisplay="none";
						var errid = df.unique()
						if (df.debug.debugwindow.document.getElementById("filter").value==""||thisdatatowrite.title==df.debug.debugwindow.document.getElementById("filter").value) {thisdisplay="block"}
						var datahtml=[]
						datahtml.push('<div id="debug_'+df.debug.debugline+'" style="width:100%;display:'+thisdisplay+';">')
						datahtml.push('<div onclick="document.getElementById(\'filter\').value=this.innerHTML;window.opener.df.debug.filterupdate()" id="debug_title_'+df.debug.debugline+'" style="font-weight:700;color:'+(thisdatatowrite.color||"black")+';cursor:pointer;">')
						datahtml.push(thisdatatowrite.title)
						datahtml.push('</div>')
						datahtml.push('<div style="font-family:arial;font-size:11px;">')
						datahtml.push(thisdatatowrite.message)
						datahtml.push('</div>')
//report caller function text
//						datahtml.push('<input type="button" value="more.." onclick="document.getElementById(\'errfunc_'+errid+'\').style.display=\'block\'"/>')
//						datahtml.push('<div id="errfunc_'+errid+'" style="display:none;font-family:arial;font-size:11px;">')
//						datahtml.push(String(thisdatatowrite.caller).replace(/[\n\f]/g,"<br>").replace(/\\/g,"\\").replace(/[^a-zA-Z0-9\{\}\!\@\#\$\%\=\^\&\*\(\)\[\]\{\}\\\|\;\'\:\"\,\.\<\>\?\/]/g,'&nbsp;&nbsp;&nbsp;&nbsp;'))
//						datahtml.push('</div>')

						datahtml.push('<hr/></div>\n')
						try{df.debug.debugwindow.document.write(datahtml.join(""));}catch(e){}
						df.debug.debugline++;
					}catch(e){
						df.debug.debugarray.push(thisdatatowrite);
						//alert('df.debug error 1')
						//df.debug.debugtmr = setTimeout("df.debug.debugwin('')",10);
					}
				} while (df.debug.debugarray.length>0)

			try{df.debug.debugwindow.document.body.scrollTop = df.debug.debugwindow.document.body.scrollHeight}catch(e){}

		}catch(e){
			//df.debug.debugwindow="";
			//df.debug.debugwin(datatowrite)
			alert('df.debug error 2:\n'+e.description+"\n"+e.message+"\n"+fx.json.serialize(datatowrite))
		}
		}
	},

	filterupdate:	function() {
		var df = dhtmlfx
		var thisfilter = df.debug.debugwindow.document.getElementById("filter").value;
		var docall = df.debug.debugwindow.document.getElementsByTagName("*");
		for (var i=0;i<docall.length;i++) {
			if (String(docall[i].id).indexOf("debug_title_")!=-1) {
				if (String(docall[i].innerHTML)==thisfilter||thisfilter=="") {
					df.debug.debugwindow.document.getElementById("debug_"+String(docall[i].id).split("_")[2]).style.display="block";
				} else {
					df.debug.debugwindow.document.getElementById("debug_"+String(docall[i].id).split("_")[2]).style.display="none";
				}
			}
		}
	},

	closewindow:	function(windowobj) {
		var df = dhtmlfx
	try{
		if (windowobj==undefined) {windowobj = df.debug.debugwindow}
		var winleft	= windowobj.screenLeft||windowobj.screenX;
		var wintop	= windowobj.screenTop||windowobj.screenY;
		var winwidth	= windowobj.document.body.clientWidth||windowobj.innerWidth;
		var winheight	= windowobj.document.body.clientHeight||windowobj.innerHeight;

		try{
			var dhtmlfxcookie={};
			var dhtml_fx_cookie = df.cookie.get("dhtml_fx_debug");
			if (dhtml_fx_cookie!=null) {
				dhtmlfxcookie=df.json.deserialize(dhtml_fx_cookie);
			}

			dhtmlfxcookie.debugwinleft	= winleft;
			dhtmlfxcookie.debugwintop	= wintop;
			dhtmlfxcookie.debugwinwidth	= winwidth;
			dhtmlfxcookie.debugwinheight	= winheight;
			df.cookie.set("dhtml_fx_debug",df.json.serialize(dhtmlfxcookie));
		}catch(e){}

		df.debug.debugwindow.close()
		df.debug.debugwindow="";
		df.debug.debugwindowready=0
	}catch(e){}
	},

	closedebug:	function() {
		var df = dhtmlfx
		try{
			if (df.debug.debugwindowready == 2) {
				if (df.debug.debugwindow.document.getElementById("sticky").checked!=true) {
					try{df.debug.closewindow(df.debug.debugwindow)}catch(e){}
				}
			}
		}catch(e){
			df.debug.debugwindow.close()
			df.debug.debugwindow="";
			df.debug.debugwindowready = 0
		}
	},
	
	remote_report:	function(datatowrite) {
	//MUST SET df.debug.remote_debug_userkey or the back-end will refuse the call
	//Must also enable df.debug.remotereport = true to enable logging to the dhtmldf.net site database
		var df = dhtmlfx

		if (datatowrite==""){return}

		if (df.debug.remote_debug_userkey=="") {return}
				
		errordata={}
		errordata.url = String(location)
//		errordata.key = encodeURIComponent(df.debug.remote_debug_userkey)
		errordata.loc= encodeURIComponent(location)
		errordata.data= encodeURIComponent(df.json.serialize(datatowrite))
		errordata.usertimecode= new Date()
		errordata.browser={}
		
		for (var selector in navigator) {
			var sel = String(selector)
			try{
				var thisval = String(navigator[selector])
			}catch(e){
				continue
			}
			if (thisval.indexOf("function ")!=-1) {continue}
			if (thisval=="") {continue}
			if (thisval.substr(0,1)=="["){continue}
			errordata.browser[selector] = encodeURIComponent(navigator[selector])
		}
		for (var selector in screen) {
			var sel = String(selector)
			try{
				var thisval = String(screen[selector])
			}catch(e){
				continue
			}
			if (thisval.indexOf("function ")!=-1) {continue}
			if (thisval=="") {continue}
			if (thisval.substr(0,1)=="["){continue}
			errordata.browser[selector] = encodeURIComponent(screen[selector])
		}
		errordata.browser.windowwidth = df.windowwidth()
		errordata.browser.windowheight = df.windowheight()
		
		var thisbrowser = df.browser
		var thisbver = df.bver
		if (df.unknown_browser!=undefined) {thisbrowser = df.navigator;thisbver = 'not available'}
		

		df.ajax.sendxssdata({
			url:"http://www.dhtmlfx.net/remote_report/remote_report.aspx",
			id:df.debug.clientid+'_'+df.unique(),
			querystring:{
				debugkey:df.debug.remote_debug_userkey,
				clientid:df.debug.clientid,
				browser:thisbrowser,
				bver:thisbver,
				title:(datatowrite.title||"")
			},
			xsserror:df.debug.remote_xsserror,
			textdata:df.json.serialize(errordata),
			callback:"df.debug.remote_callback"
		})
	},

	remote_callback:	function(remotedata) {
		var df = dhtmlfx
		if (remotedata==false) {
			df.debug.remotereport = false
		}
	},

	remote_xsserror:	function(ajaxobj) {
	//If there is a server error, disable remote reporting.
		var df = dhtmlfx
		df.debug.remotereport = false
	
	},
	
	getcaller:	function(thiscaller,thisobj,thispath) {
		var df = dhtmlfx
	try{
		if (thisobj==undefined) {thisobj = window}
		if (thispath==undefined) {thispath="window."}
		var functext = String(thiscaller).replace(/[\r\n]/g,"").substr(0,200)
		try{
		for (var selector in thisobj) {
			try{
				if (String(thisobj[selector]).replace(/[\r\n]/g,"").substr(0,200)==functext) {
					return thispath+selector
				}
				if (thisobj[selector]==undefined||thisobj[selector].constructor!=Object) {continue}
				var subpath = thispath+selector+"."
				var subcaller = df.debug.getcaller(thiscaller,thisobj[selector],subpath)
				if (subcaller!=undefined) {
					return subcaller
				}
			}catch(e){}
		}
		}catch(e){}
		return "caller unknown"
	}catch(e){
		df.debug.debugwin({title:"df.debug.getcaller",message:e.description+"<br>"+e.message,color:"red"})
		return "caller unknown"
	}
	},

//----------------------------------------------------------------------

	funcedit:	function(funcname) {
		var df = dhtmlfx
		var editwidth = 800
		var editheight = df.windowheight()-40
		
		var newdom = df.newdom({tagname:"div",attributes:{id:"dhtmlfx_funcedit",style:"position:fixed;top:10px;left:100px;width:"+editwidth+"px;height:"+editheight+"px;background-color:#aabafa;border:1px solid #5a6a8a;padding:10px;z-index:9999999999",mousefx:"{floating:true}"}})
		var edithtml = []

		//var funcname = df.debug.getcaller(thisfunc)

		edithtml.push('<input mousefx="{floating:\'nomove\'}" style="float:left;width:380px;font-family:arial;font-size:12px;font-weight:700;margin-bottom:7px;" align="left" value="'+funcname+'"/>')
		edithtml.push('<input mousefx="{floating:\'nomove\'}" type="button" onclick="df.debug.funcedit_run(\''+funcname+'\')" value="Run" style="font-size:11px;width:100px;height:20px;float:right;"/>')
		edithtml.push('<input mousefx="{floating:\'nomove\'}" type="button" onclick="df.debug.funcedit_save(\''+funcname+'\');df.debug.funcedit_run(\''+funcname+'\')" value="Save & Run" style="font-size:11px;width:100px;height:20px;float:right;"/>')
		edithtml.push('<input mousefx="{floating:\'nomove\'}" type="button" onclick="df.debug.funcedit_cancel()" value="Cancel" style="font-size:11px;width:100px;height:20px;float:right;"/>')
		edithtml.push('<input mousefx="{floating:\'nomove\'}" type="button" onclick="df.debug.funcedit_save(\''+funcname+'\')" value="UPDATE" style="font-size:11px;width:100px;height:20px;float:right;"/>')
		edithtml.push('<textarea mousefx="{floating:\'nomove\'}" id="dhtmlfx_funcedit_textarea" spellcheck="false" style="width:'+(editwidth-20)+'px;height:'+(editheight-50)+'px;font-family:courier;clear:both;">')
		edithtml.push(String(eval(funcname)))
		edithtml.push('</textarea>')

		newdom.innerHTML = edithtml.join("")
	},

	funcedit_save:	function(thisfunc) {
		var df = dhtmlfx
	try{
		var newfunc = df.dg("dhtmlfx_funcedit_textarea").value//.replace(/[\r\n]/g,"") //.replace(/\(.*?\)/,"")
		eval(thisfunc+" = "+newfunc)
	}catch(e){df.debug.debugwin({title:"funcedit_save",message:e.description+"<br>"+e.message,color:"red"})}		
	},

	funcedit_run:	function(thisfunc) {
		var df = dhtmlfx

		var functext = String(eval(thisfunc))
		var funcin = functext.split("{")[0].split(" (")[1].replace(/[\(\)]/g,"").replace(/\s.*?/g,"")
		var funcins=[]
		if (funcin.length>0) {
			funcins = funcin.split(",")
		}

		//if (funcins.length>0) {
			//alert('inputs:\n\n'+funcins.join("\n"))
		//}

		var thisret = eval(thisfunc+"()")

		if (thisret!=undefined) {
			alert("Run:\n\n"+thisret)
		}
	},

	funcedit_cancel:	function() {
		var df = dhtmlfx
		df.dg("dhtmlfx_funcedit").style.display="none"
		df.removedom(df.dg("dhtmlfx_funcedit"))	
	},
//----------------------------------------------------------------------
	monitoring:{},
	monitorinterval:0,
	monitorvalue:	function(thisvar) {
		var df = dhtmlfx
	try{	
		var editwidth = 500

		if (df.debug.monitoring[thisvar]==undefined) {
			try{df.debug.monitoring[thisvar]={ref:eval(thisvar)}}catch(e){alert("error adding: "+thisvar)}
		}

		if (df.debug.monitorinterval==0) {
			var newdom = df.newdom({tagname:"div",attributes:{id:"dhtmlfx_monitor",style:"position:fixed;top:10px;left:100px;background-color:#aabafa;border:1px solid #5a6a8a;padding:10px;z-index:9999999999",mousefx:"{floating:true}"}})
			df.debug.monitorinterval = setInterval(function() {
			try{
				var monhtml=['<table>']
				for (var selector in df.debug.monitoring) {
					var thisobj = eval(selector)
					var thistype = df.objtype(thisobj)
					if (thistype=="object"||thistype=="array") {
						monhtml.push('<tr><td>'+selector+'</td><td>'+df.json.serialize(thisobj,true)+'</td></tr>')
					} else {
						try{
							monhtml.push('<tr><td>'+selector+'</td><td>'+thisobj+'</td></tr>')
						}catch(e){
							monhtml.push('<tr><td>'+selector+'</td><td style="color:red;">[error]</td></tr>')
						}
					}
				}
				if (monhtml.length==1) {
					clearInterval(df.debug.monitorinterval);
					df.debug.monitorinterval=0
				}
				monhtml.push('</table>')
				df.dg("dhtmlfx_monitor").innerHTML = monhtml.join("")
			}catch(e){df.debug.debugwin({title:"monitorvalue innerloop",message:e.description+"<br>"+e.message+"<br>selector="+selector,color:"red"})}
			},10)
		}
	
	}catch(e){df.debug.debugwin({title:"monitorvalue",message:e.description+"<br>"+e.message,color:"red"})}
	},
//----------------------------------------------
//experimental. trying to reload one function from an object literal for fast development updates
//load new script into an iframe, then pick out the function and move to code in parent window. almost working.
	devscript:	function() {
	try{
		var thisdom = df.newdom({tagname:"div",attributes:{id:"dhtmlfx_devscript_ui",style:"position:fixed;width:350px;height:65px;top:10px;left:10px;z-index:999999999;padding:10px;background-color:#cacaca;border:1px solid #7a7a7a;",mousefx:"{floating:true}"}})
		devhtml=[]
		devhtml.push('<div style="float:left;width:50px;">file:</div><input type="text" id="dhtmlfx_devscript_scriptfile" style="width:298px;float:left;" value="/scripts/ranker_render_mainlist.js"/>')
		devhtml.push('<div style="clear:both;width:50px;float:left;">path:</div><input type="text" id="dhtmlfx_devscript_funcpath" style="width:298px;float:left;" value="ranker.render.mainlist.init"/>')
		devhtml.push('<input type="button" id="dhtmlfx_devscript_load" value="LOAD" style="clear:both;float:right;width:100px;height:30px;" onclick="df.debug.devscriptload(df.dg(\'dhtmlfx_devscript_scriptfile\').value,df.dg(\'dhtmlfx_devscript_funcpath\').value)"/>')
		thisdom.innerHTML=devhtml.join("")
	}catch(e){df.debug.debugwin({title:"df.debug.devscript",message:e.description+"<br>"+e.message,color:"red"})}
	},
	
	devscriptload:	function(scriptfile,funcpath) {
	try{
		var devframe = df.dg("dhtmlfx_debug_devscriptload_frame")
		if (devframe!=null) {
			df.removedom(devframe)
		}
		var devframe = df.newdom({
			tagname:"iframe",
			attributes:{
				id:"dhtmlfx_debug_devscriptload_frame",
				style:"width:100px;height:100px;position:absolute;top:-200px;left:-200px;"
			}
		})

		var functionpath = funcpath.split(".")

		var makepath = "devframe.contentWindow"
		for (var i=0;i<functionpath.length-1;i++) {
			eval(makepath+"."+functionpath.slice(0,i+1).join(".")+"={}")
		}

		var targetpath=[window]
		for (var i=0;i<functionpath.length;i++) {
			targetpath.push(targetpath[i][functionpath[i]])
		}
		
		
		devframe.contentDocument.open()
		devframe.contentDocument.write('<scr'+'ipt src="'+scriptfile+'"></'+'script>') //scriptfile = /scripts/ranker_render_mainlist.js
		devframe.contentDocument.close()


		df.waitfor(function(){try{var testfunc = eval("devframe.contentWindow."+funcpath);return (testfunc!=undefined)}catch(e){}},function(){

			var paths=[devframe.contentWindow]
			for (var i=0;i<functionpath.length;i++) {
				paths.push(paths[i][functionpath[i]])
			}
//why wont this work?
//is it because of local variable scope?

			//does not work:
			//targetpath[targetpath.length-1] = paths[paths.length-1]

			//works:
			//ranker.render.mainlist.init = paths[paths.length-1]
			
			
			//thisfunc =  eval("("+remotefunctionpath.toString()+")")  //window.ranker.render.mainlist.init = devframe.contentWindow.ranker.render.mainlist.init

		})

	}catch(e){df.debug.debugwin({title:"df.debug.devscriptload",message:e.description+"<br>"+e.message,color:"red"})}
	}
	

};

try{window.addEventListener("beforeunload",	window.dhtmlfx.debug.closedebug,false)	}catch(e){window.attachEvent("onbeforeunload",	window.dhtmlfx.debug.closedebug)}
