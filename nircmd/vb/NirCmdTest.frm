VERSION 5.00
Begin VB.Form frmNirCmdTest 
   Caption         =   "NirCmd DLL Test"
   ClientHeight    =   4395
   ClientLeft      =   3030
   ClientTop       =   3285
   ClientWidth     =   6585
   LinkTopic       =   "Form1"
   ScaleHeight     =   4395
   ScaleWidth      =   6585
   Begin VB.CommandButton cmdMonitorOff 
      Caption         =   "Turn Monitor Off"
      Height          =   735
      Left            =   1800
      TabIndex        =   4
      Top             =   3480
      Width           =   2655
   End
   Begin VB.CommandButton cmdUnmuteVolume 
      Caption         =   "Unmute System Volume"
      Height          =   735
      Left            =   1800
      TabIndex        =   3
      Top             =   2640
      Width           =   2655
   End
   Begin VB.CommandButton cmdMuteVolume 
      Caption         =   "Mute System Volume"
      Height          =   735
      Left            =   1800
      TabIndex        =   2
      Top             =   1800
      Width           =   2655
   End
   Begin VB.CommandButton CmdCloseCDRom 
      Caption         =   "Close CD-ROM Door"
      Height          =   735
      Left            =   1800
      TabIndex        =   1
      Top             =   960
      Width           =   2655
   End
   Begin VB.CommandButton cmdOpenCDRom 
      Caption         =   "Open CD-ROM Door"
      Height          =   735
      Left            =   1800
      TabIndex        =   0
      Top             =   120
      Width           =   2655
   End
End
Attribute VB_Name = "frmNirCmdTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Sample for using NirCmd DLL
'For more information about NirCmd:
'http://www.nirsoft.net/utils/nircmd.html

Private Declare Function DoNirCmd Lib "nircmd.dll" (ByVal lpszCommand As String) As Integer


Private Sub CmdCloseCDRom_Click()
    DoNirCmd "cdrom close"
End Sub

Private Sub cmdMonitorOff_Click()
    DoNirCmd "monitor off"
End Sub

Private Sub cmdMuteVolume_Click()
    DoNirCmd "mutesysvolume 1"
End Sub

Private Sub cmdOpenCDRom_Click()
    DoNirCmd "cdrom open"
End Sub

Private Sub cmdUnmuteVolume_Click()
    DoNirCmd "mutesysvolume 0"
End Sub
