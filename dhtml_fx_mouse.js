/*
Code by Damien Otis 2004-2010
License found at http://www.dhtmlfx.net/license.html
API Docs at http://www.dhtmlfx.net/
This library requires dhtml_fx.js and dhtml_fx_json.js
mousefx v.8
*/
dhtmlfx.mouse = {
	custom		:	"mousefx",		
	mousex		:	0,
	mousey		:	0,
	dragoffx	:	0,
	dragoffy	:	0,
	handleoffx	:	0,
	handleoffy	:	0,
	dragaccum	:	0,
	startx		:	0,
	starty		:	0,
	dragdistancex	:	0,
	dragdistancey	:	0,
	copytriggerx	:	10,
	copytriggery	:	10,
	copyopacity	:	60,
	dragobj		:	"",
	mousewheelover	:	{},
	dragz		:	500,
	xdrag		:	"",
	dragmoveobj	:	{},
	dragoverobject	:	"",	// mouseover evetsrc
	noinputs	:	true,	// stops events from acting on inputs, textarea, etc
	mouseisdown	:	false,
	
	leftimportant	:	false,
	topimportant	:	false,

	droptarget	:	"",
	lasttarget	:	"none",
	dropsource	:	"",
	lasttargetobj	:	"",
	
	floatingparent	:	"",
	primaryobject	:	"",
	
	dblclicktmr	:	0,
	dblclickwait	:	220,
	dblclickwaiting	:	false,

	positionabs	:	false,
	positionrel	:	false,
	positionfix	:	false,
	lasttargetfixed	:	false,

	context		:	false,
	
	dropzones	:	[],
	regdroptmr	:	0,
	reg_drops: function(tagname) {
		var df = dhtmlfx
		try{clearTimeout(df.mouse.regdroptmr)}catch(e){}
		df.mouse.regdroptmr = setTimeout(function(){
			window.dhtmlfx.mouse.do_reg_drops(String(tagname))
		},50);
	},

	do_reg_drops: function(tagname,domobj) {
		var df = dhtmlfx
		if (domobj==undefined) {domobj = document}
		df.mouse.dropzones=[];
		if (tagname=="undefined") {tagname=undefined}
		var docall = domobj.getElementsByTagName(tagname||"*");
		var docalllen = docall.length
		for (var i=0;i<docalllen;i++) {
			if (String(docall[i].getAttribute(df.mouse.custom)).indexOf("dodrop")!=-1&&docall[i].style.display!="none") {
				docall[i].setAttribute("domfixed",String(((df.mouse.domfixed(docall[i])==true)?1:0)))
				df.mouse.dropzones.push(docall[i]);
			}
		}
	},


	mouse_down: function (evt) {
		var df = dhtmlfx

		df.mouse.mouseisdown = true

//	console.log("evt",evt)
		//try{var evtsrc=event.srcElement;evt=event}catch(e){var evtsrc=evt.target}
//	console.log("1",evtsrc)
		var evtsrc = evt.srcdom
//	console.log("2",evtsrc)

		df.mouse.dragoverobject = evtsrc;

		var evtsrcx = df.mouse.floatchild(evtsrc,evt);
		var evtsrcchild=evtsrcx;

		df.mouse.primaryobject=evtsrcx;
		if (df.mouse.floatingparent!="") {evtsrcx=df.mouse.floatingparent;}

		try{clearTimeout(df.mouse.dblclicktmr)}catch(e){};
//		if (df.mouse.dragobj.id!=evtsrcx.id&&df.mouse.dblclickwaiting==true) {df.mouse.dblclickwaiting=false}
		if (df.mouse.dblclickwaiting==true){return}
		
		if (evtsrcx==null) {
			var thisfloat=false;
//			evtsrcx=evtsrc
		}else{
			var thisfloat=true
		}
		

		var evtsrctag = evtsrc.tagName;

		try{
			if ((
				(df.mouse.noinputs	&& (
				evtsrctag=="TEXTAREA"	||
				evtsrctag=="SELECT"	||
				evtsrctag=="INPUT"	||
				evtsrctag=="BUTTON"))	||
				evtsrctag=="BODY"	||
				evtsrctag=="HTML"	) 
				
			) {return true}
		}catch(e){}

		if (evtsrcx!=null&&thisfloat==true) {
			try{evt.preventDefault()}catch(e){}
			if (df.mouse.dragobj=="") {
				if (evtsrcx!=null) {
				try{
					df.mouse.dragobj= evtsrcx;
					var dragobjid = df.testid(df.mouse.dragobj)
					
					df.mouse.leftimportant	=	(df.getComputedStyle(df.mouse.dragobj,"left").indexOf("important")!=-1)
					df.mouse.topimportant	=	(df.getComputedStyle(df.mouse.dragobj,"top").indexOf("important")!=-1)

					df.mouse.mousey = (evt.clientY||evt.y);
					df.mouse.positionabs = (df.mouse.dragobj.style.position=="absolute")
					df.mouse.positionfix = (df.mouse.dragobj.style.position=="fixed")
					df.mouse.positionrel = (df.mouse.dragobj.style.position=="relative")
					df.mouse.startx		= parseInt(df.getleft(df.mouse.dragobj),10)||0;//.style.left
					df.mouse.starty		= parseInt(df.gettop(df.mouse.dragobj),10)||0;//.style.top
					df.mouse.handleoffx = 0
					df.mouse.handleoffy = 0

					df.mouse.dragoffx	= df.mouse.mousex + df.mouse.scrollleft() - df.mouse.startx;
					df.mouse.dragoffy	= df.mouse.mousey + df.mouse.scrolltop()  - df.mouse.starty// - (df.calcscrolltop(df.mouse.dragobj)||0)   );
					df.mouse.handleoffx	= df.mouse.dragoffx;
					df.mouse.handleoffy	= df.mouse.dragoffy;

					df.mouse.dragdistancex	= 0;
					df.mouse.dragdistancey	= 0;
					df.mouse.dragaccum	= 0;

				}catch(e){try{df.debug.debugwin({title:"dhtmlfx mouse",color:"red",message:e.description+"<br/>"})}catch(e){}}
				}
			}
		}
		try{if (df.heatmap.tmr!=0&&df.heatmap.tmr!=undefined) {df.heatmap.mousedown()}}catch(e){}

		if (df.mouse.dragobj=="") {return true}
		try{
			df.mouse.xdrag = df.json.deserialize(String(df.mouse.dragobj.getAttribute(df.mouse.custom)));
			if (df.mouse.xdrag==undefined) {df.mouse.dragobj="";return}
			
						
			if (df.mouse.xdrag.floating!=undefined&&df.mouse.xdrag.floating!="nomove") {

				df.blockiframes()

				df.mouse.collectfx(evtsrc)
				df.mouse.dragz = df.topzindex()+10 //+=5
				if (df.mouse.xdrag.tofront==true) {
				
					df.setcssText(df.mouse.dragobj,"z-index",df.mouse.dragz,(df.getComputedStyle(df.mouse.dragobj,"zIndex").indexOf("important")!=-1))
					//df.mouse.dragobj.style.zIndex = df.mouse.dragz;
				}
				//try{df.anim(df.mouse.dragobj,0,false,null)}catch(e){}
				if (df.fxarray[df.mouse.dragobj.id]!=undefined) {
					try{delete df.fxarray[df.mouse.dragobj.id].styles["top"]}catch(e){}
					try{delete df.fxarray[df.mouse.dragobj.id].styles["left"]}catch(e){}
				}
try{				
				if (df.mouse.dragobj.tagName.toLowerCase()=="tr"||df.mouse.dragobj.tagName.toLowerCase()=="td") {
					if (df.mouse.xdrag.iscopy!=true) {

						df.mouse.dragoffx	= df.getleft(df.mouse.dragobj)
						df.mouse.dragoffy	= df.gettop(df.mouse.dragobj)

						var thismousefx = df.mouse.dragobj.getAttribute(df.mouse.custom)
						var parenttable = df.mouse.dragobj
						do {
							parenttable = parenttable.parentNode
						} while (
							parenttable.tagName.toLowerCase()!="table"	&&
							parenttable.tagName.toLowerCase()!="html"
						)

						var tablediv = df.newdom({
							tagname		: "div",
							attributes	: {
								id	: "dhtmlfx_copytable",
								style	: "position:absolute;top:"+(df.gettop(df.mouse.dragobj))+"px;left:"+(df.getleft(df.mouse.dragobj))+"px;z-index:"+(df.mouse.dragz+=10)
							}
						})

						tablediv.innerHTML = ""
						var newcopy = df.copydom(parenttable,tablediv)
						newcopy.setAttribute(df.mouse.custom,"")
						var clearcustom=newcopy.getElementsByTagName("*")
						for (var i=0;i<clearcustom.length;i++) {
							clearcustom[i].setAttribute(df.mouse.custom,"")
						}
						tablediv.setAttribute(df.mouse.custom,thismousefx.replace(/\}$/,"")+",iscopy:true}")
						df.mouse.dragobj = tablediv
						df.mouse.xdrag = df.json.deserialize(thismousefx.replace(/\}$/,"")+",iscopy:true}")

						
//df.debug.debugwin({title:df.gettop(parenttable),message:df.getleft(parenttable)})

						df.mouse.xdrag.iscopy = true
						df.mouse.positionabs = true

//						if (df.mouse.xdrag.dragcopystart!=undefined) {
//							try{df.mouse.xdrag.dragcopystart(df.mouse.copysource,newcopy)}catch(e){}
//						}
						if ((df.mouse.xdrag.dragcopyx==true&&df.mouse.dragdistancex>df.mouse.xdrag.copytriggerx||0)||(df.mouse.xdrag.dragcopyy==true&&df.mouse.dragdistancey>df.mouse.xdrag.copytriggery||0)) {
							try{xdrag.dragcopystart(df.mouse.copysource,df.mouse.dragcopyobj)}catch(e){alert("mouse_move.copy error:"+e.description)}
						}		

//						df.mouse.dragcopyobj = df.mouse.dragobj
					}
				} else {
					if (
						df.mouse.dragobj.style.position!="absolute"&&
						df.mouse.dragobj.style.position!="relative"&&
						df.mouse.dragobj.style.position!="fixed"
					) {
						df.mouse.dragobj.style.position = "absolute"
						df.mouse.positionabs = true
					}
				}
}catch(e){df.debug.debugwin({title:"copytable",color:"red",message:e.description+"<br>"+e.message})}
			}

//			if (df.mouse.dragobj.getElementsByTagName("iframe").length>0) {
//				df.blockiframes()
//			}


//need to add inheritance to mousefx?
//			if (evtsrc.getAttribute(df.mouse.custom)!=null) {
//				childfx = df.json.deserialize(evtsrc.getAttribute(df.mouse.custom))
//				
//				for (var selector in childfx) {
//					df.mouse.xdrag[selector] = childfx[selector]
//				}
//			}

			//df.mouse.xdrag = df.json.simpleJSONtoOBJ(df.mouse.dragobj.getAttribute(df.mouse.custom));
			//df.debug.debugwin({title:df.mouse.dragobj.getAttribute(df.mouse.custom),message:df.json.serialize(df.mouse.xdrag)})

			if (df.mouse.xdrag.floating!=false&&df.mouse.xdrag.floating!=undefined&&df.connector!=undefined) {
				try{df.connector.mousedown(df.mouse.dragobj)}catch(e){}
			}
}catch(e){df.debug.debugwin({title:"mousedown",color:"red",message:e.description+"<br>"+e.message})}
		//}catch(e){df.mouse.xdrag=""}

		
		try{
			if (df.mouse.dblclickwaiting!=true) {
				if (df.mouse.dragobj!="") {df.mouse.floatingparent=df.mouse.dragobj}
				if (thisfloat==false) {
					df.mouse.floatingparent = evtsrcx
				}
				if (String(df.mouse.floatingparent.getAttribute(df.mouse.custom)).indexOf("mousedown")!=-1) {
					var evtsrcchilddrag = df.json.deserialize(df.mouse.floatingparent.getAttribute(df.mouse.custom))
					try{evtsrcchilddrag.mousedown(df.mouse.floatingparent,evtsrc,evt)}catch(e){}
				} else {
					if (df.mouse.xdrag.mousedown!=undefined) {
						df.mouse.xdrag.mousedown(evtsrcx,evtsrc,evt);
						try{evt.preventDefault()}catch(e){}
					}
				}
			}
		}catch(e){}

		if (df.touch) {df.killevent(evt)}

		if (thisfloat==false) {
			//try{df.mouse.dragobj.setAttribute(df.mouse.custom,df.json.serialize(df.mouse.xdrag))}catch(e){}
			df.mouse.dragobj=""
		}


		return false
	},

//----------------------------------------------------

	mouse_over		: function(evt) {
		var df = dhtmlfx
try{
		if (df.mouse.dragobj!="") {return}
		//try{var evtsrc=event.srcElement;evt=event}catch(e){try{var evtsrc=evt.target}catch(e){return}}
		var evtsrc = evt.srcdom

			df.mouse.dragoverobject = evtsrc;

			try{var thisdragchild = df.mouse.dragchild(evtsrc,'mousewheel');}catch(e){thisdragchild=null}

			if (thisdragchild!==df.mouse.lastmousewheel&&thisdragchild!=null) {
			try{
				try{thisdragchild.id = df.testid(thisdragchild)}catch(e){df.mouse.lastmousewheel="";}
				var thismousefx = df.json.deserialize(thisdragchild.getAttribute(df.mouse.custom))
				if (thismousefx.mousewheel!=undefined) {
					df.mouse.mousewheelover=df.json.deserialize(thisdragchild.getAttribute(df.mouse.custom))
					df.mouse.mousewheelover.evtsrc = thisdragchild
				} else {
					df.mouse.mousewheelover={};
				}
				df.mouse.lastmousewheel=thisdragchild;
			}catch(e){}
			}

//problem with mouseover here... maybe problem is with mousout?

			var overparent = df.mouse.dragparent(evtsrc);
			if (overparent.floating==undefined) {
				overparent = df.mouse.dragchild(evtsrc,'mouseover')
				if (overparent==null) {
					overparent = {}
				} else {
					var overmousefx = overparent.getAttribute("mousefx")
					if (overmousefx==null) {
						overparent = {}
					} else {
						overparent = df.json.deserialize(overmousefx)
					}
				}
			} else {
				evtsrc = overparent.dhtmlfx_domobj
			}

			if (overparent.floating!=undefined) {
				df.mouse.overparent = overparent //df.json.deserialize(overparent.getAttribute(df.mouse.custom))
				if (df.mouse.overparent.mouseover!=undefined) {
					try{
						df.mouse.overparent.mouseover(evtsrc,overparent,df.mouse.overparent,df.mouse.dragoverobject,(evt.srcElement||evt.target));
					}catch(e){}
				} else {
					df.mouse.floatingparent = df.mouse.floatparent(evtsrc);
					if (df.mouse.floatingparent.tagName!=undefined) {
						var floatparentx = df.json.deserialize(evtsrc.getAttribute(df.mouse.custom))
						if (floatparentx.mouseover!=undefined) {
							try{
								floatparentx.mouseover(evtsrc,floatparentx,floatparentx,df.mouse.dragoverobject,(evt.srcElement||evt.target));
							}catch(e){}
						}
					}
				}
			}
}catch(e){
	df.debug.debugwin({title:"df.mouse.mouse_over",color:"red",message:e.description+"<br>"+e.message})
}
	},

//----------------------------------------------------

	mouse_out: function(evt) {
		var df = dhtmlfx
		if (df.mouse.dragobj!="") {df.mouse.overparent = {};return}	
//		if (df.mouse.xdrag.iscopy==true) {df.mouse.overparent = {};return}
//		try{var evtsrc=event.srcElement;evt=event}catch(e){try{var evtsrc=evt.target}catch(e){return}}
		var evtsrc = evt.srcdom

		df.mouse.lastmousewheel={}
		df.mouse.mousewheelover={};

		df.mouse.overparent = {};
		var overparent = df.mouse.dragparent(evtsrc);
		try{
			if (overparent.mouseout!=undefined) {
				overparent.mouseout(evtsrc,overparent);
			}
		}catch(e){}
		df.mouse.floatingparent = ""
		
	},
	copyfrom	:"",
	
	mouse_move: function(evt) {
		var df = dhtmlfx
try{
		df.mouse.mousex = (evt.clientX||evt.x);
		df.mouse.mousey = (evt.clientY||evt.y);

		//if (df.heatmap!=undefined) {if (df.heatmap.tmr!=0&&df.heatmap.is_away==true) {clearTimeout(df.heatmap.tmr);df.heatmap.loop()}}
		if (df.mouse.dragobj=="") {return true}  //||df.mouse.dblclickwaiting==true
		if (df.touch) {df.killevent(evt)}
//		try{var evtsrc=event.srcElement;evt=event}catch(e){try{var evtsrc=evt.target}catch(e){return}}
		var evtsrc = evt.srcdom
		
//		if (df.mouse.dragobj.tagName=="INPUT"||df.mouse.dragobj.tagName=="TEXTAREA"||df.mouse.dragobj.tagName=="SELECT"||df.mouse.dragobj.tagName=="BODY"){return true}
		
		df.mouse.dragmoveobj = evtsrc;
		
		var xdrag = df.mouse.xdrag;

		if (xdrag==""||xdrag==undefined) {return}

		var scrolltop = df.mouse.scrolltop()
		var scrollleft= df.mouse.scrollleft()

var err=0
		if ((xdrag.iscopy!=true)&&(xdrag.dragcopyx==true||xdrag.dragcopyy==true)) {
err=1
			if (xdrag.copytriggerx!=undefined) {var xtest = (df.mouse.dragdistancex>xdrag.copytriggerx)}else {var xtest = true}
			if (xdrag.copytriggery!=undefined) {var ytest = (df.mouse.dragdistancey>xdrag.copytriggery)}else {var ytest = true}
err=2
			if (xtest||ytest) {
			//try{
				if (xdrag.copyunique!=true) {
					var newcopyholder = df.newdom({tagname:"div",attributes:{
						id:"dhtmlfx_copyholder",
						style:"position:absolute;top:0px;left:0px;z-index:"+Math.max(df.topzindex()+10,1000),
						unselectable:"on"
					}})
					newcopyholder.innerHTML=""
				} else {
					newcopyholder = document.body
				}
err=3
				df.mouse.copysource_original = df.mouse.dragobj;

				df.mouse.copysource = df.mouse.dragobj;

				if (df.mouse.copysource==undefined||df.mouse.copysource==null) {return}

				if (xdrag.dropable==true) {df.mouse.dropsource = df.mouse.copysource} else {df.mouse.dropsource = ""}

//				var copymousefx = df.mouse.copysource.getAttribute(df.mouse.custom)
//				try{var copymouseup = ",mouseup:"+copymousefx.match(/mouseup.*?[^\:a-zA-Z0-9\_\.]/)[0].split(":")[1].replace(/[^\:a-zA-Z0-9\_\.]/g,"")}catch(e){var copymouseup=""}
//				try{var copydropable = ",dropable:"+copymousefx.match(/dropable.*?[^\:a-zA-Z0-9\_\.]/)[0].split(":")[1].replace(/[^\:a-zA-Z0-9\_\.]/g,"")}catch(e){var copydropable=""}
//				var copymousefx = "{floating:true,iscopy:true"+copymouseup+copydropable+"}"
err=4
				var copymousefx = df.json.deserialize(df.mouse.copysource.getAttribute(df.mouse.custom))
				copymousefx.floating=true
				copymousefx.iscopy=true
				copymousefx = df.json.serialize(copymousefx,false)

				var classdom = df.classdoms(df.mouse.copysource,newcopyholder)||{}
				
				var overflowfix = classdom.inner
				if (overflowfix!=undefined) {
				while (overflowfix.id!="dhtmlfx_copyholder") {
					overflowfix.style.overflow = "visible"
					overflowfix = overflowfix.parentNode
				}
				}
err=5				
				try{
				if (df.mouse.copysource.tagName.toLowerCase()=="td"||df.mouse.copysource.tagName.toLowerCase()=="tr") {
					var newcopy = df.newdom({tagname:"div",target:classdom.inner})
					var thistable = df.parentelement(df.mouse.copysource,"table")	
					var tablecopy = df.copydom(thistable,newcopy,true,false);
					if (df.mouse.copysource.tagName.toLowerCase()=="tr") {
						df.copydom(df.mouse.copysource,tablecopy,xdrag.copyunique||false,true);					
					} else {
						df.copydom(df.mouse.copysource,tablecopy,xdrag.copyunique||false,true);
					}
				} else {
					var newcopy = df.copydom(df.mouse.copysource,(classdom.inner||newcopyholder),xdrag.copyunique||false);
					if (newcopy==undefined||newcopy==null) {return}
				}
				}catch(e){
					var newcopy = df.copydom(df.mouse.copysource,(classdom.inner||newcopyholder),xdrag.copyunique||false);
					if (newcopy==undefined||newcopy==null) {return}				
				}
err=6				
				
				df.mouse.dragobj = newcopy

//				var thisfixed = df.mouse.domfixed(df.mouse.copysource)
//df.debug.debugwin({title:"thisfixed",message:thisfixed,color:""})				

				newcopy.setAttribute("unselectable","on");
				newcopy.style.position="absolute";
				

//				if (!df.mouse.positionabs) {
//					df.mouse.dragoffy=(df.mouse.starty+df.mouse.handleoffy) - (df.calcscrolltop(df.mouse.copysource)||0)
//					df.mouse.dragoffx=(df.mouse.startx+df.mouse.handleoffx) - (df.calcscrollleft(df.mouse.copysource)||0)
//				}
err=7
				newcopy.style.zIndex = df.topzindex()+10 //(parseInt(df.mouse.copysource.style.zIndex,10)+100)||1000				
				//newcopyholder.appendChild(newcopy);

				var clearmousefx = newcopy.getElementsByTagName("*")
				for (var i=0;i<clearmousefx.length;i++) {clearmousefx[i].setAttribute(df.mouse.custom,"")}
err=8
				//if (copymousefx=="") {
				//
				//} else {
				//	copymousefx = copymousefx.substr(0,copymousefx.length-1)+",mouseup:dhtmlfx.mouse.canceldragcopy }"
				//}
				//console.log(copymousefx)
err=9
				newcopy.setAttribute(df.mouse.custom,copymousefx)

				try{xdrag.dragcopystart(df.mouse.copysource,newcopy)}catch(e){}
err=10				
				if (xdrag.copyopacity!=undefined) {df.setopacity(newcopy,xdrag.copyopacity)}
				if (xdrag.copyslideback!=undefined) {
					df.anim({
						target	: df.mouse.copysource,
						steps	: 10,
						styles	: {
							left	:{
								min	: df.current,
								max	: 0,//df.mouse.startx
								slope	: df.smoothmove
							},
							top	:{
								min	: df.current,
								max	: 0,//df.mouse.starty
								slope	: df.smoothmove
							}
						}
					})
				} else {
					df.setcssText(df.mouse.copysource,"top","0px",df.mouse.topimportant)
					df.setcssText(df.mouse.copysource,"top","0px",df.mouse.leftimportant)
//					df.mouse.copysource.style.top ="0px"//df.mouse.starty+"px"
//					df.mouse.copysource.style.left="0px"//df.mouse.startx+"px"
				}
err=11
				df.mouse.dragcopyobj = newcopy
				df.mouse.dragobj = newcopy

				df.mouse.positionabs = true

				xdrag = df.mouse.xdrag = df.json.deserialize(newcopy.getAttribute(df.mouse.custom));
err=12
			//}catch(e){alert("drag copy:"+e.message)}
			}
		}
		// else {
		//	if ((xdrag.dragcopyx==true&&df.mouse.dragdistancex>xdrag.copytriggerx)||(xdrag.dragcopyy==true&&df.mouse.dragdistancey>xdrag.copytriggery)&&xdrag.iscopy!=true) {
			//	try{xdrag.dragcopystart(df.mouse.copysource,df.mouse.dragcopyobj)}catch(e){alert("mouse_move.copy error:"+e.description)}
		//	}		
		//}

	//	if (xdrag.mousemove!=undefined) {
	//		try{evt.preventDefault()}catch(e){}
	//		try{xdrag.mousemove(evt)}catch(e){}
	//	}

		try{evt.preventDefault()}catch(e){}

		//-----------------
		//drag-over and drag-out
		var newdrop = "";
		var dropoveridx=-1;

		if (df.mouse.xdrag.dropable==true) {

			var drop_mouse_x = df.mouse.mousex//+scrollleft;
			var drop_mouse_y = df.mouse.mousey//+scrolltop;

			if (df.mouse.lasttargetobj!="") {
				var dropfixed = parseInt(df.mouse.lasttargetobj.getAttribute("domfixed"),10)
				var drop_top  = df.gettop(df.mouse.lasttargetobj)//-((dropfixed==true)?df.scrolltop():0);
				var drop_left = df.getleft(df.mouse.lasttargetobj)//-((dropfixed==true)?df.scrollleft():0);
				var drop_right= drop_left+df.mouse.lasttargetobj.offsetWidth;
				var drop_bot  = drop_top+df.mouse.lasttargetobj.offsetHeight;

//df.debug.debugwin({title:"Test",message:drop_mouse_x+"<br/>"+drop_mouse_y+"<br/>"+drop_top+"<br/>"+drop_left+"<br/>"+drop_right+"<br/>"+drop_bot})

				if (
					(drop_mouse_x)<drop_left	||
					(drop_mouse_x)>drop_right	||
					(drop_mouse_y+((dropfixed==0)?scrolltop:0))<drop_top		||
					(drop_mouse_y+((dropfixed==0)?scrolltop:0))>drop_bot
				) {
					try{df.mouse.lasttarget.dropout(df.mouse.lasttargetobj,df.mouse.lasttarget,evtsrc)}catch(e){}
//					try{clearTimeout(df.mouse.dropovertmr)}catch(e){}
					df.mouse.lasttargetobj = "";
					df.mouse.lasttarget="";
					df.mouse.lasttargetfixed = false
					df.mouse.droptarget = "";
				}				
			}

//add a sorting routine to the mouse-down code to sort 
//elements to the top of the drag array when they are clicked upon (to bring to front)
//this would allow correct drag-and-drop with overlapping windows.
//-or-
//do the sort right here -- too much CPU?


				var dropzonelen = df.mouse.dropzones.length
				for (var i=0;i<dropzonelen;i++) {

					var thisdropzone = df.mouse.dropzones[i];

					var dropfixed = parseInt(thisdropzone.getAttribute("domfixed"),10)
					var drop_top  = df.gettop(thisdropzone)//-((dropfixed==true)?df.scrolltop():0);
					var drop_left = df.getleft(thisdropzone)//-((dropfixed==true)?df.scrollleft():0);
					var drop_right= drop_left+thisdropzone.offsetWidth;
					var drop_bot  = drop_top+thisdropzone.offsetHeight;

					if (
						(drop_mouse_x)>drop_left	&&
						(drop_mouse_x)<drop_right	&&
						(drop_mouse_y+((dropfixed==0)?scrolltop:0))>drop_top		&&
						(drop_mouse_y+((dropfixed==0)?scrolltop:0))<drop_bot
					) {

						//if (df.mouse.dropzones[i]!=df.mouse.lasttargetobj) {

					if (df.mouse.lasttargetobj=="") {

						try{df.mouse.lasttarget.dropout(df.mouse.lasttargetobj,df.mouse.lasttarget,evtsrc)}catch(e){}	
						df.mouse.lasttargetobj=""
					}

					newdrop = thisdropzone;
					df.mouse.lasttargetfixed = dropfixed
					df.mouse.droptarget = newdrop;


					dropoveridx = i;

	//df.debug.debugwin({title:"test",message:"x="+drop_mouse_x+" , y="+drop_mouse_y+"<br/>"+df.mouse.lasttargetobj.id+"<br/>"+df.mouse.lasttarget})							
	//							try{
	//								df.mouse.lasttarget.dropover(newdrop,df.mouse.lasttarget,evtsrc,i);
	//							}catch(e){alert('dhtmlfx_mouse newdrop error')}
					if (df.mouse.droptarget!=df.mouse.lasttargetobj) {
						df.mouse.lasttarget = df.json.deserialize(df.mouse.droptarget.getAttribute(df.mouse.custom));
						try{df.mouse.lasttarget.dropover(newdrop,df.mouse.lasttarget,evtsrc,df.mouse.dropzones[dropoveridx])}catch(e){}
					}
					df.mouse.lasttargetobj = df.mouse.droptarget;
/*
				try{clearTimeout(df.mouse.dropovertmr)}catch(e){}
				try{
					df.mouse.dropovertmr = setTimeout(function(){
						try{
							eval(String(window.dhtmlfx.mouse.lasttargetobj.getAttribute(window.dhtmlfx.mouse.custom)).match(/dropover.*?[^\:a-zA-Z0-9\_\.]/)[0].split(":")[1].replace(/[^\:a-zA-Z0-9\_\.]/g,"") )(window.dhtmlfx.mouse.lasttargetobj,window.dhtmlfx.mouse.lasttarget,null,window.dhtmlfx.mouse.dropzones[dropoveridx])
						
						}catch(e){}
					},110)
				}catch(e){}
*/

					break
						//}
//df.debug.debugwin({title:"test",message:newdrop.id})

					}
				}
		}

		var newdragx = (df.mouse.mousex)-df.mouse.dragoffx+scrollleft;
		var newdragy = (df.mouse.mousey)-df.mouse.dragoffy+scrolltop;
		df.mouse.dragdistancex = Math.abs((df.mouse.startx)-newdragx);
		df.mouse.dragdistancey = Math.abs((df.mouse.starty)-newdragy);
		newdragx = newdragx - df.getleft(df.mouse.dragobj.parentNode);
		newdragy = newdragy - df.gettop(df.mouse.dragobj.parentNode);
		
		df.mouse.dragaccum++;

		try{
		
			var gridx = xdrag.gridx||1
			var gridy = xdrag.gridy||1

			if (xdrag.floating==true) {
				if (xdrag.xmin!=undefined) {if (newdragx<xdrag.xmin) {newdragx=xdrag.xmin}}
				if (xdrag.xmax!=undefined) {if (newdragx>xdrag.xmax) {newdragx=xdrag.xmax}}
				if (xdrag.ymin!=undefined) {if (newdragy<xdrag.ymin) {newdragy=xdrag.ymin}}
				if (xdrag.ymax!=undefined) {if (newdragy>xdrag.ymax) {newdragy=xdrag.ymax}}
			}
			var yminadj=0
			if (xdrag.ymin!=undefined) {
				yminadj = xdrag.ymin
			}
			var xminadj=0
			if (xdrag.xmin!=undefined) {
				xminadj = xdrag.xmin
			}
			df.mouse.newx = Math.round(parseInt(((newdragx-xminadj)/gridx),10)*gridx)+xminadj;
			df.mouse.newy = Math.round(parseInt(((newdragy-yminadj)/gridy),10)*gridy)+yminadj;
			

			if (xdrag.floating=='nomove') {
				if (xdrag.mousemove!=undefined) {
					try{evt.preventDefault()}catch(e){}
					try{xdrag.mousemove(df.mouse.newx,df.mouse.newy,evt,evtsrc)}catch(e){}
				}
				return
			} else {
				try{
					if (df.mouse.leftimportant) {
						df.setcssText(df.mouse.dragobj,"left",df.mouse.newx+"px",true)
					} else {
						df.mouse.dragobj.style.left = df.mouse.newx+"px"// Math.round(parseInt((newdragx/gridx),10)*gridx)+"px";
					}
				}catch(e){}
				
				try{
					if (df.mouse.topimportant) {
						df.setcssText(df.mouse.dragobj,"top",df.mouse.newy+"px",true)
					} else {
						df.mouse.dragobj.style.top = df.mouse.newy+"px" //Math.round(parseInt((newdragy/gridy),10)*gridy)+"px";					
					}
				}catch(e){}
			}

		}catch(e){df.debug.debugwin({title:"mouse moving",color:"red",message:e.description+"<br>"+e.message})}
				
		//connector mousemove
		if (xdrag.floating!=false&&xdrag.floating!=undefined&&df.connector!=undefined) {
			df.connector.mousemove(df.mouse.newx,df.mouse.newy,df.mouse.dragobj)
		}
/*
		if (dropoveridx!=-1) {
			try{
				df.mouse.lasttarget.dropover(newdrop,df.mouse.lasttarget,evtsrc,df.mouse.dropzones[dropoveridx]);
				try{clearTimeout(df.mouse.dropovertmr)}catch(e){}				
				try{df.mouse.dropovertmr = setTimeout('try{'+String(df.mouse.lasttargetobj.getAttribute(df.mouse.custom)).match(/dropover.*?[^\:a-zA-Z0-9\_\.]/)[0].split(":")[1].replace(/[^\:a-zA-Z0-9\_\.]/g,"")+'(df.mouse.lasttargetobj,df.mouse.lasttarget,null,df.mouse.dropzones['+dropoveridx+'])}catch(e){}',110);}catch(e){}
			}catch(e){}
		}
*/
		//-----------------

		try{thisevt=event.srcElement}catch(e){thisevt=evt.target}
		
		if (xdrag.report!=undefined) {
			try{xdrag.report(df.mouse.newx,df.mouse.newy,xdrag.returnval)}catch(e){}
		}

		return false
}catch(e){df.debug.debugwin({title:"df.mouse.mouse_move",color:"red",message:e.description+"<br>"+e.message+"<br>err="+err})}		
	},
	
	canceldragcopy:function() {
		window.dhtmlfx.removedom(window.dhtmlfx.dg("dhtmlfx_copyholder"))
	},

	dblclickobjid:"",
	dblclickxdrag:"",
	mouse_up: function(evt) {
		var df = dhtmlfx
try{	
		
		df.mouse.mouseisdown = false

//		try{var evtsrc=event.srcElement;evt=event}catch(e){var evtsrc=evt.target}
		var evtsrc = evt.srcdom
		try{evt.preventDefault()}catch(e){}

		var evtsrcx = df.mouse.floatchild(evtsrc,evt);
		var evtsrcchild=evtsrcx;
		df.mouse.primaryobject=evtsrcx;

		df.mouse.dragoverobject = evtsrc;

//		evtsrcx = df.mouse.floatchild(evtsrc,evt);

		if (df.mouse.dragobj!=""){
			if (df.touch) {df.killevent(evt)}

			if (df.mouse.droptarget!="") {
				//execute dodrop code
				try{df.json.get_json_prop(df.mouse.droptarget,df.mouse.custom ,"dodrop" )(df.mouse.dropsource,df.mouse.droptarget,evtsrc,df.mouse.copysource_original)}catch(e){}
			} else {
//depreciated?
//				if (df.mouse.xdrag.floating==true) {
//					try{
//						if (df.mouse.dragaccum>=2) {
//							eval("try{"+df.mouse.xdrag.floatreturn+"('"+df.mouse.dragobj.id+"','"+df.mouse.xdrag.returnval+"')}catch(e){}");
//						}
//					}catch(e){}
//				}
			}
			
			if (df.mouse.xdrag.mouseup!=undefined) {
				df.mouse.xdrag.mouseup(evtsrcx,evtsrc,df.mouse.xdrag)
			}

			if (df.mouse.xdrag.floating!=false&&df.mouse.xdrag.floating!=undefined&&df.connector!=undefined) {
				try{df.connector.mouseup(df.mouse.dragobj)}catch(e){}
			}



		} else {
			//handle mouseup on elements not being dragged (rare case?)
			try{
				var mousefx = String(evtsrcx.getAttribute(df.mouse.custom))
				if (mousefx.indexOf("mouseup")!=-1) {
					var xdrag = df.json.deserialize(mousefx);
					if (xdrag!=undefined && xdrag.mouseup!=undefined) {xdrag.mouseup(evtsrcx,evtsrc,xdrag)}
				}
			}catch(e){}
		}

		try{if (df.heatmap.tmr!=0&&df.heatmap.tmr!=undefined) {df.heatmap.mouseup()}}catch(e){}

		df.unblockiframes()
		
//df.debug.debugwin({title:"dblclick",message:df.mouse.xdrag.dblclick+"<br>dblclickwaiting="+df.mouse.dblclickwaiting})					

		if (df.mouse.xdrag!=undefined && df.mouse.dragaccum<2) {
			try{
				//if (String(df.mouse.primaryobject.getAttribute(df.mouse.custom)).indexOf("dblclick")!=-1) {

//df.debug.debugwin({title:"test",message:"df.mouse.dblclickwaiting="+df.mouse.dblclickwaiting+"<br>df.mouse.xdrag.dblclick="+df.mouse.xdrag.dblclick+"<br>df.mouse.dblclickxdrag.dblclick="+df.mouse.dblclickxdrag.dblclick+"<br>df.mouse.dblclickobjid="+df.mouse.dblclickobjid+"<br>df.mouse.xdrag="+df.json.serialize(df.mouse.xdrag)+"<br>df.mouse.dblclickxdrag="+df.json.serialize(df.mouse.dblclickxdrag)+"<br>df.mouse.primaryobject.id="+df.mouse.primaryobject.id+"<br>df.mouse.dragobj.id="+df.mouse.dragobj.id})

				if (df.mouse.xdrag.dblclick!=undefined||df.mouse.dblclickxdrag.dblclick!=undefined) {
					if (df.mouse.dblclickwaiting==false) {
						df.mouse.dblclickwaiting=true;
						df.mouse.dblclickobjid = String(df.mouse.primaryobject.id)
						df.mouse.dblclickxdrag = df.json.copyobject(df.mouse.xdrag)
//						df.mouse.dragobj = df.mouse.primaryobject;
						df.mouse.dblclicktmr=setTimeout(window.dhtmlfx.mouse.singleclick,window.dhtmlfx.mouse.dblclickwait)
						
					} else {
						try{clearTimeout(df.mouse.dblclicktmr)}catch(e){};
						df.mouse.dblclickwaiting=false;
						if (String(df.mouse.dblclickobjid)==String(df.mouse.primaryobject.id)) {
						
							try{df.mouse.dblclickxdrag.dblclick(df.dg(df.mouse.dblclickobjid),df.mouse.dblclickxdrag)}catch(e){alert('dblclick error')}
						}
						df.mouse.dblclickxdrag = ""
						df.mouse.dblclickobjid = ""
					}
				} else {
					try{clearTimeout(df.mouse.dblclicktmr)}catch(e){};
					df.mouse.dblclickwaiting=false;
				}
				
				if (df.mouse.xdrag.click!=undefined&&df.mouse.dblclickwaiting!=true) {
					try{clearTimeout(df.mouse.dblclicktmr)}catch(e){};
					try{df.mouse.xdrag.click(df.mouse.primaryobject,df.mouse.xdrag,evtsrc)}catch(e){}
				}
				
			}catch(e){}
		} else {
			df.mouse.dblclickwaiting=false;
		}
		
		//try{df.mouse.dragobj.setAttribute(df.mouse.custom,df.json.serialize(df.mouse.xdrag))}catch(e){}

		df.mouse.droptarget	= "";
		df.mouse.dropsource	= "";
		df.mouse.dragz		+=1;
		df.mouse.dragaccum	= 0;
		df.mouse.dragdistancex	= 0;
		df.mouse.dragdistancey	= 0;
		df.mouse.dragobj	= "";
		df.mouse.xdrag		= "";
		df.mouse.positionabs	= false;
		df.mouse.positionrel	= false;
		df.mouse.positionfix	= false;
		df.mouse.floatingparent = "";
		df.mouse.context	= false

		return false
}catch(e){df.debug.debugwin({title:"",color:"red",message:e.description+"<br>"+e.message})}		
	},
	
	singleclick:	function() {
		var df = dhtmlfx
		if (df.mouse.dblclickxdrag.click!=undefined) {
			try{df.mouse.dblclickxdrag.click(df.dg(df.mouse.dblclickobjid),df.mouse.dblclickxdrag);}catch(e){}
		}

		//try{df.dg(df.mouse.dblclickobjid).setAttribute(df.mouse.custom,df.json.serialize(df.mouse.dblclickxdrag))}catch(e){}

		df.mouse.dblclickwaiting=false;
		df.mouse.droptarget	= "";
		df.mouse.dropsource	= "";
		df.mouse.dragz		+=1;
		df.mouse.dragaccum	= 0;
		df.mouse.dragdistancex	= 0;
		df.mouse.dragdistancey	= 0;
		df.mouse.dragobj	= "";
		df.mouse.xdrag		= "";
		df.mouse.dblclickxdrag = ""
		df.mouse.dblclickobjid = ""
		df.mouse.positionabs	= false;
		df.mouse.positionrel	= false;
		df.mouse.positionfix	= false;
	},
	
	mouse_context:	function(evt) {
		var df = dhtmlfx

		var evtsrc = evt.srcdom
		
//	try{
/*
		try{var evtsrc=event.srcElement;evt=event}catch(e){var evtsrc=evt.target}

		var evtsrcx = df.mouse.floatchild(evtsrc,evt);

		var contextfx = df.json.deserialize(evtsrcx.getAttribute(df.mouse.custom))

		df.mouse.context = true
		
		if (contextfx.contextmenu!=undefined) {
			contextfx.contextmenu(evtsrcx)
		//	return false
		}
*/
//		return true
//	}catch(e){fx.debug.debugwin({title:"df.mouse.mouse_context",color:"red",message:e.description+"<br>"+e.message})}
	},

	floatchild: function (thisobj,evt) {
		var df = dhtmlfx

		try{
			if ((
				(df.mouse.noinputs		&& (
				thisobj.tagName=="TEXTAREA"	||
				thisobj.tagName=="SELECT"	||
				thisobj.tagName=="INPUT"	||
				thisobj.tagName=="BUTTON"))	||
				thisobj.tagName=="BODY"		||
				thisobj.tagName=="HTML"	) 
				
			) {return null}
		}catch(e){}


		try{
			var thismousefx = String(thisobj.getAttribute(df.mouse.custom))
		}catch(e){
			return null
		}

		try{
			do{
				var thismousefx = String(thisobj.getAttribute(df.mouse.custom));
				try{
					if(thismousefx!="null"){
						var testjson = df.json.deserialize(thismousefx);
						if (testjson.nofloat===true) {
							thisobj.ondrag=function (){return false};
							try{evt.preventDefault()}catch(e){}
							return null
						}
						
						if (testjson.floating===true||testjson.floating==="nomove") {
							thisobj.ondrag=function (){return false};
							try{evt.preventDefault()}catch(e){}
							break
						}
					};
					thisobj=thisobj.parentNode;
				}catch(e){break}
			}while(thisobj.tagName!="BODY");
									
			if (thisobj.tagName=="BODY"){
				return null
			};

			return thisobj
		}catch(e){
			return null
		}
	},

	dragchild: function (thisobj,dragselector) {
		var df = dhtmlfx

		try{
			if ((
				(thisobj.tagName=="TEXTAREA")	||
				(thisobj.tagName=="SELECT")	||
				(thisobj.tagName=="INPUT")	||
				(thisobj.tagName=="BUTTON")	||
				(thisobj.tagName=="BODY")	||
				(thisobj.tagName=="HTML")	) && df.mouse.noinputs==true
			){return null};

			do{
				var thismousefx = String(thisobj.getAttribute(df.mouse.custom));
				if(thismousefx!="null"){
					if (thismousefx.indexOf(dragselector+":")!=-1) {
						break
					}
				};
				thisobj=thisobj.parentNode;
			}while(thisobj.tagName!="BODY");
			if (thisobj.tagName=="BODY"){return null};
			return thisobj
		}catch(e){return null}
	},

	dragparent: function (thisobj) {
		var df = dhtmlfx
		try{
			if ((
				(thisobj.tagName=="TEXTAREA")	||
				(thisobj.tagName=="SELECT")	||
				(thisobj.tagName=="INPUT")	||
				(thisobj.tagName=="BUTTON")	||
				(thisobj.tagName=="BODY")	||
				(thisobj.tagName=="HTML")	) && df.mouse.noinputs==true
			){return {}};

			do{
				var thismousefx = String(thisobj.getAttribute(df.mouse.custom));
				if(thismousefx!="null"){
					break
				};
				thisobj=thisobj.parentNode;
			}while(thisobj.tagName!="HTML");
			if (thisobj.tagName=="HTML"){return {}};
			var thisreturn = df.json.deserialize(thismousefx)
			thisreturn.dhtmlfx_domobj = thisobj
			return thisreturn
		}catch(e){return {}}
	},

//-----------------------------------------------------------------------------

	wheeldeltamaxtmr:0,
	wheeldeltamax:0,
	move_mousewheel: function (evt) {
	var df = dhtmlfx

	try{
//		try{var evtsrc=event.srcElement;evt=event}catch(e){var evtsrc=evt.target}
		var evtsrc = evt.srcdom

		var evtsrctag = evtsrc.tagName;
		if ((
			evtsrctag=="SELECT"		||
			evtsrctag=="TEXTAREA"		||
			evtsrctag=="BUTTON"		||
			evtsrctag=="INPUT"		) && df.mouse.noinputs==true

		) {return false}


		if (df.connector!=undefined) {
			try{df.connector.mousewheel(evtsrc)}catch(e){}
		}

		if (df.mouse.mousewheelover.mousewheel==undefined) {
			try{if (df.heatmap.tmr!=0&&df.heatmap.tmr!=undefined) {df.heatmap.mousewheel()}}catch(e){}
			return true
		}

		var mousefx = df.mouse.mousewheelover
//df.debug.debugwin({title:"",message:df.json.serialize(mousefx)})		
//this probably isnt needed
//		try{var xdrag = eval("("+evtsrc.getAttribute(df.mouse.custom)+")")}catch(e){xdrag={}}

		if (df.browser=="ie"||df.browser=="safari"||df.browser=="chrome") {
			if (df.browser=="chrome") {
				var wheeldelta = -parseFloat(evt.wheelDelta)/1.2;
			} else {
				var wheeldelta = -parseFloat(evt.wheelDelta)/12;			
			}
		} else {
			if (df.browser=="ff"||df.browser=="opera") {
				var wheeldelta = parseFloat(evt.detail)*12;
			} else {
				var wheeldelta = parseFloat(evt.detail)*120;
			}
		}

		if (Math.abs(wheeldelta)>Math.abs(df.mouse.wheeldeltamax)) {
			df.mouse.wheeldeltamax = wheeldelta
		}

		mousefx.wheeldelta = parseInt(wheeldelta,10)
		mousefx.wheeldeltamax = parseInt(df.mouse.wheeldeltamax,10)

		if (mousefx.mousewheelglide!=undefined) {
			if (Math.abs(wheeldelta)>=12) {
				var thismouseglide = df.mouse.mouseglides[mousefx.evtsrc.id]
				if (thismouseglide==undefined) {				
					var thismouseglide = df.mouse.mouseglides[mousefx.evtsrc.id] = mousefx
				}
				thismouseglide.wheeldelta = parseInt(wheeldelta,10)
				thismouseglide.wheeldeltamax = parseInt(df.mouse.wheeldeltamax,10)
				thismouseglide.evtsrcid = df.testid(evtsrc)
				try{clearTimeout(df.mouse.mousewheelglidetmr)}catch(e){}
				df.mouse.mousewheelglidetmr = setTimeout(function(){window.dhtmlfx.mouse.wheelglide(mousefx.evtsrc.id)},10)
			} else {
			}
		}

		try{clearTimeout(df.mouse.wheeldeltamaxtmr)}catch(e){}
		df.mouse.wheeldeltamaxtmr = setTimeout(function(){window.dhtmlfx.mouse.clearmousedeltamax()},10)

		try{
			var wheelstatus = mousefx.mousewheel(wheeldelta,mousefx.evtsrc,mousefx);
		}catch(e){df.debug.debugwin({title:"",color:"red",message:e.description+"<br>"+e.message})}

		if (wheelstatus==false) {
			df.mouse.killevent(evt);
			setTimeout(function(){df.mouse.killevent(window.event)},20)
			evt = null
			return false
		}

	}catch(e){df.debug.debugwin({title:"df.mouse.move_mousewheel",color:"red",message:e.description+"<br>"+e.message})}
	},
	clearmousedeltamax:	function(){window.dhtmlfx.mouse.wheeldeltamax=0},
	
	mousewheelglidetmr:0,
	mouseglides:{},
	wheelglide:	function(mousewheelid) {
		var df = dhtmlfx
	try{
		try{clearTimeout(df.mouse.wheeldeltamaxtmr)}catch(e){}
		var thismouseglide = df.mouse.mouseglides[mousewheelid]
		if (thismouseglide.glideid==undefined) {thismouseglide.glideid = df.unique()}
		var steps = Math.round(Math.abs(thismouseglide.wheeldeltamax*.707))
		df.animx({target:parseInt(thismouseglide.glideid,10),mouseglide:thismouseglide,"steps":steps,"styles":{"top":{"slope":df.slideout,"min":Math.round(thismouseglide.wheeldeltamax),"max":0,"report":df.mouse.glideupdate}}})
		df.mouse.wheeldeltamax=0
	}catch(e){df.debug.debugwin({title:"wheelglide",color:"red",message:e.description+"<br>"+e.message})}
	},

	glideupdate:	function(animobj) {
		var df = dhtmlfx
	try{
		animobj.mouseglide.mousewheel(animobj.styles.top.currentval,df.dg(animobj.mouseglide.evtsrcid),animobj.mouseglide)
	}catch(e){df.debug.debugwin({title:"df.mouse.glideupdate",color:"red",message:e.description+"<br>"+e.message})}
	},

//-----------------------------------------------------------------------------

	floatparent: function (thisobj) {
		var df = dhtmlfx
		try{
			do {
				if (String(thisobj.getAttribute(df.mouse.custom)).replace(/[^a-z\:]/g,"").indexOf("floating:true")==-1) {  //||thisobj.getAttribute(df.mouse.custom).indexOf("floating:'nomove'")==-1
					thisobj=thisobj.offsetParent;
				} else {break}
			} while (thisobj.tagName!="BODY"&&thisobj.tagName!="HTML");
			if (thisobj.tagName=="BODY"||thisobj.tagName=="HTML") {return ""}
		}catch(e){return ""}
		return thisobj
	},

	domfixed: function (thisobj) {
			do {
				try{
					if (thisobj.style.position=="fixed"){
						return true
					}
				}catch(e){}
				thisobj=thisobj.parentNode
			} while (thisobj.tagName!="BODY"&&thisobj.tagName!="HTML");
			if (thisobj.tagName=="BODY"||thisobj.tagName=="HTML") {
				return false
			}
		return false
	},

	collectfx:	function(thisobj) {
		var df = dhtmlfx
		try{
			do {
				if (thisobj.getAttribute(df.mouse.custom)!=null) {
					var addfx = df.json.deserialize(thisobj.getAttribute(df.mouse.custom))
					if (adddf.floating!=undefined) {break}
					for (var selector in addfx) {
						df.mouse.xdrag[selector] = addfx[selector]
					}
				}
				thisobj = thisobj.offsetParent
			} while (thisobj.tagName!="BODY"&&thisobj.tagName!="HTML");
		}catch(e){}	
	},


	detachEvents: function (thisobject,thisevent) {
	
	
	},

	removeDOM	: function (thisnode) {
		var df = dhtmlfx
	//this is depreciated
		df.removedom(thisnode)
	},

	scrolltop	: function() {
		return (document.body.scrollTop||document.documentElement.scrollTop)
	},
	
	scrollleft	: function() {
		return (document.body.scrollLeft||document.documentElement.scrollLeft)
	},
	
	
	parentscrollTop:	function(domobj) {
		try{
			do {
				if (domobj.scrollTop==0) {
					domobj=domobj.parentNode;
				} else {
					return domobj.scrollTop
					break
				}
			} while (domobj.tagName.toLowerCase()!="body");
			if (domobj.tagName.toLowerCase()=="body") {return 0}
		}catch(e){return 0}
		return 0
	},
	
	parentscrollLeft:	function(domobj) {
		try{
			do {
				if (domobj.scrollLeft==0) {
					domobj=domobj.parentNode;
				} else {
					return domobj.scrollLeft
				}
			} while (thisobj.tagName.toLowerCase()!="body");
			if (thisobj.tagName.toLowerCase()=="body") {return 0}
		}catch(e){return 0}
		return 0
	},
	
	killevent: function(evt) {
	try{
		if (window.event) {window.event.returnValue = false}
		if (window.event&&window.event.cancelBubble) {window.event.cancelBubble=true}
		if (evt.preventDefault) {evt.preventDefault()}
		if (evt.stopPropagation) {
			evt.stopPropagation()
		} else {
			if (evt.preventBubble) {evt.preventBubble()}
			if (evt.preventCapture) {evt.preventCapture()}		
		}
	}catch(e){}
	},
	
	removelistener:	function(domobj,eventname,callback) {
		var df = dhtmlfx
	try{
		var thisdom = df.dg(df.testid(domobj))
		if (df.browser=="ie") {
		} else {
			thisdom.removeEventListener(eventname,callback,false)
		
		}
		
	
	}catch(e){df.debug.debugwin({title:"removelistener",color:"red",message:e.description+"<br>"+e.message})}
	},
	
	resizearray:	[],
	resizetmr:	0,
	/*
		{
			callback: whatever_function_name,
			data	: {
				//developer data can go here
			}
		}
	
	*/
	
	resize:		function() {
		var df = dhtmlfx
		for (var i=0;i<df.mouse.resizearray.length;i++) {
			if (df.mouse.resizearray[i].data!=undefined) {
				try{
					df.mouse.resizearray[i].callback(df.mouse.resizearray[i].data)
				}catch(e){
					//df.debug.debugwin({title:"df.mouse.resize="+i,message:e.description+"<br>"+e.message,color:"red"})
				}
			} else {
				try{
					df.mouse.resizearray[i].callback()
				}catch(e){
					//df.debug.debugwin({title:"df.mouse.resize="+i,message:e.description+"<br>"+e.message,color:"red"})
				}
			}
		}
		
	},
	
	addresize:	function(eventobj) {
		var df = dhtmlfx
		for (var i=0;i<df.mouse.resizearray.length;i++) {
			if (df.json.serialize(df.mouse.resizearray[i],false)==df.json.serialize(eventobj,false)) {return}
		}
		df.mouse.resizearray.push(eventobj);
	},
	
	killresize:	function(eventobj) {
		var df = dhtmlfx
		for (var i=0;i<df.mouse.resizearray.length;i++) {
			if (df.json.serialize(df.mouse.resizearray[i],false)==df.json.serialize(eventobj,false)) {
				df.mouse.resizearray.splice(i,1)
				break
			}
		}		
	},
	
	change:	function(evt) {
//		try{var evtsrc=event.srcElement;evt=event}catch(e){var evtsrc=evt.target}
		var evtsrc = evt.srcdom
//heatmap
	},
	
	focus:	function(evt) {
//		try{var evtsrc=event.srcElement;evt=event}catch(e){var evtsrc=evt.target}
		var evtsrc = evt.srcdom
//heatmap
	},
	
	blur:	function(evt) {
//		try{var evtsrc=event.srcElement;evt=event}catch(e){var evtsrc=evt.target}
		var evtsrc = evt.srcdom
//heatmap
	},
	
	attachEvents: function () {
		var df = dhtmlfx
		df.attachevent({domobj:document,name:"contextmenu",	handler:dhtmlfx.mouse.mouse_context})
		df.attachevent({domobj:document,name:"mousewheel",	handler:dhtmlfx.mouse.move_mousewheel})
		df.attachevent({domobj:window,name:"resize",		handler:dhtmlfx.mouse.resize})

		if (df.touch) {
			df.attachevent({domobj:document,name:"touchstart",	handler:dhtmlfx.mouse.mouse_down})
			df.attachevent({domobj:document,name:"touchmove",	handler:dhtmlfx.mouse.mouse_move})
			df.attachevent({domobj:document,name:"touchend",	handler:dhtmlfx.mouse.mouse_up})
		} else {
			df.attachevent({domobj:document,name:"mouseover",	handler:dhtmlfx.mouse.mouse_over})
			df.attachevent({domobj:document,name:"mouseout",	handler:dhtmlfx.mouse.mouse_out})
			df.attachevent({domobj:document,name:"mousedown",	handler:dhtmlfx.mouse.mouse_down})
			df.attachevent({domobj:document,name:"mousemove",	handler:dhtmlfx.mouse.mouse_move})
			df.attachevent({domobj:document,name:"mouseup",		handler:dhtmlfx.mouse.mouse_up})
		}

/*		


df.attachevent({name:"touchmove",target:document,handler:function(evtobj) {
	if (evtobj.srcdom.id=="testdiv"){return}
	df.killevent(evtobj)
	df.dg("testdiv").innerHTML += '<hr>touchmove:<br>'+df.json.serialize(evtobj)
}})

df.attachevent({name:"touchstart",target:document,handler:function(evtobj) {
	if (evtobj.srcdom.id=="testdiv"){return}
	df.killevent(evtobj)
	df.dg("testdiv").innerHTML += '<hr>touchstart:<br>'+df.json.serialize(evtobj)
}})

df.attachevent({name:"touchend",target:document,handler:function(evtobj) {
	if (evtobj.srcdom.id=="testdiv"){return}
	df.killevent(evtobj)
	df.dg("testdiv").innerHTML += '<hr>touchend:<br>'+df.json.serialize(evtobj)
}})



		try{document.addEventListener("mouseover",	df.mouse.mouse_over,false)	}catch(e){document.attachEvent("onmouseover",	df.mouse.mouse_over)}
		try{document.addEventListener("mouseout",	df.mouse.mouse_out,false)	}catch(e){document.attachEvent("onmouseout",	df.mouse.mouse_out)}
		try{document.addEventListener("mousedown",	df.mouse.mouse_down,false)	}catch(e){document.attachEvent("onmousedown",	df.mouse.mouse_down)}
		try{document.addEventListener("mousemove",	df.mouse.mouse_move,false)	}catch(e){document.attachEvent("onmousemove",	df.mouse.mouse_move)}
		try{document.addEventListener("mouseup",	df.mouse.mouse_up,  false)	}catch(e){document.attachEvent("onmouseup",	df.mouse.mouse_up)}
		try{document.addEventListener("contextmenu",	df.mouse.mouse_context,false)	}catch(e){document.attachEvent("oncontextmenu",	df.mouse.mouse_context)}
		try{window.addEventListener("resize",		df.mouse.resize,false)		}catch(e){window.attachEvent("onresize",	df.mouse.resize)}

		try{
			document.addEventListener((df.browser=="safari"||df.browser=="opera"||df.browser=="chrome")?"mousewheel":"DOMMouseScroll",   df.mouse.move_mousewheel,  false);
		}catch(e){document.attachEvent("onmousewheel",  df.mouse.move_mousewheel)}
*/

//for select/input elements - to tie into heatmap
	//	try{document.addEventListener("change",	df.mouse.change,  false)}catch(e){document.attachEvent("onchange",	df.mouse.change)}
	//	try{document.addEventListener("focus",	df.mouse.focus,  false)	}catch(e){document.attachEvent("onfocus",	df.mouse.focus)}
	//	try{document.addEventListener("blur",	df.mouse.blur,  false)	}catch(e){document.attachEvent("onblur",	df.mouse.blur)}

	},
	
	showdrops:	function() {
		var df = dhtmlfx
		for (var i=0;i<df.mouse.dropzones.length;i++) {
			df.mouse.dropzones[i].style.border="1px solid red";
		}
	
	}

//=================================================================

	
};

try{dhtmlfx.mouse.attachEvents()}catch(e){};
