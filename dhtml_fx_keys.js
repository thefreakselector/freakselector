/*
Code by Damien Otis 2004-2010
License found at http://www.dhtmldf.net/license.html
API Docs at http://www.dhtmldf.net/
*/
dhtmlfx.keys = {
	ctrl		:	false,
	alt		:	false,
	shift		:	false,
	xchar		:	-1,		//rename to keycode
	filter		:	"",
	buffer		:	[],
	bufferon	:	true,
	bufferlength	:	32,
	keystrokes	:	[],
	noescape	:	true,
	nobackspace	:	true,
	evtsrc		:	{},
	
	keycodes	:	{
					"deletekey":46,
					"escape":27,
					"esc":27,
					"up":38,
					"down":40,
					"left":37,
					"right":39,
					"backspace":8,
					"delete":46,
					"enter":13,  //"return" is not allowed, it is a reserved word
					"space":32,
					"pageup":33,
					"pagedown":34,
					"home":36,
					"end":35,
					"tab":9,
					"ctrl":17,
					"alt":18,
					"shift":16,
					"capslock":20,
					"insert":45,
					"break":19
	},
					

	inputcontrolcodes	:	[	8,	//backspace
						37,	//arrow
						38,	//arrow
						39,	//arrow
						40,	//arrow
						46	//delete
					]
	,

	keydown:	function(evt) {
		var df = dhtmlfx
		try{
			var evtsrc	= event.srcElement;
			var evt		= event
		}catch(e){
			var evtsrc=evt.target
		}
		df.keys.evtsrc	= evtsrc
		df.keys.ctrl	= evt.ctrlKey;
		df.keys.shift	= evt.shiftKey;
		df.keys.alt	= evt.altKey;
		df.keys.xchar	= evt.keyCode||evt.charCode;
//alert(df.keys.xchar)

		if (df.browser=="safari"&&df.platform.toLowerCase().indexOf("mac")!=-1){
			if (df.keys.xchar==91||df.keys.xchar==93) {df.keys.ctrl=true}
//			if (df.keys.xchar==8) {df.keys.xchar=46}// treats the backspace like a delete key for retarded mac keyboards (they are all retarded)
		}
		//df.keys.keyfilter(evt)
		if (	(
				(evtsrc.tagName=="TEXTAREA")	||
				(evtsrc.tagName=="SELECT")	||
				(evtsrc.tagName=="INPUT")
			)
		){
			if (df.platform.toLowerCase().indexOf("mac")!=-1) {
				if (df.keys.xchar==91) {df.keys.ctrl=true}
				if (df.keys.xchar==92) {df.keys.ctrl=false}		
			}

			try{
				for (var i=0;i<df.keys.inputcontrolcodes.length;i++) {
	/*
					if (df.keys.inputcontrolcodes[i]==df.keys.xchar) {
						if (window.event) {window.event.returnValue = false}
						if (window.event&&window.event.cancelBubble) {window.event.cancelBubble=true}
						if (evt.preventDefault) {evt.preventDefault()}
						if (evt.preventCapture) {evt.preventCapture()}
						if (evt.preventBubble) {evt.preventBubble()}
						if (evt.stopPropagation) {evt.stopPropagation()}
						return true
					}
	*/
				}		
			}catch(e){alert("keysfx:"+e.description)}
		} else {
			if ((df.keys.nobackspace==true)&&(df.keys.xchar==8)) {df.keys.killevent(evt)}
			if ((df.keys.noescape==true)&&(df.keys.xchar==27)) {df.keys.killevent(evt)}
		}

		df.keys.keystrokeevent(evt);
		//alert(df.keys.xchar)
//		try{df.events.fire("df.keys.keydown",{event,df.keys.ctrl,df.keys.shift,df.keys.alt,df.keys.xchar})}catch(e){}
	},

	keyup:		function(evt) {
		var df = dhtmlfx
		var evtsrc
		try{evtsrc = event.srcElement;evt=event}catch(e){evtsrc=evt.target}
		df.keys.evtsrc	= evtsrc
		df.keys.ctrl		= evt.ctrlKey;
		df.keys.shift		= evt.shiftKey;
		df.keys.alt		= evt.altKey;
//		df.keys.xchar		= -1;

if (df.browser=="safari"&&df.platform=="mac"){
	df.keys.ctrl=false
}
		
//		df.keys.keyfilter(evt);
//		try{df.events.fire("df.keys.keyup",{event,df.keys.ctrl,df.keys.shift,df.keys.alt,df.keys.xchar})}catch(e){}
	},

	keypress:	function(evt) {
		var df = dhtmlfx
		var evtsrc
		//try{evtsrc=event.srcElement;evt=event}catch(e){try{evtsrc=evt.target}catch(e){}}
		df.keys.ctrl	= evt.ctrlKey;
		df.keys.shift	= evt.shiftKey;
		df.keys.alt	= evt.altKey;
		//df.keys.xchar	= -1;
		//df.keys.xchar	= evt.keyCode;
		//df.keys.keyfilter(evt);
//		try{df.events.fire("df.keys.keypress",{event,df.keys.ctrl,df.keys.shift,df.keys.alt,df.keys.xchar})}catch(e){}
	},

	keyfilter:	function(evt,filterarray) {
		var df = dhtmlfx
		var evtsrc
		var evt
		
		try{evtsrc=event.srcElement;evt=event}catch(e){evtsrc=evt.target}
		df.keys.evtsrc	= evtsrc
		df.keys.ctrl		= evt.ctrlKey;
		df.keys.shift		= evt.shiftKey;
		df.keys.alt		= evt.altKey;
		df.keys.xchar		= evt.keyCode||evt.charCode;
		//if (df.keys.ctrl!=false||df.keys.alt!=false) {return false}

		if (filterarray.action!=undefined) {
		for (var i=0;i<filterarray.action.length;i++) {

//df.debug.debugwin({title:"trigger="+filterarray.action[i].trigger,message:"df.keys.xchar="+df.keys.xchar})
			if (filterarray.action[i].trigger==df.keys.xchar) {
				df.keys.killevent(evt);
				return filterarray.action[i].callback(evtsrc,filterarray.action[i])||true
 			}
		}
		}

		if (filterarray.reject!=undefined) {
		for (var i=0;i<filterarray.reject.length;i++) {
			if (filterarray.reject[i]==String.fromCharCode(df.keys.xchar)) {
				df.keys.killevent(evt);
				return false
			}
		}
		}

		if (filterarray.accept!=undefined) {
		for (var i=0;i<filterarray.accept.length;i++) {
			if (filterarray.accept[i]==String.fromCharCode(df.keys.xchar)) {
				if (filterarray.accept_callback!=undefined) {
					try{return filterarray.accept_callback(evtsrc)||true}catch(e){}
				}			
				return true
			}
		}
		}


/*
		for (var i=0;i<df.keys.inputcontrolcodes.length;i++) {
			if (df.keys.inputcontrolcodes[i]==df.keys.xchar) {
				df.keys.killevent(evt);
				return true
			}
		}
*/		

		
		if (filterarray.accept==undefined) {return true}
		
		return false

	/*
		if (df.keys.filter!=undefined) {
			if (df.keys.xchar.indexOf(df.keys.filter.replace(filter))==-1) {
				evt.returnValue = false;
			}
		}
	*/
		//df.dg("test1").innerHTML = df.json.serialize(df.keys,true);
//		try{df.events.fire("df.keys.keypress",{event,df.keys.ctrl,df.keys.shift,df.keys.alt,df.keys.xchar})}catch(e){}
	},
	
	keystrokeevent	:	function(evt) {
		var df = dhtmlfx
		var thisxchar = parseInt(df.keys.xchar,10)

		try{
			var evtsrc	= event.srcElement;
			var evt		= event
		}catch(e){
			var evtsrc=evt.target
		}

		if ((
			(evtsrc.tagName=="TEXTAREA")	||
			(evtsrc.tagName=="SELECT")	||
			(evtsrc.tagName=="INPUT")
		)
		){
			return false
		}
		for (var i=0;i<df.keys.keystrokes.length;i++) {
			if (df.keys.keystrokes[i].key==null&&thisxchar!=-1) {return}
			
//df.debug.debugwin({title:"keystrokeevent",message:df.keys.keystrokes[i].key+"="+thisxchar +"<br>"+df.keys.keystrokes[i].ctrl+"="+df.keys.ctrl +"<br>"+df.keys.keystrokes[i].alt+"="+df.keys.alt +"<br>"+df.keys.keystrokes[i].shift+"="+df.keys.shift,color:""})
			
				
				
				
			if (
				df.keys.keystrokes[i].key==thisxchar &&
				df.keys.keystrokes[i].ctrl==df.keys.ctrl &&
				df.keys.keystrokes[i].alt==df.keys.alt &&
				df.keys.keystrokes[i].shift==df.keys.shift
			) {
				try{
					var keybubble = df.keys.keystrokes[i].callback(df.keys.keystrokes[i],evt);
					
					if (keybubble==undefined) {keybubble=df.keys.keystrokes[i].keybubble}
				
					if (keybubble==false) {
						df.keys.killevent(evt);
					}
				}catch(e){df.debug.debugwin({title:"df.keys.keystrokeevent callback",message:e.description+"<br>"+e.message,color:"red"})}
			}
		}
		
	//	df.keys.xchar = "";
	
	},
	
	addkeystroke	:	function(keycombo) {
		var df = dhtmlfx
	//keycombo format:
	// df.keys.addkeystroke({key:27,ctrl:true,alt:false,shift:false,callback:'callback function'})
	// {key:27,alt:true,callback:callbackfunction}
	// {key:27,callback:callbackfunction}
	
		if (keycombo.callback==undefined) {return}
		if (keycombo.ctrl==undefined) {keycombo.ctrl=false}
		if (keycombo.alt==undefined) {keycombo.alt=false}
		if (keycombo.shift==undefined) {keycombo.shift=false}
		if (keycombo.key==undefined) {keycombo.key=null}

		var notfound=true;
		var thiskeycombo = df.json.serialize(keycombo)
		for (var i=0;i<df.keys.keystrokes.length;i++) {
			if (thiskeycombo==df.json.serialize(df.keys.keystrokes[i])) {notfound=false;break}
		}
		if (notfound==true) {
			df.keys.keystrokes.push(keycombo);
		}
	},

	removekeystroke	:	function(keycombo) {
		var df = dhtmlfx
		var notfound=true;
		if (keycombo.ctrl==undefined) {keycombo.ctrl=false}
		if (keycombo.alt==undefined) {keycombo.alt=false}
		if (keycombo.shift==undefined) {keycombo.shift=false}
		if (keycombo.key==undefined) {keycombo.key=null}
		for (var i=0;i<df.keys.keystrokes.length;i++) {
			if (df.json.serialize(keycombo)==df.json.serialize(df.keys.keystrokes[i])) {notfound=false;break}
		}
		if (notfound==false) {
			df.keys.keystrokes.splice(i,1);
		}		
	},
	killevent: function(evt) {
	try{
		if (window.event) {window.event.returnValue = false}
		if (window.event&&window.event.cancelBubble) {window.event.cancelBubble=true}
		if (evt.preventDefault) {evt.preventDefault()}
		if (evt.stopPropagation) {
			evt.stopPropagation()
		} else {
			if (evt.preventBubble) {evt.preventBubble()}
			if (evt.preventCapture) {evt.preventCapture()}		
		}
	}catch(e){}
	},
	attachEvents	: function() {
		var df = dhtmlfx
		df.attachevent({name:"keydown",handler:df.keys.keydown})
		df.attachevent({name:"keyup",handler:df.keys.keyup})
		df.attachevent({name:"keypress",handler:df.keys.keypress})
		
//		try{document.addEventListener("keydown",	df.keys.keydown,false)		}catch(e){	document.attachEvent("onkeydown",	df.keys.keydown)}
//		try{document.addEventListener("keyup",		df.keys.keyup,false)		}catch(e){	document.attachEvent("onkeyup",		df.keys.keyup)}
//		try{document.addEventListener("keypress",	df.keys.keypress,  false)	}catch(e){	document.attachEvent("onkeypress",	df.keys.keypress)}
		//try{df.events.fire("df.keys.attachEvents",{})}catch(e){}
	}
};

dhtmlfx.keys.attachEvents();
