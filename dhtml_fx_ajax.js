/*
Copyright 2009 Damien Otis
License found at http://www.dhtmldf.net/license.html
API Docs at http://www.dhtmldf.net/
*/


dhtmlfx.ajax = {
	connectionlimit		: 3,
	connectionoverride	: false,
	retrycount		: 4,
	retrytimer		: 2000,
	retrytmr		: 0,
	failtimeoutsecs		: 30,
	errorlimit		: 100,
	proc			: [],
	queue			: [],
	errorlog		: [], //not useful?
	
	//can add function references here inside numeric status code objects
	//custom error codes can be added.
	globalcallbacks		: {200:{},404:{},500:{}},
	
	//maximum querystring length for browsers.
	//IE8 needs to be tested. Probably need to make an automated test for this
	//when qualifying new browser versions - make a simple back-end receiver to a querystring requester when input and output dont match, it has reached the limit
	getmax			: ((dhtmlfx.browser=="ie")?2048:14000),

	logenable		: false,
	logfile			: [],

//TO DO ********************:
// FIX AJAX ERROR FUNCTIONS. timeout needs to abort correctly and follow the retry events before continuing with next query.

	failed: function(thisproc) {
//try{df.debug.debugwin({title:"failed",message:"readyState:"+df.ajax.proc[thisproc].ajx.readyState})	}catch(e){}
		
		var df = dhtmlfx
		var proc = df.ajax.proc[thisproc]
		if(proc!=null) {
			proc.aborted=true;
			df.ajax.ajaxerror(proc);
		}
	},

	loadnext: function (thisproc) {
//df.debug.debugwin({title:"loadnext",message:""})
		var df = dhtmlfx
		try{clearTimeout(df.ajax.retrytmr)}catch(e){}
		try{df.ajax.proc[thisproc].ajx.abort()}catch(e){}
		try{df.ajax.proc[thisproc].ajx.onreadystatechange=function(){};}catch(e){}
		try{df.ajax.proc[thisproc].ajx=null;}catch(e){}
		try{df.ajax.proc[thisproc]=null;}catch(e){}

//		try{delete df.ajax.proc[thisproc]}catch(e){}

		//try{df.ajax.proc.splice(thisproc,1)}catch(e){}
		if (df.ajax.queue.length>0) {
			var ajq = df.ajax.queue.shift();
			var ajproc = df.ajax.send(ajq);
		}
	},

	kill:	function(thisproc) {
		var df = dhtmlfx
		try{df.ajax.proc[thisproc].ajx.abort()}catch(e){}
		try{df.ajax.proc[thisproc].ajx.onreadystatechange=function(){};}catch(e){}
		try{df.ajax.proc[thisproc].ajx=null;}catch(e){}
		try{df.ajax.proc[thisproc]=null;}catch(e){}		
		//try{df.ajax.proc.splice(thisproc,1)}catch(e){}
	},

	ajaxerror: function (ajaxobj) {
//df.debug.debugwin({title:"ajaxerror",message:"retrycount:"+ajaxobj.aji})	
		var df = dhtmlfx
		ajaxobj.aji++;
		try{ajaxobj.ajx.abort()}catch(e){}
		if (ajaxobj==null) {return}
		ajaxobj.aborted=undefined;
		var thisproc = parseInt(ajaxobj.thisproc,10);
		if (ajaxobj.aji<df.ajax.retrycount) {
			try{ajaxobj.ajx.onreadystatechange=function(){};}catch(e){}
			try{ajaxobj.ajx=null;}catch(e){}
			try{df.ajax.proc[thisproc].ajx=null;}catch(e){}
			try{df.ajax.proc[thisproc]=null;}catch(e){}
			try{df.ajax.proc.splice(thisproc,1)}catch(e){}
			df.ajax.queue.push(ajaxobj);
			df.ajax.retrytmr = setTimeout("try{df.ajax.loadnext("+thisproc+")}catch(e){}",df.ajax.retrytimer);
		} else {
			if (ajaxobj.errorcallback!=undefined) {
				ajaxobj.errorcode = "Request failed "+df.ajax.retrycount+" times."
				ajaxobj.errorcallback(df.ajax.proc[thisproc])
			}
			if (df.ajax.errorlog.length>df.ajax.errorlimit){df.ajax.errorlog.shift()}
			try{df.ajax.proc[thisproc].ajx.abort()}catch(e){}
			try{df.ajax.proc[thisproc].ajx.onreadystatechange=function(){};}catch(e){}
			try{df.ajax.proc[thisproc].ajx=null;}catch(e){}
			try{df.ajax.proc.splice(thisproc,1)}catch(e){}
		}
	},
	
	xssloaded:{},
	xssloader:	function(ajaxobj) {
//XSS deficiencies:
//XSS is GET only. querystring limits apply.
//IE uses onreadystatechange, other browsers use onload
//only FF uses onerror, and so only FF can distinguish between
//a good load and a failed load. all other browsers can't.

		var head = document.getElementsByTagName("head")[0];

		if (ajaxobj.url.toLowerCase().indexOf(".css")!=-1||ajaxobj.loadcss==true) {
			var newobj=document.createElement("link");
			newobj.setAttribute("type","text/css");
			newobj.setAttribute("rel","stylesheet");
			newobj.setAttribute("media","screen");
		} else {
			var newobj=document.createElement("script");
			newobj.setAttribute("type","text/javascript");
		}
		if (ajaxobj.async==undefined) {ajaxobj.async = true}
		if (ajaxobj.async!=true) {
			newobj.setAttribute("async",true);
		} else {
			newobj.setAttribute("async",false);
		}

		if (newobj.tagName.toUpperCase()=="LINK") {
			newobj.href=ajaxobj.url
		} else {
			newobj.src=ajaxobj.url
		}

		head.appendChild(newobj);		
		
	},

//-------------------------------------------

	sendxssdata:	function(ajaxobj) {
//Sends blocks of data through XSS script GET
//used by debug remote report
//df.ajax.sendxssdata({url:"http://www.dhtmlfx.net/remote_report.aspx",textdata:"data to transfer in text form",objectid:"thisobjectid1234",querystring:{test:"asdf",hello:"world"},callback:"df.ajax.xsscallback"})	
		var df = dhtmlfx
	try{
		var totaltext = 0
		var splittxt = encodeURIComponent(ajaxobj.textdata)
		var qs=(ajaxobj.url.indexOf("?")!=-1)?"&":"?"

		var querystring=""
		if (ajaxobj.querystring!=undefined) {
			for (var selector in ajaxobj.querystring) {
				querystring+="&"+selector+"="+ajaxobj.querystring[selector]
			}
		}
		if (ajaxobj.callback!=undefined) {querystring+="&callback="+ajaxobj.callback}

		var urlbase = ajaxobj.url+qs+"plen=999999&objid="+(ajaxobj.objectid||df.unique())+querystring+"&data="
		var maxlen = df.ajax.getmax-urlbase.length
		var packets = Math.ceil(splittxt.length/maxlen)

		var objectid = (ajaxobj.id||df.unique())
		var leftover = ""
		
		
		for (var i=0;i<packets;i++) {
			var urlbase = ajaxobj.url+qs+"xssidx="+i+"&xsslen="+packets+"&xssid="+objectid+querystring+"&xssdata="
			var splitstart = i*maxlen
			var splitend = (i*maxlen)+maxlen
			var sendtext = leftover+splittxt.substring(splitstart,splitend)
			var nextfive = splittxt.substring((i+1)*maxlen).substr(0,5)

			leftover = ""

			var lastfive = sendtext.substr(sendtext.length-5)

			if (lastfive.indexOf("%")!=-1&&nextfive.length>0&&lastfive.length==5) {
				if (lastfive.substr(4,1)=="%") {
					leftover = "%"
					sendtext = sendtext.substr(0,sendtext.length-1)
				}
				if (lastfive.substr(3,1)=="%") {
					leftover = "%"+sendtext.substr(sendtext.length-1)
					sendtext = sendtext.substr(0,sendtext.length-2)
				}
			}
			var thisurl = urlbase+sendtext

			df.ajax.xssloader({url:thisurl})
		}
		
		return ajaxobj
	}catch(e){alert(e.description+"\n"+e.message)}
	},
	
	xssNextChunk:	function(xssid,xssidx) {
		
	
	},

	sendxssdataprog: function(ajaxobj) {
		var df = dhtmlfx
	try{
//df.debug.debugwin({title:"test",message:df.json.serialize(ajaxobj),color:""})

		if (ajaxobj.progress!=undefined) {
			ajaxobj.progress(ajaxobj)
		}


		if (ajaxobj.packetidx==(ajaxobj.packets-1)) {
			if (ajaxobj.completed!=undefined) {
				ajaxobj.completed(ajaxobj)
			}
		}
	}catch(e){df.debug.debugwin({title:"sendxssdataprog",message:e.description+"<br>"+e.message,color:"red"})}
	},

	send: function (ajaxobj) {
		var df = dhtmlfx
//df.debug.debugwin({title:"ajaxobj url: "+ajaxobj.url,message:df.debug.getcaller(df.ajax.send.caller),color:""})

		if(df.ajax.proc.length!=df.ajax.connectionlimit){
			if(df.ajax.connectionoverride==false&&(df.ajax.connectionlimit<1||df.ajax.connectionlimit>4)){df.ajax.connectionlimit=2}
		}
		if (df.ajax.proc.length>0) {
			try{var procarrlength = df.ajax.proc.join(",").match(/\[.*?\]/g).length}catch(e){var procarrlength=0}
			var nextopenproc = Math.floor((String(","+df.ajax.proc.join(",")+",").replace(/\[.*?\]/g,"|").indexOf(",,"))/2);
			if (nextopenproc<0) {nextopenproc=procarrlength}
		} else {var procarrlength=0;var nextopenproc=0}
		if (procarrlength>=df.ajax.connectionlimit||(ajaxobj.aji==undefined&&df.ajax.queue.length>0)) {
			if (ajaxobj.aji==undefined) {ajaxobj.aji=0}
			df.ajax.queue.push(ajaxobj);
			if (df.ajax.logenable) {df.ajax.logfile.push("QUEUE: "+ajaxobj.url+" - "+ajaxobj.formdata)}
			return df.ajax.queue[df.ajax.queue.length-1]
		}
		if (ajaxobj.aji==undefined) {ajaxobj.aji=0}
		if (ajaxobj.async==undefined) {ajaxobj.async=true}

		if (df.ajax.logenable) {df.ajax.logfile.push("PROC"+nextopenproc+": "+ajaxobj.url+" - "+ajaxobj.formdata)}

		if (ajaxobj.verb==undefined) {ajaxobj.verb = ((ajaxobj.formdata!=undefined)?"post":"get")}

		df.ajax.proc[nextopenproc] = ajaxobj;

		var proci=df.ajax.proc[nextopenproc];
		proci.thisproc=nextopenproc;
/*
		if (ajaxobj.formdata==undefined&&((df.browser=="ie"&&ajaxobj.url.length>=2084)||(df.browser!="ie"&&ajaxobj.url.length>=16077))) {
			ajaxobj.errorcode="Cannot send GET, size out-of-range. GET size must be IE<2084, the others <16077 bytes"
			if (ajaxobj.errorcallback!=undefined) {
				ajaxobj.errorcallback(ajaxobj)
			}
			df.ajax.loadnext(nextopenproc)
			return proci
		}
*/

		if (ajaxobj.forcexss==true) {   //String(ajaxobj.url).indexOf("http://")!=-1||

//			var senddomain = String(ajaxobj.url).split("/")[2].split(".")
//			var thisdomain = String(location).split("/")[2].split(".")
//			var sdomain = senddomain[senddomain.length-2]+"."+senddomain[senddomain.length-1]
//			var tdomain = thisdomain[thisdomain.length-2]+"."+thisdomain[thisdomain.length-1]
//			if (sdomain!=tdomain||ajaxobj.forcexss==true) {
//				ajaxobj.thisproc = proci.thisproc

				df.ajax.xssloader(ajaxobj)
				return

//				return proci
//			}
		}
		try{proci.ajx=new XMLHttpRequest()}catch(e){try{proci.ajx=new ActiveXObject("MSXML2.XMLHTTP.3.0")||new ActiveXObject("MSXML.XMLHTTP")}catch(e){ proci.ajx=new ActiveXObject("Microsoft.XMLHTTP") }}

//If an "access denied" error happens here, it is probably because the request is trying to go cross-domain.
//even going from www.google.com to google.com will cause an "access denied" error. BEWARE!
		proci.ajx.open(proci.verb,proci.url,(proci.async!=undefined)?proci.async:true);

		if(proci.formdata!=undefined){proci.ajx.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8')};
		if(proci.requestheaders!=undefined) {
			for (var selector in proci.requestheaders) {
				proci.ajx.setRequestHeader(selector,proci.requestheaders[selector]);
			}
		}		
		var timeoutseconds = ((ajaxobj.timeoutseconds!=undefined)?ajaxobj.timeoutseconds:df.ajax.failtimeoutsecs);
		proci.failtimeouttmr = setTimeout(function(){window.dhtmlfx.ajax.failed(proci.thisproc)},(timeoutseconds*1000));

//df.debug.debugwin({title:"send",message:df.json.serialize(ajaxobj,true).replace(/\"\,/g,'"<br>,')});

//this works in everything except IE6 :(
//		proci.ajx.thisproc = nextopenproc
		if (df.browser=="ff"&&proci.async==false) {	
//			try{
				proci.ajx.send(proci.formdata);
//			}catch(e){}
			df.ajax.onreadystate(window.dhtmlfx.ajax.proc[proci.thisproc])

		} else {
			proci.ajx.onreadystatechange=function(){
				//var thisproc=window.dhtmlfx.ajax.proc[this.thisproc]
				var thisproc=window.dhtmlfx.ajax.proc[proci.thisproc]
				window.dhtmlfx.ajax.onreadystate(thisproc)
			};
//			try{
				proci.ajx.send(proci.formdata);
//			}catch(e){}
		}
		
		return proci

	},
	
	onreadystate:	function(thisproc) {
		var df = dhtmlfx
		//try{

//df.debug.debugwin({title:df.json.serialize(thisproc,true),message:"status="+thisproc.ajx.status+"<br>readyState="+thisproc.ajx.readyState,color:""})	
			if (!thisproc){return}
			if (thisproc.aborted===true) {return}
			if (thisproc.ajx.readyState>=4){
				try{if (df.browser=="ff"&&thisproc.ajx.status==0) {thisproc.ajx.status=200}}catch(e){}

				try{clearTimeout(thisproc.failtimeouttmr)}catch(e){}
				df.ajax.procglobals(thisproc)
				if ( (thisproc.ajx.status<400&&thisproc.ajx.status>0) || (df.browser=="ff"&&thisproc.ajx.status<400&&thisproc.ajx.status==0)  ) {

					if (thisproc.callback!=undefined) {
						if (thisproc.callback.constructor==Function) {
							//try{
								thisproc.XML = thisproc.ajx.responseXML
								thisproc.text= thisproc.ajx.responseText
								thisproc.headers=thisproc.ajx.getAllResponseHeaders()
								if (thisproc.callback){thisproc.callback(thisproc)}
							//}catch(e){
							//	try{df.debug.debugwin({title:"df.ajax ERROR - 1",color:"red",message:"AJAX CALLBACK ERROR: on proc"+thisproc.thisproc+"<br>status="+thisproc.ajx.status+"<br>"+e.message+"<br>"+e.description+"<br>"+String(thisproc.callback)+" - "+thisproc.url+" - "+thisproc.formdata})}catch(e){}
							//	return
							//}
						} else {
							df.dg(thisproc.callback).innerHTML=thisproc.ajx.responseText
						}
					} else {
						df.ajax.loadnext(thisproc.thisproc)
					}
					try{clearTimeout(thisproc.failtimeouttmr)}catch(e){}
				} else {
					if (thisproc.ajx.status>0) {
						try{clearTimeout(thisproc.failtimeouttmr)}catch(e){}
						try{thisproc.text= thisproc.ajx.responseText;}catch(e){}
						try{thisproc.headers=thisproc.ajx.getAllResponseHeaders()}catch(e){}
						df.ajax.ajaxerror(thisproc);
					}
				}
				
				try{df.ajax.loadnext(thisproc.thisproc)}catch(e){}
				return
			}
			
		/*}catch(e){

			try{df.debug.debugwin({title:"df.ajax ERROR - 2",color:"red",message:"AJAX CALLBACK ERROR: on proc"+thisproc.thisproc+" - "+String(callback)+" - "+thisproc.url+" - "+formdata})}catch(e){}

//try{df.debug.debugwin({title:"onreadystatechange",message:"readyState:"+thisproc.ajx.readyState+'<br>status:'+thisproc.ajx.status})}catch(e){}
			try{df.ajax.loadnext(thisproc.thisproc)}catch(e){}
		}*/
	},
	
	procglobals:	function(thisproc) {
		var df = dhtmlfx
		for (var thiscallback in df.ajax.globalcallbacks[String(thisproc.ajx.status)]) {
			try{
				try{thisproc.XML = thisproc.ajx.responseXML}catch(e){}
				try{thisproc.text= thisproc.ajx.responseText}catch(e){}
				try{thisproc.headers=thisproc.ajx.getAllResponseHeaders()}catch(e){}
				df.ajax.globalcallbacks[String(thisproc.ajx.status)][thiscallback](thisproc,thisproc.ajx.status,thiscallback)
			}catch(e){}
		}				
	}
	
};
